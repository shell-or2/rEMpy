from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey, PickleType, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


# Schedule
class Schedule(Base):
    __tablename__ = "schedule"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    issued_time = Column(DateTime(), nullable=False)
    rvalue = Column(PickleType)
    validity = Column(Boolean(), default=True)

    def __repr__(self):
        return "<Schedule(id='%s', issued_time='%s', rvalue='%s', validity='%s')>" % \
               (self.id, self.issued_time, self.rvalue, self.validity)


# Task
class Task(Base):
    __tablename__ = 'task'
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), unique=False, default='EmptyName', nullable=False)
    # task params
    start_time = Column(DateTime(), nullable=False)
    energy = Column(Float(), nullable=False)
    duration = Column(Integer(), nullable=False)
    executed = Column(Boolean(), default=False)  # set to executed when send exe command
    # validity = Column(Boolean(), ForeignKey('schedule.validity'), nullable=False)  # error too much ForeignKey
    schedule_id = Column(Integer, ForeignKey('schedule.id'), nullable=False)
    schedule = relationship("Schedule", backref="task")

    def __repr__(self):
        return "<Task(id='%s', name='%s, start_time='%s', energy='%s', duration='%s', executed='%s')>" % \
               (self.id, self.name, self.start_time, self.energy, self.duration, self.executed)

