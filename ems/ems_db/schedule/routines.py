from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from ems_db.schedule.models import *
import datetime
from defines import *
import logging

logger = logging.getLogger(__name__)


# decorator to handle session open and close
def db_session(wrapped_function):
    def function_wrapper(*args, **kwargs):
        func_return = None
        session = open_session()
        try:
            func_return = wrapped_function(session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
        finally:
            session.close()
        return func_return

    return function_wrapper


def database_exists(engine):
    # Ref: https://github.com/kvesteri/sqlalchemy-utils/blob/master/sqlalchemy_utils/functions/database.py#L433

    database = engine.url.database

    if engine.dialect.name == 'sqlite':
        if database:
            return database == ':memory:' or os.path.exists(database)
        else:
            # The default SQLAlchemy database is in memory,
            # and :memory is not required, thus we should support that use-case
            return True

    else:
        raise Exception('Unsupported DB type.')


def open_session(verbose=False):

    engine = create_engine('sqlite:///' + EMS_DB_SCHEDULES, echo=verbose)

    # generate DB if missing
    if not database_exists(engine):
        Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def cache_schedules(last_schedule_id=0):

    return get_schedule_data(to_id=last_schedule_id, from_id=0, excluding=True, additional=['issued_time', 'validity'])


@db_session
def get_last_schedule_data(session):

    # Return empty dict if no match
    output_dict = dict()

    # session = open_session()
    last_schedule = session.query(Schedule).order_by(Schedule.issued_time.desc()).first()
    # session.close()

    if last_schedule:
        output_dict.update({'id': last_schedule.id, 'issued_time': last_schedule.issued_time,
                            'rvalue': last_schedule.rvalue})

    return output_dict


def get_schedule_rvalue(schedule_id):
    return \
        get_schedule_data(to_id=schedule_id, from_id=schedule_id, excluding=False,
                          additional=['issued_time', 'rvalue'])[0]


def get_schedules_rvalue(from_datetime, to_datetime):

    return query_schedules(from_datetime, to_datetime)

    # return [schedule.rvalue for schedule in matched_schedules]


@db_session
def query_schedules(session, from_datetime, to_datetime):

    # session = open_session()
    matched_schedules = session.query(Schedule).filter(Schedule.issued_time.between(from_datetime, to_datetime)).all()
    # session.close()

    return [schedule.rvalue for schedule in matched_schedules]


def get_tasks_from_schedule(schedule_id):

    matched_tasks = get_schedule_data(to_id=schedule_id, from_id=schedule_id, excluding=False, additional=['task'])[0]

    output_tasks = list()

    for task in matched_tasks.get('task') or []:
        output_tasks.append({'id': task.id, 'start_time': task.start_time, 'actual_profile': task.actual_profile})

    return output_tasks


@db_session
def get_tasks_from_interval(session, start_date, end_date):

    # session = open_session()
    matched_tasks = session.query(Task).filter(Task.start_time.between(start_date, end_date)).all()
    # session.close()

    # return matched_tasks
    return [{'actual profile': task.actual_profile, 'start_time': task.start_time} for task in matched_tasks]


@db_session
def get_schedule_data(session, to_id, from_id=1, excluding=True, additional=list()):
    # excluding options allows to perform the query "in" or "out" the given ID range

    # session = open_session()

    if excluding:
        matched_schedules = session.query(Schedule).filter(~Schedule.id.in_(list(range(from_id, to_id + 1)))).all()
    else:
        matched_schedules = session.query(Schedule).filter(Schedule.id.in_(list(range(from_id, to_id + 1)))).all()

    # session.close()

    output_data = list()

    # validate additional, after query to bind all the backrefs
    if not all(element in Schedule.__dict__.keys() for element in additional):
        return output_data

    for schedule in matched_schedules:
        schedule_dict = {'id': schedule.id}
        for param_name in additional:
            schedule_dict.update({param_name: getattr(schedule, param_name)})

        output_data.append(schedule_dict)

    return output_data


def query_tasks(session, from_datetime, to_datetime):

    # session = open_session()
    matched_tasks = session.query(Task).filter(Task.start_time.between(from_datetime, to_datetime)).all()
    # session.close()

    return matched_tasks


@db_session
def get_executable_tasks(session, from_datetime, to_datetime):

    # task is executable if start_time in given datetime range, executed is False, and task validity is True

    matched_tasks = query_tasks(session, from_datetime, to_datetime)

    output_dict = dict()

    for task in matched_tasks:
        if not task.executed and task.schedule.validity:
            output_dict.update({'id': task.id, 'name': task.name})

    return output_dict


@db_session
def get_scheduled_tasks(session, from_datetime, to_datetime):
    # get all scheduled tasks in interval

    matched_tasks = query_tasks(session, from_datetime, to_datetime)

    output_tasks = list()

    for task in matched_tasks:
        output_tasks.append({'id': task.id, 'name': task.name, 'start_time': task.start_time, 'actual_profile': task.actual_profile})

    return output_tasks


@db_session
def set_task_executed(session, task_id):

    # session = open_session()
    task = session.query(Task).get(task_id)
    task.executed = True
    # session.commit()
    # session.close()


@db_session
def add_or_update_schedule(session, schedule_timestamp, schedule_data):
    # auto detect the tasks and add

    # session = open_session()

    schedule = session.query(Schedule).filter(Schedule.issued_time == datetime.datetime.utcfromtimestamp(schedule_timestamp)).one()

    if schedule:
        # update schedule, remove tasks
        schedule.rvalue = schedule_data
        schedule.task[:] = []
    else:
        schedule = Schedule()
        schedule.issued_time = datetime.datetime.utcfromtimestamp(schedule_timestamp)
        schedule.rvalue = schedule_data
        session.add(schedule)

    for task in schedule_data.get('tasks') or []:
        schedule.task.append(add_task(task_data=task))

    # session.commit()
    # session.close()


def add_task(task_data):
    task = Task()
    task.name = task_data['name']
    task.start_time = datetime.datetime.utcfromtimestamp(task_data.get('data').get('start_time'))
    task.energy = task_data.get('specs').get('energy')
    task.duration = task_data.get('specs').get('duration')
    task.actual_profile = task_data.get('specs').get('actual_profile')

    return task


@db_session
def invalidate_schedule(session, scheduler_id):
    # session = open_session()

    schedule = session.query(Schedule).filter(Schedule.id == scheduler_id).first()
    schedule.validity = False

    # session.commit()
    # session.close()


@db_session
def set_task_executed(session, task_id):

    task = session.query(Task).filter(Task.id == task_id).first()
    task.executed = True


@db_session
def get_tasks_in_execution(session):
    # return the name of the tasks in execution for 15 minute at least (if task marked as not executed)
    # if task is marked as executed: the task name is returned if end_time + 15 minutes < actual_time

    task_deltatime = datetime.timedelta(minutes=15)

    list_of_tasks = list()
    ref_datetime = datetime.datetime.now()
    last_schedule = session.query(Schedule).order_by(Schedule.issued_time.desc()).first()

    # ordering "in execution" tasks from greater to lower energy
    for task in last_schedule.task or []:
        # add task if executed and currently in execution
        if (task.start_time + task_deltatime) < ref_datetime <= (task.start_time + datetime.timedelta(minutes=task.duration) + task_deltatime):
            # list_of_tasks.append((task.id, task.name, task.energy))
            # task name without '-#', used to get the corresponding sensor data
            list_of_tasks.append((task.id, task.name.split('-')[0], task.start_time, task.energy))

    # list_of_tasks.sort(key=operator.itemgetter(2))

    # return reversed list, greater to lower energy
    return list_of_tasks
