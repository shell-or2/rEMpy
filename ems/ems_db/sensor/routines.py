import logging.handlers
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker
from ems_db.sensor.models import *
from defines import *
from ems_db.sensor.db_defines import *

logger = logging.getLogger(__name__)


# decorator to handle session open and close
def db_session(wrapped_function):
    def function_wrapper(*args, **kwargs):
        func_return = None
        session = open_session()
        try:
            func_return = wrapped_function(session, *args, **kwargs)
            session.commit()
        except Exception as err:
            session.rollback()
            logger.error(err)
        finally:
            session.close()
        return func_return

    return function_wrapper

interfaces = {
    'sensor': Sensor,
    'status': Status,
    'serial': Serial,
    'model': Model,
    'manufacturer': Manufacturer,
    'placement': Placement,
    'location': Location,
    'description': Description,
    'type': Type,
    'alert': Alert,
    'range': Range,
    'record': Record,
    'oldrecord': OldRecord
}


def database_exists(engine):
    # Ref: https://github.com/kvesteri/sqlalchemy-utils/blob/master/sqlalchemy_utils/functions/database.py#L433

    database = engine.url.database

    if engine.dialect.name == 'sqlite':
        if database:
            return database == ':memory:' or os.path.exists(database)
        else:
            # The default SQLAlchemy database is in memory,
            # and :memory is not required, thus we should support that use-case
            return True

    else:
        raise Exception('Unsupported DB type.')


def open_session(verbose=False):

    engine = create_engine('sqlite:///' + EMS_DB_SENSORS, echo=verbose)

    # generate DB if missing
    if not database_exists(engine):
        Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()
    return session


@db_session
def add_sensor(session, obj):

    session.autoflush = False

    old_sensor_flag = 1
    sensor = session.query(Sensor).filter_by(name=obj.sensor).scalar()

    if sensor is None:
        # the sensor is not present, we can add it.
        old_sensor_flag = 0
        # sensor = Sensor(name=obj.sensor)
        sensor = Sensor(name=obj.sensor, warning_down=obj.warning_down, warning_up=obj.warning_up,
                        sensor_sensitivity=obj.sensor_sensitivity, failure_value=obj.failure_value,
                        short_interval=obj.short_interval, long_interval=obj.long_interval,
                        fault_threshold=obj.fault_threshold)
        session.add(sensor)

    # if the sensor is present, we may add the hardware to record its replacement.
    # first we check if there is the same serial among those assigned to the sensor

    serial = session.query(Serial).filter(Serial.name == obj.serial).filter(
        Model.name == obj.model).filter(Manufacturer.name == obj.manufacturer).scalar()

    if serial is not None:
        logger.error('This device is already present in the database!')
        raise Exception('This device is already present in the database!')

    if old_sensor_flag == 1:
        # if the sensor is already present, we check if the serial is already assigned
        # we are replacing an existing sensor.
        # we create a new "Replacement" status to notify the replacement of the previous hardware
        serial = sensor.status[-1].serial
        # serial = session.query(Serial).filter(Sensor.name == obj.sensor).order_by(desc(Status.issued_time)).first()
        status = Status(name="Replaced", issued_time=func.now(), ack_time=func.now())
        status.serial = serial
        sensor.status.append(status)

    # we create the new status, and append it.
    status = Status(name="Installed", issued_time=func.now())
    sensor.status.append(status)

    serial = session.query(Serial).filter(Serial.name == obj.serial).scalar()
    if serial is None:
        serial = Serial(name=obj.serial)

    status.serial = serial

    # since a new device has been inserted, we look for its model and manufacturer
    model = session.query(Model).filter(Model.name == obj.model).filter(Manufacturer.name == obj.manufacturer).scalar()
    if model is None:
        model = session.query(Model).filter(Model.name == obj.model).scalar()
        if model is None:
            model = Model(name=obj.model)

    already_present = 0

    if len(serial.model) > 0:
        for i in range(len(serial.model)):
            if serial.model[i].name == obj.model:
                already_present = 1
                break

    if already_present == 0:
        serial.model.append(model)

    manufacturer = session.query(Manufacturer).filter(Manufacturer.name == obj.manufacturer).scalar()

    if manufacturer is None:
        manufacturer = Manufacturer(name=obj.manufacturer)

    already_present = 0

    if len(model.manufacturer) > 0:
        for i in range(len(model.manufacturer)):
            if model.manufacturer[i].name == obj.manufacturer:
                already_present = 1
                break

    if already_present == 0:
        model.manufacturer.append(manufacturer)
        # manufacturer.model.append(model)

    if old_sensor_flag == 1:
        # we are done
        # session.commit()
        return

    # we inserted the hardware information, time to deal with its other attributes:

    placement = session.query(Placement).filter(Placement.name == obj.placement).filter(
        Location.name == obj.location).scalar()

    if placement is None:
        placement = session.query(Placement).filter(Placement.name == obj.placement).scalar()
        if placement is None:
            placement = Placement(name=obj.placement)

    sensor.placement = placement

    location = session.query(Location).filter(Location.name == obj.location).scalar()

    if location is None:
        location = Location(name=obj.location)

    already_present = 0

    if len(placement.location) > 0:
        for i in range(len(placement.location)):
            if placement.location[i].name == obj.location:
                already_present = 1
                break

    if already_present == 0:
        placement.location.append(location)

    description = session.query(Description).filter(Description.name == obj.description).scalar()

    if description is None:
        description = Description(name=obj.description)

    sensor.description = description

    s_type = session.query(Type).filter(Type.name == obj.type).scalar()

    if s_type is None:
        s_type = Type(name=obj.type)

    sensor.type = s_type

    # alert = session.query(Alert).filter(Alert.name == obj.alert).scalar()
    #
    # if alert is None:
    #     alert = Alert(name=obj.alert, issued_time=func.now())
    #
    # sensor.alert.append(alert)

    s_range = session.query(Range).filter(Range.min == obj.range_min).filter(Range.max == obj.range_max).scalar()

    if s_range is None:
        s_range = Range(min=obj.range_min, max=obj.range_max)

    sensor.range = s_range

    # session.commit()
    # session.close()


@db_session
def init_db(session):

    # session = open_session()

    q = session.query(Sensor).all()

    new_sensor = SensorObj()

    new_sensor.sensor = 'NewSensor' + str(len(q))
    new_sensor.serial = '12345'+str(len(q))
    new_sensor.model = 'SampleModel'
    new_sensor.manufacturer = 'SampleManufacturer'
    new_sensor.placement = "Placement1"
    new_sensor.location = "loc1"
    new_sensor.description = "Temperature Sensor"
    new_sensor.type = "therm"
    new_sensor.range_min = -10
    new_sensor.range_max = 30

    add_sensor(new_sensor)
    # session.close()


# sensor related routines #

@db_session
def get_sensor_params(session, name):

    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    # session.close()

    params = SensorObj()

    params.sensor = sensor.name
    params.serial = sensor.status.serial.name
    params.model = sensor.status.serial.model.name
    params.manufacturer = sensor.status.serial.model.manufacture.name
    params.placement = sensor.placement.name
    params.location = sensor.placement.location.name
    params.description = sensor.description.name
    params.type = sensor.type.name
    params.range_min = sensor.range.min
    params.range_max = sensor.range.max

    return params


def get_sensor(session, name):

    return session.query(Sensor).filter(Sensor.name == name).one()


@db_session
def get_sensors(session):

    # session = open_session()

    sensors_name = list()

    sensors = session.query(Sensor.name).all()
    # session.close()

    for s_name in sensors:

        sensors_name.append(s_name.name)

    return sensors_name


@db_session
def change_sensor_name(session, old_name, new_name):

    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == new_name).scalar()
    if sensor is not None:
        return NAME_IN_USE
    sensor = session.query(Sensor).filter(Sensor.name == old_name).one()
    sensor.name = new_name
    # session.commit()
    # session.close()
    return None


def add_record_series(sensor, rtime, rvalue):

    out_of_range = 0
    if rvalue < sensor.range.min or rvalue > sensor.range.max:
        out_of_range = 1
        alert = Alert(name="Out of Range Value", issued_time=datetime.datetime.now())
        sensor.alert.append(alert)

    record = Record(rtime=rtime, rvalue=rvalue)
    sensor.record.append(record)

    return out_of_range


def add_record_series_by_sensor_name(session, name, rtime_list, rvalue_list):
    # populate sensor with multiple records at once, avoid multiple query for the same sensor
    # definitely faster for init DB with already available data

    out_of_range = 0
    sensor = session.query(Sensor).filter(Sensor.name == name).one()

    for rtime, rvalue in zip(rtime_list, rvalue_list):

        if rvalue < sensor.range.min or rvalue > sensor.range.max:
            out_of_range = 1
            alert = Alert(name="Out of Range Value", issued_time=datetime.datetime.now())
            sensor.alert.append(alert)

        record = Record(rtime=rtime, rvalue=rvalue)
        sensor.record.append(record)

    return out_of_range


@db_session
def add_record(session, name, rtime, rvalue):

    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    out_of_range = 0
    if rvalue < sensor.range.min or rvalue > sensor.range.max:
        out_of_range = 1
        alert = Alert(name="Out of Range Value", issued_time=datetime.datetime.now())
        sensor.alert.append(alert)

    record = Record(rtime=rtime, rvalue=rvalue)
    sensor.record.append(record)
    # session.commit()
    return out_of_range


@db_session
def get_last_record(session, name):

    record = session.query(Record).join(Sensor).filter(Sensor.name == name).order_by(desc(Record.rtime)).first()

    if record:
        return record.rvalue


@db_session
def get_record(session, name, begin, end):

    # session = open_session()
    records = session.query(Record).join(Sensor).filter(Sensor.name == name).filter(Record.rtime.between(begin, end)).all()
    # session.close()

    value_list = []

    for record in records:

        value_list.append([record.rtime, record.rvalue])

    return value_list


def add_oldrecord_series(sensor, rtime, rvalue):

    out_of_range = 0
    if rvalue < sensor.range.min or rvalue > sensor.range.max:
        out_of_range = 1
        alert = Alert(name="Out of Range Value", issued_time=datetime.datetime.now())
        sensor.alert.append(alert)

    oldrecord = OldRecord(rtime=rtime, rvalue=rvalue)
    sensor.record.append(oldrecord)

    return out_of_range


@db_session
def add_oldrecord(session, name, rtime, rvalue):

    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    out_of_range = 0
    if rvalue < sensor.range.min or rvalue > sensor.range.max:
        out_of_range = 1
        alert = Alert(name="Out of Range Value", issued_time=datetime.datetime.now())
        sensor.alert.append(alert)

    oldrecord = OldRecord(rtime=rtime, rvalue=rvalue)
    sensor.oldrecord.append(oldrecord)
    # session.commit()
    # session.close()
    return out_of_range


@db_session
def get_oldrecord(session, name, begin, end):

    # session = open_session()

    oldrecords = session.query(OldRecord).join(Sensor).filter(Sensor.name == name).filter(OldRecord.rtime.between(begin, end)).all()

    # session.close()

    value_list = []
    for oldrecord in oldrecords:

        value_list.append([oldrecord.rtime, oldrecord.rvalue])

    return value_list


@db_session
def add_alert(session, name, message):
    # session = open_session(True)
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    alert = Alert(name=message, issued_time=datetime.datetime.now())
    sensor.alert.append(alert)
    session.commit()
    # session.close()


@db_session
def get_active_alerts(session):
    # session = open_session()
    alerts = session.query(Alert).filter(Alert.status).all()
    alerts_list = [{'id': alert.id, 'msg': alert.name, 'status': alert.status} for alert in alerts]
    # session.close()
    return alerts_list


@db_session
def set_alert_status(session, alert_id, alert_status):

    alert = session.query(Alert).filter(Alert.id == alert_id).one()
    alert.status = alert_status
    session.commit()


@db_session
def get_alert(session, name):
    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    alerts = sensor.alert

    messages = []
    for alert in alerts:
        messages += [alert.name]
        alert.ack_time = datetime.datetime.now()

    # session.commit()
    # session.close()
    return messages


@db_session
def add_status(session, name, message):
    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    serial = sensor.status[-1].serial
    status = Status(name=message, issued_time=datetime.datetime.now())
    status.serial = serial
    sensor.status.append(status)
    # session.commit()
    # session.close()


@db_session
def get_status(session, name):
    # session = open_session()
    sensor = session.query(Sensor).filter(Sensor.name == name).one()
    statuses = sensor.status

    messages = []
    for status in statuses:
        messages += [status.name]
        status.ack_time = datetime.datetime.now()

    # session.commit()
    # session.close()
    return messages


# table related routines #
@db_session
def read_table(session, name):

    # session = open_session()
    items = session.query(interfaces[name]).all()
    # session.close()
    entries = []
    for item in items:

        # if name == "record" or name == "oldrecord":
        #     entries += [[item.id, item.rtime, item.rvalue, item.sensor.name]]
        # elif name == "range":
        #     entries += [[item.id, item.min, item.max, item.sensor.name]]
        # elif name == "alert" or name == "status":
        #     entries += [[item.id, item.name, item.sensor.name]]
        # else:
        #     entries += [[item.id, item.name]]

        if name == "record" or name == "oldrecord":
            entries += [[item.rtime, item.rvalue, item.sensor.name]]
        elif name == "range":
            entries += [[item.min, item.max]]
        elif name == "alert" or name == "status":
            entries += [[item.name, item.sensor.name]]
        else:
            entries += [item.name]

    return entries


@db_session
def write_table(session, name, old_entry, new_entry):

    # session = open_session()

    if name == "record" or name == "oldrecord":
        return WRITE_NOT_ALLOWED

        # entry = session.query(interfaces[name])\
        #     .filter((interfaces[name]).rtime == old_entry.rtime)\
        #     .filter((interfaces[name]).rvalue == old_entry.rvalue).one()
        # entry.rtime = new_entry.rtime
        # entry.rvalue = new_entry.rvalue

    elif name == "range":
        entry = session.query(interfaces[name]) \
            .filter((interfaces[name]).min == old_entry.min) \
            .filter((interfaces[name]).max == old_entry.max).one()
        entry.min = new_entry.min
        entry.max = new_entry.max

    elif name == "alert" or name == "status":
        return WRITE_NOT_ALLOWED
        # entry = session.query(interfaces[name]) \
        #     .filter((interfaces[name]).name == old_entry.name) \
        #     .filter((interfaces[name]).sensor.name == old_entry.sensor).one()
        # entry.name = new_entry.name
    else:
        entry = session.query(interfaces[name]).filter((interfaces[name]).name == old_entry.name).one()
        entry.name = new_entry.name

    # session.commit()
    # session.close()


@db_session
def get_diagnosis_parameters(session, sensor_name):

    output_params = dict()

    sensor = session.query(Sensor).filter(Sensor.name == sensor_name).one()

    output_params.update({'warning_down': sensor.warning_down})
    output_params.update({'warning_up': sensor.warning_up})
    output_params.update({'sensor_sensitivity': sensor.sensor_sensitivity})
    output_params.update({'failure_value': sensor.failure_value})
    output_params.update({'short_interval': sensor.short_interval})
    output_params.update({'long_interval': sensor.long_interval})
    output_params.update({'fault_threshold': sensor.fault_threshold})

    return output_params


