import datetime
from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey, PickleType, Boolean, Table, desc
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

default_time = datetime.datetime(2017, 1, 1, 1, 0)


class SensorObj:

    sensor = ""
    # status = ""    # left for completeness
    serial = ""
    model = ""
    manufacturer = ""
    placement = ""
    location = ""
    description = ""
    type = ""
    # alert = ""    # left for completeness
    range_min = 0
    range_max = 0
    # record = ""    # left for completeness
    # oldrecord = ""    # left for completeness
    # diagnostics
    warning_down = 0
    warning_up = 250
    sensor_sensitivity = 10
    failure_value = 0
    short_interval = 30
    long_interval = 480
    fault_threshold = 10


class Sensor(Base):
    __tablename__ = "sensor"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), unique=True, nullable=False)
    placement_id = Column(Integer, ForeignKey('placement.id'), nullable=False)
    placement = relationship("Placement", backref=backref("sensor", lazy="joined"))
    description_id = Column(Integer, ForeignKey('description.id'), nullable=False)
    description = relationship("Description", backref=backref("sensor", lazy="joined"))
    type_id = Column(Integer, ForeignKey('type.id'), nullable=False)
    type = relationship("Type", backref=backref("sensor", lazy="joined"))
    range_id = Column(Integer, ForeignKey('range.id'), nullable=False)
    range = relationship("Range", backref=backref("sensor", lazy="joined"))
    # Fault diagnosis parameters
    warning_down = Column(Float, nullable=True, default=None)
    warning_up = Column(Float, nullable=True, default=None)
    sensor_sensitivity = Column(Float, nullable=True, default=None)
    failure_value = Column(Float, nullable=True, default=None)
    short_interval = Column(Float, nullable=True, default=None)
    long_interval = Column(Float, nullable=True, default=None)
    fault_threshold = Column(Float, nullable=True, default=None)

    def __repr__(self):
        return "<Sensor(id='%s', name='%s')>" % (self.id, self.name)

# Hardware description #


class ModelSerials(Base):
    __tablename__ = "model_serials"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    serial_id = Column(Integer, ForeignKey('serial.id'), nullable=False)
    model_id = Column(Integer, ForeignKey('model.id'), nullable=False)


class ManufacturerModels(Base):
    __tablename__ = "manufacturer_models"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    model_id = Column(Integer, ForeignKey('model.id'), nullable=False)
    manufacturer_id = Column(Integer, ForeignKey('manufacturer.id'), nullable=False)


class Status(Base):
    __tablename__ = "status"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), default="EmptyStatus", nullable=False)
    issued_time = Column(DateTime(), default=default_time, nullable=False)
    ack_time = Column(DateTime(), default=default_time, nullable=False)
    sensor_id = Column(Integer, ForeignKey('sensor.id'), nullable=False)
    sensor = relationship("Sensor", backref=backref('status', lazy="joined"))
    serial_id = Column(Integer, ForeignKey('serial.id'), nullable=False)
    serial = relationship("Serial", backref=backref('status', lazy="joined"))

    def __repr__(self):
        return "<Status(id='%s', name='%s', issued_time='%s', ack_time='%s', sensor_id='%s', sensor='%s', serial_id='%s', serial='%s')>" %\
               (self.id, self.name, self.issued_time, self.ack_time, self.sensor_id, self.sensor, self.serial_id, self.serial)


class Serial(Base):
    __tablename__ = "serial"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), default="EmptySerial", unique=True, nullable=False)

    def __repr__(self):
        return "<Serial(id='%s', name='%s')>" %\
               (self.id, self.name)


class Model(Base):
    __tablename__ = "model"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), default="EmptyModel", unique=True, nullable=False)
    serial = relationship("Serial", secondary='model_serials', backref=backref('model', lazy="joined"))

    def __repr__(self):
        return "<Model(id='%s', name='%s', serial='%s')>" % \
               (self.id, self.name, self.serial)


class Manufacturer(Base):
    __tablename__ = "manufacturer"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), default="EmptyManufacturer", unique=True, nullable=False)
    model = relationship("Model", secondary='manufacturer_models', backref=backref('manufacturer', lazy="joined"))

    def __repr__(self):
        return "<Manufacturer(id='%s', name='%s', model='%s')>" % \
               (self.id, self.name, self.model)

# End of hardware description #


# Hardware placement #

class LocationPlacements(Base):
    __tablename__ = "location_placements"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    placement_id = Column(Integer, ForeignKey('placement.id'), nullable=False)
    location_id = Column(Integer, ForeignKey('location.id'), nullable=False)


class Placement(Base):
    __tablename__ = "placement"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(128), default="EmptyPlacement", unique=True, nullable=False)

    def __repr__(self):
        return "<Placement(id='%s', name='%s')>" %\
               (self.id, self.name)


class Location(Base):
    __tablename__ = "location"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(64), default="EmptyLocation", unique=True, nullable=False)
    placement = relationship("Placement", secondary="location_placements", backref=backref("location", lazy="joined"))

    def __repr__(self):
        return "<Location(id='%s', name='%s', placement='%s')>" % \
               (self.id, self.name, self.placement)


# End of hardware placement #

# Sensor description #


class Description(Base):
    __tablename__ = "description"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(256), default="EmptyDescription", unique=True, nullable=False)

    def __repr__(self):
        return "<Description(id='%s', name='%s')>" %\
               (self.id, self.name)


class Type(Base):
    __tablename__ = "type"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(16), default="EmptyType", unique=True, nullable=False)

    def __repr__(self):
        return "<Type(id='%s', name='%s')>" % \
               (self.id, self.name)


# End of sensor description #

# Sensor data records


class Alert(Base):
    __tablename__ = "alert"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = Column(String(16), default="EmptyAlert", nullable=False)
    issued_time = Column(DateTime(), default=default_time, nullable=False)
    ack_time = Column(DateTime(), default=default_time, nullable=False)
    status = Column(Boolean, default=True, nullable=False)
    sensor_id = Column(Integer, ForeignKey('sensor.id'), nullable=False)
    sensor = relationship("Sensor", backref=backref("alert", lazy="joined"))

    def __repr__(self):
        return "<Alert(id='%s', status='%s', name='%s', issued_time='%s', ack_time='%s',sensor_id='%s', sensor='%s')>" %\
               (self.id, self.status, self.name, self.issued_time, self.ack_time, self.sensor_id, self.sensor)


class Range(Base):
    __tablename__ = "range"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    min = Column(Integer)
    max = Column(Integer)

    def __repr__(self):
        return "<Range(id='%s', min='%s', max='%s')>" %\
               (self.id, self.min, self.max)


class Record(Base):
    __tablename__ = "record"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    rtime = Column(DateTime(), nullable=False)
    rvalue = Column(Float())
    sensor_id = Column(Integer, ForeignKey('sensor.id'), nullable=False)
    sensor = relationship("Sensor", backref=backref("record", lazy="joined"))

    def __repr__(self):
        return "<Record(id='%s', rtime='%s', rvalue='%s', sensor_id='%s', sensor='%s')>" %\
               (self.id, self.rtime, self.rvalue, self.sensor_id, self.sensor)


class OldRecord(Base):
    __tablename__ = "oldrecord"
    id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    rtime = Column(DateTime(), nullable=False)
    rvalue = Column(Float())
    sensor_id = Column(Integer, ForeignKey('sensor.id'), nullable=False)
    sensor = relationship("Sensor", backref=backref("oldrecord", lazy="joined"))

    def __repr__(self):
        return "<OldRecord(id='%s', rtime='%s', rvalue='%s', sensor_id='%s', sensor='%s')>" % \
               (self.id, self.rtime, self.rvalue, self.sensor_id, self.sensor)

# End of sensor data records


# Fault diagnosis parameters
# class Diagnosis(Base):
#     __tablename__ = "diagnosis"
#     id = Column(Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
#     down = Column(Float, nullable=True, default=None)
#     up = Column(Float, nullable=True, default=None)
#     sensitivity = Column(Float, nullable=True, default=None)
#     value = Column(Float, nullable=True, default=None)
#     short_interval = Column(Float, nullable=True, default=None)
#     long_interval = Column(Float, nullable=True, default=None)
#     threshold = Column(Float, nullable=True, default=None)
#
#     def __repr__(self):
#         return "<Diagnosis Parameters: down='%s', up='%s', sensitivity='%s', value='%s', short interval='%s', " \
#                "long interval='%s', threshold='%s')>" % (self.down, self.up, self.sensitivity, self.value,
#                                                          self.short_interval, self.long_interval, self.threshold)


# DYNAMIC CLASS/TABLE GENERATION!!
# It is used to assign the value of "__tablenames__" at run time,
# thus to create new record classes/tables at runtime, as needed.
#
# Problems: conflict have appeared when a new class is used to append new
# records to an existing table, even when sharing the same __tablename__ value.
# In this case AutoMap should be used to load pre-existing tables.
#
# Care should be also used to avoid the overlap of dynamic and static classes
#
#  class Record(object):
#
#     record_time = Column(DateTime(), primary_key=True)
#     record_value = Column(Float())
#     status = Column(String(32))
#
#     def __init__(self, r_time, r_value, status):
#         self.record_time = r_time
#         self.record_value = r_value
#         self.status = status
#
# def new_sensor_record_class_gen(name):
#     return type(str(name), (Record, Base), {'__tablename__': name})
#
# def old_sensor_record_class_gen(name):
#     return type(str(name), (Record, Base), {'__tablename__': name, '__table_args__': {'autoload': True}})

