# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import operator
from defines import *
from ems_db.sensor.routines import get_last_record
from ems_db.schedule.routines import get_tasks_in_execution
import logging
logger = logging.getLogger(__name__)


LOW_CONSUMPTION_LEVEL = 2000  # no task suspended
MID_CONSUMPTION_LEVEL = 2500  # one task suspended
MAX_CONSUMPTION_LEVEL = 3000  # all tasks suspended


def overload_manager(data_from_core, data_to_core):

    logger.info('Started default overload manager routine')

    send_alert = False

    while True:

        alert_messages = list()
        task_ids = list()

        # get last overall house consumption
        overall_consumption = get_last_record('overall')
        tasks = get_tasks_in_execution()  # return list of tuple (id, name, start, energy)

        if LOW_CONSUMPTION_LEVEL <= overall_consumption < MID_CONSUMPTION_LEVEL:
            send_alert = True
            # just send message with alert
            alert_messages.append({'alert_code': OVERLOAD_ALERT_MIN, 'tasks': []})

        elif MID_CONSUMPTION_LEVEL <= overall_consumption < MAX_CONSUMPTION_LEVEL:
            send_alert = True
            # just send message with alert
            # detect active task with greater consumption to interrupt

            if tasks and len(tasks) > 1:
                tasks.sort(key=operator.itemgetter(3))
                # if any tasks, return the one with the greatest energy consumption
                task_ids = [tasks[-1][0]]

            alert_messages.append({'alert_code': OVERLOAD_ALERT_MED, 'tasks': task_ids})

        elif overall_consumption > MAX_CONSUMPTION_LEVEL:
            send_alert = True
            # just send message with alert
            # interrupt all active tasks, pass all tasks list
            if tasks:
                for task in tasks:
                    task_ids.append(task[0])

            alert_messages.append({'code': OVERLOAD_ALERT_MAX, 'tasks': task_ids})

        if send_alert:

            alert_message = {'alerts': alert_messages}
            data_to_core.put(alert_message)
            send_alert = False

        time.sleep(OVERLOAD_MANAGER_LOOP_TIME)
