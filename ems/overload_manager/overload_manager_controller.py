# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import communication
import overload_manager.overload_manager_core
import time
import sys
import queue
import threading
import logging
logger = logging.getLogger(__name__)


def mainprocess():

    # Init overload manager controller
    setup_logging(MODULES_NAMES[OVERLOAD_MANAGER_QUEUE])
    controller_queues = communication.ControllerQueues(MODULES_NAMES[OVERLOAD_MANAGER_QUEUE])
    logger.info('Overload manager running.')

    resolution = None
    horizon = None

    # Object shared with computation thread
    input_overload_manager_data = queue.Queue()
    output_overload_manager_data = queue.Queue()

    overload_manager_thread = None
    # first_start = True

    # Wait message from core
    while True:

        try:

            if not overload_manager_thread or not overload_manager_thread.is_alive():

                overload_manager_thread = threading.Thread(
                    name='overload_manager_thread',
                    target=overload_manager.overload_manager_core.overload_manager_core,
                    args=(input_overload_manager_data,
                          output_overload_manager_data,))

                overload_manager_thread.daemon = True
                overload_manager_thread.start()

                # if not first_start:
                #     # request db config if restarted
                #     msg = {MSG_ID_INTERFACE_REQUEST_CONFIG: []}
                #     if not controller_queues.sendMsg(msg, True):
                #         logger.info("Problem with queue to core.")
                #     send_data_to_gui.put({'status': 'Requesting configuration information...'})
                # else:
                #     first_start = False

            # Check if received message from core
            message_id, message_data = controller_queues.recvMsg(True)

            if message_id:
                logger.debug('Received message')

                if message_id == MSG_ID_CONFIG:
                    resolution = message_data.get('resolution')
                    horizon = message_data.get('horizon')

                if message_id == MSG_ID_DATA_TO_OVERLOAD_MANAGER:
                    # if not resolution or not horizon:
                    #     logger.error("Horizon and resolution not set.")

                    logger.debug("Received message for overload_manager.")

                    input_overload_manager_data.put(message_data)

                if message_id == MSG_KILL_ALL:
                    logger.debug("Received kill message.")
                    raise KeyboardInterrupt

            # check if received overload manager message
            if not output_overload_manager_data.empty():
                output_data = output_overload_manager_data.get()
                logger.debug("Send overload_manager data.")
                msg = {
                    MSG_ID_DATA_FROM_OVERLOAD_MANAGER: output_data}
                if not controller_queues.sendMsg(msg, True):
                    logger.info("Problem with queue to core.")

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Clear output messages.")

            if not output_overload_manager_data.empty():  # dump messages
                output_overload_manager_data.get()

            logger.info("New connection request.")
            # loop until new connection success
            controller_queues.reconnect()  # blocking, release when connection available

        except KeyboardInterrupt:
            logger.info("overload_manager controller process exit")
            sys.exit(0)

        except Exception as err:
            logger.critical("Unhandled exception!")
            logger.critical(err)
            sys.exit(-1)


if __name__ == '__main__':

    # start SyncManager
    # core_queues = communication.CoreQueues()

    mainprocess()
