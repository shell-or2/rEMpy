# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import communication
import time
import sys
import queue
import threading
import thermal.thermal_core

logger = logging.getLogger(__name__)


def mainprocess():

    # Init thermal controller
    setup_logging(MODULES_NAMES[THERMAL_QUEUE])
    controller_queues = communication.ControllerQueues(MODULES_NAMES[THERMAL_QUEUE])
    logger.info('Thermal running.')

    resolution = None
    horizon = None

    # Object shared with computation thread
    input_thermal_data = []
    output_thermal_data = queue.Queue()
    stop_event = threading.Event()
    thermal_thread = None

    while True:

        try:
            # Check if received message from core
            message_id, message_data = controller_queues.recvMsg(True)

            if message_id:
                logger.debug('Received message')

                if message_id == MSG_ID_CONFIG:
                    resolution = message_data.get('resolution')
                    horizon = message_data.get('horizon')

                if message_id == MSG_ID_THERMAL_REQUEST:
                    if not resolution or not horizon:
                        logger.error("Horizon and resolution not set.")

                    logger.debug("Requested thermal simulation.")

                    time.sleep(5)

                    input_thermal_data = message_data
                    thermal_thread = threading.Thread(name='thermal_thread',
                                                      target=thermal.thermal_core.thermal_core,
                                                      args=(input_thermal_data, output_thermal_data, stop_event,))
                    stop_event.clear()
                    thermal_thread.start()

                if message_id == MSG_ID_THERMAL_CANCEL:
                    logger.debug("Requested thermal cancellation.")
                    if thermal_thread.is_alive():
                        stop_event.set()
                        #  prediction_thread.terminate()
                        thermal_thread.join()
                        logger.debug('Thermal process cancelled.')
                        # TODO: send response to core in case of success?

            if not output_thermal_data.empty():
                output_data = output_thermal_data.get()
                logger.debug("Send thermal data.")
                msg = {MSG_ID_THERMAL_RESPONSE: output_data}
                if not controller_queues.sendMsg(msg, True):
                    logger.info("Problem with queue to core.")

            if stop_event.is_set():
                logger.debug('Kill event set...controlling thread.')
                if thermal_thread.is_alive():
                    logger.debug('Thread still alive!')
                else:
                    stop_event.clear()
                    logger.debug('Thread killed, event cleared.')

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Cancel thermal core action and clear output messages.")
            if thermal_thread and thermal_thread.is_alive():
                stop_event.set()
                #  prediction_thread.terminate()
                thermal_thread.join()
                logger.debug('Thermal process cancelled.')
                stop_event.clear()

            if not output_thermal_data.empty():  # dump messages
                output_thermal_data.get()

            logger.info("New connection request.")
            # loop until new connection success
            controller_queues.reconnect()  # blocking, release when connection available

        except KeyboardInterrupt:
            logger.info("Thermal controller process exit")
            sys.exit(0)

        except Exception as err:
            logger.error(err)
            logger.error("Unhandled exception!")
            sys.exit(-1)


if __name__ == '__main__':

    mainprocess()

    # """ Testing your code! """
    #
    # setup_logging("test_thermal")
    #
    # input_thermal_data = {'timestamp': 1477657697, 'resolution': 15, 'horizon': 360}
    # output_thermal_data = queue.Queue()
    # stop_event = threading.Event()
    #
    # #mainprocess(queue.Queue(), queue.Queue())
    #
    # thermal_th = threading.Thread(name='thermal_thread',
    #                               target=thermal.thermal_core.thermal_core,
    #                               args=(input_thermal_data, output_thermal_data, stop_event,))
    #
    # thermal_th.start()
    #
    # while True:
    #     if not output_thermal_data.empty():
    #         output_data = output_thermal_data.get()
    #         print(output_data)
    #         logger.debug("Received thermal data.")
    #         break
