# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import math
import logging
logger = logging.getLogger(__name__)


def write_epw(input_data):

    # i dati devono coprire le 24 ore, anche se i dati vengono inseriti
    # a partire dalle 01:00 fino al raggiungimento dell'orizzonte indicato
    # per questo motivo procedura di riempimento ripete gli utlimi valori indicati per ogni parametro.

    weather_data = input_data.get('commonData').get('weatherData')

    _temperatura = weather_data.get('outdoor_temperature')
    _umidità = weather_data.get('outdoor_humidity')
    _vento = weather_data.get('wind_vel')
    _direzione_vento = weather_data.get('wind_dir')
    _pressione = weather_data.get('pressure')
    _igpf = weather_data.get('gc_radiation')
    _idpf = weather_data.get('gd_radiation')
    _ind = weather_data.get('dni_radiation')
    _resolution = input_data.get('resolution')
    _horizon = input_data.get('horizon')

    fine = round(_horizon / 60)
    res = round(60 / _resolution)

    with open(EP_EPW_FILE, 'w+') as out_file:

        out_file.write("LOCATION,Ancona-Falconara,-,ITA,IGDG,161910,43.62,13.37,1.0,12.0")
        out_file.write("\nDESIGN CONDITIONS,1,Climate Design Data 2009 ASHRAE Handbook,,Heating,1,-2.2,-1.1,-8,1.9,1.3,"
                       "-6,2.3,2.3,12,8,10.3,8,2,200,Cooling,7,9.9,32.1,22.8,30.2,22.4,29.1,22.2,24.7,29.2,23.9,28.6,"
                       "23.1,27.8,4.3,120,23.2,18,27.6,22.2,16.9,27,21.2,15.9,26.3,74.9,29.1,71.5,28.6,68.4,28,934,"
                       "Extremes,9.8,8.1,6.9,27.9,-5.2,36.1,2.9,2,-7.4,37.5,-9.1,38.7,-10.7,39.8,-12.9,41.3")
        out_file.write("\nTYPICAL/EXTREME PERIODS,6,Summer - Week Nearest Max Temperature For Period,Extreme,8/ 3,8/ 9,"
                       "Summer - Week Nearest Average Temperature For Period,Typical,6/22,6/28,Winter - Week Nearest "
                       "Min Temperature For Period,Extreme,12/22,12/28,Winter - Week Nearest Average Temperature For "
                       "Period,Typical,1/27,2/ 2,Autumn - Week Nearest Average Temperature For Period,Typical,10/13,10/"
                       "19,Spring - Week Nearest Average Temperature For Period,Typical,4/12,4/18")
        out_file.write("\nGROUND TEMPERATURES,3,.5,,,,12.47,8.81,6.57,5.88,7.23,10.27,14.04,17.74,20.22,20.80,19.36,"
                       "16.37,2,,,,14.06,11.15,9.02,8.06,8.14,9.88,12.50,15.42,17.77,18.88,18.51,16.78,4,,,,14.72,12.72"
                       ",11.06,10.14,9.59,10.35,11.90,13.88,15.72,16.88,17.10,16.33")
        out_file.write("\nHOLIDAYS/DAYLIGHT SAVINGS,No,0,0,0")
        out_file.write("\nCOMMENTS 1,Custom/User Format -- WMO#161910; Italian Climate Data Set Gianni de Giorgio; "
                       "Period of record 1951-1970")
        out_file.write("\nCOMMENTS 2, -- Ground temps produced with a standard soil diffusivity of 2.3225760E-03 "
                       "{m**2/day}")
        out_file.write("\nDATA PERIODS,1,4,Data,Sunday, 1/ 1,1/ 1")

        for i in range(24*res):  # i copre tutte le 24 ore - (fine*res):

            str1 = "2005,1,1,"

            ore = math.floor(((i / 4)) + 1)

            minuti = (i % 4) * 15

            str2 = ",?9?9?9?9E0?9?9?9*9*9?9*9?9?9*9?9?9?9*9*_*9*9*9*9*9,"

            if i < (fine*res):
                # dati nel range da 0 a horizon. es: con horinzon=360 e resolution=15 si
                # hanno 1 + 24 dati (valore attuale più 24 predetti)

                out_file.write("\n" + str1 + str(ore) + "," + str(minuti) + str2 + str(_temperatura[i]) + ",5.0," +
                               str(int(_umidità[i])) + "," + str(int(_pressione[i] * 100)) + ",9999,9999,298," +
                               str(int(_igpf[i])) + "," + str(int(_ind[i])) + "," + str(int(_idpf[i])) +
                               ",999900,999900,999900,99990," + str(int(_direzione_vento[i])) + "," + str(_vento[i]) +
                               "," + str(int(_temperatura[i] + 3)) + "," + str(int(_temperatura[i] + 3)) +
                               ",999.0,999,9,999999999,0,0.0000,0,88,999.000,999.0,99.0")

            else:
                # oltre horizon ripeto gli ultimi valori per riempire
                out_file.write("\n" + str1 + str(ore) + "," + str(minuti) + str2 + str(_temperatura[-1]) + ",5.0," +
                               str(int(_umidità[-1])) + "," + str(int(_pressione[-1] * 100)) + ",9999,9999,298," +
                               str(int(_igpf[-1])) + "," + str(int(_ind[-1])) + "," + str(int(_idpf[-1])) +
                               ",999900,999900,999900,99990," + str(int(_direzione_vento[-1])) + "," + str(_vento[-1]) +
                               "," + str(int(_temperatura[-1] + 3)) + "," + str(int(_temperatura[-1] + 3)) +
                               ",999.0,999,9,999999999,0,0.0000,0,88,999.000,999.0,99.0")


