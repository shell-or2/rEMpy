# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import time
import subprocess
from eppy.modeleditor import IDF
import esoreader
import os
import shutil
from defines import *
from thermal.write_epw import write_epw
import logging
logger = logging.getLogger(__name__)


#rooms_names = ['1235', '1242', '1250', '1258', '1266']
rooms_names = ['Edificio:Bagno', 'Edificio:Soggiorno', 'Edificio:Camera1', 'Edificio:Camera2', 'Edificio:Cucina']

USE_REAL_TIME_FOR_DATA = False  # otherwise simulation start at 01:00 AM of 1/1/xxxx


def adaptSetpointsTo24Hours(_rooms_setpoints, _start_timestamp, _horizon, _resolution):

    start_index = int(60 / _resolution) * int(time.strftime('%H', time.localtime(_start_timestamp))) \
                  + round(int(time.strftime('%M', time.localtime(_start_timestamp))) / _resolution)
    end_index = start_index + (_horizon // _resolution)

    elements_before = start_index
    elements_after = int((24 * 60)/_resolution) - end_index

    adapted_rooms_setpoints = []

    for room_setpoints in _rooms_setpoints:

        if len(room_setpoints) == 1:
            adapted_rooms_setpoints.append(room_setpoints * (24 * (60 // _resolution)))

        elif len(room_setpoints) == (_horizon // _resolution):

            if USE_REAL_TIME_FOR_DATA:
                # [st1 st1 st1 -st1 st2 st3 st4- st4 st4 st4 st4]
                first_room_setpoints = [room_setpoints[0]] * elements_before
                last_room_setpoints = [room_setpoints[-1]] * elements_after
                # if start_index:  # if equal to 0, actual setpoint is the first adapted setpoint, no insertions required
                #     room_setpoints.insert(0, first_room_setpoint)
                adapted_rooms_setpoints.append(first_room_setpoints + room_setpoints + last_room_setpoints)

            else:
                # [st1 st2 st3 st4- st4 st4 st4 st4 st4 st4 st4]
                last_room_setpoints = [room_setpoints[-1]] * (elements_before + elements_after)
                adapted_rooms_setpoints.append(room_setpoints + last_room_setpoints)

        else:
            raise Exception('Inconsistent setpoints length!')

    return adapted_rooms_setpoints


def fillScheduleFields(new_schedule, setpoints24hours):

    field_counter = 3
    delta_minutes = (24 * 60) // len(setpoints24hours)
    time_counter_in_minutes = delta_minutes  # first field

    for setpoint in setpoints24hours:

        hours = time_counter_in_minutes // 60
        minutes = time_counter_in_minutes - (hours * 60)
        setattr(new_schedule, 'Field_' + str(field_counter), 'Until: %02d:%02d' % (hours, minutes))
        field_counter += 1
        setattr(new_schedule, 'Field_' + str(field_counter), str(setpoint))
        field_counter += 1
        time_counter_in_minutes += delta_minutes


def scale_power(outdoor, indoor, power_output):
    power_input = []

    for i in range(0, len(outdoor)):
        out_t = outdoor[i]
        in_t = indoor[i]
        out_p = power_output[i]

        if (out_t < in_t):
            # heating case
            if (out_t > 18):
                logger.info("Strange heating request. Outdoor temperature is above 18 °C")
            cop_h = in_t / (in_t - out_t)
            in_p = out_p / cop_h

        elif (out_t > in_t):
            # cooling case
            if (out_t < 22):
                logger.info("Strange cooling request. Outdoor temperature is below 22 °C")
            cop_c = out_t / (in_t - out_t)
            in_p = out_p / cop_c

        else:
            # neutral case
            if (out_p > 0):
                logger.info("Strange thermal request. Outdoor temperature matches indoor temperature")
            cop_n = 3
            in_p = out_p / cop_n

        power_input += [in_p]

    return power_input


def setIDF(input_data):

    logger.debug('Setting IDF...')

    start_timestamp = input_data.get('timestamp')
    last_timestamp = start_timestamp + (input_data.get('horizon') * 60)
    horizon = input_data.get('horizon')
    resolution = input_data.get('resolution')

    #################################################################################
    # edit the idf file
    IDF.setiddname(EP_IDD_FILE)
    idf1 = IDF(EP_IDF_FILE_TMP)
    # idf1.printidf()

    schedule_heating_setpoints_name = 'Heating Setpoint Schedule'
    schedule_cooling_setpoints_name = 'Cooling SP Sch'
    schedule_heating_availability_name = 'Heating Availability Sch'
    schedule_cooling_availability_name = 'Cooling Availability Sch'

    # Setpoint -> set temperature per fascie orario/sub-orarie
    # Availability -> set se heat/cool attivo

    # all_setpoints_combinations = [zone_names[i] + ' ' + schedule_setpoints_options[j] for i in range(len(zone_names))
    #                               for j in range(len(schedule_setpoints_options))]
    # all_availability_combinations = [zone_names[i] + ' ' + schedule_availability_options[j]
    #                                for i in range(len(zone_names)) for j in range(len(schedule_availability_options))]

    all_heating_setpoints_combinations = [rooms_names[i] + ' ' + schedule_heating_setpoints_name
                                          for i in range(len(rooms_names))]
    all_cooling_setpoints_combinations = [rooms_names[i] + ' ' + schedule_cooling_setpoints_name
                                          for i in range(len(rooms_names))]

    all_heating_availability_combinations = [rooms_names[i] + ' ' + schedule_heating_availability_name
                                             for i in range(len(rooms_names))]
    all_cooling_availability_combinations = [rooms_names[i] + ' ' + schedule_cooling_availability_name
                                             for i in range(len(rooms_names))]

    htg_rooms_setpoints = []
    clg_rooms_setpoints = []

    if not input_data.get('specs').get('setpoints'):
        # missing setpoints, set default values
        htg_rooms_setpoints = [[18] * (horizon // resolution)] * \
                              len(input_data.get('specs').get('thermal_zones') or rooms_names)
        clg_rooms_setpoints = [[27] * (horizon // resolution)] * \
                              len(input_data.get('specs').get('thermal_zones') or rooms_names)

    else:
        htg_rooms_setpoints = input_data.get('specs').get('setpoints').get('htg')
        clg_rooms_setpoints = input_data.get('specs').get('setpoints').get('clg')

    # remove schedules then add new ones
    all_schedules_names = all_heating_setpoints_combinations + \
                          all_cooling_setpoints_combinations + \
                          all_heating_availability_combinations + \
                          all_cooling_availability_combinations

    all_schedules = idf1.idfobjects['SCHEDULE:COMPACT']

    more_schedules = True

    while more_schedules:

        more_schedules = False

        for schedule in all_schedules:

            if schedule.Name in all_schedules_names:
                idf1.removeidfobject(schedule)
                more_schedules = True
                break

    # add new schedules for setpoint and availability
    # First mandatory fields:
    # 1235 Heating Setpoint Schedule,       !- Name
    # Temperature,                          !- Schedule Type Limits Name
    # Through: 12 / 31,                     !- Field 1
    # For: AllDays,                         !- Field 2

    # if Setpoints next fields:
    # Until: 24:00,                         !- Field 3
    # 30,                                   !- Field 4
    # etc

    # if availability
    # Until: 24:00,                         !- Field 3
    # 1,                                    !- Field 4

    # TODO: check room setpoints, if empty set default values before adapt to 24 hours
    htg_rooms_adapted_setpoints = adaptSetpointsTo24Hours(htg_rooms_setpoints, start_timestamp, horizon, resolution)
    clg_rooms_adapted_setpoints = adaptSetpointsTo24Hours(clg_rooms_setpoints, start_timestamp, horizon, resolution)

    # Zone-setpoints: 1235-setpoint1 (rooms_setpoints[0]), 1242-setpoint2, 1250-setpoint3, 1258-setpoint4, 1266-setpoint5

    for schedule_name in all_schedules_names:

        new_schedule = idf1.newidfobject('SCHEDULE:COMPACT',
                                         Name=schedule_name,
                                         Schedule_Type_Limits_Name='Temperature',
                                         Field_1='Through: 12/31',
                                         Field_2='For: AllDays')

        if schedule_name in all_heating_setpoints_combinations:

            fillScheduleFields(new_schedule, htg_rooms_adapted_setpoints[rooms_names.index(schedule_name.split()[0])])
            # new_schedule.Field_3 = 'Until: 12:00'
            # new_schedule.Field_4 = '20'
            # new_schedule.Field_5 = 'Until: 24:00'
            # new_schedule.Field_6 = '10'

        elif schedule_name in all_cooling_setpoints_combinations:

            fillScheduleFields(new_schedule, clg_rooms_adapted_setpoints[rooms_names.index(schedule_name.split()[0])])  # [100]

        elif schedule_name in all_heating_availability_combinations:

            fillScheduleFields(new_schedule, [1])
            # new_schedule.Field_3 = 'Until: 24:00'
            # new_schedule.Field_4 = '1'

        elif schedule_name in all_cooling_availability_combinations:

            fillScheduleFields(new_schedule, [1])

    # start_timestamp = input_data.get('timestamp')
    # last_timestamp = start_timestamp + (input_data.get('horizon') * 60)

    # print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_timestamp)))
    # print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(last_timestamp)))

    runperiod = idf1.idfobjects['RUNPERIOD'][0]

    if USE_REAL_TIME_FOR_DATA:
        runperiod.Begin_Month = time.strftime('%m', time.localtime(start_timestamp))
        runperiod.Begin_Day_of_Month = time.strftime('%d', time.localtime(start_timestamp))
        # Valid days of the week: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday)
        runperiod.Day_of_Week_for_Start_Day = time.strftime('%A', time.localtime(start_timestamp))

        runperiod.End_Month = time.strftime('%m', time.localtime(last_timestamp))
        runperiod.End_Day_of_Month = time.strftime('%d', time.localtime(last_timestamp))

    else:
        runperiod.Begin_Month = '1'
        runperiod.Begin_Day_of_Month = '1'
        # Valid days of the week: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday)
        runperiod.Day_of_Week_for_Start_Day = time.strftime('%A', time.localtime(start_timestamp))

        runperiod.End_Month = '1'
        runperiod.End_Day_of_Month = '1'

    # The user’s choice for Number of Timesteps per Hour must be evenly divisible into
    # 60; the allowable choices are 1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, and 60.
    timesteps = idf1.idfobjects['TIMESTEP'][0]

    tmp_timesteps = 60 / input_data.get('resolution')
    if tmp_timesteps.is_integer():
        timesteps.Number_of_Timesteps_per_Hour = tmp_timesteps
    else:
        raise Exception("Number_of_Timesteps_per_Hour must be evenly divisible into 60")

    # try to properly setup energy outputs
    outputs = idf1.idfobjects['OUTPUT:VARIABLE']

    # for output in outputs:
    #     if output.Variable_Name == "Zone Air System Sensible Cooling Rate":
    #         output.Variable_Name = "Zone Air System Sensible Cooling Energy"
    #     elif output.Variable_Name == "Zone Air System Sensible Heating Rate":
    #         output.Variable_Name = "Zone Air System Sensible Heating Energy"

    # enable output Zone Air Temperature

    output_variables = idf1.idfobjects['OUTPUT:VARIABLE']

    # remove output_variables then add new ones
    output_variables_names_to_remove = 'Zone Air Temperature'

    more_output_variables = True

    while more_output_variables:

        more_output_variables = False

        for output_variable in output_variables:

            if output_variable.Variable_Name == output_variables_names_to_remove:
                idf1.removeidfobject(output_variable)
                more_output_variables = True
                break

    # insert new output_variables for each zone
    for room_name in rooms_names:
        # new_output_variable =
        idf1.newidfobject('OUTPUT:VARIABLE',
                          Key_Value=room_name,
                          Variable_Name='Zone Air Temperature',
                          Reporting_Frequency='TimeStep')

    idf1.save(encoding='UTF-8')
    logger.debug('Setting IDF...Saved!')


def readEPoutput_ideal(input_data):

    start_timestamp = input_data.get('timestamp')
    horizon = input_data.get('horizon')
    resolution = input_data.get('resolution')

    outdoor_temperatures = input_data.get('outdoor_temperature')

    if USE_REAL_TIME_FOR_DATA:
        start_index = int(60/resolution) * int(time.strftime('%H', time.localtime(start_timestamp))) \
                  + round(int(time.strftime('%M', time.localtime(start_timestamp)))/15)
        end_index = start_index + (horizon // resolution)

    else:
        start_index = 0
        end_index = horizon // resolution

    eso = esoreader.read_from_path(EP_ESO_FILE)

    logger.debug('Zone Ideal Loads Supply Air Total Heating Energy')
    # first row zone reference numbers
    # therefore, total number of rows = 1 + step number * 24 (e.g. 15 minutes step => 97 rows
    # eso.find_variable('Zone Ideal Loads Supply Air Total Heating Rate')  # Zone Air System Sensible Heating Energy
    # heating_power = eso.to_frame('Zone Ideal Loads Supply Air Total Heating Rate')
    # heating_power['TOTAL'] = heating_power.sum(axis=1)  # sum all zones power

    eso.find_variable('Zone Ideal Loads Supply Air Total Heating Energy')
    heating_energy = eso.to_frame('Zone Ideal Loads Supply Air Total Heating Energy')
    heating_energy['TOTAL'] = heating_energy.sum(axis=1)/3600  # J to Wh

    logger.debug('Zone Ideal Loads Supply Air Total Cooling Energy')
    # eso.find_variable('Zone Ideal Loads Supply Air Total Cooling Rate')
    # cooling_power = eso.to_frame('Zone Ideal Loads Supply Air Total Cooling Rate')
    # cooling_power['TOTAL'] = cooling_power.sum(axis=1)  # sum all zones power

    eso.find_variable('Zone Ideal Loads Supply Air Total Cooling Energy')
    cooling_energy = eso.to_frame('Zone Ideal Loads Supply Air Total Cooling Energy')
    cooling_energy['TOTAL'] = cooling_energy.sum(axis=1)/3600  # J to Wh

    # total_power_no_cop_pandas = heating_power['TOTAL'] + cooling_power['TOTAL']
    # total_power_no_cop = total_power_no_cop_pandas.tolist()[start_index:end_index]

    total_energy_no_cop_pandas = heating_energy['TOTAL'] + cooling_energy['TOTAL']
    total_energy_no_cop = total_energy_no_cop_pandas.tolist()[start_index:end_index]

    # Zone Air Temperature
    eso.find_variable('Zone Air Temperature')
    zones_air_temperature = eso.to_frame('Zone Air Temperature')
    zones_air_temperature['AVERAGE'] = zones_air_temperature.mean(axis=1)
    indoor_temperatures = zones_air_temperature['AVERAGE'].values.tolist()[start_index:end_index]

    # for room_name in rooms_names:
    #
    #     room_output_power = heating_power[room_name] + cooling_power[room_name]
    #     power_cop[room_name] = scale_power(outdoor_temperatures, zones_air_temperature[room_name],
    #                                        room_output_power.to_list()[start_index:end_index])

    total_energy_cop = scale_power(outdoor_temperatures, indoor_temperatures, total_energy_no_cop)

    return total_energy_cop


def readEPoutput(input_data):

    start_timestamp = input_data.get('timestamp')
    horizon = input_data.get('horizon')
    resolution = input_data.get('resolution')

    logger.info('Reading ESO file.')

    outdoor_temperatures = input_data.get('outdoor_temperature')

    if USE_REAL_TIME_FOR_DATA:
        start_index = int(60/resolution) * int(time.strftime('%H', time.localtime(start_timestamp))) \
                  + round(int(time.strftime('%M', time.localtime(start_timestamp)))/15)
        end_index = start_index + (horizon // resolution)

    else:
        start_index = 0
        end_index = horizon // resolution

    eso = esoreader.read_from_path(EP_ESO_FILE)

    # Zone Air Temperature
    eso.find_variable('Water to Water Heat Pump Electric Energy')
    heat_pump_energy = eso.to_frame('Water to Water Heat Pump Electric Energy')
    total_energy = heat_pump_energy.sum(axis=1)/3600  # J to Wh

    # TMP: energy correction to meet the real power consumption
    total_energy *= (2 / 3)

    return total_energy.tolist()[start_index:end_index]


def thermal_core(input_data, output_data, stop_event):
    # Data for thermal module includes:
    # - all general settings
    # - 'weatherData' -> in 'commonData'
    # - module settings = 'thermal'
    # - 'id', 'location', 'position', 'setpoints', 'idf' for buildings

    # The thermal module returns:
    # - a list of dictionaries {'id':, 'data': {'pump_load':}} (for each building) with key 'buildings'

    try:

        logger.debug('Writing EPW file...')
        write_epw(input_data)

        input_general_settings = {k: input_data[k] for k in
                                  ['timestamp', 'horizon', 'resolution', 'position', 'location']}
        tmp_output_data = {'thermal': []}

        for thermal in input_data.get('thermal'):

            logger.info('Thermal model for ' + thermal['id'])
            thermal.update(input_general_settings)

            # Create IDF copy
            logger.debug('Copying IDF file...')
            # shutil.copy(EP_IDF_FILE, EP_IDF_FILE_TMP)
            shutil.copy(os.path.join(EP_DIR, thermal.get('specs').get('idf')), EP_IDF_FILE_TMP)

            setIDF(thermal)

            #################################################################################
            # insert here computation block
            # check the stop_event in the computation,
            # or run sub-thread or process in order to kill execution
            # here follows an example with 'while'

            energyplus_call = subprocess.Popen(EP_CALL, shell=True, stdout=subprocess.DEVNULL)

            # energyplus_call.terminate()
            # energyplus_call.wait()

            logger.debug('Start thermal computation.')

            while energyplus_call.poll() is None:

                logger.debug('Thermal thread, computing...')

                if stop_event.is_set():
                    energyplus_call.terminate()  # SIGNTERM
                    # energyplus_call.kill()  # SIGKILL
                    if energyplus_call.poll() is None:
                        logger.debug('Energyplus correctly killed')
                    else:
                        logger.warning('Energyplus still running!')
                    break
                time.sleep(1)

            # check exist status if different from 0
            if not stop_event.is_set():
                if energyplus_call.poll():
                    logger.warning('Energyplus execution failed!')
                    # send dictionary with empty values
                    # output_data.put(output_data_skeleton)

                else:  # normal execution, create data for core
                    # output_data.put({'pump_load': readEPoutput_ideal(input_data)})
                    # output_data.put({'pump_load': readEPoutput(input_data)})
                    tmp_output_data['thermal'].append(
                        {'id': thermal.get('id'), 'data': {'loads': readEPoutput(input_data)}})

            logger.info('Removing IDf and EPW files...')
            os.remove(EP_EPW_FILE)
            os.remove(EP_IDF_FILE_TMP)

        output_data.put(tmp_output_data)

    except Exception as err:
        logger.critical("Unhandled exception!")
        logger.critical(err)
        sys.exit(-1)


if __name__ == '__main__':
    """ Testing your code! """

    from defines import *
    import calendar
    import multiprocessing
    import queue

    setup_logging('test_thermal')

    # set horizon, resolution and timestamp for test
    timestamp = calendar.timegm(time.gmtime())

    input_data_skeleton = \
        {'timeout': 300, 'timestamp': 1495100721, 'position': ['43.585785', '13.513044'],
         'db': {'name': 'stored_data.mjson', 'type': 'file'},
         'commonData': {'weatherData': {
             'pressure': [1017.0, 1016.875, 1016.75, 1016.625, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.5, 1016.25, 1016.0, 1015.75, 1015.5, 1015.5, 1015.5, 1015.5],
             'gc_radiation': [541, 547, 548, 546, 538, 522, 502, 478, 452, 422, 389, 353, 315, 275, 32, 29, 23, 19, 13, 0, 0, 0, 0, 0],
             'outdoor_humidity': [58.5, 57.75, 57.0, 56.25, 55.5, 55.125, 54.75, 54.375, 54.0, 53.75, 53.5, 53.25, 53.0, 53.125, 53.25, 53.375, 53.5, 53.5, 53.5, 53.5, 53.5, 53.5, 53.5, 53.5],
             'wind_dir': [108.25, 109.125, 110.0, 110.875, 111.75, 112.5, 113.25, 114.0, 114.75, 117.8125, 120.875, 123.9375, 127.0, 127.25, 127.5, 127.75, 128.0, 128.25, 128.5, 128.75, 129.0, 129.125, 129.25, 129.375],
             'cloud': [0.7312051677593524, 0.73908909924122834, 0.74657355202460274, 0.75368813825608805, 0.75830399606886478, 0.75681614823686905, 0.75531741121148366, 0.75380766501064578, 0.75228678788310488, 0.75126662501115749, 0.750241423125794, 0.74921114479987083, 0.74817575223467192, 0.74752601786011807, 0.74687426143590396, 0.746220473508034, 0.74556464456348615, 0.74556464456348615, 0.74556464456348615, 0.74556464456348615, 0.74556464456348615, 0.74635139406967199, 0.74713520725528737, 0.74791610052819579],
             'wind_vel': [3.75, 3.8899999999999997, 4.03, 4.17, 4.31, 4.41375, 4.5175, 4.62125, 4.725, 4.76, 4.795, 4.83, 4.865, 4.96875, 5.0725, 5.17625, 5.279999999999999, 5.3149999999999995, 5.35, 5.385, 5.42, 5.45375, 5.4875, 5.52125],
             'dni_radiation': [584, 590, 595, 597, 596, 588, 578, 566, 552, 536, 516, 493, 466, 435, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
             'outdoor_temperature': [20.75, 20.95, 21.15, 21.35, 21.549999999999997, 21.7, 21.85, 22.0, 22.15, 22.25, 22.35, 22.450000000000003, 22.55, 22.6125, 22.674999999999997, 22.737499999999997, 22.799999999999997, 22.799999999999997, 22.799999999999997, 22.8, 22.8, 22.725, 22.650000000000002, 22.575],
             'gc_radiation_pv': [541, 547, 548, 546, 538, 522, 502, 478, 452, 422, 389, 353, 315, 275, 32, 29, 23, 19, 13, 0, 0, 0, 0, 0],
             'gd_radiation': [97, 99, 100, 100, 99, 97, 95, 92, 89, 86, 81, 77, 71, 65, 47, 41, 34, 27, 19, 0, 0, 0, 0, 0]}},
         'resolution': 15,
         'thermal': [{'name': 'HVAC 1', 'description': 'Impianto riscaldamento/raffrescamento Home 1',
                      'connectedTo': ['node-1'], 'id': 'ep_load-1', 'tag': 'load',
                      'specs': {'idf': 'shell_fancoil.idf', 'max': 6000,
                                'setpoints': {'clg': [[27], [27], [27], [27], [27]],
                                              'htg': [[21], [20], [18], [20], [17]]},
                                'thermal_zones': []}}],
         'horizon': 360,
         'location': 'Ancona'}

    htg_setpoint1 = [21]
    htg_setpoint2 = [20]
    htg_setpoint3 = [18]
    htg_setpoint4 = [20]
    htg_setpoint5 = [17]

    clg_setpoint1 = [26]
    clg_setpoint2 = [26]
    clg_setpoint3 = [26]
    clg_setpoint4 = [26]
    clg_setpoint5 = [26]

    for thermal in input_data_skeleton.get('thermal'):
        spec = thermal.get('specs')
        if not spec:
            spec.update({'thermal_zones': ['Edificio:Bagno', 'Edificio:Soggiorno', 'Edificio:Camera1', 'Edificio:Camera2',
                                           'Edificio:Cucina']})
            spec.update({'setpoints': {'htg': [htg_setpoint1, htg_setpoint2, htg_setpoint3, htg_setpoint4, htg_setpoint5],
                                       'clg': [clg_setpoint1, clg_setpoint2, clg_setpoint3, clg_setpoint4, clg_setpoint5]}})

    input_q = input_data_skeleton
    output_q = queue.Queue()
    stop_event = multiprocessing.Event()

    # setIDF(input_data_skeleton)

    thermal_core(input_q, output_q, stop_event)
    # total_consumption = output_q.get().get('loads')

    total_consumption = readEPoutput(input_data_skeleton)

    print(total_consumption)

    import matplotlib.pyplot as plt
    # plt.su(total_consumption_no_cop)
    fig, ax = plt.subplots()
    # ax.plot(total_consumption_no_cop, color='k')
    ax.plot(total_consumption, color='g')
    plt.show()
