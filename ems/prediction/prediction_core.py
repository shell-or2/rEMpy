# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import logging
logger = logging.getLogger(__name__)


def prediction_core(input_data, output_data, stop_event):
    # input_data structure:
    # - all general settings: 'timestamp', 'resolution', 'horizon'
    # - module settings = prediction -> 'weather_module', 'pv_module', 'load_module'
    # - 'id', 'location', 'position' for buildings and pvs (specs for pvs?)

    # The prediction module returns:
    # - weatherData: {'output_temp' etc}
    # - a list of dictionaries {'id':, 'data': {'load':}} (for each building) with key 'buildings'
    # - a list of dictionaries {'id':, 'data': {'pv_prod':}} (for each pv) with key 'photovoltaics'

    # TODO: inserire callback load e weather
    # TODO: return moduli con codice errore 0,1,2, 0-ok, altro errore da definire
    # scan the input data and use the information properly

    try:

        logger.info('Input data: ')
        for k, v in input_data.items():
            logger.debug(str(k) + ': ' + str(v))

        #################################################################################
        # prediction block must have:
        # - computation block
        # - check the stop_event in the computation, or run sub-thread or process in order to kill execution
        #
        # here follows an example with 'while'
        #
        # logger.debug('Prediction computation!')
        # counter = 0
        # while not stop_event.is_set():
        #     time.sleep(1)
        #     counter += 1
        #
        #     if counter == 10:
        #         # pass output prediction to main process
        #         output_data.put(output_data_skeleton)
        #         break
        #################################################################################

        prediction_data = input_data  # reused to keep the structure
        # prediction_data = {k: input_data[k] for k in ['timestamp', 'horizon', 'resolution']}

        input_general_settings = {k: input_data[k] for k in ['timestamp', 'horizon', 'resolution', 'position', 'location']}

        #################################################################################
        # Weather Prediction Modules

        partner_weather_prediction = __import__('prediction.weather_prediction.' + input_data['weather_module'] +
                                                '.weather_main', globals(), locals(), 'prediction', 0)

        # pass location, position information of buildings
        weather_prediction_data = input_general_settings

        if partner_weather_prediction.prediction(weather_prediction_data):
            logger.error('Problem with weather predictor.')

        prediction_data.update({'commonData': weather_prediction_data.get('commonData')})

        #################################################################################
        # Load Prediction Modules

        partner_load_prediction = __import__('prediction.load_prediction.' + input_data['load_module'] + '.load_main',
                                             globals(), locals(), 'prediction', 0)

        for load_data in prediction_data['load_prediction']:
            load_prediction_data = input_general_settings
            load_prediction_data.update(load_data)

            if partner_load_prediction.prediction(load_prediction_data):
                logger.error('Problem with load predictor.')

            load_data.update({'data': {'loads': load_prediction_data.get('loads')}})

        #################################################################################
        # PV Prediction Modules

        partner_pv_prediction = __import__('prediction.pv_prediction.' + input_data['pv_module'] + '.pv_main', globals(),
                                           locals(), 'prediction', 0)

        for production_data in prediction_data['pv_prediction']:

            if production_data.get('id').split('-')[0].upper() == 'PV':  # is PV production
                pv_prediction_data = input_general_settings
                pv_prediction_data.update(prediction_data.get('commonData').get('weatherData'))
                pv_prediction_data.update(production_data)

                if partner_pv_prediction.prediction(pv_prediction_data):
                    logger.error('Problem with pv predictor.')

                production_data.update({'data': {'production': pv_prediction_data.get('production')}})

        output_data.put(prediction_data)

    except Exception as err:
        logger.critical("Unhandled exception!")
        logger.critical(err)


if __name__ == "__main__":
    """ Testing your code! """

    import queue
    import multiprocessing

    setup_logging('test_prediction.txt')

    output_prediction_data = queue.Queue()
    stop_event = multiprocessing.Event()

    input_prediction = {'horizon': 360, 'pv_module': 'default', 'position': ['43.585785', '13.513044'],
                        'weather_module': 'default', 'timeout': 300,
                        'pv_prediction': [{'name': 'PV 1', 'id': 'pv-1', 'connectedTo': ['node-1'],
                                           'specs': {'size': 600, 'max': 6000}, 'description': 'PV tetto Home 1'}],
                        'location': 'Ancona',
                        'load_prediction': [{'name': 'Home 1', 'id': 'load-1', 'connectedTo': ['node-1'],
                                                         'specs': {'max': 6000},
                                                         'description': 'Carico elettrico laboratorio dimostrativo Home 1'}],
                        'db': {'name': 'stored_data.mjson', 'type': 'file'}, 'timestamp': 1494688423, 'resolution': 15,
                        'load_module': 'default'}

    prediction_core(input_prediction, output_prediction_data, stop_event)

    while True:
        if not output_prediction_data.empty():
            output_data = output_prediction_data.get()
            print(output_data)
            logger.debug("Received prediction data.")
            break
