# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import time
import urllib.request
import numpy as np

#start_timestamp = input_data.get('timestamp')
#last_timestamp = start_timestamp + (input_data.get('horizon') * 60)
#print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_timestamp)))


def ilmeteoF(horizon, resolution, location):

    Ora_ilmeteo=[]
    Meteo_ilmeteo=[]
    Temperatura_ilmeteo=[]
    Hum_ilmeteo=[]
    Press_ilmeteo=[]
    Wind_ilmeteo=[]
    WindDir_ilmeteo=[]

    Today=int(time.strftime('%d', time.localtime()))

    # sock = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm="+str(Today)+"&g=0&lang=ita")
    sock = urllib.request.urlopen(
        "http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=" + location + "&c=223&gm=" + str(Today) + "&g=0&lang=ita")
    htmlSource = sock.read()                            
    sock.close()  
    dati=htmlSource    
    while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
        datiN=len(dati)
        #ORA
        punt1=dati.find(b'<span class="ora">')
        datiN=len(dati)
        dati=dati[punt1:datiN]    
        appo=dati.find(b'</span>')
        ora=dati[18:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Meteo
        punt2=dati.find(b'td class="col3">')
        datiN=len(dati)
        dati=dati[punt2:datiN]    
        punt3=dati.find(b'<')
        meteo=dati[16:punt3]
        datiN=len(dati)
        dati=dati[punt3:datiN]
        
        #Temperatura
        punt4=dati.find(b'<td class="col4"><span class="boldval">')
        datiN=len(dati)
        dati=dati[punt4:datiN]    
        appo=dati.find(b'&deg;')
        temperatura=dati[39:appo].replace(b'<span style="color:blue">', b'')
        datiN=len(dati)
        dati=dati[appo:datiN]
        
        #WindDir
        punt5=dati.find(b'style="transform:rotate(')
        appo=dati.find(b')')
        winddir=dati[punt5+24:appo-3]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Wind
        punt6=dati.find(b'class="boldval">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'</span>')
        wind=dati[16:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]    

        #Hum
        punt7=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[14:appo-1]
        datiN=len(dati)
        appo=dati.find(b'class="col8">')    
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[14:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_ilmeteo.append(ora)
        Meteo_ilmeteo.append(meteo)
        Temperatura_ilmeteo.append(temperatura)
        Hum_ilmeteo.append(hum)
        Press_ilmeteo.append(press)
        Wind_ilmeteo.append(wind)
        WindDir_ilmeteo.append(winddir)


    sock1 = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&g=1&lang=ita",None,10)
    #sock1 = urllib.request.urlopen("http://www.ilmeteo.it/meteo/Ancona/previsioni-orarie?g=1")    
    htmlSource = sock1.read()                            
    sock1.close()  
    dati=htmlSource
    while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
        datiN=len(dati)
        #ORA
        punt1=dati.find(b'<span class="ora">')
        datiN=len(dati)
        dati=dati[punt1:datiN]    
        appo=dati.find(b'</span>')
        ora=dati[18:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Meteo
        punt2=dati.find(b'td class="col3">')
        datiN=len(dati)
        dati=dati[punt2:datiN]    
        punt3=dati.find(b'<')
        meteo=dati[16:punt3]
        datiN=len(dati)
        dati=dati[punt3:datiN]
        
        #Temperatura
        punt4=dati.find(b'<td class="col4"><span class="boldval">')
        datiN=len(dati)
        dati=dati[punt4:datiN]    
        appo=dati.find(b'&deg;')
        temperatura=dati[39:appo].replace(b'<span style="color:blue">', b'')
        datiN=len(dati)
        dati=dati[appo:datiN]
        
        #WindDir
        punt5=dati.find(b'style="transform:rotate(')
        appo=dati.find(b')')
        winddir=dati[punt5+24:appo-3]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Wind
        punt6=dati.find(b'class="boldval">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'</span>')
        wind=dati[16:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]    

        #Hum
        punt7=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[14:appo-1]
        datiN=len(dati)
        appo=dati.find(b'class="col8">')    
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[14:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_ilmeteo.append(ora)
        Meteo_ilmeteo.append(meteo)
        Temperatura_ilmeteo.append(temperatura)
        Hum_ilmeteo.append(hum)
        Press_ilmeteo.append(press)
        Wind_ilmeteo.append(wind)
        WindDir_ilmeteo.append(winddir)


    # sock1 = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&g=2&lang=ita",None,10)
    sock1 = urllib.request.urlopen(
        "http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=" + location + "&c=223&g=2&lang=ita", None, 10)
    #sock1 = urllib.request.urlopen("http://www.ilmeteo.it/meteo/Ancona/previsioni-orarie?g=1")    
    htmlSource = sock1.read()                            
    sock1.close()  
    dati=htmlSource
    while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
        datiN=len(dati)
        #ORA
        punt1=dati.find(b'<span class="ora">')
        datiN=len(dati)
        dati=dati[punt1:datiN]    
        appo=dati.find(b'</span>')
        ora=dati[18:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Meteo
        punt2=dati.find(b'td class="col3">')
        datiN=len(dati)
        dati=dati[punt2:datiN]    
        punt3=dati.find(b'<')
        meteo=dati[16:punt3]
        datiN=len(dati)
        dati=dati[punt3:datiN]
        
        #Temperatura
        punt4=dati.find(b'<td class="col4"><span class="boldval">')
        datiN=len(dati)
        dati=dati[punt4:datiN]    
        appo=dati.find(b'&deg;')
        temperatura=dati[39:appo].replace(b'<span style="color:blue">', b'')
        datiN=len(dati)
        dati=dati[appo:datiN]
        
        #WindDir
        punt5=dati.find(b'style="transform:rotate(')
        appo=dati.find(b')')
        winddir=dati[punt5+24:appo-3]
        datiN=len(dati)
        dati=dati[appo:datiN]

        #Wind
        punt6=dati.find(b'class="boldval">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'</span>')
        wind=dati[16:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]    

        #Hum
        punt7=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[14:appo-1]
        datiN=len(dati)
        appo=dati.find(b'class="col8">')    
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'class="hdata">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[14:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_ilmeteo.append(ora)
        Meteo_ilmeteo.append(meteo)
        Temperatura_ilmeteo.append(temperatura)
        Hum_ilmeteo.append(hum)
        Press_ilmeteo.append(press)
        Wind_ilmeteo.append(wind)
        WindDir_ilmeteo.append(winddir)

    #print(Meteo_ilmeteo)
    for n in range(len(Meteo_ilmeteo)):
        if Meteo_ilmeteo[n] == b'sereno':
            Meteo_ilmeteo[n] = 5
        elif Meteo_ilmeteo[n] == b'quasi sereno' or Meteo_ilmeteo[n] == b'poco&nbsp;nuvoloso' or Meteo_ilmeteo[n] == b'velature sparse':
            Meteo_ilmeteo[n] = 4
        elif Meteo_ilmeteo[n] == b'parz. nuvoloso' or Meteo_ilmeteo[n] == b'variabile' or Meteo_ilmeteo[n] == b'velature diffuse' or Meteo_ilmeteo[n] == b'nubi&nbsp;sparse':
            Meteo_ilmeteo[n] = 3
        elif Meteo_ilmeteo[n] == b'nuvoloso'or Meteo_ilmeteo[n] ==b'coperto' or Meteo_ilmeteo[n] == b'possibile temporale':
            Meteo_ilmeteo[n] = 1
        elif Meteo_ilmeteo[n] == b'temporale e schiarite' or Meteo_ilmeteo[n] == b'piovaschi e schiarite' or Meteo_ilmeteo[n] == b'rovesci e schiarite' or Meteo_ilmeteo[n] == b'pioviggine':
            Meteo_ilmeteo[n] = 2
        elif Meteo_ilmeteo[n] == b'pioggia' or Meteo_ilmeteo[n] == b'pioggia debole' or Meteo_ilmeteo[n] == b'pioggia moderata':
            Meteo_ilmeteo[n]=0
        else:
            Meteo_ilmeteo[n] = 1


    for n in range(len(Ora_ilmeteo)):
        Ora_ilmeteo[n]=float(bytes.decode(Ora_ilmeteo[n]))
    for n in range(len(Wind_ilmeteo)):
        Wind_ilmeteo[n]=round(float(bytes.decode(Wind_ilmeteo[n])) / float(3.6),2)
    for n in range(len(WindDir_ilmeteo)):
        WindDir_ilmeteo[n]=float(bytes.decode(WindDir_ilmeteo[n]))
    for n in range(len(Hum_ilmeteo)):
        Hum_ilmeteo[n]=int(bytes.decode(Hum_ilmeteo[n]))
    for n in range(len(Press_ilmeteo)):
        Press_ilmeteo[n]=int(bytes.decode(Press_ilmeteo[n]))   
    for n in range(len(Temperatura_ilmeteo)):
        Temperatura_ilmeteo[n]=float(bytes.decode(Temperatura_ilmeteo[n]))

    #print(Ora_ilmeteo)
    #print(Meteo_ilmeteo)
    #print(Temperatura_ilmeteo)
    #print(Hum_ilmeteo)
    #print(Press_ilmeteo)
    #print(Wind_ilmeteo)
    #print(WindDir_ilmeteo)

    #print(len(Ora_ilmeteo))
    #print(len(Wind_ilmeteo))
    #print(len(Meteo_ilmeteo))
    #print(len(WindDir_ilmeteo))

    fine = round(horizon/60)

    res = round(60/resolution)

    fine=fine+1

    Ora1=[]

    Ora2 = Ora_ilmeteo[0:fine]

    for j in range(len(Ora2)):
        Ora2[j]=Ora2[0]+j

    for n in range(1,fine*res-2):
        newOra = Ora2[0] + (n -1) / res
    #    if newOra > 23.75:
    #        newOra = newOra - 24
        
        Ora1.append(newOra)

    M3=[]
    M3 = np.interp(Ora1, Ora2, Meteo_ilmeteo[0:fine])
    M4 = np.interp(Ora1, Ora2, Temperatura_ilmeteo[0:fine])
    M5 = np.interp(Ora1, Ora2, Hum_ilmeteo[0:fine])
    M6 = np.interp(Ora1, Ora2, Wind_ilmeteo[0:fine])
    M7 = np.interp(Ora1, Ora2, WindDir_ilmeteo[0:fine])
    M8 = np.interp(Ora1, Ora2, Press_ilmeteo[0:fine])


    #print(M3)
    #print(Ora1)
    #print(Ora2)
    #print(M4)
    #print(M5)
    return (M3,M4,M5,M6,M7,M8)

#prevFuzzy=[]
#Meteo=[0,0,0,0,0,0]

#for n in range(len(Temperatura)):
#    Meteo=[0,0,0,0,0,0]
#    Meteo[Meteo_ilmeteo[n]]=1
#    prevFuzzy.append(fuzzy(Meteo, Temperatura[n]))


#for n in range(len(prevFuzzy)):
#    prevFuzzy[n]=round(float(prevFuzzy[n]),2)

#print(prevFuzzy)




