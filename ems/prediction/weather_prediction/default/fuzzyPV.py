# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

#La funzione fuzzy prende come parametri:
#  - L: vettore di sei elementi della previsione meteo su una scala 0-5(coperto-sereno); ogni elemento è un valore in percentuale in base a quanti siti meteo
#       hanno dato quel valore (per esempio se 2 siti su 4 danno sereno(5) allora L[5]=50%);
#  - t: valore della temperatura.

import numpy as np
import skfuzzy as fuzz


def fuzzyPV(M3, M3_i, t):

    tip=[]

    for i in range(len(t)):
        L=[0,0,0,0,0,0]

        if M3[i]==0:
            L[0]=0.5
        elif M3[i]==1:
            L[1]=0.5
        elif M3[i]==2:
            L[2]=0.5
        elif M3[i]==3:
            L[3]=0.5
        elif M3[i]==4:
            L[4]=0.5
        elif M3[i]==5:
            L[5]=0.5
        else:
            L[5]=0.5

        if M3_i[i]==0:
            L[0]=L[0]+0.5
        elif M3_i[i]==1:
            L[1]=L[1]+0.5
        elif M3_i[i]==2:
            L[2]=L[2]+0.5
        elif M3_i[i]==3:
            L[3]=L[3]+0.5
        elif M3_i[i]==4:
            L[4]=L[4]+0.5
        elif M3_i[i]==5:
            L[5]=L[5]+0.5
        else:
            L[5]=L[5]+0.5
    
        #assi x
        weight=np.arange(0,1.2,0.01)
        temp=np.arange(0,50,1)

        #input function
        low=fuzz.trapmf(temp,[0,0,18,25])
        medium=fuzz.trapmf(temp,[18,25,28,32])
        high=fuzz.trapmf(temp,[28,32,50,50])


        #output function
        b=fuzz.trapmf(weight,[0,0,0.3,0.4])
        m_b=fuzz.trapmf(weight,[0.3,0.4,0.5,0.6])
        m=fuzz.trapmf(weight,[0.45,0.6,0.7,0.8])
        m_a=fuzz.trapmf(weight,[0.65,0.75,0.9,0.95])
        a=fuzz.trapmf(weight,[0.9,1,1.1,1.15])
        molto_a=fuzz.trapmf(weight,[1.1,1.2,1.35,1.35])

        #fuzzy input membership
        temp_1=fuzz.interp_membership(temp,low, t[i])
        temp_2=fuzz.interp_membership(temp,medium,t[i])
        temp_3=fuzz.interp_membership(temp,high,t[i])
        

        #weight of rules
        rule1_1=np.fmin(L[5],temp_1)
        rule1_2=np.fmin(L[5],temp_2)
        rule1_3=np.fmin(L[5],temp_3)
        rule2_1=np.fmin(L[4],temp_1)
        rule2_2=np.fmin(L[4],temp_2)
        rule2_3=np.fmin(L[4],temp_3)
        rule3_1=np.fmin(L[3],temp_1)
        rule3_2=np.fmin(L[3],temp_2)
        rule3_3=np.fmin(L[3],temp_3)
        rule4=L[2]
        rule5=L[1]
        rule6=L[0]
        

        #apply implication
        imp1_1=rule1_1*molto_a
        imp1_2=rule1_2*a
        imp1_3=rule1_3*a
        imp2_1=rule2_1*a
        imp2_2=rule2_2*m_a
        imp2_3=rule2_3*m_a
        imp3_1=rule3_1*m_a
        imp3_2=rule3_2*m
        imp3_3=rule3_3*m
        imp4=rule4*m
        imp5=rule5*m_b
        imp6=rule6*b

        #aggregate
        agg1=np.fmax(imp1_1,np.fmax(imp1_2,imp1_3))
        agg2=np.fmax(imp2_1,np.fmax(imp2_2,imp2_3))
        agg3=np.fmax(imp3_1,np.fmax(imp3_2,imp3_3))
        agg4=np.fmax(imp4,np.fmax(imp5,imp6))
        aggregate=np.fmax(agg1,np.fmax(agg2,np.fmax(agg3,agg4)))

        #defuzzyfy
        tip1=fuzz.defuzz(weight,aggregate,'centroid')

        tip.append(tip1)
    
    return tip
