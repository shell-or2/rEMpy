# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import csv
import time
from defines import *


def read_file_rad(timestamp, hor, res, meteo):

    csv.register_dialect('pipes', delimiter='\t')

    appo=[]

    with open(os.path.join(EMS_STORE_DATA, 'dailyrad433438N_133052E_0deg_0deg.txt'), 'r', encoding='iso-8859-1') as f:
        reader = csv.reader(f, dialect='pipes')
        for row in reader:
            # print(row)
            appo.append(row)
            
    ap=1

    Gc=[]
    Gd=[]
    DNIc=[]
    Ora=[]

    i = 9

    while ap == 1:

        if appo[i+1] == ['G: Irradianza globale su un piano fisso  (W/m2)']:
            ap = 0

        Gd.append(int(appo[i-1][4]))
        Gc.append(int(appo[i-1][6]))
        DNIc.append(int(appo[i-1][10]))
        ap0=appo[i-1][0]
        ap1=int(ap0[0:2])
        ap2=int(ap0[3:5])
        ap3=round(ap2*4/60)/4
        Ora.append(ap1+ap3)
        i=i+1    

    # adde = time.strftime("%H:%M")
    adde = time.strftime('%H:%M', time.localtime(timestamp))
    ap1 = int(adde[0:2])
    ap2 = int(adde[3:5])
    ap3 = round(ap2*4/60)/4
    adesso = ap1+ap3

    k=0
    check=0

    lung = len(Ora)

    for k in range(len(Ora)):
        if Ora[k] == adesso:
            check = k
    M9 = []
    M10=[]
    M11=[]
    j = 0
    ind = 0
    if check == 0:
        if (adesso > Ora[lung-1]):    
            ind = (24 - adesso) * 4 + Ora[0] * 4
        if (adesso < Ora[0]):
            ind = (Ora[0] - adesso) * 4

    if (adesso > Ora[lung-1]) or (adesso < Ora[0]):
        while j <= int(hor/res):
            while j <= ind:
                M9.append(0)
                M10.append(0)
                M11.append(0)
                j=j+1

            if check <= len(Gc)-1:    
                M9.append(Gc[check])
                M10.append(Gd[check])
                M11.append(DNIc[check])
                check=check+1
                j=j+1
            else:
                M9.append(0)
                M10.append(0)
                M11.append(0)
                j=j+1
    else:
        count = check
        ind = (24 - Ora[lung-1]) * 4 + Ora[0] * 4
        while j <= int(hor/res):
            if check <= len(Gc)-1:    
                M9.append(Gc[check])
                M10.append(Gd[check])
                M11.append(DNIc[check])
                check=check+1
                j=j+1
            else:
                kk = 0
                while j <= check - count + ind:
                    M9.append(0)
                    M10.append(0)
                    M11.append(0)
                    j=j+1

                while kk < count:
                    M9.append(Gc[kk])
                    M10.append(Gd[kk])
                    M11.append(DNIc[kk])
                    kk= kk+1
                    j=j+1

    for ii in range(len(meteo)-2):
        M9[ii]=M9[ii] * (meteo[ii] + 1)/6
        M10[ii]=M10[ii] * (meteo[ii] + 1)/6
        M11[ii]=M11[ii] * (meteo[ii] + 1)/6

    return M9, M10, M11

