# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from prediction.weather_prediction.default.trebmeteoF import trebmeteoF
from prediction.weather_prediction.default.ilmeteoF import ilmeteoF
from prediction.weather_prediction.default.fuzzyPV import fuzzyPV
from prediction.weather_prediction.default.prevIRR import prevIRR
import numpy as np
import logging
logger = logging.getLogger(__name__)


def prediction(prediction_data):

    error_code = 0

    forced_horizon = 24 * 60

    _timestamp = prediction_data.get('timestamp')
    _horizon = prediction_data.get('horizon')
    _resolution = prediction_data.get('resolution')
    _location = prediction_data.get('location')
    _n_slots = _horizon // _resolution

    logger.debug('Predicting weather data...')
    # return: Meteo, Temperatura, Hum, Wind, WindDir, Press, Sorge, Tramonta
    #meteo, temperatura, umidità, vento, direzione_vento, pressione, sorge, tramonta = trebmeteoF(24*60, _resolution)  # horizon forced to 24 hours (in minutes)
    M3, M4, M5, M6, M7, M8, s, t, Ora = trebmeteoF(forced_horizon, _resolution, _location)  # horizon forced to 24 hours (in minutes)
    M3_i, M4_i, M5_i, M6_i, M7_i, M8_i = ilmeteoF(forced_horizon, _resolution, _location)

    TEM = np.array([M4, M4_i])
    temperatura = np.mean(TEM, axis=0)  # M4

    HUM = np.array([M5, M5_i])
    umidità = np.mean(HUM, axis=0)  # M5

    W = np.array([M6, M6_i])
    velocità_vento = np.mean(W, axis=0)  # M6

    WD = np.array([M7, M7_i])
    direzione_vento = np.mean(WD, axis=0)  # M7

    PR = np.array([M8, M8_i])
    pressione = np.mean(PR, axis=0)  # M8

    meteo = fuzzyPV(M3, M3_i, temperatura)

    logger.debug('Predicting weather data...OK!')

    logger.debug('Predicting radiation data...')
    # return: Irradianza globale su un piano fisso (Gc), Irradianza diffusa su un piano fisso (Gd), Irradianza normale diretta (DNI-Direct Normal Irradiance)
    #igpf, idpf, ind = read_file_rad(_timestamp, 24*60, _resolution, meteo)  # horizon forced to 24 hours (in minutes)
    # M9, M10, M11, PV = prevIRR(meteo, size, Ora)
    gc, gd, dnic = prevIRR(meteo, Ora, 'dailyrad433656N_133108E_25deg_0deg(1).txt')
    gc_pv, _, _ = prevIRR(meteo, Ora, 'dailyrad433656N_133108E_25deg_0deg(1).txt')  # tiene conto dell'inclinazioone del pannello
    logger.debug('Predicting radiation data...OK!')

    prediction_data.update({'commonData': {'weatherData': {
        'outdoor_temperature': temperatura.tolist()[0:_n_slots],
        'outdoor_humidity': umidità.tolist()[0:_n_slots],
        'wind_vel': velocità_vento.tolist()[0:_n_slots],
        'wind_dir': direzione_vento.tolist()[0:_n_slots],
        'cloud': meteo[0:_n_slots],
        'pressure': pressione.tolist()[0:_n_slots],
        'gc_radiation': gc[0:_n_slots],
        'gd_radiation': gd[0:_n_slots],
        'dni_radiation': dnic[0:_n_slots],
        'gc_radiation_pv': gc_pv[0:_n_slots],
    }}})

    return error_code

if __name__ == '__main__':

    prediction_data = {'timestamp': 1483452646, 'horizon': 360, 'resolution': 15}

    prediction(prediction_data)

    print(prediction_data)
