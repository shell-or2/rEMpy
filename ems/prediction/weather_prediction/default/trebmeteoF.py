# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import urllib.request
import numpy as np
from prediction.weather_prediction.default.wind2deg import wind2deg


def trebmeteoF(horizon, resolution, location):

    #horizon = 1440
    #resolution = 15

    Ora_3bmeteo=[]
    Meteo_3bmeteo=[]
    Temperatura_3bmeteo=[]
    Hum_3bmeteo=[]
    Press_3bmeteo=[]
    Wind_3bmeteo=[]
    WindDir_3bmeteo=[]

    # sock = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona", None, 30)
    sock = urllib.request.urlopen("http://www.3bmeteo.com/meteo/" + location.lower(), None, 30)
    htmlSource = sock.read()                            
    sock.close()  
    dati=htmlSource    
    while dati.find(b'<div class="col-xs-1-4 big ') != -1:           #lettura 3bmeteo
        datiN=len(dati)
        #Ora
        punt1=dati.find(b'<div class="col-xs-1-4 big ')
        dati=dati[punt1+28:datiN]
        appo=dati.find(b' ')
        ora=dati[appo+1:appo+3]
        datiN=len(dati)
        dati=dati[appo+5:datiN]
        
        #Meteo
        datiN=len(dati)
        start=15
        punt2=dati.find(b'"col-xs-2-4 ">')
        punt2bis=dati.find(b'"col-xs-2-4 zoom_prv">')
        if punt2bis != -1:
            if punt2 > punt2bis:
                punt2=punt2bis
                start=23
        dati=dati[punt2:datiN]
        punt3=dati.find(b'<')
        meteo=dati[start:punt3-2]
        dati=dati[punt3+3:datiN]
        datiN=len(dati)
        
        #Temperatura
        punt4=dati.find(b'"switchcelsius switch-te active">')
        appo=dati.find(b'&deg')
        temperatura=dati[punt4+33:appo]
        dati=dati[appo:datiN]

        #Wind
        punt6=dati.find(b'"switchkm switch-vi active">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'<')
        wind=dati[28:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]   

        #WindDir
        punt5=dati.find(b'"switchnodi switch-vi">')
        datiN=len(dati)
        dati=dati[punt5:datiN]    
        appo=dati.find(b'&nbsp;')
        datiN=len(dati)
        dati=dati[appo:datiN]
        appo2=dati.find(b'<')
        winddir=dati[6:appo2-2]
        datiN=len(dati)
        dati=dati[appo2:datiN] 

        #Hum
        punt7=dati.find(b'altriDati-umidita">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[19:appo-3]
        datiN=len(dati) 
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'altriDati-pressione">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[21:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_3bmeteo.append(ora)
        Meteo_3bmeteo.append(meteo)
        Temperatura_3bmeteo.append(temperatura)
        Hum_3bmeteo.append(hum)
        Press_3bmeteo.append(press)
        Wind_3bmeteo.append(wind)
        WindDir_3bmeteo.append(winddir)

    punta=dati.find(b'Sorge: ')
    sorge=dati[punta+7:punta+12]
    punta=dati.find(b'Tramonta: ')
    tramonta=dati[punta+10:punta+15]
    #print(sorge)
    #print(tramonta)

    # sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona/dettagli_orari/1", None, 30)
    sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/" + location.lower() + "/dettagli_orari/1", None, 30)
    htmlSource = sock1.read()                            
    sock1.close()  
    dati=htmlSource

    while dati.find(b'<div class="col-xs-1-4 big ') != -1:           #lettura 3bmeteo
        datiN=len(dati)
        #Ora
        punt1=dati.find(b'<div class="col-xs-1-4 big ')
        dati=dati[punt1+28:datiN]
        appo=dati.find(b' ')
        ora=dati[appo+1:appo+3]
        datiN=len(dati)
        dati=dati[appo+5:datiN]
        datiN=len(dati)
        punt2=dati.find(b'"col-xs-2-4 ">')
        dati=dati[punt2:datiN]
        punt3=dati.find(b'<')
        meteo=dati[15:punt3-2]
        dati=dati[punt3+3:datiN]
        datiN=len(dati)
        punt4=dati.find(b'"switchcelsius switch-te active">')
        appo=dati.find(b'&deg')
        temperatura=dati[punt4+33:appo]
        #Wind
        punt6=dati.find(b'"switchkm switch-vi active">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'<')
        wind=dati[28:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]   

        #WindDir
        punt5=dati.find(b'"switchnodi switch-vi">')
        datiN=len(dati)
        dati=dati[punt5:datiN]    
        appo=dati.find(b'&nbsp;')
        datiN=len(dati)
        dati=dati[appo:datiN]
        appo2=dati.find(b'<')
        winddir=dati[6:appo2-2]
        datiN=len(dati)
        dati=dati[appo2:datiN] 

        #Hum
        punt7=dati.find(b'altriDati-umidita">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[19:appo-3]
        datiN=len(dati) 
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'altriDati-pressione">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[21:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_3bmeteo.append(ora)
        Meteo_3bmeteo.append(meteo)
        Temperatura_3bmeteo.append(temperatura)
        Hum_3bmeteo.append(hum)
        Press_3bmeteo.append(press)
        Wind_3bmeteo.append(wind)
        WindDir_3bmeteo.append(winddir)

    # sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona/dettagli_orari/2", None, 30)
    sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/" + location.lower() + "/dettagli_orari/2", None, 30)
    htmlSource = sock1.read()                            
    sock1.close()  
    dati=htmlSource
    while dati.find(b'<div class="col-xs-1-4 big ') != -1:           #lettura 3bmeteo
        datiN=len(dati)
        #Ora
        punt1=dati.find(b'<div class="col-xs-1-4 big ')
        dati=dati[punt1+28:datiN]
        appo=dati.find(b' ')
        ora=dati[appo+1:appo+3]
        datiN=len(dati)
        dati=dati[appo+5:datiN]
        datiN=len(dati)
        punt2=dati.find(b'"col-xs-2-4 ">')
        dati=dati[punt2:datiN]
        punt3=dati.find(b'<')
        meteo=dati[15:punt3-2]
        dati=dati[punt3+3:datiN]
        datiN=len(dati)
        punt4=dati.find(b'"switchcelsius switch-te active">')
        appo=dati.find(b'&deg')
        temperatura=dati[punt4+33:appo]
        #Wind
        punt6=dati.find(b'"switchkm switch-vi active">')
        datiN=len(dati)
        dati=dati[punt6:datiN]
        appo=dati.find(b'<')
        wind=dati[28:appo]
        datiN=len(dati)
        dati=dati[appo:datiN]   

        #WindDir
        punt5=dati.find(b'"switchnodi switch-vi">')
        datiN=len(dati)
        dati=dati[punt5:datiN]    
        appo=dati.find(b'&nbsp;')
        datiN=len(dati)
        dati=dati[appo:datiN]
        appo2=dati.find(b'<')
        winddir=dati[6:appo2-2]
        datiN=len(dati)
        dati=dati[appo2:datiN] 

        #Hum
        punt7=dati.find(b'altriDati-umidita">')
        datiN=len(dati)
        dati=dati[punt7:datiN]    
        appo=dati.find(b'<')
        hum=dati[19:appo-3]
        datiN=len(dati) 
        dati=dati[appo:datiN]
        
        #Press
        punt8=dati.find(b'altriDati-pressione">')
        datiN=len(dati)
        dati=dati[punt8:datiN]    
        appo=dati.find(b'<')
        press=dati[21:appo-2]
        datiN=len(dati)
        dati=dati[appo:datiN]    
        
        #Append
        Ora_3bmeteo.append(ora)
        Meteo_3bmeteo.append(meteo)
        Temperatura_3bmeteo.append(temperatura)
        Hum_3bmeteo.append(hum)
        Press_3bmeteo.append(press)
        Wind_3bmeteo.append(wind)
        WindDir_3bmeteo.append(winddir)


    for n in range(len(Meteo_3bmeteo)):
        if Meteo_3bmeteo[n] == b'sereno':
            Meteo_3bmeteo[n] = 5
        elif Meteo_3bmeteo[n] == b'quasi sereno' or Meteo_3bmeteo[n] == b'velature lievi' or Meteo_3bmeteo[n] == b'velature sparse':
            Meteo_3bmeteo[n] = 4
        elif Meteo_3bmeteo[n] == b'parz. nuvoloso' or Meteo_3bmeteo[n] == b'variabile' or Meteo_3bmeteo[n] == b'velature diffuse' or Meteo_3bmeteo[n] == b'poche nubi sparse':
            Meteo_3bmeteo[n] = 3
        elif Meteo_3bmeteo[n] == b'nuvoloso':
            Meteo_3bmeteo[n] = 2
        elif Meteo_3bmeteo[n] == b'coperto' or Meteo_3bmeteo[n] == b'temporale e schiarite' or Meteo_3bmeteo[n] == b'piovaschi e schiarite' or Meteo_3bmeteo[n] == b'possibile temporale' or Meteo_3bmeteo[n] == b'rovesci e schiarite' or Meteo_3bmeteo[n] == b'pioviggine':
            Meteo_3bmeteo[n] = 1
        elif Meteo_3bmeteo[n] == b'pioggia' or Meteo_3bmeteo[n] == b'pioggia debole' or Meteo_3bmeteo[n] == b'pioggia moderata':
            Meteo_3bmeteo[n]=0
        else:
            Meteo_3bmeteo[n] = 1


    WindDir_3bmeteo=wind2deg(WindDir_3bmeteo)
    #print(Ora_3bmeteo)

    for n in range(len(Ora_3bmeteo)):
        Ora_3bmeteo[n]=float(bytes.decode(Ora_3bmeteo[n]))
    for n in range(len(Wind_3bmeteo)):
        Wind_3bmeteo[n]=round(float(bytes.decode(Wind_3bmeteo[n])) / float(3.6),2)
    #for n in range(len(WindDir_3bmeteo)):
    #    WindDir_3bmeteo[n]=bytes.decode(WindDir_3bmeteo[n])
    for n in range(len(Hum_3bmeteo)):
        Hum_3bmeteo[n]=int(bytes.decode(Hum_3bmeteo[n]))
    for n in range(len(Press_3bmeteo)):
        Press_3bmeteo[n]=int(bytes.decode(Press_3bmeteo[n]))    
    for n in range(len(Temperatura_3bmeteo)):
        Temperatura_3bmeteo[n]=float(bytes.decode(Temperatura_3bmeteo[n]))


    # non va prova www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm=4

    #print (Ora_3bmeteo)
    #print (Meteo_3bmeteo)
    #print (Temperatura_3bmeteo)
    #print (Hum_3bmeteo)
    #print (Wind_3bmeteo)
    #print (WindDir_3bmeteo)
    #print (Press_3bmeteo)


    fine = round(horizon/60)

    res = round(60/resolution)

    fine=fine+1

    Ora1=[]

    Ora2 = Ora_3bmeteo[0:fine]
    #print(Ora2)
    if Ora2[1]-Ora2[0]>1:
        Ora2[0]=Ora2[1]-1

    for j in range(len(Ora2)):
        Ora2[j]=Ora2[0]+j

    for n in range(1,fine*res-2):
        newOra = Ora2[0] + (n -1) / res
    #    if newOra > 23.75:
    #        newOra = newOra - 24
        
        Ora1.append(newOra)

    M3=[]
    M3 = np.interp(Ora1, Ora2, Meteo_3bmeteo[0:fine])
    M4 = np.interp(Ora1, Ora2, Temperatura_3bmeteo[0:fine])
    M5 = np.interp(Ora1, Ora2, Hum_3bmeteo[0:fine])
    M6 = np.interp(Ora1, Ora2, Wind_3bmeteo[0:fine])
    M7 = np.interp(Ora1, Ora2, WindDir_3bmeteo[0:fine])
    M8 = np.interp(Ora1, Ora2, Press_3bmeteo[0:fine])


    #print(M3)
    #print(Ora1)
    for n in range(len(Ora1)):
        if Ora1[n] > 95.75:
            Ora1[n] = Ora1[n] - 96
        if Ora1[n] > 71.75:
            Ora1[n] = Ora1[n] - 72
        if Ora1[n] > 47.75:
            Ora1[n] = Ora1[n] - 48
        if Ora1[n] > 23.75:
            Ora1[n] = Ora1[n] - 24
            
    #print(Ora1)
    #print(M4)
    #print(M5)
    return (M3,M4,M5,M6,M7,M8, sorge, tramonta, Ora1)







#prevFuzzy=[]
#Meteo=[0,0,0,0,0,0]

#for n in range(len(Temperatura_3bmeteo)):
#    Meteo=[0,0,0,0,0,0]
#    Meteo[Meteo_3bmeteo[n]]=1
#    prevFuzzy.append(fuzzy(Meteo, Temperatura_3bmeteo[n]))


#for n in range(len(prevFuzzy)):
#    prevFuzzy[n]=round(float(prevFuzzy[n]),2)

#print(prevFuzzy)






