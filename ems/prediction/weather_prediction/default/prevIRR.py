# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

#rispetto alla funzione di prima, qui vado a pesare i vari vettori di irraggiamento
#con i pesi relativi all'inferenza fuzzy che tiene conto dei diversi valori meteo

#il size in input è la dimensione dell'impianto PV (NB gli effetti della temperatura
#sulla produzione sono considerati nell'inferenza stessa i.e. col gelo o con il caldo estremo riduce la produzione)

import csv
import time
from defines import *


# def prevIRR(meteo, size,ora):
def prevIRR(meteo, ora, irr_filename):

    csv.register_dialect('pipes', delimiter='\t')

    appo=[];

    #with open(os.path.join(EMS_STORE_DATA, 'dailyrad433656N_133108E_25deg_0deg(1).txt'), 'r', encoding='iso-8859-1') as f:
    with open(os.path.join(EMS_DATASETS, 'meteo_data', irr_filename), 'r', encoding='iso-8859-1') as f:
        reader = csv.reader(f, dialect='pipes')
        for row in reader:
            #print(row)
            appo.append(row)
            
    ap=1

    Gc=[]
    Gd=[]
    DNIc=[]
    Ora1=[]
    #print(appo)

    i = 9

    while ap == 1:

        if appo[i+1] == ['G: Global irradiance on a fixed plane  (W/m2)']:
            ap = 0

        Gd.append(int(appo[i-1][4]))
        Gc.append(int(appo[i-1][6]))
        DNIc.append(int(appo[i-1][10]))
        ap0=appo[i-1][0]
        ap1=int(ap0[0:2])
        ap2=int(ap0[3:5])
        ap3=round(ap2*4/60)/4
        Ora1.append(ap1+ap3)
        i=i+1    
    
    #print(Ora1)
    adde = time.strftime("%H:%M")
    ap1 = int(adde[0:2])
    ap2 = int(adde[3:5])
    ap3 = round(ap2*4/60)/4
    adesso = ap1+ap3
    #print(adesso)
    #print(ap3)

    #print(ora)
    diff=adesso-ora[0]
    for i in range(len(ora)):
        ora[i]=ora[i]+ diff

    #print(ora)

    M9 = []
    M10=[]
    M11=[]
    PV=[]

    for k in range(len(ora)):
        check = 0
        for kk in range(len(Ora1)):
            if ora[k] == Ora1[kk]:
                M9.append(Gc[kk])
                M10.append(Gd[kk])
                M11.append(DNIc[kk])
                check = 1
        if check == 0:
            M9.append(0)
            M10.append(0)
            M11.append(0)

    for ii in range(len(meteo)-2):
        M9[ii]=int(M9[ii] * (meteo[ii]))
        
        M10[ii]=int(M10[ii] * (meteo[ii]))
        
        M11[ii]=int(M11[ii] * (meteo[ii]))

        # PV.append(int(M9[ii] * size))

    # return(M9,M10,M11,PV)
    return (M9, M10, M11)

