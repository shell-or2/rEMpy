# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

def wind2deg(WindDir_3bmeteo):

    for n in range(len(WindDir_3bmeteo)):
        if WindDir_3bmeteo[n] == b'ENE':
            WindDir_3bmeteo[n] = 67.5
        elif WindDir_3bmeteo[n] == b'ESE':
            WindDir_3bmeteo[n] = 112.5
        elif WindDir_3bmeteo[n] == b'NNE':
            WindDir_3bmeteo[n] = 22.5
        elif WindDir_3bmeteo[n] == b'NNO':
            WindDir_3bmeteo[n] = 337.5
        elif WindDir_3bmeteo[n] == b'SSE':
            WindDir_3bmeteo[n] = 157.5
        elif WindDir_3bmeteo[n] == b'SSO':
            WindDir_3bmeteo[n] = 202.5
        elif WindDir_3bmeteo[n] == b'OSO':
            WindDir_3bmeteo[n] = 247.5
        elif WindDir_3bmeteo[n] == b'ONO':
            WindDir_3bmeteo[n] = 292.5
        elif WindDir_3bmeteo[n] == b'NE':
            WindDir_3bmeteo[n] = 45
        elif WindDir_3bmeteo[n] == b'SE':
            WindDir_3bmeteo[n] = 135
        elif WindDir_3bmeteo[n] == b'SO':
            WindDir_3bmeteo[n] = 225
        elif WindDir_3bmeteo[n] == b'NO':
            WindDir_3bmeteo[n] = 315
        elif WindDir_3bmeteo[n] == b'S':
            WindDir_3bmeteo[n] = 180
        elif WindDir_3bmeteo[n] == b'O':
            WindDir_3bmeteo[n] = 270
        elif WindDir_3bmeteo[n] == b'N':
            WindDir_3bmeteo[n] = 0
        elif WindDir_3bmeteo[n] == b'E':
            WindDir_3bmeteo[n] = 90
        else:
            WindDir_3bmeteo[n] = 0

    return WindDir_3bmeteo
