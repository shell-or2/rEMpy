# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import time
import string
import urllib.request                                       
import datetime as dt
import re
import numpy as np
import skfuzzy as fuzz

from fuzzy import fuzzy


#start_timestamp = input_data.get('timestamp')
#last_timestamp = start_timestamp + (input_data.get('horizon') * 60)
#print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_timestamp)))



Ora_ilmeteo=[]
Meteo_ilmeteo=[]
Temperatura_ilmeteo=[]
Hum_ilmeteo=[]
Press_ilmeteo=[]
Wind_ilmeteo=[]
WindDir_ilmeteo=[]

Today=int(time.strftime('%d', time.localtime()))

sock = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm="+str(Today)+"&g=0&lang=ita") 
htmlSource = sock.read()                            
sock.close()  
dati=htmlSource    
while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
    datiN=len(dati)
    #ORA
    punt1=dati.find(b'<span class="ora">')
    datiN=len(dati)
    dati=dati[punt1:datiN]    
    appo=dati.find(b'</span>')
    ora=dati[18:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Meteo
    punt2=dati.find(b'td class="col3">')
    datiN=len(dati)
    dati=dati[punt2:datiN]    
    punt3=dati.find(b'<')
    meteo=dati[16:punt3]
    datiN=len(dati)
    dati=dati[punt3:datiN]
    
    #Temperatura
    punt4=dati.find(b'<td class="col4"><span class="boldval">')
    datiN=len(dati)
    dati=dati[punt4:datiN]    
    appo=dati.find(b'&deg;')
    temperatura=dati[39:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]
    
    #WindDir
    punt5=dati.find(b'style="transform:rotate(')
    appo=dati.find(b')')
    winddir=dati[punt5+24:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Wind
    punt6=dati.find(b'class="boldval">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'</span>')
    wind=dati[16:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    

    #Hum
    punt7=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[14:appo]
    datiN=len(dati)
    appo=dati.find(b'class="col8">')    
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[14:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_ilmeteo.append(ora)
    Meteo_ilmeteo.append(meteo)
    Temperatura_ilmeteo.append(temperatura)
    Hum_ilmeteo.append(hum)
    Press_ilmeteo.append(press)
    Wind_ilmeteo.append(wind)
    WindDir_ilmeteo.append(winddir)

sock1 = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm="+str(Today+1)+"&g=1&lang=ita") 
htmlSource = sock1.read()                            
sock1.close()  
dati=htmlSource
while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
    datiN=len(dati)
    #ORA
    punt1=dati.find(b'<span class="ora">')
    datiN=len(dati)
    dati=dati[punt1:datiN]    
    appo=dati.find(b'</span>')
    ora=dati[18:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Meteo
    punt2=dati.find(b'td class="col3">')
    datiN=len(dati)
    dati=dati[punt2:datiN]    
    punt3=dati.find(b'<')
    meteo=dati[16:punt3]
    datiN=len(dati)
    dati=dati[punt3:datiN]
    
    #Temperatura
    punt4=dati.find(b'<td class="col4"><span class="boldval">')
    datiN=len(dati)
    dati=dati[punt4:datiN]    
    appo=dati.find(b'&deg;')
    temperatura=dati[39:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]
    
    #WindDir
    punt5=dati.find(b'style="transform:rotate(')
    appo=dati.find(b')')
    winddir=dati[punt5+24:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Wind
    punt6=dati.find(b'class="boldval">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'</span>')
    wind=dati[16:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    

    #Hum
    punt7=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[14:appo]
    datiN=len(dati)
    appo=dati.find(b'class="col8">')    
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[14:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_ilmeteo.append(ora)
    Meteo_ilmeteo.append(meteo)
    Temperatura_ilmeteo.append(temperatura)
    Hum_ilmeteo.append(hum)
    Press_ilmeteo.append(press)
    Wind_ilmeteo.append(wind)
    WindDir_ilmeteo.append(winddir)

sock1 = urllib.request.urlopen("http://www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm="+str(Today+2)+"&g=2&lang=ita") 
htmlSource = sock1.read()                            
sock1.close()  
dati=htmlSource
while dati.find(b'<span class="ora">') != -1:           #lettura ilmeteo
    datiN=len(dati)
    #ORA
    punt1=dati.find(b'<span class="ora">')
    datiN=len(dati)
    dati=dati[punt1:datiN]    
    appo=dati.find(b'</span>')
    ora=dati[18:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Meteo
    punt2=dati.find(b'td class="col3">')
    datiN=len(dati)
    dati=dati[punt2:datiN]    
    punt3=dati.find(b'<')
    meteo=dati[16:punt3]
    datiN=len(dati)
    dati=dati[punt3:datiN]
    
    #Temperatura
    punt4=dati.find(b'<td class="col4"><span class="boldval">')
    datiN=len(dati)
    dati=dati[punt4:datiN]    
    appo=dati.find(b'&deg;')
    temperatura=dati[39:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]
    
    #WindDir
    punt5=dati.find(b'style="transform:rotate(')
    appo=dati.find(b')')
    winddir=dati[punt5+24:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]

    #Wind
    punt6=dati.find(b'class="boldval">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'</span>')
    wind=dati[16:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    

    #Hum
    punt7=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[14:appo]
    datiN=len(dati)
    appo=dati.find(b'class="col8">')    
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'class="hdata">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[14:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_ilmeteo.append(ora)
    Meteo_ilmeteo.append(meteo)
    Temperatura_ilmeteo.append(temperatura)
    Hum_ilmeteo.append(hum)
    Press_ilmeteo.append(press)
    Wind_ilmeteo.append(wind)
    WindDir_ilmeteo.append(winddir)


for n in range(len(Meteo_ilmeteo)):
    if Meteo_ilmeteo[n] == b'sereno':
        Meteo_ilmeteo[n] = 5
    elif Meteo_ilmeteo[n] == b'quasi sereno' or Meteo_ilmeteo[n] == b'poco&nbsp;nuvoloso' or Meteo_ilmeteo[n] == b'velature sparse':
        Meteo_ilmeteo[n] = 4
    elif Meteo_ilmeteo[n] == b'parz. nuvoloso' or Meteo_ilmeteo[n] == b'variabile' or Meteo_ilmeteo[n] == b'velature diffuse' or Meteo_ilmeteo[n] == b'nubi&nbsp;sparse':
        Meteo_ilmeteo[n] = 3
    elif Meteo_ilmeteo[n] == b'nuvoloso'or Meteo_ilmeteo[n] ==b'coperto' or Meteo_ilmeteo[n] == b'possibile temporale':
        Meteo_ilmeteo[n] = 1
    elif Meteo_ilmeteo[n] == b'temporale e schiarite' or Meteo_ilmeteo[n] == b'piovaschi e schiarite' or Meteo_ilmeteo[n] == b'rovesci e schiarite' or Meteo_ilmeteo[n] == b'pioviggine':
        Meteo_ilmeteo[n] = 2
    elif Meteo_ilmeteo[n] == b'pioggia' or Meteo_ilmeteo[n] == b'pioggia debole' or Meteo_ilmeteo[n] == b'pioggia moderata':
        Meteo_ilmeteo[n]=0
    else:
        Meteo_ilmeteo[n] = 1


for n in range(len(Ora_ilmeteo)):
    Ora_ilmeteo[n]=bytes.decode(Ora_ilmeteo[n])
for n in range(len(Wind_ilmeteo)):
    Wind_ilmeteo[n]=round(float(bytes.decode(Wind_ilmeteo[n])) / float(3.6),2)
for n in range(len(WindDir_ilmeteo)):
    WindDir_ilmeteo[n]=bytes.decode(WindDir_ilmeteo[n])
for n in range(len(Hum_ilmeteo)):
    Hum_ilmeteo[n]=bytes.decode(Hum_ilmeteo[n])
for n in range(len(Press_ilmeteo)):
    Press_ilmeteo[n]=bytes.decode(Press_ilmeteo[n])    
for n in range(len(Temperatura_ilmeteo)):
    Temperatura_ilmeteo[n]=float(bytes.decode(Temperatura_ilmeteo[n]))

print(Ora_ilmeteo)
print(Meteo_ilmeteo)
print(Temperatura_ilmeteo)
print(Hum_ilmeteo)
print(Press_ilmeteo)
print(Wind_ilmeteo)
print(WindDir_ilmeteo)

print(len(Ora_ilmeteo))
print(len(Wind_ilmeteo))
print(len(Meteo_ilmeteo))
print(len(WindDir_ilmeteo))

#prevFuzzy=[]
#Meteo=[0,0,0,0,0,0]

#for n in range(len(Temperatura)):
#    Meteo=[0,0,0,0,0,0]
#    Meteo[Meteo_ilmeteo[n]]=1
#    prevFuzzy.append(fuzzy(Meteo, Temperatura[n]))


#for n in range(len(prevFuzzy)):
#    prevFuzzy[n]=round(float(prevFuzzy[n]),2)

#print(prevFuzzy)




