# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import time
import string
import urllib.request
import datetime as dt
import re
import numpy as np
import skfuzzy as fuzz

from fuzzy import fuzzy


Ora_3bmeteo=[]
Meteo_3bmeteo=[]
Temperatura_3bmeteo=[]
Hum_3bmeteo=[]
Press_3bmeteo=[]
Wind_3bmeteo=[]
WindDir_3bmeteo=[]

sock = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona") 
htmlSource = sock.read()                            
sock.close()  
dati=htmlSource    
while dati.find(b'class="col-xs-1-4 big">') != -1:           #lettura 3bmeteo
    datiN=len(dati)
    #Ora
    punt1=dati.find(b'class="col-xs-1-4 big">')
    appo=dati.find(b'<span class="small">')
    ora=dati[punt1+24:appo]
    dati=dati[punt1+29:datiN]
    
    #Meteo
    datiN=len(dati)
    start=15
    punt2=dati.find(b'"col-xs-2-4 ">')
    punt2bis=dati.find(b'"col-xs-2-4 zoom_prv">')
    if punt2bis != -1:
        if punt2 > punt2bis:
            punt2=punt2bis
            start=23
    dati=dati[punt2:datiN]
    punt3=dati.find(b'<')
    meteo=dati[start:punt3-2]
    dati=dati[punt3+3:datiN]
    datiN=len(dati)
    
    #Temperatura
    punt4=dati.find(b'"switchcelsius switch-te active">')
    appo=dati.find(b'&deg')
    temperatura=dati[punt4+33:appo]
    dati=dati[appo:datiN]

    #Wind
    punt6=dati.find(b'"switchkm switch-vi active">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'<')
    wind=dati[28:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]   

    #WindDir
    punt5=dati.find(b'"switchnodi switch-vi">')
    datiN=len(dati)
    dati=dati[punt5:datiN]    
    appo=dati.find(b'&nbsp;')
    datiN=len(dati)
    dati=dati[appo:datiN]
    appo2=dati.find(b'<')
    winddir=dati[6:appo2-2]
    datiN=len(dati)
    dati=dati[appo2:datiN] 

    #Hum
    punt7=dati.find(b'altriDati-umidita">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[19:appo-2]
    datiN=len(dati) 
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'altriDati-pressione">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[21:appo-2]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_3bmeteo.append(ora)
    Meteo_3bmeteo.append(meteo)
    Temperatura_3bmeteo.append(temperatura)
    Hum_3bmeteo.append(hum)
    Press_3bmeteo.append(press)
    Wind_3bmeteo.append(wind)
    WindDir_3bmeteo.append(winddir)


    
    Temperatura_3bmeteo.append(temperatura)
    Ora_3bmeteo.append(ora)
    Meteo_3bmeteo.append(meteo)

sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona/dettagli_orari/1") 
htmlSource = sock1.read()                            
sock1.close()  
dati=htmlSource
while dati.find(b'class="col-xs-1-4 big">') != -1:
    datiN=len(dati)    
    punt1=dati.find(b'class="col-xs-1-4 big">')
    appo=dati.find(b'<span class="small">')
    ora=dati[punt1+24:appo]
    dati=dati[punt1+29:datiN]
    datiN=len(dati)
    punt2=dati.find(b'"col-xs-2-4 ">')
    dati=dati[punt2:datiN]
    punt3=dati.find(b'<')
    meteo=dati[15:punt3-2]
    dati=dati[punt3+3:datiN]
    datiN=len(dati)
    punt4=dati.find(b'"switchcelsius switch-te active">')
    appo=dati.find(b'&deg')
    temperatura=dati[punt4+33:appo]
    #Wind
    punt6=dati.find(b'"switchkm switch-vi active">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'<')
    wind=dati[28:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]   

    #WindDir
    punt5=dati.find(b'"switchnodi switch-vi">')
    datiN=len(dati)
    dati=dati[punt5:datiN]    
    appo=dati.find(b'&nbsp;')
    datiN=len(dati)
    dati=dati[appo:datiN]
    appo2=dati.find(b'<')
    winddir=dati[6:appo2-2]
    datiN=len(dati)
    dati=dati[appo2:datiN] 

    #Hum
    punt7=dati.find(b'altriDati-umidita">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[19:appo-2]
    datiN=len(dati) 
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'altriDati-pressione">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[21:appo-2]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_3bmeteo.append(ora)
    Meteo_3bmeteo.append(meteo)
    Temperatura_3bmeteo.append(temperatura)
    Hum_3bmeteo.append(hum)
    Press_3bmeteo.append(press)
    Wind_3bmeteo.append(wind)
    WindDir_3bmeteo.append(winddir)

sock1 = urllib.request.urlopen("http://www.3bmeteo.com/meteo/ancona/dettagli_orari/2") 
htmlSource = sock1.read()                            
sock1.close()  
dati=htmlSource
while dati.find(b'class="col-xs-1-4 big">') != -1:
    datiN=len(dati)    
    punt1=dati.find(b'class="col-xs-1-4 big">')
    appo=dati.find(b'<span class="small">')
    ora=dati[punt1+24:appo]
    dati=dati[punt1+29:datiN]
    datiN=len(dati)
    punt2=dati.find(b'"col-xs-2-4 ">')
    dati=dati[punt2:datiN]
    punt3=dati.find(b'<')
    meteo=dati[15:punt3-2]
    dati=dati[punt3+3:datiN]
    datiN=len(dati)
    punt4=dati.find(b'"switchcelsius switch-te active">')
    appo=dati.find(b'&deg')
    temperatura=dati[punt4+33:appo]
    #Wind
    punt6=dati.find(b'"switchkm switch-vi active">')
    datiN=len(dati)
    dati=dati[punt6:datiN]
    appo=dati.find(b'<')
    wind=dati[28:appo]
    datiN=len(dati)
    dati=dati[appo:datiN]   

    #WindDir
    punt5=dati.find(b'"switchnodi switch-vi">')
    datiN=len(dati)
    dati=dati[punt5:datiN]    
    appo=dati.find(b'&nbsp;')
    datiN=len(dati)
    dati=dati[appo:datiN]
    appo2=dati.find(b'<')
    winddir=dati[6:appo2-2]
    datiN=len(dati)
    dati=dati[appo2:datiN] 

    #Hum
    punt7=dati.find(b'altriDati-umidita">')
    datiN=len(dati)
    dati=dati[punt7:datiN]    
    appo=dati.find(b'<')
    hum=dati[19:appo-2]
    datiN=len(dati) 
    dati=dati[appo:datiN]
    
    #Press
    punt8=dati.find(b'altriDati-pressione">')
    datiN=len(dati)
    dati=dati[punt8:datiN]    
    appo=dati.find(b'<')
    press=dati[21:appo-2]
    datiN=len(dati)
    dati=dati[appo:datiN]    
    
    #Append
    Ora_3bmeteo.append(ora)
    Meteo_3bmeteo.append(meteo)
    Temperatura_3bmeteo.append(temperatura)
    Hum_3bmeteo.append(hum)
    Press_3bmeteo.append(press)
    Wind_3bmeteo.append(wind)
    WindDir_3bmeteo.append(winddir)


for n in range(len(Meteo_3bmeteo)):
    if Meteo_3bmeteo[n] == b'sereno':
        Meteo_3bmeteo[n] = 5
    elif Meteo_3bmeteo[n] == b'quasi sereno' or Meteo_3bmeteo[n] == b'velature lievi' or Meteo_3bmeteo[n] == b'velature sparse':
        Meteo_3bmeteo[n] = 4
    elif Meteo_3bmeteo[n] == b'parz. nuvoloso' or Meteo_3bmeteo[n] == b'variabile' or Meteo_3bmeteo[n] == b'velature diffuse' or Meteo_3bmeteo[n] == b'poche nubi sparse':
        Meteo_3bmeteo[n] = 3
    elif Meteo_3bmeteo[n] == b'nuvoloso':
        Meteo_3bmeteo[n] = 2
    elif Meteo_3bmeteo[n] == b'coperto' or Meteo_3bmeteo[n] == b'temporale e schiarite' or Meteo_3bmeteo[n] == b'piovaschi e schiarite' or Meteo_3bmeteo[n] == b'possibile temporale' or Meteo_3bmeteo[n] == b'rovesci e schiarite' or Meteo_3bmeteo[n] == b'pioviggine':
        Meteo_3bmeteo[n] = 1
    elif Meteo_3bmeteo[n] == b'pioggia' or Meteo_3bmeteo[n] == b'pioggia debole' or Meteo_3bmeteo[n] == b'pioggia moderata':
        Meteo_3bmeteo[n]=0
    else:
        Meteo_3bmeteo[n] = 1


for n in range(len(Ora_3bmeteo)):
    Ora_3bmeteo[n]=bytes.decode(Ora_3bmeteo[n])
for n in range(len(Wind_3bmeteo)):
    Wind_3bmeteo[n]=round(float(bytes.decode(Wind_3bmeteo[n])) / float(3.6),2)
for n in range(len(WindDir_3bmeteo)):
    WindDir_3bmeteo[n]=bytes.decode(WindDir_3bmeteo[n])
for n in range(len(Hum_3bmeteo)):
    Hum_3bmeteo[n]=bytes.decode(Hum_3bmeteo[n])
for n in range(len(Press_3bmeteo)):
    Press_3bmeteo[n]=bytes.decode(Press_3bmeteo[n])    
for n in range(len(Temperatura_3bmeteo)):
    Temperatura_3bmeteo[n]=float(bytes.decode(Temperatura_3bmeteo[n]))


# non va prova www.ilmeteo.it/portale/meteo/previsioni1.php?citta=Ancona&c=223&gm=4


m=0          
#Ora_tot=[Ora_clmeteo,Ora_3bmeteo,Ora_repmeteo,Ora_centrometeo]
#Meteo_tot=[Meteo_clmeteo,Meteo_repmeteo,Meteo_centrometeo]
#for n in range(4):                                    #calcolo il massimo
#    k=0
#    while Ora_tot[n][k] == '':
#        k+=1
#    if Ora_tot[n][k]>m:
#        m=Ora_tot[n][k]
#print (m)

#print (Ora_clmeteo)
#print (Meteo_clmeteo)

print (Ora_3bmeteo)
print (Meteo_3bmeteo)
print (Temperatura_3bmeteo)
print (Hum_3bmeteo)
print (Wind_3bmeteo)
print (WindDir_3bmeteo)
print (Press_3bmeteo)

#prevFuzzy=[]
#Meteo=[0,0,0,0,0,0]

#for n in range(len(Temperatura_3bmeteo)):
#    Meteo=[0,0,0,0,0,0]
#    Meteo[Meteo_3bmeteo[n]]=1
#    prevFuzzy.append(fuzzy(Meteo, Temperatura_3bmeteo[n]))


#for n in range(len(prevFuzzy)):
#    prevFuzzy[n]=round(float(prevFuzzy[n]),2)

#print(prevFuzzy)


#Ora_tot[1:2]=[]
#for n in range(3):                                      #allineo davanti
#    while Ora_tot[n][0] =='':
#        Ora_tot[n][0:1]=[]
#        Meteo_tot[n][0:1]=[]
#    while Ora_tot[n][0] < m:
#        Ora_tot[n][0:1]=[]
#        Meteo_tot[n][0:1]=[]

#while Ora_3bmeteo[0] < m:
#    Ora_3bmeteo[0:1]=[]
#    Meteo_3bmeteo[0:1]=[]
#    Temperatura_3bmeteo[0:1]=[]

#Ora_tot.insert(1,Ora_3bmeteo)
#Meteo_tot.insert(1,Meteo_3bmeteo)

#for n in range(4):                                   #allineo in fondo
#    Ora_tot[n][48-int(m):len(Ora_tot[n])]=[]
#    Meteo_tot[n][48-int(m):len(Meteo_tot[n])]=[]
#Temperatura_3bmeteo[48-int(m):len(Temperatura_3bmeteo)]=[]
#print (Ora_tot)
#print (Meteo_tot)


#file=open('C:\Users\Lucio\Desktop\oggi-domani.txt','r')
#file.readline()
#R=[]
#for n in range(int(m)):
#    R.append(file.readline())
#file.close()
#file=open('C:\Users\Lucio\Desktop\oggi-domani.txt','w')
#file.write('ora\t')
#file.write('meteo1\tmeteo2\tmeteo3\tmeteo4\tTemperatura\n')
#for n in range(len(R)):
#    file.write(str(R[n]))
#for n in range(len(Ora_tot[0])):
#    file.write(Ora_tot[0][n])
#    file.write('\t')
#    file.write(str(Meteo_tot[0][n]))
#    file.write('\t')
#    file.write(str(Meteo_tot[1][n]))
#    file.write('\t')
#    file.write(str(Meteo_tot[2][n]))
#    file.write('\t')
#    file.write(str(Meteo_tot[3][n]))
#    file.write('\t')
#    file.write(str(Temperatura_3bmeteo[n]))
#    file.write('\n')
#file.close()

