# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import csv
import numpy as np
from scipy import *
from scipy.linalg import norm
from pyswarm import pso
import datetime
import logging
from defines import *
from ems_db.sensor.routines import get_record
logger = logging.getLogger(__name__)


def read_amp(input_data):
    timestamp = int(input_data.get('timestamp')) - 3 * 365 * 24 * 60 * 60  # 1 years correction
    horizon = int(input_data.get('horizon'))
    resolution = int(input_data.get('resolution'))

    load_amp = []

    with open(os.path.join(EMS_DATASETS, 'ene_data', 'AMP_Load.csv'), 'r') as f:

        reader = csv.reader(f, delimiter=',')

        for row_counter, row in enumerate(reader):

            if row_counter == 0:
                continue
            # skip first row
            elif int(row[0]) >= (timestamp - (resolution / 2 * 60)) and int(row[0]) < (
                timestamp + ((horizon + resolution / 2) * 60)):
                # print(timestamp)
                # print(timestamp + (horizon * 60))
                # print(row[0])
                # print(row[1])
                load_amp.append(float(row[1]))

    return load_amp


class RBF:
    def __init__(self, indim, numCenters, outdim):
        self.indim = indim
        self.outdim = outdim
        self.numCenters = numCenters
        self.centers = [random.uniform(-1, 1, indim) for i in range(numCenters)]
        self.beta = 0.000003
        self.W = random.random((self.numCenters, self.outdim))

    def _basisfunc(self, c, d):
        assert len(d) == self.indim
        return exp(-self.beta * norm(c - d) ** 2)

    def _calcAct(self, X):
        # funz. attivazioni dei neuroni
        G = zeros((X.shape[0], self.numCenters), float)
        for ci, c in enumerate(self.centers):
            for xi, x in enumerate(X):
                G[xi, ci] = self._basisfunc(c, x)
        return G

    def train(self, X, Y):
        """ X: tutte le n osservazioni degli "indim" ingressi: n x indim  
            y: vettore colonna delle n uscite reali n x 1 """

        # i centri li prendo random
        rnd_idx = random.permutation(X.shape[0])[:self.numCenters]
        self.centers = [X[i, :] for i in rnd_idx]

        # print("center", self.centers)
        # calcolo le funz. di attivazione
        G = self._calcAct(X)

        # print(G)

        # trovo i pesi con la PSO minimizzando l'errore quad. delle osservazioni
        def errore(x, *args):
            self.W = x
            G, Y = args

            return sum((dot(G, self.W) - Y) ** 2)

        # self.W = dot(pinv(G), Y)

        args = (G, Y)
        lb = [100 for i in range(self.numCenters)]
        ub = [10000 for i in range(self.numCenters)]
        self.W, appo = pso(errore, lb, ub, args=args, swarmsize=200, omega=0.6, phip=1.8, phig=1.8, maxiter=2000,
                           minstep=1e-11, minfunc=1e-14)

    def test(self, X):
        """ X: tutte le n osservazioni degli "indim" ingressi: n x indim """

        G = self._calcAct(X)
        Y = dot(G, self.W)
        return Y


def prediction(prediction_data):
    error_code = 0

    _timestamp = prediction_data.get('timestamp')
    _horizon = prediction_data.get('horizon')
    _resolution = prediction_data.get('resolution')
    _n_slots = _horizon // _resolution

    logger.info('Predicting load data for ' + prediction_data.get('id'))

    # y = read_amp({'timestamp': _timestamp, 'horizon': 1440 * 15, 'resolution': _resolution})
    # x1 = read_amp({'timestamp': (_timestamp - 1440 * 28), 'horizon': 1440 * 28, 'resolution': 15})

    # Load from DB
    # from current time to 15 days ahead ([: ,1] -> get only the values without dates)
    y = np.asarray(get_record('overall', datetime.datetime.fromtimestamp(_timestamp),
                              datetime.datetime.fromtimestamp(_timestamp) + datetime.timedelta(days=15)))[:, 1]
    # from 28 days ago to current date ([: ,1] -> get only the values without dates)
    x1 = np.asarray(get_record('overall', datetime.datetime.fromtimestamp(_timestamp) - datetime.timedelta(days=28),
                               datetime.datetime.fromtimestamp(_timestamp)))[:, 1]

    n = 50
    x2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    fine = len(x1)

    for i in range(int(1440 * 15 / 15)):
        appo = []
        for j in range(4):
            aa = np.mean([x1[fine - 96 * 15 + i - 96 * j], x1[fine - 4 - 96 * 15 + i - 96 * j],
                          x1[fine - 96 * 15 + i - 1 - 96 * j], x1[fine - 96 * 15 + i - 2 - 96 * j],
                          x1[fine - 96 * 15 + i - 3 - 96 * j]])
            appo = np.append(appo, aa)
            appo = np.append(appo, np.mean(
                [x1[fine - 96 * 15 - 8 + i - 96 * j], x1[fine - 96 * 15 - 8 + i - 96 * j + 1],
                 x1[fine - 96 * 15 - 8 + i - 1 - 96 * j], x1[fine - 96 * 15 - 8 + i - 2 - 96 * j],
                 x1[fine - 96 * 15 - 8 + i - 3 - 96 * j]]))
            appo = np.append(appo, np.mean(
                [x1[fine - 96 * 15 - 4 + i - 96 * j], x1[fine - 96 * 15 - 4 + i - 96 * j - 4],
                 x1[fine - 96 * 15 - 4 + i - 1 - 96 * j], x1[fine - 96 * 15 - 4 + i - 2 - 96 * j],
                 x1[fine - 96 * 15 - 4 + i - 3 - 96 * j]]))

        x2 = np.vstack((x2, appo))

    x2 = np.delete(x2, 0, 0)
    y = np.delete(y, 0, 0)

    for i in range(len(y) - 3):
        y[i] = np.mean([y[i], y[i + 1], y[i + 2]])

    rbf = RBF(12, 4, 1)
    rbf.train(x2, y)
    z = rbf.test(x2)

    logger.info('Predicting load data...OK!')

    prediction_data.update({'loads': z.tolist()[0:_n_slots]})

    return error_code


if __name__ == '__main__':
    import calendar
    import time

    # calendar.timegm(time.gmtime())
    prediction({'timestamp': 1464732000, 'horizon': 360, 'resolution': 15, 'id': 'test-1'})
