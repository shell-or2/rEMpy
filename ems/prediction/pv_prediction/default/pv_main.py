# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import logging
logger = logging.getLogger(__name__)


def prediction(prediction_data):

    error_code = 0
    # il size in input è la dimensione dell'impianto PV (NB gli effetti della temperatura
    # sulla produzione sono considerati nell'inferenza stessa i.e. col gelo o con il caldo estremo riduce la produzione)
    size = prediction_data.get('specs').get('size') / 1000

    _timestamp = prediction_data.get('timestamp')
    _horizon = prediction_data.get('horizon')
    _resolution = prediction_data.get('resolution')

    logger.debug('Predicting pv for ' + prediction_data.get('id'))

    prediction_data.update({'production': [(int(elem_igpf * size) / (60 / prediction_data.get('resolution')))
                                           for elem_igpf in prediction_data.get('gc_radiation_pv')]})
    # Wp to Wh Wh=Wp/(60_/res_in_minuti)

    logger.debug('Predicting pv...OK!')

    return error_code
