# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import communication
import prediction.prediction_core
import time
import sys
import queue
import threading
import logging
logger = logging.getLogger(__name__)


def mainprocess():

    # Init prediction controller
    setup_logging(MODULES_NAMES[PREDICTION_QUEUE])
    controller_queues = communication.ControllerQueues(MODULES_NAMES[PREDICTION_QUEUE])
    logger.info('Prediction running.')

    resolution = None
    horizon = None

    # Object shared with computation thread
    input_prediction_data = []
    output_prediction_data = queue.Queue()
    stop_event = threading.Event()

    prediction_thread = None

    th_prediciton = None

    # Wait message from core
    while True:

        try:
            # Check if received message from core
            message_id, message_data = controller_queues.recvMsg(True)

            if message_id:
                logger.debug('Received message')

                if message_id == MSG_ID_CONFIG:
                    resolution = message_data.get('resolution')
                    horizon = message_data.get('horizon')

                if message_id == MSG_ID_PREDICTION_REQUEST:
                    if not resolution or not horizon:
                        logger.error("Horizon and resolution not set.")

                    logger.debug("Requested prediction simulation.")

                    input_prediction_data = message_data
                    prediction_thread = threading.Thread(name='prediction_thread',
                                                         target=prediction.prediction_core.prediction_core,
                                                         args=(input_prediction_data, output_prediction_data, stop_event,))
                    stop_event.clear()
                    prediction_thread.start()

                    # send (put) input (partial) data to prediction function
                    # input_prediction_data.put({'timestamp': 123456})

                    # time.sleep(2)  # wait for thread to read the queue

                    # thread can not be killed (threading.Thread(...))
                    # th_process = multiprocessing.Process(name='prediction_thread',
                    #                                      target=prediction_core,
                    #                                      args=(shared_prediction_to_core_q,
                    #                                            shared_core_to_prediction_q, 60))
                    # th_process.start()

                    # logger.debug("Send prediction data.")
                    # msg = {MSG_ID_PREDICTION_RESPONSE: {'timestamp': [1, 2, 3, 4, 5], 't_in': [1, 2, 3, 4, 5], 't_out': [1, 2, 3, 4, 5]}}
                    # if not ems.communication.sendMsg(shared_prediction_to_core_q, shared_core_to_prediction_q, msg, True):
                    #    logger.info("Problem with queue to core.")

                if message_id == MSG_ID_PREDICTION_CANCEL:
                    logger.debug("Requested prediction cancellation.")
                    if prediction_thread.is_alive():
                        stop_event.set()
                        #  prediction_thread.terminate()
                        prediction_thread.join()
                        logger.debug('Prediction process cancelled.')
                        # TODO: send response to core in case of success?

            # wait to prediciton to terminate
            # if prediction_thread:
            #    if prediction_thread.is_alive():
            if not output_prediction_data.empty():
                output_data = output_prediction_data.get()
                logger.debug("Send prediction data.")
                msg = {MSG_ID_PREDICTION_RESPONSE: output_data}  # {'timestamp': [1, 2, 3, 4, 5], 't_in': [1, 2, 3, 4, 5], 't_out': [1, 2, 3, 4, 5]}}
                if not controller_queues.sendMsg(msg, True):
                    logger.info("Problem with queue to core.")

            if stop_event.is_set():
                logger.debug('Kill event set...controlling thread.')
                if prediction_thread.is_alive():
                    logger.debug('Thread still alive!')
                else:
                    stop_event.clear()
                    logger.debug('Thread killed, event cleared.')

            # TODO: stop_event, inserire kill funzioni con timer

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Cancel prediction core action and clear output messages.")
            if prediction_thread and prediction_thread.is_alive():
                stop_event.set()
                #  prediction_thread.terminate()
                prediction_thread.join()
                logger.debug('Thermal process cancelled.')
                stop_event.clear()

            if not output_prediction_data.empty():  # dump messages
                output_prediction_data.get()

            logger.info("New connection request.")
            # loop until new connection success
            controller_queues.reconnect()  # blocking, release when connection available

        except KeyboardInterrupt:
            logger.info("Prediction controller  process exit")
            sys.exit(0)

        except Exception as err:
            logger.error(err)
            logger.error("Unhandled exception!")
            sys.exit(-1)

if __name__ == '__main__':

    mainprocess()
