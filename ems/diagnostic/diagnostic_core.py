# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import logging
logger = logging.getLogger(__name__)


def diagnostic_core(data_from_core, data_to_core):

    partner_diagnostic = __import__('diagnostic.' + 'default' + '.diagnostic_main', globals(), locals(),
                                    'diagnostic', 0)

    partner_diagnostic.diagnostic(data_from_core, data_to_core)