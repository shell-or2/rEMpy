# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from ems_db.sensor.routines import get_record, read_table, get_diagnosis_parameters
from ems_db.schedule.routines import get_tasks_in_execution
from defines import *
import logging
logger = logging.getLogger(__name__)


def sensorWarning(sensor_data, down, up):
    for value in sensor_data:
        if value >= up or value <= down:
            return True

    return False


def sensorStuck(sensor_data, sensitivity):

    counter=1
    for i in range(1, len(sensor_data), 1):
        if sensor_data[i] == sensor_data[i-1] and counter >= sensitivity:
            return True
        elif sensor_data[i] == sensor_data[i-1]:
            counter = 1
        else:
            counter = counter+1

    return False


def sensorFailure(sensor_data, sensitivity, value):

    counter = 1
    for i in range(1, len(sensor_data), 1):
        delta = sensor_data[i] - sensor_data[i-1]
        if delta == value and counter >= sensitivity:
            return True
        elif delta != value:
            counter = 1
        else:
            counter += 1

    return False


def lostcommunication(data_from_core, intervall):

    # data_from_core = np.asarray(
    #     get_record(sensors, datetime.datetime.utcfromtimestamp(_timestamp),
    #                datetime.datetime.utcfromtimestamp(_timestamp)))[:,0]

    delta = datetime.datetime.now()-data_from_core[0]
    if (delta.total_seconds() // 60) == intervall:
        return False
    else:
        return True


# Function Actuator Fault
def actuatorFault(sensor_data, threshold):

    if np.mean(sensor_data) > threshold:
        return False
    else:
        return True


def diagnostic(data_from_core, data_to_core):

    logger.info('Started default diagnostic routine')

    delay = datetime.timedelta(days=1)

    while True:

        alert_messages = list()

        # populate available sensors&actuators(tasks)
        available_sensors = read_table('sensor')
        active_tasks = get_tasks_in_execution()
        # available_actuators = read_table('actuator')

        for available_sensor in available_sensors or []:

            sensor_data = np.asarray(get_record(available_sensor, datetime.datetime.now() - delay,
                                                datetime.datetime.now()))

            diagnosis_params = get_diagnosis_parameters(available_sensor)

            if not sensor_data.any():
                alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_MISSING_DATA, 'sensor_name': available_sensor})
            else:
                if sensorWarning(sensor_data[:, 1], diagnosis_params.get('warning_down'), diagnosis_params.get('warning_up')):
                    alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_SENSOR_WARNING, 'sensor_name': available_sensor})

                if sensorStuck(data_from_core[:, 1], diagnosis_params.get('sensor_sensitivity')):
                    alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_SENSOR_STUCK, 'sensor_name': available_sensor})

                if sensorFailure(data_from_core[:, 1], diagnosis_params.get('sensor_sensitivity'),
                                 diagnosis_params.get('failure_value')):
                    alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_SENSOR_FAILURE, 'sensor_name': available_sensor})

                # short lost communication
                if lostcommunication(data_from_core[:, 0], diagnosis_params.get('short_interval')):
                    alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_LOST_COMMUNICATION_SHORT, 'sensor_name': available_sensor})

                # long lost communication
                if lostcommunication(data_from_core[:, 0], diagnosis_params.get('long_interval')):
                    alert_messages.append(
                        {'alert_code': DIAGNOSTIC_ALERT_LOST_COMMUNICATION_LONG, 'sensor_name': available_sensor})

        # alert for tasks
        for active_task in active_tasks or []:
            # active_task = (id, name, start, energy)

            task_data = np.asarray(get_record(active_task[1], active_task[2], datetime.datetime.now()))
            diagnosis_params = get_diagnosis_parameters(active_task[1])

            if not task_data.any():
                alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_MISSING_DATA, 'sensor_name': active_task[1]})
            else:
                if actuatorFault(data_from_core[:, 0], diagnosis_params.get('failure_value')):
                    alert_messages.append({'alert_code': DIAGNOSTIC_ALERT_ACTUATOR_FAULT, 'sensor_name': active_task[1]})

        if alert_messages:

            data_to_core.put({'alerts': alert_messages})

        time.sleep(DIAGNOSTIC_LOOP_TIME)
