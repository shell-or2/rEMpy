# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import communication
import diagnostic.diagnostic_core
import time
import sys
import queue
import threading
import logging
logger = logging.getLogger(__name__)


def mainprocess():

    # Init diagnostic controller
    setup_logging(MODULES_NAMES[DIAGNOSTIC_QUEUE])
    controller_queues = communication.ControllerQueues(MODULES_NAMES[DIAGNOSTIC_QUEUE])

    resolution = None
    horizon = None

    # Object shared with computation thread
    input_diagnostic_data = queue.Queue()
    output_diagnostic_data = queue.Queue()

    diagnostic_thread = None

    # Wait message from core
    while True:

        try:
            if not diagnostic_thread or not diagnostic_thread.is_alive():

                diagnostic_thread = threading.Thread(name='diagnostic_thread',
                                                     target=diagnostic.diagnostic_core.diagnostic_core,
                                                     args=(input_diagnostic_data,
                                                           output_diagnostic_data,))

                diagnostic_thread.daemon = True
                diagnostic_thread.start()

                # if not first_start:
                #     # request db config if restarted
                #     msg = {MSG_ID_INTERFACE_REQUEST_CONFIG: []}
                #     if not controller_queues.sendMsg(msg, True):
                #         logger.info("Problem with queue to core.")
                #     send_data_to_gui.put({'status': 'Requesting configuration information...'})
                # else:
                #     first_start = False

            # Check if received message from core
            message_id, message_data = controller_queues.recvMsg(True)

            if message_id:
                logger.debug('Received message')

                if message_id == MSG_ID_CONFIG:
                    resolution = message_data.get('resolution')
                    horizon = message_data.get('horizon')

                if message_id == MSG_ID_DATA_TO_DIAGNOSTIC:
                    # if not resolution or not horizon:
                    #     logger.error("Horizon and resolution not set.")

                    logger.debug("Received message for diagnostic.")

                    input_diagnostic_data.put(message_data)

                if message_id == MSG_KILL_ALL:
                    logger.debug("Received kill message.")
                    raise KeyboardInterrupt

            # check if received diagnostic message
            if not output_diagnostic_data.empty():
                output_data = output_diagnostic_data.get()
                logger.debug("Send diagnostic data.")
                msg = {
                    MSG_ID_DATA_FROM_DIAGNOSTIC: output_data}
                if not controller_queues.sendMsg(msg, True):
                    logger.info("Problem with queue to core.")

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Clear output messages.")

            if not output_diagnostic_data.empty():  # dump messages
                output_diagnostic_data.get()

            logger.info("New connection request.")
            # loop until new connection success
            controller_queues.reconnect()  # blocking, release when connection available

        except KeyboardInterrupt:
            logger.info("Diagnostic controller process exit")
            sys.exit(0)

        except Exception as err:
            logger.error(err)
            logger.error("Unhandled exception!")
            sys.exit(-1)


if __name__ == '__main__':
    mainprocess()
