# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import time
import sys
import threading
import logging
import queue
from defines import *
import interface.interface_core
import communication
logger = logging.getLogger(__name__)


def mainprocess():

    # Init interface controller
    setup_logging(MODULES_NAMES[INTERFACE_QUEUE])
    controller_queues = communication.ControllerQueues(MODULES_NAMES[INTERFACE_QUEUE])

    resolution = None
    horizon = None

    gui_thread = None

    send_data_to_gui = queue.Queue()
    recv_data_from_gui = queue.Queue()

    first_start = True

    while True:

        # print('Into main loop!')
        # send_data_to_gui.put('Log info ' + str(counter))
        # counter += 1

        try:

            if not gui_thread or not gui_thread.is_alive():
                gui_thread = threading.Thread(name='gui_thread', target=interface.interface_core.interface_core,
                                              args=(send_data_to_gui, recv_data_from_gui,))
                gui_thread.daemon = True
                gui_thread.start()

                if not first_start:
                    # request db config if restarted
                    msg = {MSG_ID_INTERFACE_REQUEST_CONFIG: []}
                    if not controller_queues.sendMsg(msg, True):
                        logger.info("Problem with queue to core.")
                    send_data_to_gui.put({'status': 'Requesting configuration information...'})
                else:
                    first_start = False

            # Check if received message from core
            message_id, message_data = controller_queues.recvMsg(True)

            if message_id:
                logger.debug('Received message')

                if message_id == MSG_ID_CONFIG:
                    resolution = message_data.get('resolution')
                    horizon = message_data.get('horizon')
                    # send_data_to_gui.put('Set parameter: resolution=' + str(resolution) + ' horizon=' + str(horizon))

                if message_id == MSG_ID_DATA_TO_INTERFACE:  # or message_id == MSG_ID_INTERFACE_SEND_PLOT:
                    # msg = ''
                    # for k, v in message_data.items():
                    #     msg = msg + k + ': ' + str(v) + '\n'

                    # send_data_to_gui.put(message_data.get('log_msg'))
                    send_data_to_gui.put(message_data)
                    # send_data_to_gui.put_nowait(message_data)

            # if shared_core_to_interface_q.empty():  # if no connection, block until connection available
            #     logger.debug("Waiting message from core...")
            # else:
            #     # message_from_core = shared_core_to_controller_q.get()
            #     messageId_from_core, data_from_core = shared_core_to_interface_q.get().popitem()
            #     # tmp = shared_core_to_controller_q.get(True, QUEUE_MAX_WAIT)

            # check if received interface message
            if not recv_data_from_gui.empty():
                output_data = recv_data_from_gui.get()
                logger.debug("Send prediction data.")
                msg = {MSG_ID_DATA_FROM_INTERFACE: output_data}
                if not controller_queues.sendMsg(msg, True):
                    logger.info("Problem with queue to core.")

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Clear output messages.")

            if not recv_data_from_gui.empty():  # dump messages
                recv_data_from_gui.get()

            logger.info("New connection request.")
            # loop until new connection success
            controller_queues.reconnect()  # blocking, release when connection available

        except KeyboardInterrupt:
            logger.info("Interface controller process exit")
            sys.exit(0)

        except Exception as err:
            logger.error(err)
            logger.error("Unhandled exception!")
            sys.exit(-1)

if __name__ == "__main__":

    mainprocess()

    # """ Testing your code! """
    #
    # setup_logging("test_interface")
    #
    # core_queues = communication.CoreQueues()
    # controller_queues = communication.ControllerQueues('interface')
    #
    # core_queues.sendMsg(INTERFACE_QUEUE, {MSG_ID_DATA_TO_INTERFACE: {'status': 'il testo del log'}}, False)
    #
    # mainprocess(controller_queues, True)
