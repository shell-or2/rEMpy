from django import forms
from bootstrap3_datetime.widgets import DateTimePicker

from .models import *


class MicroGridForm(forms.ModelForm):
    tag = 'microgrid'

    class Meta:
        model = MicroGrid
        fields = ['name', 'location', 'latitude', 'longitude', 'horizon', 'resolution']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].initial = 'Grid ' + str(MicroGrid.objects.all().count() + 1)
        self.fields['location'].initial = 'Ancona'
        self.fields['latitude'].initial = 43.585785
        self.fields['longitude'].initial = 13.513044
        self.fields['horizon'].initial = 360
        self.fields['resolution'].initial = 15

    def clean(self):
        cleaned_data = super(MicroGridForm, self).clean()

        if not cleaned_data['horizon'] % cleaned_data['resolution'] == 0:
            self.add_error('horizon', 'Horizon must be a multiple of the resolution.')
            self.add_error('resolution', 'Horizon must be a multiple of the resolution.')


class NodeForm(forms.ModelForm):
    tag = 'node'

    class Meta:
        model = Node
        fields = ['name']

    def set_fields(self, microgrid, type, initialize):
        if initialize:
            if type == 'el':
                self.fields['name'].initial = 'E-Node ' + str(microgrid.el_node_set().count() + 1)
            elif type == 'gas':
                self.fields['name'].initial = 'G-Node ' + str(microgrid.gas_node_set().count() + 1)
            elif type == 'heating':
                self.fields['name'].initial = 'H-Node ' + str(microgrid.heating_node_set().count() + 1)
            elif type == 'cooling':
                self.fields['name'].initial = 'C-Node ' + str(microgrid.cooling_node_set().count() + 1)
            elif type == 'water':
                self.fields['name'].initial = 'W-Node ' + str(microgrid.water_node_set().count() + 1)


class MultiTaskForm(forms.Form):
    microgrid = MicroGrid()
    node = forms.ModelChoiceField(queryset=Node.objects.all())
    min_start_time = forms.DateTimeField(label='Starts after:',
                                         widget=DateTimePicker(options={"format": "YYYY-MM-DD HH:mm",
                                                                        "pickSeconds": False}),
                                         initial=get_current_time(zone_aware=False),
                                         required=True)

    max_end_time = forms.DateTimeField(label='Ends before:',
                                       widget=DateTimePicker(options={"format": "YYYY-MM-DD HH:mm",
                                                                      "pickSeconds": False}),
                                       required=True)

    def __init__(self, appliance_tag, task_list, *args, **kwargs):
        super().__init__(*args, **kwargs)
        setattr(self, 'appliance_tag', appliance_tag)
        setattr(self, 'task_list', task_list)
        for task in task_list:
            self.fields[task['name']] = forms.BooleanField(label=task['name'].title(), initial=True, required=False,)
            self.fields[task['name']].widget.attrs.update({'onclick': 'inable_disable("'+task['name']+'")'})
            self.fields[task['name'] + '_consumption'] = forms.FloatField(
                label=task['name'].title() + ' Consumption (Wh)',
                initial=task['consumption'],
                required=False)
            self.fields[task['name'] + '_duration'] = forms.IntegerField(label=task['name'].title() + ' Duration (min)',
                                                                         initial=task['duration'],
                                                                         required=False)
            self.fields[task['name'] + '_delay'] = forms.IntegerField(label=task['name'].title() + ' Delay (min)',
                                                                      initial=task['delay'],
                                                                      required=False)

    def to_tasks(self):
        task_list = []

        for task in self.task_list:
            if self.cleaned_data[task['name']]:
                task_list.append(Task())
                task_list[-1].microgrid = self.microgrid
                task_list[-1].node = self.cleaned_data['node']
                task_list[-1].name = task['name'].title()
                task_list[-1].secondary_id = Task.objects.filter(microgrid=self.microgrid,
                                                                 name=task['name'].title()).count() + 1
                task_list[-1].duration = self.cleaned_data[task['name'] + '_duration']
                task_list[-1].delay = self.cleaned_data[task['name'] + '_delay']
                task_list[-1].consumption = self.cleaned_data[task['name'] + '_consumption']
                task_list[-1].min_start_time = self.cleaned_data['min_start_time']
                task_list[-1].max_end_time = self.cleaned_data['max_end_time']
                task_list[-1].save()

        for task_nr in range(0, len(task_list)):
            if task_nr > 0:
                task_list[task_nr].master_id = task_list[task_nr - 1].id

            if task_nr < len(task_list) - 1:
                task_list[task_nr].slave_id = task_list[task_nr + 1].id

            task_list[task_nr].save()

    def set_fields(self, microgrid):
        self.microgrid = microgrid
        self.fields['node'].queryset = microgrid.el_node_set()
        if self.microgrid.el_node_set().count() == 1:
            self.fields['node'].initial = microgrid.el_node_set()[0]

    def clean(self):
        cleaned_data = super(MultiTaskForm, self).clean()
        total_duration = 0
        for task in self.task_list:
            if cleaned_data[task['name'] ]:
                if task['name'] + '_duration' not in cleaned_data:
                    self.add_error(task['name'] + '_duration', 'This field is required.')
                else:
                    total_duration += cleaned_data[task['name'] + '_duration']

                if task['name'] + '_delay' not in cleaned_data:
                    self.add_error(task['name'] + '_delay', 'This field is required.')
                else:
                    total_duration += cleaned_data[task['name'] + '_delay']

                if task['name'] + '_consumption' not in cleaned_data:
                    self.add_error(task['name'] + '_consumption', 'This field is required.')

        total_duration = timedelta(minutes=total_duration)

        current_time = get_current_time(minutes_round=self.microgrid.resolution)

        min_start_time = round_time(self.cleaned_data['min_start_time'], minutes=self.microgrid.resolution)
        min_start_time = LOCAL_TZ.localize(min_start_time.replace(tzinfo=None))
        self.cleaned_data['min_start_time'] = min_start_time

        max_end_time = round_time(self.cleaned_data['max_end_time'], minutes=self.microgrid.resolution)
        max_end_time = LOCAL_TZ.localize(max_end_time.replace(tzinfo=None))
        self.cleaned_data['max_end_time'] = max_end_time

        schedule_start = get_schedule_time(self.microgrid.horizon, reference_time=min_start_time)
        schedule_end = schedule_start + timedelta(minutes=self.microgrid.horizon)

        if min_start_time < current_time:
            self.add_error('min_start_time', 'Start time cannot be before current time.')

        elif min_start_time > max_end_time:
            self.add_error('max_end_time', 'End time cannot be before start time.')

        elif min_start_time + total_duration > max_end_time:
            self.add_error('max_end_time', 'Your washing needs more time.')

        elif min_start_time + total_duration > schedule_end:
            self.add_error('min_start_time',
                           'Your washing duration exceeds the schedule end time (' + schedule_end.strftime(
                               "%Y-%m-%d %H:%M:%S") + ').')

        elif max_end_time > schedule_end:
            self.add_error('max_end_time', 'Your end time exceeds the schedule end time (' + schedule_end.strftime(
                "%Y-%m-%d %H:%M:%S") + ').')


class BaseStructureForm(forms.ModelForm):
    def set_fields(self, microgrid, type, initialize=False):

        if type == 'el':
            self.fields['nodes'].queryset = microgrid.el_node_set()

        elif type == 'gas':
            self.fields['nodes'].queryset = microgrid.gas_node_set()

        elif type == 'heating':
            self.fields['nodes'].queryset = microgrid.heating_node_set()

        elif type == 'cooling':
            self.fields['nodes'].queryset = microgrid.cooling_node_set()

        elif type == 'water':
            self.fields['nodes'].queryset = microgrid.water_node_set()

        if initialize:
            if type == 'el':
                if microgrid.el_node_set().count() == 1:
                    self.fields['nodes'].initial = microgrid.el_node_set()

            elif type == 'gas':
                if microgrid.gas_node_set().count() == 1:
                    self.fields['nodes'].initial = microgrid.gas_node_set()

            elif type == 'heating':
                if microgrid.heating_node_set().count() == 1:
                    self.fields['nodes'].initial = microgrid.heating_node_set()

            elif type == 'cooling':
                if microgrid.cooling_node_set().count() == 1:
                    self.fields['nodes'].initial = microgrid.cooling_node_set()

            elif type == 'water':
                if microgrid.water_node_set().count() == 1:
                    self.fields['nodes'].initial = microgrid.water_node_set()


class StorageForm(BaseStructureForm):
    tag = 'storage'

    class Meta:
        model = Storage
        fields = ['name', 'description', 'size', 'initial_charge', 'nodes']

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)
        if initialize:
            self.fields['name'].initial = type.title() + ' Storage ' + str(microgrid.storage_set.count() + 1)
            self.fields['description'].initial = type.title() + ' Storage'

    def save(self, commit=True):
        instance = super(StorageForm, self).save(commit=False)
        instance.charge_efficiency = DEF_STORAGE_CHARGE_EFF
        instance.discharge_efficiency = DEF_STORAGE_DISCHARGE_EFF

        if commit:
            instance.save()
        return instance


class ProductionForm(BaseStructureForm):
    tag = 'production'

    class Meta:
        model = Production
        fields = ['name', 'description', 'max', 'nodes', 'profile']

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)
        if initialize:
            self.fields['name'].initial = type.title() + ' Production ' + str(microgrid.production_set.count() + 1)
            self.fields['description'].initial = type.title() + ' Production'

        profiles = get_profiles(structure_type=type, structure_tag=self.tag, microgrid=microgrid)
        if len(profiles) > 0:
            self.fields['profile'].initial = profiles[0]['name']
        else:
            self.fields['profile'].initial = 'No profile selectable.'


class SellerForm(BaseStructureForm):
    tag = 'seller'

    class Meta:
        model = Seller
        fields = ['name', 'description', 'max', 'nodes', 'profile']

    def __init__(self, *args, **kwargs):
        super(SellerForm, self).__init__(*args, **kwargs)
        self.fields['profile'].widget.attrs['readonly'] = True

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)
        if initialize:
            self.fields['name'].initial = type.title() + ' Seller ' + str(microgrid.seller_set.count() + 1)
            self.fields['description'].initial = type.title() + ' Seller'

            profiles = get_profiles(structure_type=type, structure_tag=self.tag, microgrid=microgrid)
            if len(profiles) > 0:
                self.fields['profile'].initial = profiles[0]['name']
            else:
                self.fields['profile'].initial = 'No profile selectable.'


class PurchaserForm(BaseStructureForm):
    tag = 'purchaser'

    class Meta:
        model = Purchaser
        fields = ['name', 'description', 'max', 'nodes', 'profile']

    def __init__(self, *args, **kwargs):
        super(PurchaserForm, self).__init__(*args, **kwargs)
        self.fields['profile'].widget.attrs['readonly'] = True

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)
        if initialize:
            self.fields['name'].initial = type.title() + ' Purchaser ' + str(microgrid.purchaser_set.count() + 1)
            self.fields['description'].initial = type.title() + ' Purchaser'

            profiles = get_profiles(structure_type=type, structure_tag=self.tag, microgrid=microgrid)
            if len(profiles) > 0:
                self.fields['profile'].initial = profiles[0]['name']
            else:
                self.fields['profile'].initial = 'No profile selectable.'


class LoadForm(BaseStructureForm):
    tag = 'load'

    class Meta:
        model = Load
        fields = ['name', 'description', 'max', 'nodes', 'profile']

    def __init__(self, *args, **kwargs):
        super(LoadForm, self).__init__(*args, **kwargs)
        self.fields['profile'].widget.attrs['readonly'] = True

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)

        if initialize:
            self.fields['name'].initial = type.title() + ' Load ' + str(
                microgrid.load_set.filter(type=type).count() + 1)
            self.fields['description'].initial = type.title() + ' Load'

            profiles = get_profiles(structure_type=type, structure_tag=self.tag, microgrid=microgrid)
            if len(profiles) > 0:
                self.fields['profile'].initial = profiles[0]['name']
            else:
                self.fields['profile'].initial = 'No profile selectable.'

    def save(self, commit=True):
        instance = super(LoadForm, self).save(commit=False)

        if commit:
            instance.save()
        return instance


class EpLoadForm(BaseStructureForm):
    tag = 'ep_load'

    class Meta:
        model = EpLoad
        fields = ['name', 'description', 'idf', 'max', 'nodes']

    def set_fields(self, microgrid, type, initialize=False):
        super().set_fields(microgrid, type, initialize)
        if initialize:
            self.fields['name'].initial = type.title() + ' Ep Load ' + str(
                microgrid.epload_set.filter(type=type).count() + 1)
            self.fields['description'].initial = type.title() + ' Ep Load'

    def save(self, commit=True):
        instance = super(EpLoadForm, self).save(commit=False)

        if commit:
            instance.save()
        return instance


class PipeForm(forms.ModelForm):
    tag = 'pipe'

    class Meta:
        model = Pipe
        fields = ['name', 'input_node', 'output_node', 'max']

    def set_fields(self, microgrid, type, initialize=False):

        if type == 'el':
            self.fields['input_node'].queryset = microgrid.el_node_set()
            self.fields['output_node'].queryset = microgrid.el_node_set()

        elif self.type == 'gas':
            self.fields['input_node'].queryset = microgrid.gas_node_set()
            self.fields['output_node'].queryset = microgrid.gas_node_set()

        elif self.type == 'heating':
            self.fields['input_node'].queryset = microgrid.heating_node_set()
            self.fields['output_node'].queryset = microgrid.heating_node_set()

        elif self.type == 'cooling':
            self.fields['input_node'].queryset = microgrid.cooling_node_set()
            self.fields['output_node'].queryset = microgrid.cooling_node_set()

        elif self.type == 'water':
            self.fields['input_node'].queryset = microgrid.water_node_set()
            self.fields['output_node'].queryset = microgrid.water_node_set()

        if initialize:
            if type == 'el':
                self.fields['name'].initial = 'El. Pipe ' + str(microgrid.el_pipe_set().count() + 1)

            elif type == 'gas':
                self.fields['name'].initial = 'Gas Pipe ' + str(microgrid.gas_pipe_set().count() + 1)

            elif type == 'heating':
                self.fields['name'].initial = 'Heating Pipe ' + str(microgrid.heating_pipe_set().count() + 1)

            elif type == 'cooling':
                self.fields['name'].initial = 'Cooling Pipe ' + str(microgrid.cooling_pipe_set().count() + 1)

            elif type == 'water':
                self.fields['name'].initial = 'Water Pipe ' + str(microgrid.water_pipe_set().count() + 1)

    def clean(self):
        cleaned_data = super(PipeForm, self).clean()
        input_node = cleaned_data.get("input_node")
        output_node = cleaned_data.get("output_node")

        if input_node == output_node:
            self.add_error('input_node', 'Input and output must be different.')
            self.add_error('output_node', 'Input and output must be different.')


class HeaterForm(forms.ModelForm):
    tag = 'heater'

    class Meta:
        model = Generator
        fields = ['name', 'input_nodes', 'output_nodes', 'max']

    def set_fields(self, microgrid, type, initialize):

        if type == 'el':
            self.fields['input_nodes'].queryset = microgrid.el_node_set()

        elif type == 'gas':
            self.fields['input_nodes'].queryset = microgrid.gas_node_set()

        self.fields['output_nodes'].queryset = microgrid.heating_node_set() | microgrid.water_node_set()

        if initialize:
            if type == 'el':
                if microgrid.el_node_set().count() == 1:
                    self.fields['input_nodes'].initial = microgrid.el_node_set()

                self.fields['name'].initial = 'El. Boiler ' + str(microgrid.el_generator_set().count() + 1)

            elif type == 'gas':
                if microgrid.gas_node_set().count() == 1:
                    self.fields['input_nodes'].initial = microgrid.gas_node_set()

                self.fields['name'].initial = 'Gas Heater ' + str(microgrid.el_generator_set().count() + 1)

    def clean(self):
        cleaned_data = super(HeaterForm, self).clean()
        output_nodes = cleaned_data.get("output_nodes")

        output_type = output_nodes[0].type
        for node in output_nodes:
            if node.type != output_type:
                self.add_error('output_nodes', 'Output nodes must be of same type.')

    def save(self, commit=True):
        instance = super(HeaterForm, self).save(commit=False)
        if instance.type == 'el':
            instance.efficiency = DEF_EL_HEATER_EFF
        elif instance.type == 'gas':
            instance.efficiency = DEF_GAS_HEATER_EFF

        if commit:
            instance.save()
        return instance


class CoolerForm(forms.ModelForm):
    tag = 'cooler'

    class Meta:
        model = Generator
        fields = ['name', 'input_nodes', 'output_nodes', 'max']

    def set_fields(self, microgrid, type, initialize):

        if type == 'el':
            self.fields['input_nodes'].queryset = microgrid.el_node_set()

        elif type == 'gas':
            self.fields['input_nodes'].queryset = microgrid.gas_node_set()

        self.fields['output_nodes'].queryset = microgrid.cooling_node_set()

        if initialize:
            if type == 'el':
                if microgrid.el_node_set().count() == 1:
                    self.fields['input_nodes'].initial = microgrid.el_node_set()

                self.fields['name'].initial = 'El. Cooler ' + str(microgrid.el_generator_set().count() + 1)

            elif type == 'gas':
                if microgrid.gas_node_set().count() == 1:
                    self.fields['input_nodes'].initial = microgrid.gas_node_set()

                self.fields['name'].initial = 'Gas Cooler ' + str(microgrid.el_generator_set().count() + 1)

            if microgrid.cooling_node_set().count() == 1:
                self.fields['output_nodes'].initial = microgrid.cooling_node_set()

    def save(self, commit=True):
        instance = super(CoolerForm, self).save(commit=False)
        if instance.type == 'el':
            instance.efficiency = DEF_EL_COOLER_EFF
        elif instance.type == 'gas':
            instance.efficiency = DEF_GAS_COOLER_EFF

        if commit:
            instance.save()
        return instance


class TrigeneratorForm(forms.ModelForm):
    tag = 'trigenerator'

    class Meta:
        model = Trigenerator
        fields = ['name', 'input_nodes', 'output_nodes_ele', 'output_nodes_heat', 'output_nodes_chill', 'max']

    def set_fields(self, microgrid, type, initialize=False):

        if type == 'gas':
            self.fields['input_nodes'].queryset = microgrid.gas_node_set()
        else:
            pass  # TO ADD IF OTHER INPUTS ARE TO BE USED

        self.fields['output_nodes_ele'].queryset = microgrid.el_node_set()
        self.fields['output_nodes_heat'].queryset = microgrid.heating_node_set()
        self.fields['output_nodes_chill'].queryset = microgrid.cooling_node_set()

        if initialize:
            if type == 'gas':
                self.fields['name'].initial = 'Gas Trigenerator ' + str(microgrid.gas_trigenerator_set().count() + 1)
            else:
                pass  # TO ADD IF OTHER INPUTS ARE TO BE USED

            if microgrid.gas_node_set().count() == 1:
                self.fields['input_nodes'].initial = microgrid.gas_node_set()

            if microgrid.el_node_set().count() == 1:
                self.fields['output_nodes_ele'].initial = microgrid.el_node_set()

            if microgrid.heating_node_set().count() == 1:
                self.fields['output_nodes_heat'].initial = microgrid.heating_node_set()

            if microgrid.cooling_node_set().count() == 1:
                self.fields['output_nodes_chill'].initial = microgrid.cooling_node_set()

    def save(self, commit=True):
        instance = super(TrigeneratorForm, self).save(commit=False)
        instance.ele_eff = DEF_TRIGEN_EL_EFF
        instance.heat_eff = DEF_TRIGEN_HEAT_EFF
        instance.chill_eff = DEF_TRIGEN_CHILL_EFF
        if commit:
            instance.save()
        return instance


class CogeneratorForm(forms.ModelForm):
    tag = 'cogenerator'

    class Meta:
        model = Trigenerator
        fields = ['name', 'input_nodes', 'output_nodes_ele', 'output_nodes_heat', 'max']

    def set_fields(self, microgrid, type, initialize=False):

        if type == 'gas':
            self.fields['input_nodes'].queryset = microgrid.gas_node_set()
        else:
            pass  # TO ADD IF OTHER INPUTS ARE TO BE USED

        self.fields['output_nodes_ele'].queryset = microgrid.el_node_set()

        self.fields['output_nodes_heat'].queryset = microgrid.heating_node_set()

        if initialize:
            if type == 'gas':
                self.fields['name'].initial = 'Gas Cogenerator ' + str(microgrid.gas_trigenerator_set().count() + 1)
            else:
                pass  # TO ADD IF OTHER INPUTS ARE TO BE USED

            if microgrid.gas_node_set().count() == 1:
                self.fields['input_nodes'].initial = microgrid.gas_node_set()

            if microgrid.el_node_set().count() == 1:
                self.fields['output_nodes_ele'].initial = microgrid.el_node_set()

            if microgrid.heating_node_set().count() == 1:
                self.fields['output_nodes_heat'].initial = microgrid.heating_node_set()

    def save(self, commit=True):
        instance = super(CogeneratorForm, self).save(commit=False)
        instance.ele_eff = DEF_COGEN_EL_EFF
        instance.heat_eff = DEF_COGEN_HEAT_EFF
        instance.chill_eff = DEF_COGEN_CHILL_EFF
        if commit:
            instance.save()
        return instance


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
