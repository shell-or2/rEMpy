from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(name='filter_1')
@stringfilter
def filter_1(value):
    # filter used to eliminate underscores, replacing them with spaces, and capitalize each word
    value = value.split("_")
    for i in range(0, len(value)):
        value[i] = value[i].title()
    return str.join(" ", value)
