from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.shortcuts import render, get_object_or_404, render_to_response

from .forms import *

from django.http import Http404

from ems_db.schedule.routines import *
from ems_db.sensor.routines import set_alert_status


def ems_message(request):
    if len(EMS_MESSAGES.messages) == 0 or EMS_MESSAGES is None:
        latest_message = 'Connecting to Energy Manager...'
    else:
        latest_message = EMS_MESSAGES.messages[-1]

    return render(request, 'structures/ems_message.html', {'latest_message': latest_message})


def check_ems_alerts(request):
    new_alerts_nr = EMS_ALERTS.check_new_alerts(get_active_alerts())

    return render(request, 'structures/check_ems_alerts.html', {'new_alerts_nr': new_alerts_nr})


def disable_alert(request, alert_id):
    set_alert_status(alert_id=alert_id, alert_status=False)
    EMS_ALERTS.delete_alert(alert_id)
    context = get_index_context(request.user)

    return render(request, 'structures/index.html', context)


def create_microgrid(request):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        form = MicroGridForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            microgrid = form.save(commit=False)
            microgrid.user = request.user
            microgrid.save()
            set_default_structures(microgrid)
            context = {'microgrid': microgrid}
            update_detail_context(context)
            return render(request, 'structures/detail.html', context)
        context = {
            "form": form,
        }
        return render(request, 'structures/create_microgrid.html', context)


def set_default_structures(microgrid):
    for node_type in ['el', 'gas', 'heating', 'cooling', 'water']:
        default_node = Node()
        default_node.microgrid = microgrid
        default_node.type = node_type
        default_node.name = node_type.title()[0] + '-Node 1'
        default_node.save()

    # default_el_purchaser = Purchaser()
    # default_el_purchaser.microgrid = microgrid
    # default_el_purchaser.type = 'el'
    # default_el_purchaser.name = 'Default Electrical Grid'
    # default_el_purchaser.description = 'Default Electrical Grid'
    # default_el_purchaser.tag = 'purchaser'
    # default_el_purchaser.max = 3000.0
    # default_el_purchaser.profile = 'No profile.'
    # default_el_purchaser.save()
    # default_el_purchaser.nodes = microgrid.el_node_set()
    # default_el_purchaser.save()
    #
    # default_gas_purchaser = Purchaser()
    # default_gas_purchaser.microgrid = microgrid
    # default_gas_purchaser.type = 'gas'
    # default_gas_purchaser.name = 'Default Gas Grid'
    # default_gas_purchaser.description = 'Default Gas Grid'
    # default_gas_purchaser.tag = 'purchaser'
    # default_gas_purchaser.max = 3000.0
    # default_gas_purchaser.profile = 'No profile.'
    # default_gas_purchaser.save()
    # default_gas_purchaser.nodes = microgrid.gas_node_set()
    # default_gas_purchaser.save()

    return


def edit_microgrid(request, microgrid_id):
    microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
    form = MicroGridForm(request.POST or None, request.FILES or None, instance=microgrid)
    if form.is_valid():
        form.save()
        context = {'microgrid': microgrid}
        update_detail_context(context)
        return render(request, 'structures/detail.html', context)
    context = {
        'microgrid': microgrid,
        'form': form,
    }
    return render(request, 'structures/edit_structure.html', context)


def delete_microgrid(request, microgrid_id):
    microgrid = MicroGrid.objects.get(pk=microgrid_id)
    microgrid.delete()
    context = get_index_context(request.user)

    return render(request, 'structures/index.html', context)


def export_microgrid(request, microgrid_id):
    for microgrid in MicroGrid.objects.exclude(id=microgrid_id):
        microgrid.is_active = False
        microgrid.save()

    exported = MicroGrid.objects.get(id=microgrid_id)
    exported.is_active = True
    exported.save()

    with open(EMS_JSON_PATH, 'w') as outfile:
        json.dump(exported.to_json(), outfile)

    context = get_index_context(request.user)

    return render(request, 'structures/index.html', context)


def clone_microgrid(request, microgrid_id):
    cloned = MicroGrid.objects.get(id=microgrid_id)
    cloned.duplicate()

    context = get_index_context(request.user)

    return render(request, 'structures/index.html', context)


def create_structure(request, microgrid_id, structure_type, structure_tag):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
        form = get_structure_form(request, structure_tag=structure_tag)
        form.set_fields(microgrid=microgrid, type=structure_type, initialize=True)
        if form.is_valid():
            structure = form.save(commit=False)
            structure.microgrid = microgrid
            structure.tag = structure_tag
            structure.type = structure_type
            structure.save()
            form.save_m2m()
            context = {'microgrid': microgrid}
            update_detail_context(context)
            return render(request, 'structures/detail.html', context)
        context = {
            'microgrid': microgrid,
            'form': form,
        }

        if structure_tag in PROFILED_OBJECTS:
            profiles = get_profiles(structure_type=structure_type, structure_tag=structure_tag, microgrid=microgrid,
                                    output_data=True)

            context.update({'profiles': profiles})

        return render(request, 'structures/add_structure.html', context)


def edit_structure(request, microgrid_id, structure_tag, structure_id):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
        structure = get_structure(microgrid_id=microgrid_id, structure_tag=structure_tag, structure_id=structure_id)

        form = get_structure_form(request, structure_tag=structure.tag, structure=structure)

        form.set_fields(microgrid=microgrid, type=structure.type, initialize=False)

        if form.is_valid():
            form.save()
            context = {'microgrid': microgrid}
            update_detail_context(context)
            return render(request, 'structures/detail.html', context)
        context = {
            'microgrid': microgrid,
            'form': form,
        }

        profiles = get_profiles(structure_type=structure.type, structure_tag=structure_tag, microgrid=microgrid,
                                output_data=True)

    if len(profiles):
        context.update({'profiles': profiles})

    return render(request, 'structures/edit_structure.html', context)


def delete_structure(request, microgrid_id, structure_tag, structure_id):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
        structure = get_structure(microgrid_id=microgrid_id, structure_tag=structure_tag, structure_id=structure_id)
        structure.delete()
        context = {'microgrid': microgrid}
        update_detail_context(context)
        return render(request, 'structures/detail.html', context)


def create_task(request, microgrid_id, appliance_tag, profile_tag):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
        for appliance in APPLIANCES:
            if appliance['name'] == appliance_tag:
                for profile in appliance['profiles']:
                    if profile['name'] == profile_tag:
                        task_list = profile['tasks']
        form = MultiTaskForm(appliance_tag, task_list, request.POST or None, request.FILES or None)
        form.set_fields(microgrid)
        if form.is_valid():
            form.to_tasks()
            context = {'microgrid': microgrid}
            update_detail_context(context)
            return render(request, 'structures/detail.html', context)
        context = {
            'microgrid': microgrid,
            'form': form,
        }
        return render(request, 'structures/add_tasks.html', context)


def get_structure(microgrid_id, structure_tag, structure_id):
    microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
    if structure_tag == 'production':
        structure = microgrid.production_set.get(pk=structure_id)
    elif structure_tag == 'storage':
        structure = microgrid.storage_set.get(pk=structure_id)
    elif structure_tag == 'purchaser':
        structure = microgrid.purchaser_set.get(pk=structure_id)
    elif structure_tag == 'seller':
        structure = microgrid.seller_set.get(pk=structure_id)
    elif structure_tag == 'load':
        structure = microgrid.load_set.get(pk=structure_id)
    elif structure_tag == 'ep_load':
        structure = microgrid.epload_set.get(pk=structure_id)
    elif structure_tag == 'node':
        structure = microgrid.node_set.get(pk=structure_id)
    elif structure_tag == 'pipe':
        structure = microgrid.pipe_set.get(pk=structure_id)
    elif structure_tag == 'heater' or structure_tag == 'cooler':
        structure = microgrid.generator_set.get(pk=structure_id)
    elif structure_tag == 'cogenerator' or structure_tag == 'trigenerator':
        structure = microgrid.trigenerator_set.get(pk=structure_id)
    elif structure_tag == 'task':
        structure = microgrid.task_set.get(pk=structure_id)
    else:
        raise Exception('No valid tag')

    return structure


def get_structure_form(request, structure_tag, structure=None):
    if structure_tag == 'production':
        form = ProductionForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'storage':
        form = StorageForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'purchaser':
        form = PurchaserForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'seller':
        form = SellerForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'load':
        form = LoadForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'ep_load':
        form = EpLoadForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'node':
        form = NodeForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'pipe':
        form = PipeForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'heater':
        form = HeaterForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'cooler':
        form = CoolerForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'cogenerator':
        form = CogeneratorForm(request.POST or None, request.FILES or None, instance=structure)
    elif structure_tag == 'trigenerator':
        form = TrigeneratorForm(request.POST or None, request.FILES or None, instance=structure)
    else:
        raise Exception('No valid tag')

    return form


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'structures/login.html', context)


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                context = get_index_context(request.user)
                return render(request, 'structures/index.html', context)
            else:
                return render(request, 'structures/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'structures/login.html', {'error_message': 'Invalid login'})
    return render(request, 'structures/login.html')


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                context = get_index_context(request.user)
                return render(request, 'structures/index.html', context)
    context = {
        "form": form,
    }
    return render(request, 'structures/register.html', context)


def index(request):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrids = MicroGrid.objects.filter(user=request.user)
        for microgrid in microgrids:
            if microgrid.is_active:
                microgrid.check_modifications(EMS_JSON_PATH)

        context = get_index_context(request.user)

        return render(request, 'structures/index.html', context)


def detail(request, microgrid_id):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        microgrid = get_object_or_404(MicroGrid, pk=microgrid_id)
        context = {'microgrid': microgrid}
        update_detail_context(context)
        return render(request, 'structures/detail.html', context)


def graphs(request, schedule_id):
    if not request.user.is_authenticated():
        return render(request, 'structures/login.html')
    else:
        try:
            microgrid = get_object_or_404(MicroGrid, is_active=True, user=request.user)

            requested_schedule = None
            schedules = cache_schedules(last_schedule_id=0)
            schedules.reverse()

            if schedules:
                if int(schedule_id) == 0:
                    requested_schedule = schedules[0]
                    for schedule in schedules:
                        if schedule['issued_time'] > requested_schedule['issued_time']:
                            requested_schedule = schedule
                else:
                    for schedule in schedules:
                        if schedule['id'] == int(schedule_id):
                            requested_schedule = schedule

                if requested_schedule:
                    data = get_schedule_rvalue(requested_schedule['id']).get('rvalue')
                    clean_none(data, 0)

                    context_data = dict()
                    for device in data.get('devices'):
                        context_data.update(
                            {device.get('tag') + 's': (context_data.get(device.get('tag') + 's') or []) + [device]})

                    # each key-tag has to contain, at least, an empty list
                    tags = ['loads', 'productions', 'pipes', 'storages', 'sellers', 'purchasers', 'gens', 'trigens']
                    for tag in [tag for tag in tags if tag not in context_data.keys()]:
                        context_data.update({tag: []})

                    context_data.update({'tasks': data.get('tasks') or []})

                    context = {'microgrid': microgrid, 'schedules': schedules, 'data': context_data,
                               'requested_schedule': requested_schedule}

                    return render(request, 'structures/graphs.html', context)
                else:
                    raise ValueError('Requested schedule not found.')

            else:
                return render(request, 'structures/graphs.html', {'microgrid': microgrid})

        except Http404:
            return render(request, 'structures/graphs.html')


def get_index_context(user):
    context = {}

    EMS_ALERTS.add_alerts(get_active_alerts())

    microgrids = MicroGrid.objects.filter(user=user)

    if not microgrids:
        message = 'no microgrid found, add one from the top right button'
        context.update({'message': message})
    elif not MicroGrid.objects.filter(user=user, is_active=True):
        message = 'no active microgrid found, apply one to visualize alerts and messages'
        context.update({'message': message, 'microgrids': microgrids})
    else:
        context.update({'alerts': EMS_ALERTS.alerts, 'messages': EMS_MESSAGES.messages, 'microgrids': microgrids})

    return context


def update_detail_context(context):
    context.update({'appliances': APPLIANCES})
