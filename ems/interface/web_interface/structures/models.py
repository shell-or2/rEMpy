from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models

from interface.web_interface.web_interface_defines import *

from datetime import timedelta


class MicroGrid(models.Model):
    user = models.ForeignKey(User, default=1)
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    is_modified = models.BooleanField(default=False)

    location = models.CharField(max_length=100)
    latitude = models.FloatField()
    longitude = models.FloatField()

    horizon = models.IntegerField()
    resolution = models.IntegerField()

    def get_absolute_url(self):
        return reverse('structures:detail', kwargs={'pk': self.pk})

    def to_json(self):
        data = {"settings": {
            "general": {
                "id": "microgrid-" + str(self.id),
                "name": self.name,
                "location": self.location,
                "position": [str(self.latitude), str(self.longitude)],
                "resolution": 15,
                "horizon": 360,
                "db": {
                    "type": "file",
                    "name": "stored_data.mjson"
                }
            },
            "module": {
                "thermal": {
                    "timeout": 300
                },
                "prediction": {
                    "pv_module": "default",
                    "weather_module": "default",
                    "load_module": "default",
                    "timeout": 300
                },
                "wheather": {},
                "interface": {
                    "interface_module": "default",
                    "timeout": 300
                },
                "scheduler": {
                    "scheduler_module": "default",
                    "timeout": 300
                }
            }},
            "devices": [],
            "tasks": [],
            "commonData": {
                "weatherData": {}
            }
        }

        for storage in self.storage_set.all():
            data['devices'] += [storage.to_dictionary()]

        for production in self.production_set.all():
            data['devices'] += [production.to_dictionary()]

        for seller in self.seller_set.all():
            data['devices'] += [seller.to_dictionary()]

        for purchaser in self.purchaser_set.all():
            data['devices'] += [purchaser.to_dictionary()]

        for load in self.load_set.all():
            data['devices'] += [load.to_dictionary()]

        for pipe in self.pipe_set.all():
            data['devices'] += [pipe.to_dictionary()]

        for generator in self.generator_set.all():
            data['devices'] += [generator.to_dictionary()]

        for trigenerator in self.trigenerator_set.all():
            data['devices'] += [trigenerator.to_dictionary()]

        for task in self.task_set.all():
            data['tasks'] += [task.to_dictionary()]

        return data

    def el_node_set(self):
        return self.node_set.filter(type='el')

    def gas_node_set(self):
        return self.node_set.filter(type='gas')

    def heating_node_set(self):
        return self.node_set.filter(type='heating')

    def cooling_node_set(self):
        return self.node_set.filter(type='cooling')

    def water_node_set(self):
        return self.node_set.filter(type='water')

    def el_storage_set(self):
        return self.storage_set.filter(type='el')

    def gas_storage_set(self):
        return self.storage_set.filter(type='gas')

    def heating_storage_set(self):
        return self.storage_set.filter(type='heating')

    def water_storage_set(self):
        return self.storage_set.filter(type='water')

    def el_production_set(self):
        return self.production_set.filter(type='el')

    def gas_production_set(self):
        return self.production_set.filter(type='gas')

    def el_load_set(self):
        return self.load_set.filter(type='el')

    def gas_load_set(self):
        return self.load_set.filter(type='gas')

    def heating_load_set(self):
        return self.load_set.filter(type='heating')

    def cooling_load_set(self):
        return self.load_set.filter(type='cooling')

    def water_load_set(self):
        return self.load_set.filter(type='water')

    def el_purchaser_set(self):
        return self.purchaser_set.filter(type='el')

    def gas_purchaser_set(self):
        return self.purchaser_set.filter(type='gas')

    def el_seller_set(self):
        return self.seller_set.filter(type='el')

    def el_pipe_set(self):
        return self.pipe_set.filter(type='el')

    def gas_pipe_set(self):
        return self.pipe_set.filter(type='gas')

    def heating_pipe_set(self):
        return self.pipe_set.filter(type='heating')

    def cooling_pipe_set(self):
        return self.pipe_set.filter(type='cooling')

    def water_pipe_set(self):
        return self.pipe_set.filter(type='water')

    def gas_trigenerator_set(self):
        return self.trigenerator_set.filter(type='gas')

    def el_generator_set(self):
        return self.generator_set.filter(type='el')

    def gas_generator_set(self):
        return self.generator_set.filter(type='gas')

    def duplicate(self):
        kwargs = {}
        for field in self._meta.fields:
            kwargs[field.name] = getattr(self, field.name)
        kwargs.pop('id')
        kwargs['is_active'] = False
        new_microgrid = self.__class__(**kwargs)
        new_microgrid.save()

        node_id_map = {}
        for node in self.node_set.all():
            old_pk = node.id
            node.id = None
            node.microgrid = new_microgrid
            node.save()
            node_id_map.update({str(old_pk): node.id})

        for purchaser in self.purchaser_set.all():
            old_pk = purchaser.id
            purchaser.id = None
            purchaser.microgrid = new_microgrid
            purchaser.save()

            purchaser.nodes.clear()
            old = Purchaser.objects.get(pk=old_pk)
            for node in old.nodes.all():
                purchaser.nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            purchaser.save()

        for seller in self.seller_set.all():
            old_pk = seller.id
            seller.id = None
            seller.microgrid = new_microgrid
            seller.save()

            seller.nodes.clear()
            old = Seller.objects.get(pk=old_pk)
            for node in old.nodes.all():
                seller.nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            seller.save()

        for load in self.load_set.all():
            old_pk = load.id
            load.id = None
            load.microgrid = new_microgrid
            load.save()

            load.nodes.clear()
            old = Load.objects.get(pk=old_pk)
            for node in old.nodes.all():
                load.nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            load.save()

        for production in self.production_set.all():
            old_pk = production.id
            production.id = None
            production.microgrid = new_microgrid
            production.save()

            production.nodes.clear()
            old = Production.objects.get(pk=old_pk)
            for node in old.nodes.all():
                production.nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            production.save()

        for storage in self.storage_set.all():
            old_pk = storage.id
            storage.id = None
            storage.microgrid = new_microgrid
            storage.save()

            storage.nodes.clear()
            old = Storage.objects.get(pk=old_pk)
            for node in old.nodes.all():
                storage.nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            storage.save()

        for pipe in self.pipe_set.all():
            old_pk = pipe.id
            pipe.id = None
            pipe.microgrid = new_microgrid
            pipe.save()

            old = Pipe.objects.get(pk=old_pk)
            pipe.input_node = Node.objects.get(pk=node_id_map[str(old.input_node.id)])
            pipe.output_node = Node.objects.get(pk=node_id_map[str(old.output_node.id)])

            pipe.save()

        for generator in self.generator_set.all():
            old_pk = generator.id
            generator.id = None
            generator.microgrid = new_microgrid
            generator.save()

            old = Generator.objects.get(pk=old_pk)
            for node in old.input_nodes.all():
                generator.input_nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            for node in old.output_nodes.all():
                generator.output_nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            generator.save()

        for trigenerator in self.trigenerator_set.all():
            old_pk = trigenerator.id
            trigenerator.id = None
            trigenerator.microgrid = new_microgrid
            trigenerator.save()

            old = Trigenerator.objects.get(pk=old_pk)
            for node in old.input_nodes.all():
                trigenerator.input_nodes.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            for node in old.output_nodes_ele.all():
                trigenerator.output_nodes_ele.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            for node in old.output_nodes_heat.all():
                trigenerator.output_nodes_heat.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            for node in old.output_nodes_chill.all():
                trigenerator.output_nodes_chill.add(Node.objects.get(pk=node_id_map[str(node.id)]))

            trigenerator.save()

        return

    def __str__(self):
        return self.name + ' - ' + self.location

    def check_modifications(self, json_path):
        if os.path.isfile(json_path):
            with open(json_path) as data_file:
                data = json.load(data_file)

            current_grid = self.to_json()

            self.is_modified = False

            # Check settings
            if current_grid['settings'] != data['settings']:
                self.is_modified = True
                return

            # Check devices
            if len(current_grid['devices']) > 0 and len(data['devices']) > 0 and len(current_grid['devices']) == len(
                    data['devices']):
                dev_ids_1 = []
                dev_ids_2 = []
                for i in range(len(current_grid['devices'])):
                    dev_ids_1.append(current_grid['devices'][i]['id'])
                    dev_ids_2.append(data['devices'][i]['id'])

                if sorted(dev_ids_1) != sorted(dev_ids_2):
                    self.is_modified = True
                    return

            else:
                self.is_modified = True
                return

            # Check tasks
            if len(current_grid['tasks']) > 0 and len(data['tasks']) > 0 and len(current_grid['tasks']) == len(
                    data['tasks']):
                task_ids_1 = []
                task_ids_2 = []
                for i in range(len(current_grid['tasks'])):
                    task_ids_1.append(current_grid['tasks'][i]['id'])
                    task_ids_2.append(data['tasks'][i]['id'])

                if sorted(task_ids_1) != sorted(task_ids_2):
                    self.is_modified = True
                    return

            else:
                self.is_modified = True
                return

        else:
            self.is_active = True


class Node(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, default='node')
    type = models.CharField(max_length=30)

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Pipe(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, default='pipe')
    type = models.CharField(max_length=30)

    input_node = models.ForeignKey(Node, related_name='pipe_input_node')
    output_node = models.ForeignKey(Node, related_name='pipe_output_node')
    name = models.CharField(max_length=30)
    max = models.FloatField()

    def to_dictionary(self):
        data = {"id": "pipe-" + str(self.id),
                "tag": "pipe",
                "name": self.name,
                "connectedTo": [self.input_node, self.output_node],
                "specs": {"max": self.max}
                }

        return data

    def __str__(self):
        return self.name


class Generator(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, default='generator')
    type = models.CharField(max_length=30)

    input_nodes = models.ManyToManyField(Node, related_name='gen_input_nodes')
    output_nodes = models.ManyToManyField(Node, related_name='gen_output_nodes')
    name = models.CharField(max_length=30)
    max = models.FloatField()
    efficiency = models.FloatField(default=0.)

    def to_dictionary(self):
        input_nodes_list = []
        output_nodes_list = []

        for node in self.input_nodes.all():
            input_nodes_list.append("node-" + str(node.id))

        for node in self.output_nodes.all():
            output_nodes_list.append("node-" + str(node.id))

        data = {"id": "gen-" + str(self.id),
                "tag": "gen",
                "name": self.name,
                "connectedTo": [input_nodes_list, output_nodes_list],
                "specs": {"max": self.max,
                          "eff": self.efficiency}
                }

        return data

    def __str__(self):
        return self.name


class Trigenerator(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, default='trigenerator')
    type = models.CharField(max_length=30, default='gas')

    input_nodes = models.ManyToManyField(Node, related_name='trigen_input_nodes')
    output_nodes_ele = models.ManyToManyField(Node, related_name='trigen_output_nodes_ele')
    output_nodes_heat = models.ManyToManyField(Node, related_name='trigen_output_nodes_heat')
    output_nodes_chill = models.ManyToManyField(Node, related_name='trigen_output_nodes_chill')
    name = models.CharField(max_length=30)
    max = models.FloatField()
    ele_eff = models.FloatField()
    heat_eff = models.FloatField()
    chill_eff = models.FloatField()

    def to_dictionary(self):
        input_nodes_list = []
        output_nodes_ele_list = []
        output_nodes_heat_list = []
        output_nodes_chill_list = []

        for node in self.input_nodes.all():
            input_nodes_list.append("node-" + str(node.id))

        for node in self.output_nodes_ele.all():
            output_nodes_ele_list.append("node-" + str(node.id))

        for node in self.output_nodes_heat.all():
            output_nodes_heat_list.append("node-" + str(node.id))

        for node in self.output_nodes_chill.all():
            output_nodes_chill_list.append("node-" + str(node.id))

        data = {"id": "trigen-" + str(self.id),
                "tag": "trigen",
                "name": self.name,
                "connectedTo": [input_nodes_list, output_nodes_ele_list, output_nodes_heat_list,
                                output_nodes_chill_list],
                "specs": {"max": self.max,
                          "ele": self.ele_eff,
                          "chill": self.chill_eff,
                          "heat": self.heat_eff}
                }

        return data

    def __str__(self):
        return self.name


class Storage(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'Gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30, default='storage')

    size = models.FloatField()
    charge_efficiency = models.FloatField()
    discharge_efficiency = models.FloatField()
    initial_charge = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        data = {"id": "storage-" + str(self.id),
                "tag": "storage",
                "name": self.name,
                "description": self.description,
                "connectedTo": connected_nodes,
                "specs": {"size": self.size,
                          "charge_eff": self.charge_efficiency,
                          "discharge_eff": self.discharge_efficiency
                          },
                "data": {
                    "init_charge": self.initial_charge,
                    "levels": [],
                    "charge_activities": [],
                    "discharge_activities": []
                }
                }

        return data


class Production(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30)

    profile = models.CharField(max_length=30)

    max = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        if self.type == 'el':
            prod_id = 'pv-' + str(self.id)
        else:
            prod_id = 'prod-' + str(self.id)

        data = {"id": prod_id,
                "tag": "prod",
                "name": self.name,
                "description": self.description,
                "connectedTo": connected_nodes,
                "specs": {
                    "max": self.max
                },
                "data": {"production": []
                         }
                }

        return data


class Seller(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'Gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30)

    profile = models.CharField(max_length=30)

    max = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        profiles = get_profiles(structure_type=self.type, structure_tag=self.tag, microgrid=self.microgrid,
                                output_data=True)

        if len(profiles) > 0:
            for profile in profiles:
                if profile['name'] == self.profile:
                    prices = profile['levels']
        else:
            prices = []

        data = {"id": "seller-" + str(self.id),
                "tag": "seller",
                "name": self.name,
                "connectedTo": connected_nodes,
                "specs": {
                    "max": self.max,
                    "selling_prices": prices
                },
                "data": {"sales": []}
                }

        return data


class Purchaser(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30)

    profile = models.CharField(max_length=30)

    max = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        profiles = get_profiles(structure_type=self.type, structure_tag=self.tag, microgrid=self.microgrid,
                                output_data=True)

        if len(profiles) > 0:
            for profile in profiles:
                if profile['name'] == self.profile:
                    prices = profile['levels']
        else:
            prices = []

        data = {"id": "purchaser-" + str(self.id),
                "tag": "purchaser",
                "name": self.name,
                "connectedTo": connected_nodes,
                "specs": {
                    "max": self.max,
                    "buying_prices": prices
                },
                "data": {"consumption": []}
                }

        return data


class Load(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30, default='load')

    profile = models.CharField(max_length=30)

    max = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        profiles = get_profiles(structure_type=self.type, structure_tag=self.tag, microgrid=self.microgrid,
                                output_data=True)

        if len(profiles) > 0:
            for profile in profiles:
                if profile['name'] == self.profile:
                    loads = profile['levels']
        else:
            loads = []

        data = {"id": "load-" + str(self.id),
                "tag": "load",
                "name": self.name,
                "description": self.description,
                "connectedTo": connected_nodes,
                "specs": {"max": self.max},
                "data": {"loads": loads}
                }

        return data


class EpLoad(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    type = models.CharField(max_length=100)  # one of 'el', 'gas', 'heating' or 'water'
    nodes = models.ManyToManyField(Node)
    tag = models.CharField(max_length=30, default='load')
    idf = models.CharField(max_length=100, default='')

    max = models.FloatField()

    def to_dictionary(self):
        connected_nodes = []
        for node in self.nodes.all():
            connected_nodes.append("node-" + str(node.id))

        data = {"id": "ep_load-" + str(self.id),
                "tag": "load",
                "name": self.name,
                "description": self.description,
                "connectedTo": connected_nodes,
                "specs": {"max": self.max,
                          "idf": self.idf},
                "data": {"loads": []}
                }

        return data


class Task(models.Model):
    microgrid = models.ForeignKey(MicroGrid, on_delete=models.CASCADE)
    node = models.ForeignKey(Node, on_delete=models.CASCADE)
    tag = models.CharField(max_length=30, default='task')
    name = models.CharField(max_length=30)
    secondary_id = models.IntegerField(default=0)
    slave_id = models.IntegerField(default=0)
    master_id = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)
    delay = models.FloatField(default=0.0)
    consumption = models.FloatField(default=0.0)
    min_start_time = models.DateTimeField()
    max_end_time = models.DateTimeField()

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        if self.slave_id != 0:
            slave_task = Task.objects.get(id=self.slave_id)
            slave_task.delete()
        if self.master_id != 0:
            master_task = Task.objects.get(id=self.master_id)
            master_task.slave_id = 0
            master_task.save()
        super().delete(*args, **kwargs)

    def to_dictionary(self):
        if self.master_id != 0:
            master_task = Task.objects.get(id=self.master_id)
            master_id = "task-" + str(self.master_id) + str(self.id)
            master_duration = master_task.duration
            master_delay = master_task.delay

        else:
            master_id = None
            master_duration = 0
            master_delay = 0

        if self.slave_id:
            slave_task = Task.objects.get(id=self.slave_id)
            slave_id = "task-" + str(self.id) + str(self.slave_id)
            slave_duration = slave_task.duration
            slave_delay = slave_task.delay
        else:
            slave_id = None
            slave_duration = 0
            slave_delay = 0

        forced_slots = []
        allowed_slots = []

        for slot in range(0, self.microgrid.horizon // self.microgrid.resolution):
            forced_slots.append(0)
            current_slot_time = get_current_time() + timedelta(minutes=slot * self.microgrid.resolution)
            if self.min_start_time <= current_slot_time <= self.max_end_time:
                allowed_slots.append(1)
            else:
                allowed_slots.append(0)

        data = {"id": "task-" + str(self.id),
                "tag": "task",
                "connectedTo": ["node-" + str(self.node.id), master_id, slave_id],
                "specs": {"duration": float(self.duration // self.microgrid.resolution),
                          "energy": float(self.consumption // (self.duration // self.microgrid.resolution)),
                          "mduration": float(master_duration // self.microgrid.resolution),
                          "sduration": float(slave_duration // self.microgrid.resolution),
                          "mdelay": float(master_delay // self.microgrid.resolution),
                          "sdelay": float(slave_delay // self.microgrid.resolution),
                          "forced_slots": forced_slots,
                          "allowed_slots": allowed_slots
                          },
                "data": {}
                }

        return data

    def get_total_time(self):
        total_time = self.duration
        if self.master_task:
            total_time += self.master_task.duration
        if self.slave_task:
            total_time += self.slave_task.duration
        return total_time
