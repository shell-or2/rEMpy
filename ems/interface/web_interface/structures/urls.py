from django.conf.urls import url
from . import views


app_name = 'structures'

urlpatterns = [
    # /structures/
    url(r'^$', views.index, name='index'),
    url(r'^ems_message/$', views.ems_message, name='ems_message'),
    url(r'^check_ems_alerts/$', views.check_ems_alerts, name='check_ems_alerts'),
    url(r'^disable_alert/(?P<alert_id>[0-9]+)/$', views.disable_alert, name='disable_alert'),

    url(r'^register/$', views.register, name='register'),
    url(r'^login_user/$', views.login_user, name='login_user'),
    url(r'^logout_user/$', views.logout_user, name='logout_user'),
    url(r'^(?P<microgrid_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^create_microgrid/$', views.create_microgrid, name='create_microgrid'),
    url(r'^(?P<microgrid_id>[0-9]+)/delete_microgrid/$', views.delete_microgrid, name='delete_microgrid'),
    url(r'^(?P<microgrid_id>[0-9]+)/edit_microgrid/$', views.edit_microgrid, name='edit_microgrid'),
    url(r'^(?P<microgrid_id>[0-9]+)/export_microgrid/$', views.export_microgrid, name='export_microgrid'),
    url(r'^(?P<microgrid_id>[0-9]+)/clone_microgrid/$', views.clone_microgrid, name='clone_microgrid'),
    url(r'^(?P<microgrid_id>[0-9]+)/create_structure/(?P<structure_type>[\w\-]+)/(?P<structure_tag>[\w\-]+)/$',
        views.create_structure, name='create_structure'),
    url(r'^(?P<microgrid_id>[0-9]+)/delete_structure/(?P<structure_tag>[\w\-]+)/(?P<structure_id>[0-9]+)/$',
        views.delete_structure, name='delete_structure'),
    url(r'^(?P<microgrid_id>[0-9]+)/edit_structure/(?P<structure_tag>[\w\-]+)/(?P<structure_id>[0-9]+)/$',
        views.edit_structure, name='edit_structure'),
    url(r'^(?P<microgrid_id>[0-9]+)/create_task/(?P<appliance_tag>[\w\-]+)/(?P<profile_tag>[\w\-]+)/$',
        views.create_task, name='create_task'),
    url(r'^graphs/(?P<schedule_id>[0-9]+)/$', views.graphs, name='graphs'),
]
