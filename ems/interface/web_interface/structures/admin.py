from django.contrib import admin
from .models import *

admin.site.register(MicroGrid)
admin.site.register(Node)
admin.site.register(Pipe)
admin.site.register(Storage)
admin.site.register(Production)



