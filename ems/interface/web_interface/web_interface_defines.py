from defines import *
from ems_db.sensor.routines import get_active_alerts

from datetime import datetime, timedelta
import pytz
import csv

# ===== Messages from EMS ===== #

MAX_MESSAGES = 10


class EmsMessages:
    messages = []

    def add_message(self, message):
        if len(self.messages) > MAX_MESSAGES:
            self.messages.pop(0)
            self.messages.append(message)
        else:
            self.messages.append(message)

EMS_MESSAGES = EmsMessages()

# ===== Alerts from EMS ===== #


class EmsAlerts:
    alerts = []

    def __init__(self):
        self.alerts = get_active_alerts()

    def check_new_alerts(self, alerts):
        new_alerts_nr = 0

        for alert in alerts:
            found = False
            for old_alert in self.alerts:
                if alert['id'] == old_alert['id']:
                    found = True

            if not found:
                new_alerts_nr += 1

        return new_alerts_nr

    def add_alerts(self, alerts, return_new=False):
        if return_new:
            new_alerts = []

        for alert in alerts:
            found = False
            for old_alert in self.alerts:
                if alert['id'] == old_alert['id']:
                    found = True

            if not found:
                self.alerts.append(alert)
                if return_new:
                    new_alerts.append(alert)

        if return_new:
            return new_alerts

    def delete_alert(self, alert_id):
        for alert_nr, alert in enumerate(self.alerts):
            if alert['id'] == int(alert_id):
                del self.alerts[alert_nr]


EMS_ALERTS = EmsAlerts()

# ===== Static Variables ===== #

TIME_ZONE = 'Europe/Rome'

LOCAL_TZ = pytz.timezone(TIME_ZONE)

DEF_STORAGE_CHARGE_EFF = 0.9
DEF_STORAGE_DISCHARGE_EFF = 0.9

DEF_EL_HEATER_EFF = 0.9
DEF_GAS_HEATER_EFF = 0.9

DEF_EL_COOLER_EFF = 0.9
DEF_GAS_COOLER_EFF = 0.9

DEF_TRIGEN_EL_EFF = 0.3
DEF_TRIGEN_HEAT_EFF = 0.45
DEF_TRIGEN_CHILL_EFF = 0.2

DEF_COGEN_EL_EFF = 0.3
DEF_COGEN_HEAT_EFF = 0.65
DEF_COGEN_CHILL_EFF = 0.

# ===== Static Paths ===== #

EMS_JSON_PATH = os.path.join(EMS_DIR, 'config', 'ems.json')

WEB_INTERFACE_DIR = os.path.join(EMS_DIR, 'interface', 'web_interface')

PROFILES_DIR = os.path.join(WEB_INTERFACE_DIR, 'profiles')

PROFILED_OBJECTS = ['load', 'production']

# ===== SuperTasks ===== #

APPLIANCES = [{'name': 'washing_machine',
               'profiles': [{'name': 'profile_1',
                             'tasks': [{'name': 'heating',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30},
                                       {'name': 'washing',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30},
                                       {'name': 'centrifuge',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30}
                                       ]},
                            {'name': 'profile_2',
                             'tasks': [{'name': 'heating',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30}]
                             }]
               },

              {'name': 'oven',
               'profiles': [{'name': 'profile_1',
                             'tasks': [{'name': 'heating',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30},
                                       {'name': 'cooking',
                                        'consumption': 1000,
                                        'delay': 0,
                                        'duration': 30}
                                       ]}
                            ]
               }]


def get_schedule_time(horizon, reference_time=None):
    if reference_time is None:
        reference_time = get_current_time()

    reference_midnight = reference_time.replace(hour=0, minute=0, second=0, microsecond=0)

    for schedule_nr in range(1, (1440 // horizon) + 1):
        schedule_start = reference_midnight + timedelta(minutes=(schedule_nr - 1) * horizon)
        if reference_time < schedule_start + timedelta(minutes=horizon):
            return schedule_start


def get_current_time(zone_aware=True, minutes_round=None):
    if zone_aware:
        current_time = datetime.now(pytz.timezone(TIME_ZONE))
    else:
        current_time = datetime.now(pytz.timezone(TIME_ZONE)).replace(tzinfo=None)

    if minutes_round is not None:
        current_time = round_time(current_time, minutes_round)

    return current_time


def make_zone_aware(dt):
    return pytz.utc.localize(dt)


def round_time(dt=None, minutes=1):
    """Round a datetime object to a multiple of a timedelta
    dt : datetime.datetime object, default now.
    dateDelta : timedelta object, we round to a multiple of this, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
            Stijn Nevens 2014 - Changed to use only datetime objects as variables
    """
    date_delta = timedelta(minutes=minutes)
    round_to = date_delta.total_seconds()
    if dt is None:
        dt = datetime.now()

    # Make sure dt and datetime.min have the same timezone
    tzmin = dt.min.replace(tzinfo=dt.tzinfo)

    seconds = (dt - tzmin).seconds
    # // is a floor division, not a comment on following line:
    rounding = (seconds + round_to / 2) // round_to * round_to
    return dt + timedelta(0, rounding - seconds, -dt.microsecond)


def clean_none(dictionary, substitute):
    if isinstance(dictionary, dict):
        for key in dictionary.keys():
            if isinstance(dictionary[key], dict):
                clean_none(dictionary[key], substitute)
            elif isinstance(dictionary[key], list):
                for i, element in enumerate(dictionary[key]):
                    if isinstance(element, dict):
                        clean_none(element, substitute)
                    elif element is None:
                        dictionary[key][i] = substitute
            elif dictionary[key] is None:
                dictionary[key] = substitute
            else:
                pass
    else:
        raise ValueError('No dictionary given.')

    return


def remove_field(dictionary, field_name='data'):
    for key in dictionary.keys():
        if key == field_name:
            del dictionary[key]
        elif isinstance(dictionary[key], dict):
            remove_field(dictionary[key], field_name)

    return dictionary


def ordered(obj):
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj


def get_profiles(structure_type, structure_tag, microgrid, start_time=None, output_data=False):
    if start_time is None:
        start_time = get_current_time(zone_aware=False, minutes_round=15)

    end_time = start_time + timedelta(minutes=microgrid.horizon)

    # Adjustment needed to get 2016 data
    start_time = start_time - timedelta(days=365)
    end_time = end_time - timedelta(days=365)

    start_time = int(start_time.timestamp())
    end_time = int(end_time.timestamp())

    profiles = []
    profiles_dir = os.path.join(PROFILES_DIR, structure_tag+'s')

    if os.path.isdir(profiles_dir):
        for filename in os.listdir(profiles_dir):
            if filename.endswith('.csv') and convert_structure_type(structure_type) == filename.split('_')[0]:
                name = (filename.split(sep='.')[0]).split(sep='_')[1]
                profiles.append({'name': name, 'levels': None})
                if output_data:
                    temp = []
                    with open(os.path.join(profiles_dir, filename)) as csv_file:
                        csv_reader = csv.reader(csv_file)
                        for row in csv_reader:
                            if start_time <= int(row[0]) < end_time:
                                temp.append(row[1])
                    if microgrid.resolution == 15:
                        profiles[-1]['levels'] = temp
                    else:
                        profiles[-1]['levels'] = []
                        for slot in range(0, len(temp)//(microgrid.resolution//15)):
                            temp_sum = 0
                            for i in range(slot*(microgrid.resolution//15), (slot+1)*(microgrid.resolution//15)):
                                temp_sum += float(temp[i])
                            profiles[-1]['levels'].append(str(temp_sum))

                else:
                    profiles[-1]['levels'] = 'Not requested.'

    return profiles


def convert_structure_type(interface_type):
    if interface_type == 'el':
        ems_type = 'ele'
    elif interface_type == 'gas':
        ems_type = 'gas'
    elif interface_type == 'heating':
        ems_type = 'heat'
    elif interface_type == 'cooling':
        ems_type = 'chill'
    else:
        ems_type = 'not recognized'
    return ems_type
