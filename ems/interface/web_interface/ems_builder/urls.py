from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^structures/', include('structures.urls')),
    url(r'^', include('structures.urls')),
]
