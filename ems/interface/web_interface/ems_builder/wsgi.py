"""
WSGI config for ems_builder project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from interface.web_interface.deamons import InterfaceThreads

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ems_builder.settings")

application = get_wsgi_application()

threads = InterfaceThreads()


