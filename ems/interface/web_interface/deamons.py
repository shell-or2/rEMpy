import communication
from interface.web_interface.web_interface_defines import *
from interface.web_interface.structures.models import MicroGrid
from ems_db.sensor.routines import *


def check_message():
    print('Starting message daemon...')
    controller_queues = communication.ControllerQueues(MODULES_NAMES[INTERFACE_QUEUE])
    while True:
        try:
            message_id, message_data = controller_queues.recvMsg(True)
            if message_id:
                if message_id == MSG_ID_DATA_TO_INTERFACE:
                    EMS_MESSAGES.add_message(message_data.get('status'))
                elif message_id == MSG_ID_UPDATE_CONFIG_FILE:
                    try:
                        for microgrid in MicroGrid.objects.all():
                            if microgrid.is_active:
                                with open(EMS_JSON_PATH, 'w') as outfile:
                                    json.dump(microgrid.to_json(), outfile)
                    except:
                        raise EnvironmentError('Impossible to export configuration on manager request.')

                    EMS_MESSAGES.add_message('Configuration exported on manager request.')

            time.sleep(MSG_CHECK_TIME)

        except IOError as err:
            logger.error(err)

            # Interrupt all core actions and try to reconnect
            logger.warning("Clear output messages.")
            logger.info("New connection request.")
            # loop until new connection success
            EMS_MESSAGES.add_message('Connection lost, trying to reconnect...')
            controller_queues.reconnect()  # blocking, release when connection available


class InterfaceThreads:
    message_thread = threading.Thread(name='message_thread', target=check_message)

    def __init__(self):
        self.message_thread.daemon = True
        self.message_thread.start()
