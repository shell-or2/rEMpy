# EMS builder - *web interface for building your ems system* 

This is the EMS builder official repository.

------------------

## Getting started

* No database comes (yet) with the software, so you will have to create one. To do so, run: 'python manage.py makemigrations' from the web_interface folder, and then run 'python manage.py migrate' to set up the database.

* Run 'python manage.py runserver 127.0.0.1:8000' to open a local http connection. Open a browser page and go to the given ip address, create an account and start using the EMS builder.

------------------

## Requirements

Django is of course required (run 'pip install django' to install it), other dependencies will be added if needed.

Also the Django Rest Framework is required (run 'pip install djangorestframework' to install it).

------------------

## Deploying Django with Apache and mod_wsgi

Ref: https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/modwsgi/

