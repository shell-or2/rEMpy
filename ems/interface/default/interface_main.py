# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import matplotlib
matplotlib.use("TkAgg")

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib import style
from tkinter import *
from defines import *
from ems_structures import *
import logging
logger = logging.getLogger(__name__)

LARGE_FONT = ("Verdana", 12)
style.use("ggplot")

ems_data = EMSData()

# Wait a valid config file
while True:
    ems_data.update()
    if not ems_data.isEmpty():
        break
    time.sleep(1)

geometry = [4, 1]
f = Figure(figsize=(6, 6), dpi=100)

axarr = {}

if geometry[0] == 1 and geometry[1] == 1:
    axarr = f.add_subplot(1, 1, 1)

elif geometry[0] > 1 and geometry[1] > 1:

    for k in range(0, geometry[1]):
        for j in range(0, geometry[0]):
            index = j + (k * geometry[0])
            axarr[j, k] = f.add_subplot(geometry[0], geometry[1], index + 1)

else:
    for k in range(0, geometry[1]):
        for j in range(0, geometry[0]):
            index = j + (k * geometry[0])
            axarr[index] = f.add_subplot(geometry[0], geometry[1], index + 1)

f.subplots_adjust(hspace=1)

# data_to_plot = None
log_msg = None
log_lines = None


class PlotData:
    def __init__(self, x, y, labels, title):
        self.x = x
        self.y = y
        self.labels = labels
        self.ylabel = "Wh"
        self.title = title


class App:
    def __init__(self, master_frame, data_from_core):

        # TODO: set the right access modifiers, now all public

        self.bg1_color = None  # 'red'
        self.bg2_color = None  # 'green'
        self.bg3_color = None  # 'blue'

        self.data_from_core = data_from_core

        master_frame.title("EMS - User Interface")
        # master_label = Label(master_frame, text="Dati monitoraggio e info esecuzione task", bg=self.bg2_color)
        # master_label.pack()

        # create 2 main-frames
        # create activity frame
        activity_frame = Frame(master_frame, height=300, width=400, bg=self.bg3_color, relief=RIDGE, borderwidth=5)
        activity_frame.propagate(0)
        activity_frame.pack(side=LEFT, fill=BOTH, expand=YES)

        activity_label = Label(activity_frame, text="EMS Activity Log", bg=self.bg2_color)
        activity_label.pack(fill=BOTH)

        self.T = Text(activity_frame, state=DISABLED)
        self.T.pack()
        #T.grid(column=0, row=1)

        # create scheduler frame nad put graph inside
        scheduled_task_frame = Frame(master_frame, width=400, height=200, bg=self.bg2_color, relief=RIDGE, borderwidth=5)
        scheduled_task_frame.pack_propagate(0)
        scheduled_task_frame.pack(side=LEFT, fill=BOTH, expand=YES)

        frame = GraphPage(scheduled_task_frame, self)
        frame.grid(row=0, column=0, sticky="nsew")

        # # make the top right close button minimize (iconify) the main window
        # master_frame.protocol("WM_DELETE_WINDOW", master_frame.iconify)
        # master_frame.protocol("WM_DELETE_WINDOW", lambda: update_log('esco'))
        #
        # # make Esc exit the program
        # master_frame.bind('<Escape>', lambda e: master_frame.destroy())

        def save_log_msg():
            global log_msg
            log_msg = self.T.get('1.0', 'end')

            global log_lines
            log_lines = self.line_counter

            master_frame.destroy()

        master_frame.protocol("WM_DELETE_WINDOW", lambda: save_log_msg())

        self.line_counter = 0

        def update_log(msg):

            self.T.config(state="normal")

        # se presente un vecchio log, lo carico e resetto i valori globali a None
            global log_msg
            global log_lines

            if log_msg:
                self.T.insert('end', log_msg)
                self.line_counter = log_lines

                log_msg = None
                log_lines = None

            if self.line_counter == 10:
                self.T.delete('1.0', '2.0')
            else:
                self.line_counter += 1

            self.T.insert('end', msg + "\n")
            self.T.config(state="disabled")

        def check_messages():

            if not self.data_from_core.empty():

                msg = self.data_from_core.get()

                if msg.get('status'):

                    update_log(msg.get('status'))

                if msg.get('alert'):
                    # TODO: display alert
                    update_log('Received alert')

                if msg.get('db'):

                    db_info = msg.get('db')
                    # TODO: check if db or file
                    if db_info.get('type') is 'file':
                        global db_path
                        db_path = db_info.get('name')

                # elif msg.get('grid_load'):
                #
                #     global data_to_plot
                #     data_to_plot = msg
                #
                #     logger.debug('Received data to plot.')
                #     update_log('Received data to plot.')
                #
                # else:
                #     logger.debug('Unsupported message!')

            activity_frame.after(1000, check_messages)

        activity_frame.after(1000, check_messages)


class GraphPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self, parent)
        label = Label(self, text="Graph Page")
        label.pack(pady=10, padx=10)

        # button1 = ttk.Button(self, text="Back to Home",
        #                      command=lambda: controller.show_frame(StartPage))
        # button1.pack()

        canvas = FigureCanvasTkAgg(f, self)
        canvas.show()
        canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)

        toolbar = NavigationToolbar2TkAgg(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=True)


def animate(i):

    from matplotlib.font_manager import FontProperties
    fontP = FontProperties()
    fontP.set_size('small')

    # Abuso
    ems_data.load()  # TODO: inserire passaggio path file e non usare quello di default

    if ems_data.timestamp:  # not empty

        xList = list(range(0, 1 + ems_data.settings.general['horizon'] // ems_data.settings.general['resolution']))

        # ElPurchasers and ElSellers  exchanged energy
        all_el_purchasers_and_selers_data = list()
        all_el_purchasers_and_selers_labels = list()
        all_el_purchasers_and_selers_el_sellers_xlist = list()

        for el_purchaser in ems_data.grid.el_purchasers:
            all_el_purchasers_and_selers_data.append(el_purchaser.get("consumption"))
            all_el_purchasers_and_selers_labels.append(el_purchaser.get_id())
            all_el_purchasers_and_selers_el_sellers_xlist.append(xList)

        for el_seller in ems_data.grid.el_sellers:
            all_el_purchasers_and_selers_data.append(el_seller.get("sales"))
            all_el_purchasers_and_selers_labels.append(el_seller.get_id())
            all_el_purchasers_and_selers_el_sellers_xlist.append(xList)

        plots = [PlotData(all_el_purchasers_and_selers_el_sellers_xlist, all_el_purchasers_and_selers_data,
                          all_el_purchasers_and_selers_labels, "Purchased vs Sold Energy")]
        plots[-1].ylabel = "Wh"

        # Loads and Thermal loads
        all_loads_and_thermals = list()
        all_load_and_thermals_labels = list()
        all_loads_and_thermals_xlist = list()

        for load in ems_data.grid.loads:
            all_loads_and_thermals.append(load.get('loads'))
            all_load_and_thermals_labels.append(load.get_id())
            all_loads_and_thermals_xlist.append(xList)

        for thermal in ems_data.grid.thermals:
            all_loads_and_thermals.append(thermal.get('loads'))
            all_load_and_thermals_labels.append(thermal.get_id())
            all_loads_and_thermals_xlist.append(xList)

        plots += [PlotData(all_loads_and_thermals_xlist, all_loads_and_thermals,
                           all_load_and_thermals_labels, "Total Energy Load")]
        plots[-1].ylabel = "Wh"

        # Photovoltainc production
        pvs_production = list()
        pvs_labels = list()
        pvs_xlist = list()

        for pv in ems_data.grid.photovoltaics:
            pvs_production.append(pv.get('production'))
            pvs_labels.append(pv.get_id())
            pvs_xlist.append(xList)

        plots += [PlotData(pvs_xlist, pvs_production, pvs_labels, "PVs Energy Production")]
        plots[-1].ylabel = "Wh"

        # Storages
        storages_levels = list()
        storages_labels = list()
        storages_xlist = list()

        for storage in ems_data.grid.storages:
            storages_levels.append(storage.get('levels'))
            storages_levels.append(storage.get('charge_activities'))
            storages_levels.append(storage.get('discharge_activities'))
            storages_labels.append(storage.get_id() + " levels")
            storages_labels.append(storage.get_id() + " charges")
            storages_labels.append(storage.get_id() + " discharges")
            storages_xlist.append(xList)  # fix * 3 when activities avail

        plots += [PlotData(storages_xlist, storages_levels, storages_labels, "Storages")]
        plots[-1].ylabel = "Wh"

    else:

        plots = [PlotData([list(range(0,25))], [[1] * 25], ["Empty"], "Empty")]
        plots[-1].ylabel = "empty"

    plots_count = len(plots)

    if geometry[0] == 1 and geometry[1] == 1:
        # axarr = f.add_subplot(1, 1, 1)
        axarr.clear()
        for i in range(0, len(plots[0].x)):
            axarr.plot(plots[0].x[i], plots[0].y[i], label=plots[0].labels[i])
            axarr.set_title(plots[0].title)
            axarr.set_xlabel('Time Slots')
            axarr.set_ylabel(plots[0].ylabel)
            axarr.legend(loc='center', bbox_to_anchor=(1, 1), prop=fontP)

    elif geometry[0] > 1 and geometry[1] > 1:

        for k in range(0, geometry[1]):
            for j in range(0, geometry[0]):
                index = j + (k * geometry[0])
                # axarr[j, k] = f.add_subplot(geometry[0], geometry[1], index + 1)
                axarr[j, k].clear()
                if (index < plots_count):
                    for i in range(0, len(plots[index].x)):
                        axarr[j, k].plot(plots[index].x[i], plots[index].y[i], label=plots[index].labels[i])
                        axarr[j, k].set_title(plots[index].title, fontsize=8)
                        axarr[j, k].set_xlabel('Time Slots', fontsize=8)
                        axarr[j, k].set_ylabel(plots[index].ylabel, fontsize=8)
                        axarr[j, k].legend(loc='lower right', bbox_to_anchor=(1, 1), prop=fontP)
                        axarr[j, k].tick_params(axis='x', labelsize=8)
                        axarr[j, k].tick_params(axis='y', labelsize=8)

    else:
        for k in range(0, geometry[1]):
            for j in range(0, geometry[0]):
                index = j + (k * geometry[0])
                # axarr[index] = f.add_subplot(geometry[0], geometry[1], index + 1)
                axarr[index].clear()
                if index < plots_count:
                    for i in range(0, len(plots[index].x)):
                        # axarr[index].clear()
                        axarr[index].plot(plots[index].x[i], plots[index].y[i], label=plots[index].labels[i])
                        axarr[index].set_title(plots[index].title, fontsize=8)
                        axarr[index].set_xlabel('Time Slots', fontsize=8)
                        axarr[index].set_ylabel(plots[index].ylabel, fontsize=8)
                        axarr[index].legend(loc='lower right', bbox_to_anchor=(1, 1), prop=fontP)
                        axarr[index].tick_params(axis='x', labelsize=8)
                        axarr[index].tick_params(axis='y', labelsize=8)


def interface(data_from_core, data_to_core):

    root = Tk()
    app = App(root, data_from_core)
    ani = animation.FuncAnimation(f, animate, interval=15000)
    root.mainloop()
    # root.destroy()