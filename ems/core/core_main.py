# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import calendar
import datetime
import logging
import queue
import threading
import time
from defines import *
import multiprocessing
from ems_structures import *
import communication
import core.task_agent
from ems_db.sensor.routines import add_alert
logger = logging.getLogger(__name__)


def difftime(epoch_time, threshold_in_minutes):
    """ Compute the difference between the given time and the current one.
        Return true if the difference exceeds the given threshold.
    """

    diff_in_sec = (calendar.timegm(time.gmtime()) - epoch_time)

    if diff_in_sec > threshold_in_minutes*60:
        return True

    return False


def mainprocess():
    """ Main process: manage the scheduler and control the connection queues. """
    # shared_core_to_prediction_q, shared_prediction_to_core_q,
    #           shared_core_to_thermal_q, shared_thermal_to_core_q,
    #           shared_core_to_interface_q, shared_interface_to_core_q):

    # Init Core
    try:

        setup_logging("core")
        core_queues = communication.CoreQueues()
        logger.info('Core running.')

        FIRST_SCHEDULE = True
        REQUESTED_RESCHEDULE = False
        CRITICAL_ERROR = False

        def send_msg_to_interface(text_msg):

            msg = {MSG_ID_DATA_TO_INTERFACE: {'status': text_msg}}
            if EMS_TEST_MODE:
                return 0
            if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
                logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

        # init scheduler thread vars
        output_scheduler_data = queue.Queue()
        stop_event = threading.Event()  # set if the interruption of a module execution is requested, pass as module param.
        timeout_event = threading.Event()  # set if the execution timeout for a module is reached
        scheduler_thread = None

        # Create structure for store exchanged data
        ems_data = EMSData()

        # Wait a valid config file
        while True:

            ems_data.update()
            if not ems_data.isEmpty():
                break

            time.sleep(1)

        ems_data.load()

        ems_scheduling_state = EMSStateClass(EMS_WAIT)
        ems_queue_task_agent = multiprocessing.Queue()  # used ot send list of stoppable tasks
        ems_scheduling_event = ems_scheduling_state.get_event()  # set if overall scheduling is in execution

        task_agent_thread = threading.Thread(name='task_agent', target=core.task_agent.task_agent_thread,
                                             args=(ems_queue_task_agent, ems_scheduling_event,))
        task_agent_thread.daemon = True
        task_agent_thread.start()

        # first of all, send default settings (horizon and resolution) to all the modules
        # msg = {MSG_ID_CONFIG: ems_data.get_temporal_data()}
        #
        # for q in core_queues.queues_number():
        #     logger.debug("Send message to: " + MODULES_NAMES[q])
        #
        #     if EMS_TEST_MODE:
        #         continue
        #
        #     if not core_queues.sendMsg(q, msg, True):
        #         logger.info("Problem with queue: " + MODULES_NAMES[q])
        # send config to interface
        # msg = {MSG_ID_DATA_TO_INTERFACE: {'status': 'Core sending configuration information to Interface',
        #                                   'db_config': {'db_type': 'file', 'path': EMS_STORE_DATA_FILE}}}

        tmp_dict = ems_data.get_data_for_interface()
        tmp_dict.update({'status': 'Core sending configuration information to Interface'})

        msg = {MSG_ID_DATA_TO_INTERFACE: tmp_dict}

        # if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
        #     logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

        # example, set ems_scheduling_state and epoch_timestamp
        # TODO: define scheduler data and process information to save and restore in case of re-spawn
        time.sleep(6)
        send_status_to_interface('hello')
        #######################################################
        ################ MAIN LOOP ############################
        #######################################################
        while True:

            actual_time_minutes = datetime.datetime.now().hour * 60 + datetime.datetime.now().minute
            # execution start form 00:00 and repeat every horizon intervals
            if actual_time_minutes % ems_data.settings.general['horizon'] == 0 or FIRST_SCHEDULE or REQUESTED_RESCHEDULE:
            # if not ems_data.timestamp or difftime(ems_data.timestamp, ems_data.settings.general['horizon']):

                if ems_scheduling_state.isEqual(EMS_WAIT):

                    if not FIRST_SCHEDULE:
                        # compute real indices and update stored value
                        ems_data.compute_real_indices()
                        ems_data.store()

                        # request to interface to update config file
                        msg = {MSG_ID_UPDATE_CONFIG_FILE: None}
                        core_queues.sendMsg(INTERFACE_QUEUE, msg, True)
                        time.sleep(10)  # time to upgrade file to interface
                        ems_data.update()

                    if REQUESTED_RESCHEDULE:
                        REQUESTED_RESCHEDULE = False
                        # set ems properly (remove task, change allowed slot etc
                        ems_data.reschedule_preparation()

                        send_msg_to_interface('Start re-scheduling routine.')
                        logger.info('Start re-scheduling routine.')
                        ems_scheduling_state.set(EMS_SCHEDULING_REQUEST)  # EMS_PREDICTION_REQUEST
                        logger.info(ems_scheduling_state)

                    else:
                        # first schedule or standard schedule
                        ems_data.timestamp = calendar.timegm(time.gmtime())
                        logger.info('Updated epoch start time: ' + str(datetime.datetime.fromtimestamp(ems_data.timestamp))
                                    + ' (Epochs:' + str(ems_data.timestamp) + ')')
                        send_msg_to_interface('Start scheduling routine.')
                        logger.info('Start scheduling routine.')
                        ems_scheduling_state.update()  # EMS_PREDICTION_REQUEST
                        logger.info(ems_scheduling_state)

                else:
                    logger.warning('Scheduler routine in progress, or something is wrong!')
                    # TODO: check why state is on EMS_WAIT

                FIRST_SCHEDULE = False

            # check if received messages in each input queues (controller to core queue)
            for q in core_queues.queues_number():
                message_id, message_data = core_queues.recvMsg(q, True)

                if message_id:
                    # logger.debug("Received message from " + MODULES_NAMES[q] + ": "
                    #  + str(message_id) + ": " + str(message_data))

                    ###########################################################################
                    # evaluate if message from INTERFACE
                    if message_id == MSG_ID_INTERFACE_REQUEST_CONFIG:
                        logger.debug("Received config request from Interface.")

                        msg = {MSG_ID_DATA_TO_INTERFACE: {'status': 'Core sending configuration information to Interface',
                                                            'db_config': {'db_type': 'file',
                                                                          'path': EMS_STORE_DATA_FILE}}}

                        if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
                            logger.error("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

                    ###########################################################################
                    # evaluate if message from PREDICTION
                    if message_id == MSG_ID_PREDICTION_RESPONSE:
                        logger.info("Received prediction data.")

                        # check EMS status and update it
                        if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):

                            # update ems_data with data receiver from prediction
                            # ems_data.set_data(message_data)
                            ems_data.set_data_from_prediction(message_data)

                            ems_scheduling_state.update()  # EMS_THERMAL_REQUEST
                            logger.info(ems_scheduling_state)
                            send_msg_to_interface('Computing prediction...OK!')
                            # cancel timeout timer
                            prediction_timer.cancel()
                        else:
                            logger.warning('Prediction not requested.')

                    if message_id == MSG_ID_DATA_FROM_DIAGNOSTIC:

                        # parse over list of alert data
                        for alert in message_data.get('alerts') or []:  # avoid to loop over NoneType

                            add_alert(alert.get('sensor_name'), alert.get('alert_code'))

                            if alert.get('alert_code') == DIAGNOSTIC_ALERT_ACTUATOR_FAULT:
                                logger.warning('Removing task for fault.')
                                ems_data.grid.remove_task(alert.get('sensor_name'))
                                REQUESTED_RESCHEDULE = True  # if task is removed for actuator fault request rescheduling

                            if alert.get('alert_code')[0] == '2':  # critical alert
                                CRITICAL_ERROR=False

                    if message_id == MSG_ID_DATA_FROM_OVERLOAD_MANAGER:

                        for alert in message_data.get('alerts') or []:  # avoid to loop over NoneType

                            add_alert('overall', alert.get('alert_code'))

                            # request task interruption
                            ems_queue_task_agent.put_nowait(alert.get('tasks'))

                            # correct task duration in ems_data, and request reschedule
                            for task_id in alert.get('tasks'):
                                ems_data.grid.remove_task(task_id)

                            REQUESTED_RESCHEDULE = True

                            if alert.get('alert_code')[0] == '2':  # critical alert
                                CRITICAL_ERROR = False

                    ###########################################################################
                    # evaluate if message from THERMAL
                    if message_id == MSG_ID_THERMAL_RESPONSE:
                        logger.info("Received thermal data.")

                        # check EMS status and update it
                        if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):

                            # update ems_data with data receiver from thermal
                            # ems_data.set_data(message_data)
                            ems_data.set_data_from_thermal(message_data)

                            ems_scheduling_state.update()  # EMS_SCHEDULING_REQUEST
                            logger.info(ems_scheduling_state)
                            send_msg_to_interface('Computing thermal model...OK!')
                            thermal_timer.cancel()
                        else:
                            logger.warning('Thermal modelling not requested.')

            ###########################################################################
            # test for request cancellation
            ###########################################################################
            if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):

                test_kill = False
                if test_kill:
                    time.sleep(2)
                    # request_kill(PREDICTION_QUEUE)
                    # TODO: wait response from module if request cancellation?
                    ems_scheduling_state.reset()  # EMS_WAIT
                    logger.info(ems_scheduling_state)

            if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):

                test_kill = False

                if test_kill:
                    time.sleep(2)
                    # request_kill(THERMAL_QUEUE)
                    # TODO: wait response from module if request cancellation?
                    ems_scheduling_state.reset()  # EMS_WAIT
                    logger.info(ems_scheduling_state)

            ###########################################################################
            ###########################################################################

            # TODO: Send messages to controllers/modules.
            # TODO: Insert a timer for each communication, and delete message from queue when timeout reached.

            if ems_scheduling_state.isEqual(EMS_PREDICTION_REQUEST):
                logger.info("Requesting prediction.")

                # Compose data for prediction
                msg = {MSG_ID_PREDICTION_REQUEST: ems_data.get_data_for_prediction()}  # prediction_data_skeleton

                if core_queues.sendMsg(PREDICTION_QUEUE, msg, True):
                    ems_scheduling_state.update()  # EMS_PREDICTION_WAIT
                    logger.info(ems_scheduling_state)
                    send_msg_to_interface('Computing prediction...')
                    # start timer for max response wait
                    prediction_timer = threading.Timer(ems_data.settings.module.get('prediction').get('timeout'),
                                                       lambda: timeout_event.set())
                    prediction_timer.start()
                else:
                    logger.error("Problem with queue: " + MODULES_NAMES[PREDICTION_QUEUE])

            if ems_scheduling_state.isEqual(EMS_THERMAL_REQUEST):
                logger.info("Requesting thermal modelling.")

                for load in ems_data.grid.get_loads('ep_load'):
                    # load is a class!
                    if not load.get('setpoints'):
                        # if missing setpoints, set default values for current horizon
                        # moved in thermal model
                        #
                        # rooms_names = ['Edificio:Bagno', 'Edificio:Soggiorno', 'Edificio:Camera1', 'Edificio:Camera2', 'Edificio:Cucina']
                        # camera1 matrimoniale
                        # camera2 bambini                        if
                        n_slots = (ems_data.settings.general['horizon'] // ems_data.settings.general['resolution'])
                        htg_setpoint1 = [21] * n_slots
                        htg_setpoint2 = [20] * n_slots
                        htg_setpoint3 = [18] * n_slots
                        htg_setpoint4 = [20] * n_slots
                        htg_setpoint5 = [17] * n_slots

                        clg_setpoint1 = [26] * n_slots
                        clg_setpoint2 = [26] * n_slots
                        clg_setpoint3 = [26] * n_slots
                        clg_setpoint4 = [26] * n_slots
                        clg_setpoint5 = [26] * n_slots

                        load.update({'thermal_zones': ['Edificio:Bagno', 'Edificio:Soggiorno', 'Edificio:Camera1', 'Edificio:Camera2', 'Edificio:Cucina']})
                        load.update(
                            {'setpoints':
                                 {'htg': [htg_setpoint1, htg_setpoint2, htg_setpoint3, htg_setpoint4, htg_setpoint5],
                                  'clg': [clg_setpoint1, clg_setpoint2, clg_setpoint3, clg_setpoint4, clg_setpoint5]}})

                # Compose data for thermal model
                msg = {MSG_ID_THERMAL_REQUEST: ems_data.get_data_for_thermal()}  # thermal_data_skeleton}
                print(msg)

                if core_queues.sendMsg(THERMAL_QUEUE, msg, True):
                    ems_scheduling_state.update()  # EMS_THERMAL_WAIT
                    logger.info(ems_scheduling_state)
                    send_msg_to_interface('Computing thermal model...')
                    thermal_timer = threading.Timer(ems_data.settings.module.get('thermal').get('timeout'),
                                                    lambda: timeout_event.set())
                    thermal_timer.start()
                else:
                    logger.error("Problem with queue: " + MODULES_NAMES[THERMAL_QUEUE])

            if ems_scheduling_state.isEqual(EMS_SCHEDULING_REQUEST):
                logger.info("Requesting scheduling.")
                ems_scheduling_state.update()
                logger.info(ems_scheduling_state)

                # grandezze da ottenere dal framework di interoperabilità
                n_slots = (ems_data.settings.general['horizon'] // ems_data.settings.general['resolution'])

                for purchaser in ems_data.grid.get_purchasers():
                    if not purchaser.get('buying_prices'):
                        purchaser.update({'buying_prices': [1] * n_slots})

                for seller in ems_data.grid.get_sellers():
                    if not seller.get('selling_prices'):
                        seller.update({'selling_prices': [0.5] * n_slots})

                for storage in ems_data.grid.get_storages():
                    if not storage.get("init_charge"):
                        storage.update({"init_charge": 1000})

                for task in ems_data.grid.get_tasks():
                    if not task.get("allowed_slots") or len(task.get("allowed_slots")) is not n_slots:
                        task.update({'allowed_slots': [1] * n_slots})

                    if not task.get("forced_slots") or len(task.get('forced_slots')) is not n_slots:
                        task.update({'forced_slots': [0] * n_slots})

                partner_scheduler = __import__('core.scheduler.' + ems_data.settings.module.get('scheduler')
                                               .get('scheduler_module') + '.scheduler_main', globals(),
                                               locals(), 'prediction', 0)

                input_scheduler_data = ems_data.get_data_for_scheduler()
                logger.debug('Send data to scheduler')
                logger.debug(input_scheduler_data)
                scheduler_thread = threading.Thread(name='scheduler_thread',
                                                    target=partner_scheduler.scheduler,
                                                    args=(input_scheduler_data, output_scheduler_data, stop_event,))
                stop_event.clear()
                scheduler_thread.start()
                send_msg_to_interface('Computing scheduling...')

            if ems_scheduling_state.isEqual(EMS_SCHEDULING_WAIT):
                if scheduler_thread.isAlive():
                    logger.info('Scheduling in progress.')
                else:
                    logger.info('Scheduling complete.')
                    ems_scheduling_state.update()
                    logger.info(ems_scheduling_state)

                    try:
                        logger.info("Received data from scheduler.")
                        ems_data.set_data_from_scheduler(output_scheduler_data.get(block=True, timeout=30))

                    except queue.Empty:
                        logger.error("Not received data from scheduler.")

                    # send_plot_to_interface(ems_data.get_plot_data())

            if ems_scheduling_state.isEqual(EMS_COMPLETE):

                ems_data.store()
                # TODO: how to use the scheduling data?
                ems_scheduling_state.update()  # back to EMS_WAIT
                logger.info(ems_scheduling_state)
                logger.info('EMS Scheduling routine complete!')
                send_msg_to_interface('Computing scheduling...OK!')

            ###########################################################################################################
            ################ TIMEOUTS CONTROL #########################################################################
            ###########################################################################################################

            # prediction wait
            if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):
                if timeout_event.is_set():
                    timeout_event.clear()
                    logger.warning('Prediction WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(PREDICTION_QUEUE)
                    ems_scheduling_state.update()
                    logger.info(ems_scheduling_state)
                    send_msg_to_interface('Computing prediction...FAIL!')

                    # send kill request
                    logger.warning('Requesting kill of: ' + MODULES_NAMES[PREDICTION_QUEUE])
                    msg = {MSG_ID_PREDICTION_CANCEL: None}
                    if core_queues.sendMsg(PREDICTION_QUEUE, msg, True):
                        logger.warning('Requested prediction cancellation.')
                    else:
                        logger.error("Problem with queue: " + MODULES_NAMES[PREDICTION_QUEUE])
            # thermal wait
            if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):
                if timeout_event.is_set():
                    logger.warning('Thermal WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(THERMAL_QUEUE)
                    ems_scheduling_state.update()
                    logger.info(ems_scheduling_state)
                    send_msg_to_interface('Computing thermal model...FAIL!')

                    # send kill request
                    logger.warning('Requesting kill of: ' + MODULES_NAMES[THERMAL_QUEUE])
                    msg = {MSG_ID_THERMAL_CANCEL: None}
                    if core_queues.sendMsg(THERMAL_QUEUE, msg, True):
                        logger.warning('Requested thermal cancellation.')
                    else:
                        logger.error("Problem with queue: " + MODULES_NAMES[THERMAL_QUEUE])

            # scheduler wait
            if ems_scheduling_state.isEqual(EMS_SCHEDULING_WAIT):
                if timeout_event.is_set():
                    logger.warning('Scheduler WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(THERMAL_QUEUE)
                    ems_scheduling_state.update()
                    logger.info(ems_scheduling_state)
                    send_msg_to_interface('Computing scheduling...FAIL!')

                    # send kill request
                    logger.warning('Requesting kill of scheduling-solver.')
                    stop_event.set()

            if ems_scheduling_state.isEqual(EMS_WAIT):

                ems_data.compute_estimated_indices()
                ems_data.store()
                ems_data.update()

            if CRITICAL_ERROR:
                send_msg_to_interface(                logger.critical('Critical error, block EMS routine.')
)
                logger.critical('Critical error, block EMS routine.')
                break

            time.sleep(MSG_CHECK_TIME)

    except KeyboardInterrupt:
        logger.info("Server process exit.")

        sys.exit(0)

    except Exception as err:
        logger.critical("Unhandled exception!")
        logger.critical(err)
        sys.exit(-1)


if __name__ == "__main__":

    mainprocess()
