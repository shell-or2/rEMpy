# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.
from numbers import Number

class RunTimeData:
    def __init__(self, runtime_data):

        if (runtime_data == None):
            self.id = ''
            self.tag = ""
            self.connections = []
            self.filename = ""
            self.energy = 0.0
            self.power = 0.0
            self.capacity = 0.0
            self.lb = []  # lower bounds of variables.
            self.ub = []  # upper bounds of variables.
            self.lbounds = []  # bounds of local constraints lower and upper.
            self.ofc = []  # objective function multiplicative coefficients
        else:
            self.id = runtime_data.id
            self.tag = runtime_data.tag
            self.connections = runtime_data.connections
            self.filename = runtime_data.filename
            self.energy = runtime_data.energy
            self.power = runtime_data.power
            self.capacity = runtime_data.capacity
            self.lb = runtime_data.lb  # lower bounds of variables.
            self.ub = runtime_data.ub  # upper bounds of variables.
            self.lbounds = runtime_data.lbounds  # bounds of local constraints lower and upper.
            self.ofc = runtime_data.ofc  # objective function multiplicative coefficients


class SubProblem:
    def __init__(self):
        self.id = ''
        self.firstvar = 0
        self.lastvar = 0
        self.tag = ""
        self.connections = []
        self.x = []
        self.n_slots = 0
        self.input_energy = []
        self.output_energy = []
        self.energy = 0
        self.activity = []
        self.incomes = []
        self.costs = []
        self.obj_fun = []

    def extract_values(self, solution):
        self.x = solution[self.firstvar:self.lastvar]

        if self.tag == "storage":
            self.input_energy = self.x[0:self.n_slots]
            self.output_energy = self.x[self.n_slots:2 * self.n_slots]
            self.x = self.x[2 * self.n_slots:3 * self.n_slots]

        elif self.tag == 'task':
            self.activity = [int(round(x)) for x in self.x]
            self.input_energy = [a * self.energy for a in self.activity]

        elif self.tag == 'seller':
            self.incomes = [a * (-b) for a, b in zip(self.x, self.obj_fun)]

        elif self.tag == 'purchaser':
            self.costs = [a * b for a, b in zip(self.x, self.obj_fun)]

        elif self.tag == 'pipe':
            self.input_energy = self.x[0:self.n_slots]
            self.output_energy = self.input_energy

        elif self.tag == 'prod_hub':
            self.input_energy = self.x[0:self.n_slots]
            n_outputs = (len(self.x) // self.n_slots) - 1
            for output in range(1, n_outputs+1):
                self.output_energy.append(self.x[self.n_slots*output:self.n_slots*output+self.n_slots])
            self.input_energy = list(reversed(self.output_energy))

        elif self.tag == 'load_hub':
            self.output_energy = self.x[0:self.n_slots]
            n_inputs = (len(self.x) // self.n_slots) - 1
            for input in range(1, n_inputs+1):
                self.input_energy.append(self.x[self.n_slots*input:self.n_slots*input+self.n_slots])
            self.input_energy = list(reversed(self.input_energy))


class MainProblem:
    def __init__(self):

        self.__vcount_ = 0
        self.__iccount_ = 0
        self.__eccount_ = 0
        self.__obj_fun_ = []
        self.__lb_ = []
        self.__ub_ = []
        self.__varnames_ = []
        self.__vartypes_ = []
        self.__lcbody_ = []
        self.__lclhs_ = []
        self.__lcrhs_ = []
        self.__lcrval_ = []
        self.__gcbody_ = []
        self.__gcconst_ = []
        self.__gcbounds_ = []
        self.__gclhs_ = []
        self.__gcrhs_ = []
        self.__gcrval_ = []

    def add_element(self, rawmodel, runtime_data):

        """This function will integrate the raw model into the problem."""

        subproblem = SubProblem()

        error = 0

        power_parnum = 0
        energy_parnum = 0
        capacity_parnum = 0

        ### we get the scaling parameters from the model

        for i in rawmodel.params:  # we check the string identifier
            if i[0] == "P":  # we got the power maximum of the model
                if (i[1] == "data"):
                    power_coeff = runtime_data.power
                    power_parnum = 1
                else:
                    power_coeff = i[1:]
                    power_parnum = len(power_coeff)
            elif i[0] == "E":  # we got the energy maximum of the model
                if (i[1] == "data"):
                    energy_coeff = runtime_data.energy
                    energy_parnum = 1
                else:
                    energy_coeff = i[1:]
                    energy_parnum = len(energy_coeff)
            elif i[0] == "C":  # we got the capacity maximum of the model
                if (i[1] == "data"):
                    capacity_coeff = runtime_data.capacity
                    capacity_parnum = 1
                else:
                    capacity_coeff = i[1:]
                    capacity_parnum = len(capacity_coeff)
            else:  # we got some additional, unknown parameter set
                continue

        if (power_parnum > 1 or energy_parnum > 1 or capacity_parnum > 1):
            print("model params error\n")
            print("data lengths", power_parnum, energy_parnum, capacity_parnum, "\n")

            ### we get the objective function variable's map and type

        obj_fun_coeff = []
        obj_fun_varnum = 0
        obj_fun_type = []
        obj_fun_typenum = 0

        for i in rawmodel.obj_fun:  # we check the string identifier
            if i[0] == "OF":  # we got the coefficients of the objective function variables
                obj_fun_coeff = i[1:]
                obj_fun_varnum = len(obj_fun_coeff)
            elif i[0] == "OFT":  # we got the type of the objective function variables
                obj_fun_type = i[1:]
                obj_fun_typenum = len(obj_fun_type)
            else:  # we got some additional, unknown parameter set
                continue

                ### we check the model sanity
        bounds_num = len(rawmodel.vbounds)

        if obj_fun_varnum == 0:
            print("The model is corrupted: error 1")
            error = 1

        if bounds_num != obj_fun_varnum:
            print("The model is corrupted: error 2")
            error = 2

        if obj_fun_typenum != obj_fun_varnum:
            print("The model is corrupted : error 3")
            error = 3

            ### we load the coefficients of upper and lower bounds
        lower_bounds = []
        upper_bounds = []

        if error != 0:
            return None

        for i in rawmodel.vbounds:  # we assume that the bound are already in-order (for now)
            lower_bounds += [i[1]]
            upper_bounds += [i[2]]

        subproblem.firstvar = self.__vcount_
        subproblem.lastvar = self.__vcount_ + obj_fun_varnum
        self.__vcount_ += obj_fun_varnum

        ### we multiply the scaling params and the coefficients and
        ### then we include the values into the main problem.

        slot_num = len(runtime_data.ofc)
        for i in range(0, obj_fun_varnum):

            if (obj_fun_coeff[i] == 'P'):
                self.__obj_fun_ += [power_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == 'E'):
                self.__obj_fun_ += [energy_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == 'C'):
                self.__obj_fun_ += [capacity_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == 'data'):
                self.__obj_fun_ += [runtime_data.obj_fun[i] * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == '-P'):
                self.__obj_fun_ += [-1.0 * power_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == '-E'):
                self.__obj_fun_ += [-1.0 * energy_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == '-C'):
                self.__obj_fun_ += [-1.0 * capacity_coeff * runtime_data.ofc[i % slot_num]]
            elif (obj_fun_coeff[i] == '-data'):
                self.__obj_fun_ += [-1.0 * runtime_data.obj_fun[i] * runtime_data.ofc[i % slot_num]]
            elif isinstance(obj_fun_coeff[i], Number):
                self.__obj_fun_ += [obj_fun_coeff[i] * runtime_data.ofc[i % slot_num]]

            if (lower_bounds[i] == 'P'):
                self.__lb_ += [power_coeff]
            elif (lower_bounds[i] == 'E'):
                self.__lb_ += [energy_coeff]
            elif (lower_bounds[i] == 'C'):
                self.__lb_ += [capacity_coeff]
            elif (lower_bounds[i] == 'data'):
                self.__lb_ += [runtime_data.lb[i]]
            elif (lower_bounds[i] == '-P'):
                self.__lb_ += [-1.0 * power_coeff]
            elif (lower_bounds[i] == '-E'):
                self.__lb_ += [-1.0 * energy_coeff]
            elif (lower_bounds[i] == '-C'):
                self.__lb_ += [-1.0 * capacity_coeff]
            elif (lower_bounds[i] == '-data'):
                self.__lb_ += [-1.0 * runtime_data.lb[i]]
            elif isinstance(lower_bounds[i], Number):
                self.__lb_ += [lower_bounds[i]]

            if (upper_bounds[i] == 'P'):
                self.__ub_ += [power_coeff]
            elif (upper_bounds[i] == 'E'):
                self.__ub_ += [energy_coeff]
            elif (upper_bounds[i] == 'C'):
                self.__ub_ += [capacity_coeff]
            elif (upper_bounds[i] == 'data'):
                self.__ub_ += [runtime_data.ub[i]]
            elif (upper_bounds[i] == '-P'):
                self.__ub_ += [-1.0 * power_coeff]
            elif (upper_bounds[i] == '-E'):
                self.__ub_ += [-1.0 * energy_coeff]
            elif (upper_bounds[i] == '-C'):
                self.__ub_ += [capacity_coeff]
            elif (upper_bounds[i] == '-data'):
                self.__ub_ += [-1.0 * runtime_data.ub[i]]
            elif isinstance(upper_bounds[i], Number):
                self.__lb_ += [upper_bounds[i]]

            self.__varnames_ += ["x" + str(i + subproblem.firstvar)]
            self.__vartypes_ += [obj_fun_type[i]]

            ### we load the local constraints

        for i in range(0, len(rawmodel.lconsts)):
            var_coef = []
            var_list = []

            for j in range(1, len(rawmodel.lconsts[i])):
                if (rawmodel.lconsts[i][j] == 'P'):
                    var_coef += [power_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == 'E'):
                    var_coef += [energy_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == 'C'):
                    var_coef += [capacity_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == 'data'):
                    var_coef += runtime_data.lconsts[i][j]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == '-P'):
                    var_coef += [-1.0 * power_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == '-E'):
                    var_coef += [-1.0 * energy_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == '-C'):
                    var_coef += [-1.0 * capacity_coeff]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif (rawmodel.lconsts[i][j] == '-data'):
                    var_coef += -1.0 * runtime_data.lconsts[i][j]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]
                elif rawmodel.lconsts[i][j] != 0.0 and isinstance(rawmodel.lconsts[i][j], Number):
                    var_coef += [rawmodel.lconsts[i][j]]
                    var_list += [self.__varnames_[j - 1 + subproblem.firstvar]]



            self.__lcbody_ += [[var_list, var_coef]]
            if (rawmodel.lbounds[i][1] == 'P'):
                self.__lclhs_ += [power_coeff]
            elif (rawmodel.lbounds[i][1] == 'E'):
                self.__lclhs_ += [energy_coeff]
            elif (rawmodel.lbounds[i][1] == 'C'):
                self.__lclhs_ += [capacity_coeff]
            elif (rawmodel.lbounds[i][1] == 'data'):
                self.__lclhs_ += [runtime_data.lbounds[i][1]]
            elif (rawmodel.lbounds[i][1] == '-P'):
                self.__lclhs_ += [-1.0 * power_coeff]
            elif (rawmodel.lbounds[i][1] == '-E'):
                self.__lclhs_ += [-1.0 * energy_coeff]
            elif (rawmodel.lbounds[i][1] == '-C'):
                self.__lclhs_ += [-1.0 * capacity_coeff]
            elif (rawmodel.lbounds[i][1] == '-data'):
                self.__lclhs_ += [-1.0 * runtime_data.lbounds[i][1]]
            elif isinstance(rawmodel.lbounds[i][1], Number):
                self.__lclhs_ += [rawmodel.lbounds[i][1]]

            if (rawmodel.lbounds[i][2] == 'P'):
                self.__lcrhs_ += [power_coeff]
            elif (rawmodel.lbounds[i][2] == 'E'):
                self.__lcrhs_ += [energy_coeff]
            elif (rawmodel.lbounds[i][2] == 'C'):
                self.__lcrhs_ += [capacity_coeff]
            elif (rawmodel.lbounds[i][2] == 'data'):
                self.__lcrhs_ += [runtime_data.lbounds[i][2]]
            elif (rawmodel.lbounds[i][2] == '-P'):
                self.__lcrhs_ += [-1.0 * power_coeff]
            elif (rawmodel.lbounds[i][2] == '-E'):
                self.__lcrhs_ += [-1.0 * energy_coeff]
            elif (rawmodel.lbounds[i][2] == '-C'):
                self.__lcrhs_ += [-1.0 * capacity_coeff]
            elif (rawmodel.lbounds[i][2] == '-data'):
                self.__lcrhs_ += [-1.0 * runtime_data.lbounds[i][2]]
            elif isinstance(rawmodel.lbounds[i][2], Number):
                self.__lcrhs_ += [rawmodel.lbounds[i][2]]

            self.__lcrval_ += [self.__lcrhs_[-1] - self.__lclhs_[-1]]
            ### we load the global constraints

        #         print ("search begins")
        for i in range(0, len(rawmodel.gconsts)):
            var_coef = []
            var_list = []
            gc_insertion = 0
            for j in range(0, len(self.__gcbody_)):

                #                 print ("rawmod = " + str(rawmodel.gconsts[i][0]) + " i  = " + str(i))
                #                 print ("body   = " + str(self.__gcbody_[j][1][0])+ " j  = " + str(j))

                ### we search for the same keyword
                if (rawmodel.gconsts[i][0] == self.__gcbody_[j][1][0]):
                    ### match found we can parse the constraint
                    #                     print ("match found: "+ str(rawmodel.gconsts[i][0]))
                    for k in range(1, len(rawmodel.gconsts[i])):

                        if (rawmodel.gconsts[i][k] == 'P'):
                            var_coef += [power_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == 'E'):
                            var_coef += [energy_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == 'C'):
                            var_coef += [capacity_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == 'data'):
                            var_coef += [runtime_data.gcconst[i][k]]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == '-P'):
                            var_coef += [-1.0 * power_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == '-E'):
                            var_coef += [-1.0 * energy_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == '-C'):
                            var_coef += [-1.0 * capacity_coeff]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] == '-data'):
                            var_coef += [-1.0 * runtime_data.gcconst[i][k]]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                        elif (rawmodel.gconsts[i][k] != 0.0) and isinstance(rawmodel.gconsts[i][k], Number):
                            var_coef += [rawmodel.gconsts[i][k]]
                            var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]

                            #                     print(str(self.__gcbody_))
                    self.__gcbody_[j][0] += var_list
                    self.__gcbody_[j][1] += var_coef
                    #                     print(str(self.__gcbody_))
                    gc_insertion = 1
                    break

            if (gc_insertion == 0):
                ### match not found, we have a new constraint to append
                #                 print ("match not found: " + str(rawmodel.gconsts[i][0]))
                var_coef = [rawmodel.gconsts[i][0]]

                for k in range(1, len(rawmodel.gconsts[i])):

                    if (rawmodel.gconsts[i][k] == 'P'):
                        var_coef += [power_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == 'E'):
                        var_coef += [energy_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == 'C'):
                        var_coef += [capacity_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == 'data'):
                        var_coef += [runtime_data.gcconst[i][k]]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == '-P'):
                        var_coef += [-1.0 * power_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == '-E'):
                        var_coef += [-1.0 * energy_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == '-C'):
                        var_coef += [-1.0 * capacity_coeff]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] == '-data'):
                        var_coef += [-1.0 * runtime_data.gcconst[i][k]]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]
                    elif (rawmodel.gconsts[i][k] != 0.0) and isinstance(rawmodel.gconsts[i][k], Number):
                        var_coef += [rawmodel.gconsts[i][k]]
                        var_list += [self.__varnames_[k - 1 + subproblem.firstvar]]

                        #                 print(str(self.__gcbody_))
                self.__gcbody_ += [[var_list, var_coef]]
                #                 print(str(self.__gcbody_))

        for i in range(0, len(rawmodel.gbounds)):
            gc_insertion = 0
            for j in range(0, len(self.__gcbounds_)):
                ### we search for the same keyword
                if (rawmodel.gbounds[i][0] == self.__gcbounds_[j][0]):
                    gc_insertion = 1
                    break

            if (gc_insertion == 0):
                ### match not found, we have a new constraint to append
                #                 print ("match not found: " + str(rawmodel.gbounds[i][0]))
                var_coef = [rawmodel.gbounds[i][0]]

                for k in range(1, len(rawmodel.gbounds[i])):

                    if (rawmodel.gbounds[i][k] == 'P'):
                        var_coef += [power_coeff]
                    elif (rawmodel.gbounds[i][k] == 'E'):
                        var_coef += [energy_coeff]
                    elif (rawmodel.gbounds[i][k] == 'C'):
                        var_coef += [capacity_coeff]
                    # elif (rawmodel.gbounds[i][k] == 'data'):
                    #     var_coef += [runtime_data.gcconst[i][k]]
                    elif (rawmodel.gbounds[i][k] == '-P'):
                        var_coef += [-1.0 * power_coeff]
                    elif (rawmodel.gbounds[i][k] == '-E'):
                        var_coef += [-1.0 * energy_coeff]
                    elif (rawmodel.gbounds[i][k] == '-C'):
                        var_coef += [-1.0 * capacity_coeff]
                    # elif (rawmodel.gbounds[i][k] == '-data'):
                    #     var_coef += [-1.0 * runtime_data.gcconst[i][k]]
                    elif isinstance(rawmodel.gbounds[i][k], Number):
                        var_coef += [rawmodel.gbounds[i][k]]

                        #                 print(str(self.__gcbounds_))
                self.__gcbounds_ += [var_coef]
                #                 print(str(self.__gcbounds_))

            #    print(self.__gcbody_)
        subproblem.obj_fun = self.__obj_fun_[subproblem.firstvar:subproblem.lastvar]
        subproblem.tag = runtime_data.tag
        subproblem.id = runtime_data.id
        subproblem.energy = runtime_data.energy
        subproblem.n_slots = len(runtime_data.ofc)
        subproblem.connections = runtime_data.connections

        return subproblem

    def finalize_model(self, runtime_data):

        for j in range(0, len(self.__gcbounds_)):
            for i in range(0, len(runtime_data)):
                #if runtime_data[i][0][0:5] == self.__gcbody_[j][1][0][0:5]:
                if runtime_data[i][0][:] == self.__gcbounds_[j][0][0:len(runtime_data[i][0][:])]:
                    #print(str(runtime_data[i][0][0:5]))
                    #print(str(self.__gcbounds_[j][1][0][0:5]))

                    self.__gcbounds_[j][1] = runtime_data[i][1]
                    self.__gcbounds_[j][2] = runtime_data[i][2]

                    # self.__gclhs_ += [runtime_data[i][1]]
                    # self.__gcrhs_ += [runtime_data[i][2]]
                    # self.__gcrval_ += [runtime_data[i][2] - runtime_data[i][1]]
                    break
            self.__gclhs_ += [self.__gcbounds_[j][1]]
            self.__gcrhs_ += [self.__gcbounds_[j][2]]
            self.__gcrval_ += [self.__gcbounds_[j][2] - self.__gcbounds_[j][1]]



        temp_gcbody = []

        for i in range(0, len(self.__gcbody_)):
            temp_gcbody += [[self.__gcbody_[i][0], self.__gcbody_[i][1][1:]]]

        self.__gcconst_ = self.__gcbody_
        self.__gcbody_ = temp_gcbody

    def obj_fun(self):
        return self.__obj_fun_

    def lb(self):
        return self.__lb_

    def ub(self):
        return self.__ub_

    def vartypes(self):
        return self.__vartypes_

    def varnames(self):
        return self.__varnames_

    def constraints(self):
        return (self.__lcbody_ + self.__gcbody_)

    def lhs(self):
        return (self.__lclhs_ + self.__gclhs_)

    def rhs(self):
        return (self.__lcrhs_ + self.__gcrhs_)

    def range_values(self):
        return (self.__lcrval_ + self.__gcrval_)

    def c_senses(self):
        return ['R'] * (len(self.__lclhs_) + len(self.__gclhs_))

    def print_obj_fun(self):
        print(str(self.__varnames_))
        print(str(self.__obj_fun_))
        print(str(self.__lb_))
        print(str(self.__ub_))

    def print_constraints(self):
        for i in range(0, len(self.__lcbody_)):
            print("LLB <= " + str(self.__lcbody_[i][0]) + " <= LUB")
            print(str(self.__lclhs_[i]) + " <= " + str(self.__lcbody_[i][1]) + " <= " + str(
                self.__lclhs_[i] + self.__lcrval_[i]))
        for i in range(0, len(self.__gcbody_)):
            print("GLB <= " + str(self.__gcbody_[i][0]) + " <= GUB")
            print(str(self.__gclhs_[i]) + " <= " + str(self.__gcbody_[i][1]) + " <= " + str(
                self.__gclhs_[i] + self.__gcrval_[i]))
