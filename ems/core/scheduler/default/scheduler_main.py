# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import core.scheduler.default.problem_solver as problem_solver
from core.scheduler.default.mainproblem import RunTimeData
import core.scheduler.default.rawmodel as rawmodel
import core.scheduler.default.rawtask as rawtask

from defines import *
import datetime

logger = logging.getLogger(__name__)
input_data = {"timestamp": int, "horizon": int, "resolution": int, "scheduler_module": str,
              "storages": [{"id": "storage-1",
                            "connectedTo": [],  # List of connected ids
                            "data": {"init_charge": float},
                            "specs": {"size": float,
                                      "charge_eff": float,
                                      "discharge_eff": float}  # charge_eff and discharge_eff are within [0,1]
                            }],
              "devices": [{"id": "prod-1",
                           "tag": "prod",
                           "connectedTo": [],
                           "specs": {"max": float},
                           "data": {"production": []}
                           },
                          {"id": "purchaser-1",
                           "tag": "purchaser",
                           "connectedTo": [],
                           "specs": {"max": float,
                                     "buying_prices": []}
                           },
                          {"id": "seller-1",
                           "connectedTo": [],
                           "specs": {"max": float,
                                     "selling_prices": []}
                           }, {"id": "load-1",
                               "connectedTo": [],
                               "specs": {"max": float},
                               "data": {"loads": []}
                               },
                          {"id": "pipe-1",
                           "connectedTo": [],
                           "specs": {"max": float}  # eff is within [0,1]
                           },
                          {"id": "gen-1",
                           "connectedTo": [
                               [],  # list of input nodes
                               []  # list of output nodes
                           ],
                           "specs": {"max": float,
                                     "eff": float}  # eff is within [0,1]
                           },
                          {"id": "trigen-1",
                           "connectedTo": [
                               [],  # list of input nodes
                               [],  # list of el output nodes
                               [],  # list of heating output nodes
                               []  # list of chilling output nodes
                           ],
                           "specs": {"max": float,
                                     "ele": float,
                                     "heat": float,
                                     "chill": float}  # ele heat and chill are within [0,1]
                           }],

              "tasks": [{"id": "task-1",
                         "connectedTo": ["node_id",  # Supply node
                                         "task-" + "master_nr" + "/" + "current_task_nr" or None,  # Master node
                                         "task-" + "current_task_nr" + "/" + "slave_nr" or None],  # Slave node
                         "specs": {"duration": float,
                                   "energy": float,
                                   "mduration": float or 0.,  # Zero if the task is not a slave
                                   "sduration": float or 0.,  # Zero if the task is not a master
                                   "mdelay": float or 0.,
                                   "sdelay": float or 0.,
                                   "forced_slots": [],  # Represent the lower bounds
                                   "allowed_slots": []}  # Represent the upper bounds
                         }]
              }

output_data = {'devices': [{'id': 'purchaser-1',
                            'data': {'consumption': []}
                            },
                           {'id': 'seller-1',
                            'data': {'sales': []}},
                           {'id': 'storage-1',
                            'data': {'levels': [],
                                     'charge_activities': [],

                                     'discharge_activities': []}
                            },
                           {'id': 'production-1',
                            'data': {'consumption': []}
                            }],
               'tasks': [{'id': 'task-1',
                          "data": {"start_time": "timestamp"}
                          }]
               }


def scheduler(input_data, output_data, stop_event):
    logger.info('Enter in scheduler routine.')

    logger.info('Removing old .ini models.')

    models_path = os.path.join(EMS_DIR, 'core', 'scheduler', input_data.get('scheduler_module'), 'models')

    if os.path.isdir(models_path):
        shutil.rmtree(models_path)

    logger.info('Old .ini models removed.')

    horizon = input_data['horizon']
    resolution = input_data['resolution']

    n_slots = (horizon // resolution)

    # check existence of model_<res>_<hor>_<name>.ini
    # inserito in previsione della generazione automatica del filename
    prefix = os.path.join(models_path, 'model_' + str(horizon) + '_' + str(resolution) + '_')
    ext = '.ini'

    device_list = []
    gchss = []

    for structure in input_data.get('devices') or []:
        structure.update(get_max_energy(input_data, structure, n_slots))

        add_device(structure=structure, device_list=device_list, n_slots=n_slots, prefix=prefix, extension=ext,
                   gchss=gchss)

    for task in input_data.get('tasks') or []:
        add_task(task=task, device_list=device_list, n_slots=n_slots, prefix=prefix, extension=ext)

    problem_dictionary = {'devices': device_list, 'gchss': gchss}

    subproblem_list = problem_solver.solver(problem_dictionary)

    output = get_output(input_data['timestamp'], input_data['resolution'], subproblem_list, n_slots)

    output_data.put(output)


def get_output(timestamp, resolution, subproblem_list, n_slots):
    output = {'devices': [],
              'tasks': []}

    costs = [0.0] * n_slots
    incomes = [0.0] * n_slots
    charges = [0.0] * n_slots
    discharges = [0.0] * n_slots
    loads = [0.0] * n_slots
    pur_loads = [0.0] * n_slots
    sell_sales = [0.0] * n_slots
    levels = [0.0] * n_slots

    for subproblem in subproblem_list:

        if subproblem.tag == 'purchaser':
            costs = [sum(x) for x in zip(costs, subproblem.costs)]
            pur_loads = [sum(x) for x in zip(pur_loads, subproblem.x)]

            output['devices'].append({'id': subproblem.id,
                                      'tag': subproblem.tag,
                                      'data': {'consumption': subproblem.x}})

        elif subproblem.tag == 'seller':
            incomes = [sum(x) for x in zip(incomes, subproblem.incomes)]
            sell_sales = [sum(x) for x in zip(sell_sales, subproblem.x)]

            output['devices'].append({'id': subproblem.id,
                                      'tag': subproblem.tag,
                                      'data': {'sales': subproblem.x}})

        elif subproblem.tag == 'storage':
            charges = [sum(x) for x in zip(charges, subproblem.input_energy)]
            discharges = [sum(x) for x in zip(discharges, subproblem.output_energy)]

            initial_charge = subproblem.x[0] - subproblem.input_energy[0] + subproblem.output_energy[0]

            # temp = [initial_charge] + subproblem.x
            levels = [sum(x) for x in zip(levels, subproblem.x)]

            output['devices'].append({'id': subproblem.id,
                                      'tag': subproblem.tag,
                                      'data': {'levels': subproblem.x,
                                               # 'levels': [initial_charge] + subproblem.x,
                                               'charge_activities': subproblem.input_energy,
                                               'discharge_activities': subproblem.output_energy}})

        elif subproblem.tag == 'load':
            loads = [sum(x) for x in zip(loads, subproblem.x)]

        elif subproblem.tag == 'prod':
            output['devices'].append({'id': subproblem.id,
                                      'tag': subproblem.tag,
                                      'data': {'use': subproblem.x}})

        elif subproblem.tag == 'task':
            active_slots = [i for i, x in enumerate(subproblem.activity) if x == 1]
            date_time_delta = datetime.timedelta(seconds=active_slots[0] * resolution * 60)
            date_time = datetime.datetime.fromtimestamp(timestamp)
            execution_date_time = date_time + date_time_delta
            execution_timestamp = execution_date_time.timestamp()

            loads = [sum(x) for x in zip(loads, subproblem.input_energy)]

            output['tasks'].append({'id': subproblem.id,
                                    'tag': subproblem.tag,
                                    'data': {'start_time': execution_timestamp,
                                             'loads': subproblem.input_energy}})

    output.update({'costs': costs,
                   'incomes': incomes,
                   'total_charges': charges,
                   'total_discharges': discharges,
                   'total_levels': levels,
                   'total_loads': loads,
                   'pur_loads': pur_loads,
                   'sell_sales': sell_sales})

    return output


def get_connected_structures(structure_type, connections):
    output_ids = []
    for structure_id in connections:
        if structure_id.split('-')[0] == structure_type:
            output_ids += [structure_id]

    return output_ids


def get_number(structure_id):
    structure_number = structure_id.split('-')[1]
    return structure_number


def check_if_in_gchss(gchss, constraint_id):
    is_present = False

    for structure_constraints in gchss:
        if constraint_id in structure_constraints[0]:
            is_present = True

    return is_present


def add_task(task, device_list, n_slots, prefix, extension):
    device_list += [RunTimeData(None)]
    device_list[-1].id = task['id']
    device_list[-1].tag = task['tag']
    device_list[-1].energy = task['specs']['energy']
    device_list[-1].lb = list(map(float, task['specs']['forced_slots']))
    device_list[-1].ub = list(map(float, task['specs']['allowed_slots']))
    device_list[-1].filename = prefix + task['id'] + extension
    device_list[-1].lbounds = [
        ['LC1.1', float(task['specs']['duration']), float(task['specs']['duration'])]]  # forza l'esecuzione completa
    # device_list[-1].lbounds = [['LC1.1', 0, float(task['specs']['duration']]] # permette l'esecuzione parziale
    device_list[-1].ofc = [1.0] * n_slots
    device_list[-1].connections = task['connectedTo']

    gcset = []

    for connection in task['connectedTo']:
        if connection:
            gcset.append('GC_' + connection + '_')
        else:
            gcset.append(None)

    rawtask.RawTask(flag=task['tag'],
                    params={'slots': n_slots,
                            'duration': task['specs']['duration'],
                            'mduration': task['specs']['mduration'],
                            'sduration': task['specs']['sduration'],
                            'mdelay': task['specs']['mdelay'],
                            'sdelay': task['specs']['sdelay']},
                    gcset=gcset,
                    filename=prefix + task['id'] + extension)

    return


def add_device(structure, device_list, n_slots, prefix, extension, gchss):
    device_list += [build_device(device_tag=structure['tag'], structure=structure, n_slots=n_slots)]
    device_list[-1].filename = prefix + structure['id'] + extension

    if structure['tag'] == 'pipe':
        rawmodel.RawModel(flag=structure['tag'],
                          params={'slots': n_slots},
                          gcset=['GC_' + structure['connectedTo'][0] + '_',
                                 'GC_' + structure['connectedTo'][1] + '_'],
                          filename=prefix + structure['id'] + extension)

        for node_id in structure['connectedTo']:
            if not check_if_in_gchss(gchss, node_id):
                gchss.extend([['GC_' + node_id + '_1', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_2', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_3', 0.0, 0.0]])

    elif structure['tag'] == 'gen':

        # CHECK ON GEN INPUT NODES #
        if len(structure['connectedTo'][0]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][0]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='load_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'load_hub-' + structure['id'] + extension

            rawmodel.RawModel(flag='load_hub-' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'input_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'load_hub-' + structure['id'] + extension)

            gchss.extend([['GC_' + 'input_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'input_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'input_' + structure['id'] + '_3', 0.0, 0.0]])

            gen_input_node = 'input_' + structure['id']
        else:
            gen_input_node = structure['connectedTo'][0][0]

        # CHECK ON GEN OUTPUT NODES #
        if len(structure['connectedTo'][1]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][1]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'prod_hub-' + 'el_output_' + structure['id'] + extension

            rawmodel.RawModel(flag='prod_hub-' + 'output_' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'output_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'prod_hub-' + 'el_output_' + structure['id'] + extension)

            gchss.extend([['GC_' + 'output_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'output_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'output_' + structure['id'] + '_3', 0.0, 0.0]])

            gen_output_node = 'output_' + structure['id']
        else:
            gen_output_node = structure['connectedTo'][1][0]

        rawmodel.RawModel(flag=structure['tag'],
                          params={'slots': n_slots,
                                  'eff': structure['specs']['eff']},
                          gcset=['GC_' + gen_input_node + '_',
                                 'GC_' + gen_output_node + '_'],
                          filename=prefix + structure['id'] + extension)

        for node_list in structure['connectedTo']:
            for node_id in node_list:
                if not check_if_in_gchss(gchss, node_id):
                    gchss.extend([['GC_' + node_id + '_1', 0.0, structure['max_E']],
                                  ['GC_' + node_id + '_2', 0.0, structure['max_E']],
                                  ['GC_' + node_id + '_3', 0.0, 0.0]])

    elif structure['tag'] == 'trigen':

        # CHECK ON TRIGEN INPUT NODES #
        if len(structure['connectedTo'][0]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][0]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='load_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'load_hub-' + structure['id'] + extension

            rawmodel.RawModel(flag='load_hub-' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'input_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'load_hub-' + structure['id'] + extension)

            gchss.extend([['GC_' + 'input_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'input_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'input_' + structure['id'] + '_3', 0.0, 0.0]])

            trigen_input_node = 'input_' + structure['id']
        else:
            trigen_input_node = structure['connectedTo'][0][0]

        # CHECK ON TRIGEN ELE OUTPUT NODES #
        if len(structure['connectedTo'][1]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][1]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'prod_hub-' + 'el_output_' + structure['id'] + extension

            rawmodel.RawModel(flag='prod_hub-' + 'el_output_' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'el_output_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'prod_hub-' + 'el_output_' + structure['id'] + extension)

            gchss.extend([['GC_' + 'el_output_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'el_output_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'el_output_' + structure['id'] + '_3', 0.0, 0.0]])

            trigen_output_node_el = 'el_output_' + structure['id']
        else:
            trigen_output_node_el = structure['connectedTo'][1][0]

        # CHECK ON TRIGEN HEAT OUTPUT NODES #
        if len(structure['connectedTo'][2]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][1]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'prod_hub-' + 'heat_output_' + structure['id'] + extension

            rawmodel.RawModel(flag='prod_hub-' + 'heat_output_' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'heat_output_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'prod_hub-' + 'heat_output_' + structure['id'] + extension)

            gchss.extend([['GC_' + 'heat_output_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'heat_output_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'heat_output_' + structure['id'] + '_3', 0.0, 0.0]])

            trigen_output_node_heat = 'heat_output_' + structure['id']
        else:
            trigen_output_node_heat = structure['connectedTo'][2][0]

        # CHECK ON TRIGEN CHILL OUTPUT NODES #
        if len(structure['connectedTo'][3]) > 1:
            constraint_list = []
            for node_id in structure['connectedTo'][1]:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'prod_hub-' + 'chill_output_' + structure['id'] + extension

            rawmodel.RawModel(flag='prod_hub-' + 'chill_output_' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + 'chill_output_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'prod_hub-' + 'chill_output_' + structure['id'] + extension)

            gchss.extend([['GC_' + 'chill_output_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + 'chill_output_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + 'chill_output_' + structure['id'] + '_3', 0.0, 0.0]])

            trigen_output_node_chill = 'chill_output_' + structure['id']
        else:
            trigen_output_node_chill = structure['connectedTo'][3][0]

        rawmodel.RawModel(flag=structure['tag'],
                          params={'slots': n_slots,
                                  'ele': structure['specs']['ele'],
                                  'heat': structure['specs']['heat'],
                                  'chill': structure['specs']['chill']},
                          gcset=['GC_' + trigen_input_node + '_',  # input (fuel/gas)
                                 'GC_' + trigen_output_node_el + '_',  # electricity output
                                 'GC_' + trigen_output_node_heat + '_',  # thermal output
                                 'GC_' + trigen_output_node_chill + '_'],  # chill output
                          filename=prefix + structure['id'] + extension)

        for node_list in structure['connectedTo']:
            for node_id in node_list:
                if not check_if_in_gchss(gchss, node_id):
                    gchss.extend([['GC_' + node_id + '_1', 0.0, structure['max_E']],
                                  ['GC_' + node_id + '_2', 0.0, structure['max_E']],
                                  ['GC_' + node_id + '_3', 0.0, 0.0]])

    elif structure['tag'] == 'storage':

        if len(structure['connectedTo']) > 1:
            constraint_list = []
            for node_id in structure['connectedTo']:
                constraint_list += ["GC_" + node_id + "_"]

            device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'prod_hub-' + structure['id'] + extension

            rawmodel.RawModel(flag='prod_hub-' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'prod_hub-' + structure['id'] + extension)

            device_list += [build_device(device_tag='load_hub', structure=structure, n_slots=n_slots)]
            device_list[-1].filename = prefix + 'load_hub-' + structure['id'] + extension

            rawmodel.RawModel(flag='load_hub-' + structure['id'],
                              params={'slots': n_slots},
                              gcset=['GC_' + structure['id'] + '_'] + constraint_list,
                              filename=prefix + 'load_hub-' + structure['id'] + extension)

            gchss.extend([['GC_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + structure['id'] + '_3', 0.0, 0.0]])

            storage_node = structure['id']
        else:
            storage_node = structure['connectedTo'][0]

        rawmodel.RawModel(flag=structure['tag'],
                          params={'slots': n_slots,
                                  'charge_eff': structure['specs']['charge_eff'],
                                  'discharge_eff': structure['specs']['discharge_eff']},
                          gcset=['GC_' + storage_node + '_'],
                          filename=prefix + structure['id'] + extension)

        for node_id in structure['connectedTo']:
            if not check_if_in_gchss(gchss, node_id):
                gchss.extend([['GC_' + node_id + '_1', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_2', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_3', 0.0, 0.0]])

    else:

        if len(structure['connectedTo']) > 1:

            rawmodel.RawModel(flag=structure['tag'],
                              params={'slots': n_slots},
                              gcset=['GC_' + structure['id'] + '_'],
                              filename=prefix + structure['id'] + extension)

            constraint_list = []
            for node_id in structure['connectedTo']:
                constraint_list += ["GC_" + node_id + "_"]

            if structure['tag'] == 'prod' or structure['tag'] == 'purchaser':

                device_list += [build_device(device_tag='prod_hub', structure=structure, n_slots=n_slots)]
                device_list[-1].filename = prefix + 'prod_hub-' + structure['id'] + extension

                rawmodel.RawModel(flag='prod_hub-' + structure['id'],
                                  params={'slots': n_slots},
                                  gcset=['GC_' + structure['id'] + '_'] + constraint_list,
                                  filename=prefix + 'prod_hub-' + structure['id'] + extension)

            elif structure['tag'] == 'seller' or structure['tag'] == 'load':
                # or structure['tag'] == 'therm':

                device_list += [build_device(device_tag='load_hub', structure=structure, n_slots=n_slots)]
                device_list[-1].filename = prefix + 'load_hub-' + structure['id'] + extension

                rawmodel.RawModel(flag='load_hub-' + structure['id'],
                                  params={'slots': n_slots},
                                  gcset=['GC_' + structure['id'] + '_'] + constraint_list,
                                  filename=prefix + 'load_hub-' + structure['id'] + extension)

            gchss.extend([['GC_' + structure['id'] + '_1', 0.0, structure['max_E']],
                          ['GC_' + structure['id'] + '_2', 0.0, structure['max_E']],
                          ['GC_' + structure['id'] + '_3', 0.0, 0.0]])

        else:

            rawmodel.RawModel(flag=structure['tag'],
                              params={'slots': n_slots},
                              gcset=['GC_' + structure['connectedTo'][0] + '_'],
                              filename=prefix + structure['id'] + extension)

        for node_id in structure['connectedTo']:
            if not check_if_in_gchss(gchss, node_id):
                gchss.extend([['GC_' + node_id + '_1', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_2', 0.0, structure['max_E']],
                              ['GC_' + node_id + '_3', 0.0, 0.0]])

    return


def build_device(device_tag, structure, n_slots):
    device = RunTimeData(None)
    device.connections = structure['connectedTo']

    if device_tag == "storage":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure["max_E"] / 2
        device.energy = structure["max_E"] / 2
        device.capacity = structure["max_E"]
        # struct_list[0].lb        = [0.0]*num_slot                     #lower bounds of variables.
        # struct_list[0].ub        = [max_E]*num_slot                   #upper bounds of variables.
        device.lbounds = [['LC1', structure['data']['init_charge'], structure['data']['init_charge']]]
        device.ofc = [1.0] * n_slots
    elif device_tag == "load":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['specs']['max']
        device.energy = structure['specs']['max']
        device.capacity = structure['specs']['max']
        device.lb = structure['data']['loads']
        device.ub = structure['data']['loads']
        device.ofc = [1.0] * n_slots
    elif device_tag == "prod":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        device.lb = [0.0] * n_slots
        device.ub = structure['data']['production']
        device.ofc = [1.0] * n_slots
    # elif device_tag == "thermal":
    #     device.id = structure['id']
    #     device.tag = device_tag
    #     device.power = structure['specs']['max']
    #     device.energy = structure['specs']['max']
    #     device.capacity = structure['specs']['max']
    #     device.lb = structure['data']['loads'][1:n_slots + 1]
    #     device.ub = structure['data']['loads'][1:n_slots + 1]
    #     device.ofc = [1.0] * n_slots
    elif device_tag == "purchaser":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        device.lb = [0.0] * n_slots
        device.ub = [structure['specs']['max']] * n_slots
        # device.ub = structure['load_therm'][1:n_slots + 1]
        device.ofc = structure['specs']['buying_prices']
    elif device_tag == "seller":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        device.lb = [0.0] * n_slots
        device.ub = [structure['specs']['max']] * n_slots
        # device.ub = structure['data']['production'][1:n_slots + 1]
        device.ofc = structure['specs']['selling_prices']
    elif device_tag == "pipe":
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        # device.lb = structure_data.get("load")
        # device.ub = structure_data.get("load")
        device.ofc = [1.0] * n_slots
    elif device_tag == "gen":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        # device.lb = structure_data.get("load")
        # device.ub = structure_data.get("load")
        device.ofc = [1.0] * n_slots
    elif device_tag == "trigen":
        device.id = structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        # device.lb = structure_data.get("load")
        # device.ub = structure_data.get("load")
        device.ofc = [1.0] * n_slots
    elif device_tag == "load_hub":
        device.id = "load_hub-" + structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        # device.lb = structure_data.get("load")
        # device.ub = structure_data.get("load")
        device.ofc = [1.0] * n_slots
    elif device_tag == "prod_hub":
        device.id = "prod_hub-" + structure['id']
        device.tag = device_tag
        device.power = structure['max_E']
        device.energy = structure['max_E']
        device.capacity = structure['max_E']
        # device.lb = structure_data.get("load")
        # device.ub = structure_data.get("load")
        device.ofc = [1.0] * n_slots

    return device


def get_max_energy(input_data, structure, n_slots):
    node_load = [0] * n_slots

    for node_id in structure['connectedTo']:
        for device in input_data['devices']:
            if device['tag'] == 'load':
                if node_id in device['connectedTo']:
                    node_load = [sum(x) for x in zip(node_load, device['data']['loads'])]

    if structure['tag'] == 'storage':
        maximum = structure['specs']['size']
    else:
        maximum = structure['specs']['max']

    max_E = max(max(node_load), maximum)

    return {'max_E': max_E, 'load': node_load}


if __name__ == "__main__":
    """ Testing your code! """

    import queue
    import multiprocessing
    import matplotlib
    import json

    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
    import core.scheduler.default.plot_data as pd

    setup_logging('test_scheduler.txt')

    input_path = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(input_path, 'input_data_tests',
                              'input_data_trigen_and_tasks.json')  # MODIFY THIS FOR DIFFERENT INPUT CONFIGURATIONS

    with open(input_file) as file:
        input_data = json.load(file)

    n_slots = (input_data.get('horizon') // input_data.get('resolution'))

    output_data = queue.Queue()
    stop_event = multiprocessing.Event()

    scheduler(input_data, output_data, stop_event)

    output_dictionary = output_data.get()

    # cost and incomes
    x_axis = [list(range(0, n_slots))] * 2
    y_axis = [output_dictionary['costs'], output_dictionary['incomes']]

    legend_str = ['Costs', 'Incomes']
    plots = [pd.PlotData(x_axis, y_axis, legend_str, "Costs and incomes")]
    plots[-1].ylabel = "euro"

    # energy_plot
    x_axis = [list(range(0, n_slots))] * 2
    y_axis = [output_dictionary['total_loads'], output_dictionary['pur_loads']]

    legend_str = ["Loads", "Grid"]
    plots += [pd.PlotData(x_axis, y_axis, legend_str, "Total load vs purchase")]

    # storage plot
    x_axis = [list(range(0, n_slots))] * 3
    y_axis = [output_dictionary['total_levels'], output_dictionary['total_charges'],
              output_dictionary['total_discharges']]

    legend_str = ["Level", "Charge", "Discharge"]
    plots += [pd.PlotData(x_axis, y_axis, legend_str, "Storage activity")]

    pd.plot_data([3, 1], plots)

    plt.show()
