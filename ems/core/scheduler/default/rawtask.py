# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import os


class RawTask:
    """Template of a raw sub problem model for an optimization problem."""

    def __init__(self, flag="", params={'slots': 0, 'duration': 0, 'mduration': 0, 'sduration': 0, 'mdelay': 0, 'sdelay': 0}, gcset=[], filename=""):

        self.params = []

        self.obj_fun = []
        self.vbounds = []

        self.lconsts = []
        self.lbounds = []

        self.gconsts = []
        self.gbounds = []

        if flag != "" and params['slots'] != 0:

            ini_file = filename

            if not os.path.isfile(ini_file):
                os.makedirs(os.path.dirname(filename), exist_ok=True)

                self.__modelgen_(flag, params, gcset)
                self.store_to_file(filename)

    def __modelgen_(self, flag="", params={'slots': 0, 'duration': 0, 'mduration': 0, 'sduration': 0, 'mdelay': 0, 'sdelay': 0}, gcset=["GC"]):

        slots = params['slots']
        duration = params['duration']
        mduration = params['mduration']
        sduration = params['sduration']
        mdelay = params['mdelay']
        sdelay = params['sdelay']

        self.params = [["E", 'data'], ["P", 'data'], ["C", 'data']]
        # model params done

        self.obj_fun = [["OF"] + [0.0] * slots, ["OFT"] + ['B'] * slots]
        # obj funct done

        for i in range(1, slots + 1):
            self.vbounds += [["X" + str(i), 'data', 'data']]
        # v bounds done

        self.lconsts = [["LC1.1"]+[1.0] * slots]
        for i in range(1, slots - duration):
            for j in range(1, slots - duration - i + 2):
                self.lconsts += [["LC" + str(i+1) + "." + str(j)] +
                                 [0.0] * (i-1) + [1.0] + [0.0] * (duration - 1) +
                                 [0.0] * (j-1) + [1.0] + [0.0] * (slots - i - duration - j + 1)]
        # local constraints done

        self.lbounds = [["LC1.1", 'data', 'data']]
        for i in range(1, slots - duration):
            for j in range(1, slots - duration - i + 2):
                self.lbounds += [["LC" + str(i+1) + "." + str(j), 0.0, 1.0]]
        # local constraints bounds done


        ### Energy balance constraints ###
        self.gconsts = [[gcset[0]+"1.1", 'E'] + [0.0] * (slots - 1)]
        for i in range(2, slots + 1):
            self.gconsts += [[gcset[0]+"1." + str(i)] + [0.0] * (i - 1) + ['E'] + [0.0] * (slots - i)]

        self.gconsts += [[gcset[0]+"3.1", '-E'] + [0.0] * (slots - 1)]
        for i in range(2, slots + 1):
            self.gconsts += [[gcset[0]+"3." + str(i)] + [0.0] * (i - 1) + ['-E'] + [0.0] * (slots - i)]
            # global constraints done

        for i in range(1, slots + 1):
            self.gbounds += [[gcset[0]+"1." + str(i), 0.0, 0.0]]
        for i in range(1, slots + 1):
            self.gbounds += [[gcset[0]+"3." + str(i), 0.0, 0.0]]
        ### End of energy balance constraints ###

        ### Slave constraints  (master complement) ###
        if gcset[1] is not None:
            gap = duration + mduration + mdelay

            # this prevents the slave task from starting early
            for i in range(1, slots + 1):
                self.gconsts += [[gcset[1] + "0." + str(i)] +
                                 [1.0] * i + [0.0] * (slots - i)]

            # this prevent the slave task from starting late
            for i in range(1, slots + 2 - gap):
                for j in range(i+gap, slots + 1):
                    self.gconsts += [[gcset[1] + str(i) + "." + str(j)] +
                                         [0.0] * (j - 1) + [1.0] + [0.0] * (slots - j)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "0." + str(i), 0.0, float(duration)]]

            for i in range(1, slots + 2 - gap):
                for j in range(i + gap, slots + 1 - gap):
                    self.gbounds += [[gcset[1] + str(i) + "." + str(j), 0.0, 1.0]]

        ### End of slave constraints ###

        ### Master constraints (slave complement)###
        if gcset[2] is not None:
            gap = duration + sduration + sdelay
            # this prevents the slave task from starting early
            for i in range(1, slots + 1):
                self.gconsts += [[gcset[2] + "0." + str(i)] +
                                 [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

            # this prevents the slave task from starting late
            for i in range(1, slots + 2 - gap):
                for j in range(i + gap, slots + 1):
                    self.gconsts += [[gcset[2] + str(i) + "." + str(j)] + [0.0] * (i - 1) +
                                         [1.0] + [0.0] * (slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[2] + "0." + str(i), 0.0, float(sduration)]]

            for i in range(1, slots + 2 - gap):
                for j in range(i + gap, slots + 1):
                    self.gbounds += [[gcset[2] + str(i) + "." + str(j), 0.0, 1.0]]



                        ### End of master constraints ###

        # global constraints bounds done

    def store_to_file(self, filename):
        """This function stores the model of a subproblem to a file."""

        filehandle = open(filename, 'w')

        filehandle.write("[params]\n\n")

        for i in self.params:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[object function]\n\n")
        for i in self.obj_fun:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[variable bounds]\n\n")
        for i in self.vbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[local constraints]\n\n")
        for i in self.lconsts:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")
        filehandle.write("[local con. bounds]\n\n")
        for i in self.lbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[global constraints]\n\n")
        for i in self.gconsts:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")
        filehandle.write("[global con. bounds]\n\n")
        for i in self.gbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[end]\n\n")

        filehandle.close()

    def load_from_file(self, filename):
        """This function loads the model of a subproblem from a file."""

        filehandle = open(filename, 'r')

        while True:
            currentline = filehandle.readline()
            currentline = currentline.rstrip('\n')

            if len(currentline) == 0:
                continue
            elif currentline == "[end]":
                break
            elif (currentline[0] == "[" and (
                                                currentline == "[params]" or
                                                currentline == "[object function]" or
                                                currentline == "[variable bounds]" or
                                                currentline == "[local constraints]" or
                                                currentline == "[local con. bounds]" or
                                                currentline == "[global constraints]" or
                                                currentline == "[global con. bounds]")
                  ):
                lastline = currentline
                continue
            elif lastline == "[params]":
                self.params += [eval(currentline)]
            elif lastline == "[object function]":
                self.obj_fun += [eval(currentline)]
            elif lastline == "[variable bounds]":
                self.vbounds += [eval(currentline)]
            elif lastline == "[local constraints]":
                self.lconsts += [eval(currentline)]
            elif lastline == "[local con. bounds]":
                self.lbounds += [eval(currentline)]
            elif lastline == "[global constraints]":
                self.gconsts += [eval(currentline)]
            elif lastline == "[global con. bounds]":
                self.gbounds += [eval(currentline)]

        filehandle.close()
