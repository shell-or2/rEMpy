# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import os


class RawModel:
    """Template of a raw sub problem model for an optimization problem."""

    def __init__(self, flag="", params={'slots': 0, 'ele': 0.3, 'heat': 0.6, 'chill': 0,
                                        'eff': 0.9, 'charge_eff': 0.9, 'discharge_eff': 0.9}, gcset=[], filename=""):

        slots = params['slots']

        self.params = []

        self.obj_fun = []
        self.vbounds = []

        self.lconsts = []
        self.lbounds = []

        self.gconsts = []
        self.gbounds = []

        if flag != "" and slots != 0:

            ini_file = filename

            if not os.path.isfile(ini_file):
                os.makedirs(os.path.dirname(filename), exist_ok=True)

                self.__modelgen_(flag, params, gcset)
                self.store_to_file(filename)

    def __modelgen_(self, flag="", params={'slots': 0, 'ele': 0.3, 'heat': 0.6, 'chill': 0,
                                           'eff': 0.9, 'charge_eff': 0.9, 'discharge_eff': 0.9}, gcset=["GC"]):

        slots = params['slots']
        self.params = [["E", 'data'], ["P", 'data'], ["C", 'data']]
        # model params done

        if (flag == "storage"):

            charge_eff = params['charge_eff']
            discharge_eff = params['discharge_eff']

            # self.obj_fun = [["OF"]+[-1.0]*slots+[1.0]*slots+[0.0]*slots,["OFT"]+['C']*3*slots]
            self.obj_fun = [["OF"] + [0.0] * slots + [0.0] * slots + [0.0] * slots, ["OFT"] + ['C'] * 3 * slots]
            # obj funct done

            for i in range(1, 2 * slots + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]
            for i in range(2 * slots + 1, 3 * slots + 1):
                self.vbounds += [["X" + str(i), 0.0, 'C']]
            # v bounds done


            self.lconsts = [
                ["LC1", -charge_eff] + [0.0] * (slots - 1) + [1.0/discharge_eff] + [0.0] * (slots - 1) + [1.0] + [0.0] * (slots - 1)]
            for i in range(2, slots + 1):
                self.lconsts += [["LC" + str(i)] +
                                 [0.0] * (i - 1) + [-charge_eff] + [0.0] * (slots - i) +
                                 [0.0] * (i - 1) + [1.0/discharge_eff] + [0.0] * (slots - i) +
                                 [0.0] * (i - 2) + [-1.0] + [1.0] + [0.0] * (slots - i)]

            # local constraints done

            self.lbounds = [["LC1", 'data', 'data']]
            for i in range(2, slots + 1):
                self.lbounds += [["LC" + str(i), 0.0, 0.0]]

            # local constraints bounds done

            self.gconsts = [[gcset[0]+"1.1", 1.0] + [0.0] * (3 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0]+"1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (3 * slots - i)]

            self.gconsts += [[gcset[0]+"2.1"] + [0.0] * (slots) + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0]+"2." + str(i)] + [0.0] * (slots) + [0.0] * (i - 1) + [1.0] + [0.0] * (2 * slots - i)]

            self.gconsts += [[gcset[0]+"3.1", -1.0] + [0.0] * (slots - 1) + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0]+"3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - 1) + [1.0]
                                 + [0.0] * (2 * slots - i)]
            # global constraints done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0]+"1." + str(i), 0.0, 0.0]]
            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0]+"2." + str(i), 0.0, 0.0]]
            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0]+"3." + str(i), 0.0, 0.0]]

                # global constraints bounds done

        elif flag == "prod" or flag == "purchaser" or flag == "seller" or flag == "load":

            if (flag == "seller"):
                self.obj_fun = [["OF"]+[-1.0]*slots,["OFT"]+['C']*slots]
            elif (flag == "purchaser"):
                self.obj_fun = [["OF"] + [1.0] * slots, ["OFT"] + ['C'] * slots]
            elif (flag == "prod"):
                # self.obj_fun = [["OF"]+[1.0]*slots,["OFT"]+['C']*slots]
                self.obj_fun = [["OF"] + [0.0] * slots, ["OFT"] + ['C'] * slots]
            else:
                # self.obj_fun = [["OF"]+[-1.0]*slots,["OFT"]+['C']*slots]
                self.obj_fun = [["OF"] + [0.0] * slots, ["OFT"] + ['C'] * slots]
            # obj funct done

            for i in range(1, slots + 1):
                self.vbounds += [["X" + str(i), 'data', 'data']]
            # v bounds done

            self.lconsts = []

            # local constraints done

            self.lbounds = []

            # local constraints bounds done

            if (flag == "prod" or flag == "purchaser"):
                self.gconsts = [[gcset[0]+"2.1", 1.0] + [0.0] * (slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [[gcset[0]+"2." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

                self.gconsts += [[gcset[0]+"3.1", 1.0] + [0.0] * (slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [[gcset[0]+"3." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]
                    # global constraints done

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[0]+"2." + str(i), 0.0, 0.0]]
                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[0]+"3." + str(i), 0.0, 0.0]]

            # global constraints bounds done
            else:

                self.gconsts = [[gcset[0]+"1.1", 1.0] + [0.0] * (slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [[gcset[0]+"1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

                self.gconsts += [[gcset[0]+"3.1", -1.0] + [0.0] * (slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [[gcset[0]+"3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - i)]
                    # global constraints done

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[0]+"1." + str(i), 0.0, 0.0]]
                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[0]+"3." + str(i), 0.0, 0.0]]

                    # global constraints bounds done

        elif flag == "gen":

            eff = params['eff']

            self.obj_fun = [["OF"] + [0.0] * slots * 2, ["OFT"] + ['C'] * slots * 2]

            # obj funct done

            for i in range(1, slots * 2 + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]

            # v bounds done

            self.lconsts = [["LC1.1", -eff] + [0.0] * (slots - 1) + ([1.0] + [0.0] * (slots - 1))]

            for i in range(2, slots + 1):
                self.lconsts += [["LC1." + str(i)] +
                                 [0.0] * (i - 1) + [-eff] + [0.0] * (slots - i) +
                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i))]

            # local constraints done

            self.lbounds = [["LC1.1", 0.0, 0.0]]

            for i in range(2, slots + 1):
                self.lbounds += [["LC1." + str(i), 0.0, 0.0]]

            # local constraints bounds done

            self.gconsts += [[gcset[0] + "1.1"] + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0] + "1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (2 * slots - i)]

            self.gconsts += [[gcset[0] + "3.1"] + [-1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0] + "3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (2 * slots - i)]
            # global constraints first set done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "1." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "3." + str(i), 0.0, 0.0]]
            # global constraints bounds first set done

            self.gconsts += [[gcset[1] + "2.1"] + [0.0] * slots + [1.0] + [0.0] * (slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[1] + "2." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

            self.gconsts += [[gcset[1] + "3.1"] + [0.0] * slots + [1.0] + [0.0] * (slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[1] + "3." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "3." + str(i), 0.0, 0.0]]

                # global constraints bounds done

        elif flag == "trigen":

            ele = params['ele']
            heat = params['heat']
            chill = params['chill']

            self.obj_fun = [["OF"] + [0.0] * slots * 5, ["OFT"] + ['C'] * slots * 5]

            # obj funct done


            for i in range(1, slots * 5 + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]

                # v bounds done

            self.lconsts = [["LC1.1"] +
                            [-1.0] + [0.0] * (slots - 1) +
                            (([1.0] + [0.0] * (slots - 1)) * 4)]
            for i in range(2, slots + 1):
                self.lconsts += [["LC1." + str(i)] +
                                 [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - i) +
                                 (([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) * 4)]
            self.lconsts += [["LC2.1"] +
                             [-ele] + [0.0] * (slots - 1) +
                             ([1.0] + [0.0] * (slots - 1)) +
                             [0.0] * slots * 3]
            for i in range(2, slots + 1):
                self.lconsts += [["LC2." + str(i)] +
                                 [0.0] * (i - 1) + [-ele] + [0.0] * (slots - i) +
                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) +
                                 [0.0] * slots * 3]
            self.lconsts += [["LC3.1"] +
                             [-heat] + [0.0] * (slots - 1) + [0.0] * slots +
                             ([1.0] + [0.0] * (slots - 1)) + [0.0] * slots * 2]
            for i in range(2, slots + 1):
                self.lconsts += [["LC3." + str(i)] +
                                 [0.0] * (i - 1) + [-heat] + [0.0] * (slots - i) + [0.0] * slots +
                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) + [0.0] * slots * 2]
            self.lconsts += [["LC4.1"] +
                             [-chill] + [0.0] * (slots - 1) + [0.0] * slots * 2 +
                             ([1.0] + [0.0] * (slots - 1)) + [0.0] * slots]
            for i in range(2, slots + 1):
                self.lconsts += [["LC4." + str(i)] +
                                 [0.0] * (i - 1) + [-chill] + [0.0] * (slots - i) + [0.0] * slots * 2 +
                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) + [0.0] * slots]
            # local constraints done

            self.lbounds = []

            for i in range(1, slots + 1):
                self.lbounds += [["LC1." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.lbounds += [["LC2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.lbounds += [["LC3." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.lbounds += [["LC4." + str(i), 0.0, 0.0]]

            # local constraints bounds done

            self.gconsts += [[gcset[0] + "1.1"] + [1.0] + [0.0] * (5 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0] + "1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (5 * slots - i)]

            self.gconsts += [[gcset[0] + "3.1"] + [-1.0] + [0.0] * (5 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0] + "3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (5 * slots - i)]
            # global constraints first set done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "1." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "3." + str(i), 0.0, 0.0]]
            # global constraints bounds first set done
            # input (gas-watthours) constraints and bounds done

            self.gconsts += [[gcset[1] + "2.1"] + [0.0] * slots + [1.0] + [0.0] * (4 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[1] + "2." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * (4 * slots - i)]

            self.gconsts += [[gcset[1] + "3.1"] + [0.0] * slots + [1.0] + [0.0] * (4 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[1] + "3." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * (4 * slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "3." + str(i), 0.0, 0.0]]
            # output (electricity-watthours) constraints and bounds done


            self.gconsts += [[gcset[2] + "2.1"] + [0.0] * slots * 2 + [1.0] + [0.0] * (3 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[2] + "2." + str(i)] + [0.0] * slots * 2 + [0.0] * (i - 1) + [1.0] + [0.0] * (3 * slots - i)]

            self.gconsts += [[gcset[2] + "3.1"] + [0.0] * slots * 2 + [1.0] + [0.0] * (3 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[2] + "3." + str(i)] + [0.0] * slots * 2 + [0.0] * (i - 1) + [1.0] + [0.0] * (3 * slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[2] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[2] + "3." + str(i), 0.0, 0.0]]
            # output (heat-watthours) constraints and bounds done


            self.gconsts += [[gcset[3] + "2.1"] + [0.0] * slots * 3 + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[3] + "2." + str(i)] + [0.0] * slots * 3 + [0.0] * (i - 1) + [1.0] + [0.0] * (2 * slots - i)]

            self.gconsts += [[gcset[3] + "3.1"] + [0.0] * slots * 3 + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[3] + "3." + str(i)] + [0.0] * slots * 3 + [0.0] * (i - 1) + [1.0] + [0.0] * (2 * slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[3] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[3] + "3." + str(i), 0.0, 0.0]]
            # output (chill-watthours) constraints and bounds done

        elif flag == "pipe":

            self.obj_fun = [["OF"] + [0.0] * slots * 2, ["OFT"] + ['C'] * slots * 2]

            # obj funct done


            for i in range(1, slots * 2 + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]

            # v bounds done


            self.lconsts = [["LC1.1", -1.0] + [0.0] * (slots - 1) + ([1.0] + [0.0] * (slots - 1))]

            for i in range(2, slots + 1):
                self.lconsts += [["LC1." + str(i)] +
                                 [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - i) +
                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i))]

            # local constraints done

            self.lbounds = [["LC1.1", 0.0, 0.0]]

            for i in range(2, slots + 1):
                self.lbounds += [["LC1." + str(i), 0.0, 0.0]]

            # local constraints bounds done

            self.gconsts += [[gcset[0] + "1.1"] + [1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0] + "1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (2 * slots - i)]

            self.gconsts += [[gcset[0] + "3.1"] + [-1.0] + [0.0] * (2 * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0] + "3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (2 * slots - i)]
            # global constraints first set done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "1." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "3." + str(i), 0.0, 0.0]]
            # global constraints bounds first set done

            self.gconsts += [[gcset[1] + "2.1"] + [0.0] * slots + [1.0] + [0.0] * ( slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[1] + "2." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * ( slots - i)]

            self.gconsts += [[gcset[1] + "3.1"] + [0.0] * slots + [1.0] + [0.0] * (slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[1] + "3." + str(i)] + [0.0] * slots + [0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[1] + "3." + str(i), 0.0, 0.0]]

            # global constraints bounds done

        elif flag == "load_hub":

            num_sets = len(gcset)

            # print("num sets =" + str(num_sets))

            # self.obj_fun = [["OF"]+[-1.0]*slots,["OFT"]+['C']*slots]

            self.obj_fun = [["OF"] + [0.0] * slots * num_sets, ["OFT"] + ['C'] * slots * num_sets]

            # obj funct done


            for i in range(1, slots * num_sets + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]

            # v bounds done


            self.lconsts = [

                ["LC1.1", -1.0] + [0.0] * (slots - 1) + ([1.0] + [0.0] * ( slots - 1)) * (num_sets - 1)]

            for i in range(2, slots + 1):
                self.lconsts += [["LC1." + str(i)] +

                                 [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - i) +

                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) * (num_sets - 1)]

            # local constraints done

            self.lbounds = [["LC1.1", 0.0, 0.0]]

            for i in range(2, slots + 1):
                self.lbounds += [["LC1." + str(i), 0.0, 0.0]]

            # local constraints bounds done


            # self.gconsts = [[gcset[0] + "1.1"] + [1.0] + [0.0] * (num_sets * slots - 1)]
            # for i in range(2, slots + 1):
            #     self.gconsts += [
            #         [gcset[0] + "1." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (num_sets * slots - i)]

            self.gconsts = [[gcset[0] + "1.1"] + [0.0] + [0.0] * (num_sets * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0] + "1." + str(i)] + [0.0] * (i - 1) + [0.0] + [0.0] * (num_sets * slots - i)]

            self.gconsts += [[gcset[0] + "3.1"] + [1.0] + [0.0] * (num_sets * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0] + "3." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (num_sets * slots - i)]
            # global constraints first set done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "1." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "3." + str(i), 0.0, 0.0]]

            for ns in range(1, num_sets):
                # global constraints bounds first set done

                self.gconsts += [[gcset[ns] + "1.1"] + [0.0] * slots * ns + [1.0] + [0.0] * ((num_sets - ns) * slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [
                        [gcset[ns] + "1." + str(i)] + [0.0] * slots * ns + [0.0] * (i - 1) + [1.0] + [0.0] * ((num_sets - ns) * slots - i)]

                self.gconsts += [[gcset[ns] + "3.1"] + [0.0] * slots * ns + [-1.0] + [0.0] * ((num_sets - ns) * slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [
                        [gcset[ns] + "3." + str(i)] + [0.0] * slots * ns + [0.0] * (i - 1) + [-1.0] + [0.0] * ((num_sets - ns) * slots - i)]

              # global constraints done

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[ns] + "1." + str(i), 0.0, 0.0]]

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[ns] + "3." + str(i), 0.0, 0.0]]

                # global constraints bounds done

        elif flag == "prod_hub":

            num_sets = len(gcset)

            # print("num sets =" + str(num_sets))

            # self.obj_fun = [["OF"]+[-1.0]*slots,["OFT"]+['C']*slots]

            self.obj_fun = [["OF"] + [0.0] * slots * num_sets, ["OFT"] + ['C'] * slots * num_sets]

            # obj funct done


            for i in range(1, slots * num_sets + 1):
                self.vbounds += [["X" + str(i), 0.0, 'E']]

            # v bounds done


            self.lconsts = [

                ["LC1.1", -1.0] + [0.0] * (slots - 1) + ([1.0] + [0.0] * (slots - 1)) * (num_sets - 1)]

            for i in range(2, slots + 1):
                self.lconsts += [["LC1." + str(i)] +

                                 [0.0] * (i - 1) + [-1.0] + [0.0] * (slots - i) +

                                 ([0.0] * (i - 1) + [1.0] + [0.0] * (slots - i)) * (num_sets - 1)]

            # local constraints done

            self.lbounds = [["LC1.1", 0.0, 0.0]]

            for i in range(2, slots + 1):
                self.lbounds += [["LC1." + str(i), 0.0, 0.0]]

            # local constraints bounds done

            # self.gconsts += [[gcset[0] + "2.1"] + [1.0] + [0.0] * (num_sets * slots - 1)]
            # for i in range(2, slots + 1):
            #     self.gconsts += [
            #         [gcset[0] + "2." + str(i)] + [0.0] * (i - 1) + [1.0] + [0.0] * (num_sets * slots - i)]

            self.gconsts += [[gcset[0] + "2.1"] + [0.0] + [0.0] * (num_sets * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [
                    [gcset[0] + "2." + str(i)] + [0.0] * (i - 1) + [0.0] + [0.0] * (num_sets * slots - i)]

            self.gconsts += [[gcset[0] + "3.1"] + [-1.0] + [0.0] * (num_sets * slots - 1)]
            for i in range(2, slots + 1):
                self.gconsts += [[gcset[0] + "3." + str(i)] + [0.0] * (i - 1) + [-1.0] + [0.0] * (num_sets * slots - i)]

            # global constraints first set done

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "2." + str(i), 0.0, 0.0]]

            for i in range(1, slots + 1):
                self.gbounds += [[gcset[0] + "3." + str(i), 0.0, 0.0]]
            # global constraints bounds first set done

            for ns in range(1, num_sets):

                self.gconsts += [
                    [gcset[ns] + "2.1"] + [0.0] * slots * ns + [1.0] + [0.0] * ((num_sets - ns) * slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [
                        [gcset[ns] + "2." + str(i)] + [0.0] * slots * ns + [0.0] * (i - 1) + [1.0] + [0.0] * (
                        (num_sets - ns) * slots - i)]

                self.gconsts += [
                    [gcset[ns] + "3.1"] + [0.0] * slots * ns + [1.0] + [0.0] * ((num_sets - ns) * slots - 1)]
                for i in range(2, slots + 1):
                    self.gconsts += [
                        [gcset[ns] + "3." + str(i)] + [0.0] * slots * ns + [0.0] * (i - 1) + [1.0] + [0.0] * (
                        (num_sets - ns) * slots - i)]

                    # global constraints done

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[ns] + "2." + str(i), 0.0, 0.0]]

                for i in range(1, slots + 1):
                    self.gbounds += [[gcset[ns] + "3." + str(i), 0.0, 0.0]]

                    # global constraints bounds done





    def store_to_file(self, filename):
        """This function stores the model of a subproblem to a file."""

        filehandle = open(filename, 'w')

        filehandle.write("[params]\n\n")

        for i in self.params:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[object function]\n\n")
        for i in self.obj_fun:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[variable bounds]\n\n")
        for i in self.vbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[local constraints]\n\n")
        for i in self.lconsts:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")
        filehandle.write("[local con. bounds]\n\n")
        for i in self.lbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[global constraints]\n\n")
        for i in self.gconsts:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")
        filehandle.write("[global con. bounds]\n\n")
        for i in self.gbounds:
            filehandle.write(str(i) + "\n")
        filehandle.write("\n\n")

        filehandle.write("[end]\n\n")

        filehandle.close()

    def load_from_file(self, filename):
        """This function loads the model of a subproblem from a file."""

        filehandle = open(filename, 'r')

        while True:
            currentline = filehandle.readline()
            currentline = currentline.rstrip('\n')

            if len(currentline) == 0:
                continue
            elif currentline == "[end]":
                break
            elif (currentline[0] == "[" and (
                                                currentline == "[params]" or
                                                currentline == "[object function]" or
                                                currentline == "[variable bounds]" or
                                                currentline == "[local constraints]" or
                                                currentline == "[local con. bounds]" or
                                                currentline == "[global constraints]" or
                                                currentline == "[global con. bounds]")
                  ):
                lastline = currentline
                continue
            elif lastline == "[params]":
                self.params += [eval(currentline)]
            elif lastline == "[object function]":
                self.obj_fun += [eval(currentline)]
            elif lastline == "[variable bounds]":
                self.vbounds += [eval(currentline)]
            elif lastline == "[local constraints]":
                self.lconsts += [eval(currentline)]
            elif lastline == "[local con. bounds]":
                self.lbounds += [eval(currentline)]
            elif lastline == "[global constraints]":
                self.gconsts += [eval(currentline)]
            elif lastline == "[global con. bounds]":
                self.gbounds += [eval(currentline)]

        filehandle.close()
