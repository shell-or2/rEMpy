# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import core.scheduler.default.mainproblem as mainproblem
import cplex
from cplex.exceptions import CplexError

from . import rawmodel
from defines import *

logger = logging.getLogger(__name__)


def solver(data):
    devices = data['devices']

    data_list = []
    obj_list = []
    subproblem_list = []
    gchss_data = data['gchss']

    problem = mainproblem.MainProblem()

    try:
        for i in range(0, len(devices)):

            obj_list += [rawmodel.RawModel()]
            obj_list[-1].load_from_file(devices[i].filename)
            data_list += [mainproblem.RunTimeData(devices[i])]
            subproblem_list += [problem.add_element(obj_list[-1], data_list[-1])]

            if subproblem_list[i] is None:
                logger.debug("subproblem_list[" + str(i) + "] == None")
                return subproblem_list[i]

    except CplexError as exc:
        logger.error(exc)
        return

    #     print(str(problem.constraints()))
    problem.finalize_model(gchss_data)

    #     print(str(problem.constraints()))
    #     print(str(problem.varnames()))
    #     print(str(problem.obj_fun()))
    #     print(str(problem.lb()))
    #     print(str(problem.ub()))

    #     problem.print_obj_fun()
    #     problem.print_constraints()

    try:
        prob = cplex.Cplex()

        prob.set_log_stream(os.path.join(LOG_DIR, "cplex_log.txt"), fn=None)
        prob.set_warning_stream(os.path.join(LOG_DIR, "cplex_warning.txt"), fn=None)
        prob.set_error_stream(os.path.join(LOG_DIR, "cplex_error.txt"), fn=None)
        prob.set_results_stream(os.path.join(LOG_DIR, "cplex_results.txt"), fn=None)

        prob.objective.set_sense(prob.objective.sense.minimize)

        prob.variables.add(obj=problem.obj_fun(), lb=problem.lb(), ub=problem.ub(), types=problem.vartypes(),
                           names=problem.varnames())
        prob.linear_constraints.add(lin_expr=problem.constraints(), senses=problem.c_senses(), rhs=problem.lhs(),
                                    range_values=problem.range_values())

        #       prob.write("problem_save","SAV")
        prob.set_problem_type(prob.problem_type.MILP, None)

        prob.solve()

    except CplexError as exc:
        logger.error(exc)
        return

    prob_sol = prob.solution.get_values()
    logger.debug("The solution vector is: " + str(prob_sol))
    #         prob.conflict.refine(prob.conflict.linear_constraints())
    #         print(str(prob.conflict.get()))

    for i in range(len(subproblem_list)):
        subproblem_list[i].extract_values(prob_sol)

    #        print(str(subproblem_list[i].obj_fun))
    #        print(str(subproblem_list[i].firstvar))
    #        print(str(subproblem_list[i].lastvar))
    #        print(str(subproblem_list[i].x))
    #        print(str(subproblem_list[i].charge))
    #        print(str(subproblem_list[i].discharge))

    return subproblem_list
