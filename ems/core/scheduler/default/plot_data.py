# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties


class PlotData:
    def __init__(self, x, y, labels, title):
        self.x = x
        self.y = y
        self.labels = labels
        self.ylabel = "Wh"
        self.title = title


def plot_data(geometry, data):
    fontP = FontProperties()
    fontP.set_size('small')
    fig, axarr = plt.subplots(geometry[0], geometry[1])
    data_count = len(data)
    if (geometry[0] == 1 and geometry[1] == 1):
        for i in range(0, len(data[0].x)):
            axarr.plot(data[0].x[i], data[0].y[i], label=data[0].labels[i])
            axarr.set_title(data[0].title)
            axarr.set_xlabel('Time Slots')
            axarr.set_ylabel(data[0].ylabel)
            axarr.legend(loc='center', bbox_to_anchor=(1, 1), prop=fontP)

    elif (geometry[0] > 1 and geometry[1] > 1):
        for k in range(0, geometry[1]):
            for j in range(0, geometry[0]):
                index = j + (k * geometry[0])
                if (index < data_count):
                    for i in range(0, len(data[index].x)):
                        axarr[j, k].plot(data[index].x[i], data[index].y[i], label=data[index].labels[i])
                        axarr[j, k].set_title(data[index].title, fontsize=8)
                        axarr[j, k].set_xlabel('Time Slots', fontsize=8)
                        axarr[j, k].set_ylabel(data[index].ylabel, fontsize=8)
                        axarr[j, k].legend(loc='lower right', bbox_to_anchor=(1, 1), prop=fontP)
                        axarr[j, k].tick_params(axis='x', labelsize=8)
                        axarr[j, k].tick_params(axis='y', labelsize=8)
    else:
        for k in range(0, geometry[1]):
            for j in range(0, geometry[0]):
                index = j + (k * geometry[0])
                if (index < data_count):
                    for i in range(0, len(data[index].x)):
                        axarr[index].plot(data[index].x[i], data[index].y[i], label=data[index].labels[i])
                        axarr[index].set_title(data[index].title, fontsize=8)
                        axarr[index].set_xlabel('Time Slots', fontsize=8)
                        axarr[index].set_ylabel(data[index].ylabel, fontsize=8)
                        axarr[index].legend(loc='lower right', bbox_to_anchor=(1, 1), prop=fontP)
                        axarr[index].tick_params(axis='x', labelsize=8)
                        axarr[index].tick_params(axis='y', labelsize=8)

    fig.subplots_adjust(hspace=1)
    fig.show()

