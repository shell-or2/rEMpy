# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import queue
from defines import *
from ems_db.schedule.routines import *
from ems_db.sensor.routines import *
logger = logging.getLogger(__name__)


MAX_ATTEMPTS = 3


def send_execution_command(task_name):
    # add exe command to interoperability framework

    # if command sent, return true, otherwise False
    return True  # False


def request_task_interruption(task_id):
    # add exe command to interrupt task

    # if command sent, return true, otherwise False
    return True  # False


def task_agent_thread(queue_with_core, ems_scheduling_event):

    try:

        while True:

            if ems_scheduling_event.is_set():
                logger.debug("Can't perform operation. Scheduling in progress.")
            else:
                logger.debug("Can perform operation. Scheduling not in progress.")
                # check DB and send command to run task
                # set DB task 'executed' and remove it from config file ems.json
                from_datetime = datetime.datetime.now()
                to_datetime = from_datetime + datetime.timedelta(seconds=30)
                tasks = get_executable_tasks(from_datetime, to_datetime)

                tasks = get_scheduled_tasks(datetime.datetime(2016, 1, 1), datetime.datetime(2016, 12, 30))

                # 3 attempts, if failed set Alert
                for task in tasks:
                    attempts_counter = 0
                    logger.debug("Sending execution command for task: " + task.get('name'))
                    while attempts_counter < MAX_ATTEMPTS:
                        logger.debug("Attempt " + str(attempts_counter))
                        if send_execution_command(task.get('name')):
                            set_task_executed(task.get('id'))
                            break
                        attempts_counter += 1

                    if attempts_counter == MAX_ATTEMPTS:
                        logger.debug("Added alert for" + task.get('name').lower())
                        add_alert(task.get('name').lower(), "Unable to execute task!")

                # stop task if requested
                try:
                    tasks_to_stop = queue_with_core.get_nowait()
                    for task_id in tasks_to_stop:
                        request_task_interruption(task_id)
                except queue.Empty:
                    pass  # no message in queue

            time.sleep(30)

    except Exception as err:
        logger.critical("Unhandled exception!")
        logger.critical(err)
        sys.exit(-1)


if __name__ == '__main__':

    setup_logging('task_agent_test')

    ems_scheduling_event = threading.Event()

    ems_scheduling_event.set()
    ems_scheduling_event.clear()

    # task_agent_thread = threading.Thread(name='task_agent', target=task_agent_thread,
    #                                      args=(ems_scheduling_event,))
    # task_agent_thread.start()
    #
    # time.sleep(10)
    #
    # ems_scheduling_event.clear()

    task_agent_thread(ems_scheduling_event)
