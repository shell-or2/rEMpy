# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import calendar
import logging
import queue
import threading
import time

import communication
from defines import *

logger = logging.getLogger(__name__)


# skeletons data for prediction and thermal (field names in defines.py)
prediction_data_skeleton = {'timestamp': []}

thermal_data_skeleton = {  # 'timestamp': [],
                         'outdoor_temperature': [],  # outdoor temperature
                         'outdoor_humidity': [],  # outdoor humidity
                         'radiation': [],  # radiation
                         'wind_vel': [],  # wind velocity
                         'wind_dir': [],  # wind direction
                         'cloud': [],  # cloudiness
                         'pressure': [],  # atmospheric pressure
                         'setpoint1': [],  # temperature set-point room 1
                         'setpoint2': [],  # temperature set-point room 2
                         'setpoint3': [],  # temperature set-point room 3
                         'setpoint4': [],  # temperature set-point room 4
                         'setpoint5': []}  # temperature set-point room 5


def difftime(epoch_time, threshold_in_minutes):
    """ Compute the difference between the given time and the current one.
        Return true if the difference exceeds the given threshold.
    """

    diff_in_sec = (calendar.timegm(time.gmtime()) - epoch_time)

    if diff_in_sec > threshold_in_minutes*60:
        return True

    return False


# def check_respawn(ems_data):
#     """ Specify routine that verify if process re-spawn.
#         Reload previous information and data.
#      """
#     ems_data.load()
#
#     # no stored data
#     if not ems_data.get_value('timestamp'):
#         ems_data.set_data({'horizon': DEFAULT_HORIZON, 'resolution': DEFAULT_RESOLUTION})
#         logger.debug('No EMS data stored, default initialization.')
#     else:
#         logger.debug('Loaded EMS data with timestamp: ' + str(ems_data.get_value('timestamp')))
#         send_plot_to_interface(ems_data.get_plot_data())
#
#     ems_scheduling_state = EMSStateClass(EMS_WAIT)
#
#     return ems_scheduling_state


# def request_kill(queue_number):
#
#     logger.debug('Requesting kill of: ' + MODULES_NAMES[queue_number])
#     msg = {MSG_ID_PREDICTION_CANCEL: None}
#     if core_queues.sendMsg(queue_number, msg, True):
#         logger.debug('Requested prediction cancellation.')
#     else:
#         logger.info("Problem with queue: " + MODULES_NAMES[queue_number])


def mainprocess():
    """ Main process: manage the scheduler and control the connection queues. """
    # shared_core_to_prediction_q, shared_prediction_to_core_q,
    #           shared_core_to_thermal_q, shared_thermal_to_core_q,
    #           shared_core_to_interface_q, shared_interface_to_core_q):

    # Init Core
    setup_logging("core")
    core_queues = communication.CoreQueues()

    def send_status_to_interface(text_msg):
        msg = {MSG_ID_DATA_TO_INTERFACE: {'status': text_msg}}
        if EMS_TEST_MODE:
            return 0
        if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
            logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

    # def send_plot_to_interface(plot_data):
    #     msg = {MSG_ID_DATA_TO_INTERFACE: plot_data}
    #
    #     if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
    #         logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

    def check_respawn(_ems_data):
        """ Specify routine that verify if process re-spawn.
               Reload previous information and data.
        """
        _ems_data.load()

        # no stored data
        if not _ems_data.get_value('timestamp'):
            _ems_data.set_data({'horizon': DEFAULT_HORIZON, 'resolution': DEFAULT_RESOLUTION})
            logger.info('No EMS data stored, default initialization.')
        else:
            logger.info('Loaded EMS data with timestamp: ' + str(_ems_data.get_value('timestamp')))
            send_status_to_interface('EMS restored data with timestamp: ' +
                                     time.strftime('%d-%m-%Y %H:%M:%S',
                                                   time.localtime(_ems_data.get_value('timestamp'))))
            # send_plot_to_interface(_ems_data.get_plot_data())

        return EMSStateClass(EMS_WAIT)

    # init scheduler thread vars
    output_scheduler_data = queue.Queue()
    stop_event = threading.Event()
    scheduler_thread = None

    # Create structure for store exchanged data
    ems_data = EMSData()

    # epoch_starttime = None  # initialized to None for the first control
    ems_scheduling_state = check_respawn(ems_data)

    # first of all, send default settings (horizon and resolution) to all the modules
    # msg = {MSG_ID_CONFIG: ems_data.get_temporal_data()}
    #
    # for q in core_queues.queues_number():
    #     logger.debug("Send message to: " + MODULES_NAMES[q])
    #
    #     if EMS_TEST_MODE:
    #         continue
    #
    #     if not core_queues.sendMsg(q, msg, True):
    #         logger.info("Problem with queue: " + MODULES_NAMES[q])
    # send config to interface
    msg = {MSG_ID_DATA_TO_INTERFACE: {'status': 'Core sending configuration information to Interface',
                                      'db_config': {'db_type': 'file', 'path': EMS_STORE_DATA_FILE}}}

    if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
        logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

    timeout_event = threading.Event()

    # example, set ems_scheduling_state and epoch_timestamp
    # TODO: define scheduler data and process information to save and restore in case of re-spawn

    #######################################################
    ################ MAIN LOOP ############################
    #######################################################
    while True:

        # if None -> epoch initialization and first scheduler call
        # other calls only if passed resolution time
        if not ems_data.get_value('timestamp') or difftime(ems_data.get_value('timestamp'), ems_data.get_value('horizon')):

            if ems_scheduling_state.isEqual(EMS_WAIT):
                # epoch_starttime = calendar.timegm(time.gmtime())
                # ems_data.set_value('timestamp', epoch_starttime)

                ems_data.set_value('timestamp', calendar.timegm(time.gmtime()))
                logging.debug('Updated epoch start time: ' + str(ems_data.get_value('timestamp')))

                send_status_to_interface('Start scheduling routine.')
                logger.debug('Start scheduling routine.')
                ems_scheduling_state.update()  # EMS_PREDICTION_REQUEST
                logger.debug(ems_scheduling_state)

            else:
                logger.warning('Scheduler routine in progress, or something is wrong!')
                # TODO: check why state is on EMS_WAIT

        try:

            # check if received messages in each input queues (controller to core queue)
            for q in core_queues.queues_number():
                message_id, message_data = core_queues.recvMsg(q, True)

                if message_id:
                    # logger.debug("Received message from " + MODULES_NAMES[q] + ": "
                    #  + str(message_id) + ": " + str(message_data))

                    ###########################################################################
                    # evaluate if message from INTERFACE
                    if message_id == MSG_ID_INTERFACE_REQUEST_CONFIG:
                        logger.debug("Received config request from Interface.")

                        msg = {MSG_ID_DATA_TO_INTERFACE: {'status': 'Core sending configuration information to Interface',
                                                            'db_config': {'db_type': 'file',
                                                                          'path': EMS_STORE_DATA_FILE}}}

                        if not core_queues.sendMsg(INTERFACE_QUEUE, msg, True):
                            logger.info("Problem with queue: " + MODULES_NAMES[INTERFACE_QUEUE])

                    ###########################################################################
                    # evaluate if message from PREDICTION
                    if message_id == MSG_ID_PREDICTION_RESPONSE:
                        logger.debug("Received prediction data.")

                        # check EMS status and update it
                        if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):

                            # update ems_data with data receiver from prediction
                            ems_data.set_data(message_data)

                            ems_scheduling_state.update()  # EMS_THERMAL_REQUEST
                            logger.debug(ems_scheduling_state)
                            send_status_to_interface('Computing prediction...OK!')
                            # cancel timeout timer
                            prediction_timer.cancel()
                        else:
                            logger.warning('Prediction not requested.')

                    ###########################################################################
                    # evaluate if message from THERMAL
                    if message_id == MSG_ID_THERMAL_RESPONSE:
                        logger.debug("Received thermal data.")

                        # check EMS status and update it
                        if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):

                            # update ems_data with data receiver from thermal
                            ems_data.set_data(message_data)

                            ems_scheduling_state.update()  # EMS_SCHEDULING_REQUEST
                            logger.debug(ems_scheduling_state)
                            send_status_to_interface('Computing thermal model...OK!')
                            thermal_timer.cancel()
                        else:
                            logger.warning('Thermal modelling not requested.')

            ###########################################################################
            # test for request cancellation
            ###########################################################################
            if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):

                test_kill = False
                if test_kill:
                    time.sleep(2)
                    # request_kill(PREDICTION_QUEUE)
                    # TODO: wait response from module if request cancellation?
                    ems_scheduling_state.reset()  # EMS_WAIT
                    logger.debug(ems_scheduling_state)

            if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):

                test_kill = False

                if test_kill:
                    time.sleep(2)
                    # request_kill(THERMAL_QUEUE)
                    # TODO: wait response from module if request cancellation?
                    ems_scheduling_state.reset()  # EMS_WAIT
                    logger.debug(ems_scheduling_state)

            ###########################################################################
            ###########################################################################

            # TODO: Send messages to controllers/modules.
            # TODO: Insert a timer for each communication, and delete message from queue when timeout reached.

            if ems_scheduling_state.isEqual(EMS_PREDICTION_REQUEST):
                logger.debug("Requesting prediction.")

                # Compose data for prediction
                msg = {MSG_ID_PREDICTION_REQUEST: ems_data.get_prediction_data()}  # prediction_data_skeleton

                if core_queues.sendMsg(PREDICTION_QUEUE, msg, True):
                    ems_scheduling_state.update()  # EMS_PREDICTION_WAIT
                    logger.debug(ems_scheduling_state)
                    send_status_to_interface('Computing prediction...')
                    # start timer for max response wait
                    prediction_timer = threading.Timer(PREDICTION_TIMEOUT, lambda: timeout_event.set())
                    prediction_timer.start()
                else:
                    logger.info("Problem with queue: " + MODULES_NAMES[PREDICTION_QUEUE])

            if ems_scheduling_state.isEqual(EMS_THERMAL_REQUEST):
                logger.debug("Requesting thermal modelling.")

                # if missing setpoints, set default values for current horizon
                if not ems_data.get_value('setpoint1'):

                    # rooms_names = ['Edificio:Bagno', 'Edificio:Soggiorno', 'Edificio:Camera1', 'Edificio:Camera2', 'Edificio:Cucina']
                    # camera1 matrimoniale
                    # camera2 bambini
                    setpoint1 = [21] * 25
                    setpoint2 = [20] * 25
                    setpoint3 = [18] * 25
                    setpoint4 = [20] * 25
                    setpoint5 = [17] * 25

                    ems_data.set_data({'setpoint1': setpoint1})
                    ems_data.set_data({'setpoint2': setpoint2})
                    ems_data.set_data({'setpoint3': setpoint3})
                    ems_data.set_data({'setpoint4': setpoint4})
                    ems_data.set_data({'setpoint5': setpoint5})

                # Compose data for thermal model
                msg = {MSG_ID_THERMAL_REQUEST: ems_data.get_thermal_data()}  # thermal_data_skeleton}

                if core_queues.sendMsg(THERMAL_QUEUE, msg, True):
                    ems_scheduling_state.update()  # EMS_THERMAL_WAIT
                    logger.debug(ems_scheduling_state)
                    send_status_to_interface('Computing thermal model...')
                    thermal_timer = threading.Timer(THERMAL_TIMEOUT, lambda: timeout_event.set())
                    thermal_timer.start()
                else:
                    logger.info("Problem with queue: " + MODULES_NAMES[THERMAL_QUEUE])

            if ems_scheduling_state.isEqual(EMS_SCHEDULING_REQUEST):
                logger.debug("Requesting scheduling.")

                input_scheduler_data = ems_data.get_scheduler_data()

                #  grandezze temporanee da ottenere dal framework di interoperabilità
                storage_leves = ems_data.get_value('storage_levels')

                if not storage_leves:
                    init_charge = 3300*2
                else:
                    # TODO: insert timestep check
                    init_charge = storage_leves[-1]

                input_scheduler_data.update({'storage_init_charge': init_charge,  # attenzione: se storage troppo grande cplex non trova soluzione
                                             'storage_size': 10000.0,
                                             'price': [1] * 25,
                                             'max_load': 3300,
                                             'max_PV': 3300,
                                             'max_therm': 3300})

                partner_scheduler = __import__('core.scheduler.' + SCHEDULER_MODULE + '.scheduler_main', globals(),
                                               locals(), 'prediction', 0)

                scheduler_thread = threading.Thread(name='scheduler_thread',
                                                    target=partner_scheduler.scheduler,
                                                    args=(input_scheduler_data, output_scheduler_data, stop_event,))
                stop_event.clear()
                ems_scheduling_state.update()
                logger.debug(ems_scheduling_state)
                scheduler_thread.start()
                send_status_to_interface('Computing scheduling...')

            if ems_scheduling_state.isEqual(EMS_SCHEDULING_WAIT):
                if scheduler_thread.isAlive():
                    logger.debug('Scheduling in progress.')
                else:
                    logger.debug('Scheduling complete.')
                    ems_scheduling_state.update()
                    logger.debug(ems_scheduling_state)

                    ems_data.set_data(output_scheduler_data.get())
                    logger.debug("Received data from scheduler.")
                    logger.debug("Sending plot data...")
                    # send_plot_to_interface(ems_data.get_plot_data())

            if ems_scheduling_state.isEqual(EMS_COMPLETE):

                ems_data.store()
                # TODO: how to use the scheduling data?
                ems_scheduling_state.update()  # back to EMS_WAIT
                logger.debug(ems_scheduling_state)
                logger.debug('EMS Scheduling routine complete!')
                send_status_to_interface('Computing scheduling...OK!')

            # RUNTIME CONTROLS ########################################################################################
            # verify if timeouts are set
            # prediction wait
            if ems_scheduling_state.isEqual(EMS_PREDICTION_WAIT):
                if timeout_event.is_set():
                    timeout_event.clear()
                    logger.debug('Prediction WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(PREDICTION_QUEUE)
                    ems_scheduling_state.update()
                    logger.debug(ems_scheduling_state)
                    send_status_to_interface('Computing prediction...FAIL!')

                    # send kill request
                    logger.debug('Requesting kill of: ' + MODULES_NAMES[PREDICTION_QUEUE])
                    msg = {MSG_ID_PREDICTION_CANCEL: None}
                    if core_queues.sendMsg(PREDICTION_QUEUE, msg, True):
                        logger.debug('Requested prediction cancellation.')
                    else:
                        logger.info("Problem with queue: " + MODULES_NAMES[PREDICTION_QUEUE])

            if ems_scheduling_state.isEqual(EMS_THERMAL_WAIT):
                if timeout_event.is_set():
                    logger.debug('Thermal WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(THERMAL_QUEUE)
                    ems_scheduling_state.update()
                    logger.debug(ems_scheduling_state)
                    send_status_to_interface('Computing thermal model...FAIL!')

                    # send kill request
                    logger.debug('Requesting kill of: ' + MODULES_NAMES[THERMAL_QUEUE])
                    msg = {MSG_ID_THERMAL_CANCEL: None}
                    if core_queues.sendMsg(THERMAL_QUEUE, msg, True):
                        logger.debug('Requested thermal cancellation.')
                    else:
                        logger.info("Problem with queue: " + MODULES_NAMES[THERMAL_QUEUE])

            if ems_scheduling_state.isEqual(EMS_SCHEDULING_WAIT):
                if timeout_event.is_set():
                    logger.debug('Scheduler WAIT Timeout!')
                    # TODO: insert timeout action (collect missing data and go to next state),send cancellation request?
                    # request_kill(THERMAL_QUEUE)
                    ems_scheduling_state.update()
                    logger.debug(ems_scheduling_state)
                    send_status_to_interface('Computing scheduling...FAIL!')

                    # send kill request
                    logger.debug('Requesting kill of scheduling-solver.')
                    stop_event.set()

        except KeyboardInterrupt:
            logger.info("Server process exit.")
            sys.exit(0)

        time.sleep(MSG_CHECK_TIME)


if __name__ == "__main__":

    mainprocess()

    # """ Testing your code! """
    #
    # from defines import *
    #
    # setup_logging("core_test")
    #
    # core_queues = communication.CoreQueues()
    #
    # mainprocess(core_queues)

