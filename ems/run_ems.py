# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import time
import multiprocessing
import core.core_main
import interface.interface_controller
import prediction.prediction_controller
import thermal.thermal_controller
import diagnostic.diagnostic_controller
import overload_manager.overload_manager_controller
logger = logging.getLogger(__name__)


def runall():

    processes = []

    try:

        processes.append(multiprocessing.Process(name='core', target=core.core_main.mainprocess))
        # processes.append(multiprocessing.Process(name='interface_controller', target=interface.interface_controller.mainprocess))
        processes.append(multiprocessing.Process(name='prediction_controller', target=prediction.prediction_controller.mainprocess))
        processes.append(multiprocessing.Process(name='thermal_controller', target=thermal.thermal_controller.mainprocess))
        processes.append(multiprocessing.Process(name='diagnostic_controller', target=diagnostic.diagnostic_controller.mainprocess))
        processes.append(multiprocessing.Process(name='overload_manager_controller', target=overload_manager.overload_manager_controller.mainprocess))

        for p in processes:
            p.start()
            time.sleep(1)

    except KeyboardInterrupt:
        logger.info("Properly shutdown server/process and exit")

        for p in processes:
            p.terminate()

        sys.exit(0)


if __name__ == "__main__":
    runall()
