# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

from defines import *
import queue
import socket
import time
import multiprocessing.managers
from functools import partial
logger = logging.getLogger(__name__)

#######################################################################
# Windows compatibility, required Picklable Objects
from queue import Queue as _Queue


class Queue(_Queue):
    """ A picklable queue. """
    def __getstate__(self):
        # Only pickle the state we care about
        return (self.maxsize, self.queue, self.unfinished_tasks)

    def __setstate__(self, state):
        # Re-initialize the object, then overwrite the default state with
        # our pickled state.
        Queue.__init__(self)
        self.maxsize = state[0]
        self.queue = state[1]
        self.unfinished_tasks = state[2]


def get_q(q):
    return q


class JobQueueManager(multiprocessing.managers.SyncManager):
    pass
#######################################################################


def msgValidation(msg):

    if isinstance(msg, dict):
        msg_id, msg_data = msg.copy().popitem()  # copy required to preserve the content of the message
        if msg_id.isdigit() and len(msg_id) == MSD_ID_LENGTH:
            if isinstance(msg_data, dict) or not msg_data:
                return True

    # TODO: check if valid message ID

    logger.error("Not a valid structure for message.")

    return False


def sendMsg(output_queue, input_queue, msg, WAIT_ACK=True):

    if not msgValidation(msg):
        return False

    output_queue.put(msg)
    logger.debug("Send message: " + str(msg))
    logger.debug("Wait ACK? " + str(WAIT_ACK))

    if not WAIT_ACK:
        return True

    attempt_number = 0

    while True:

        attempt_number += 1

        try:
            message_id, message_data = input_queue.get(block=True, timeout=CONNECTION_TIMEOUT).popitem()

            if message_id == MSG_ID_ACK and not message_data:
                logger.debug("ACK received.")
                return True  # ACK received, exit and return True
            else:
                raise Exception("Unexpected message from queue: " + message_id + ": " + message_data)

        except queue.Empty:
            if attempt_number == MAX_CONNECTION_ATTEMPTS:
                logger.error("Too many sending attempts without ACK.")
                return False  # Too many attempts, exit with False
            # else:
            #     # clean message in output_queue and try resend
            #     output_queue.get()

        except Exception as err:
            logger.error(err)
            return False


def recvMsg(output_queue, input_queue, SEND_ACK=True):

    try:

        message_id, message_data = input_queue.get(block=False).popitem()
        logger.debug("Received message:")
        logger.debug("ID: " + str(message_id))
        logger.debug("DATA: " + str(message_data))
        logger.debug("Send ACK? " + str(SEND_ACK))

        if SEND_ACK:
            output_queue.put({MSG_ID_ACK: None})

        return message_id, message_data

    except queue.Empty:
        return None, None  # return None if no message in queue

    except Exception:
        # if SyncManager down an empty error is produced (why? then produce a BrokenPipe)
        raise IOError("SyncManagerServer down.")


class CoreQueues:

    def __init__(self):

        self.__get_core_to_controller_str = list()
        self.__get_controller_to_core_str = list()
        self.__set_controller_params()

        self.__manager = self.__make_server_manager()

        self.__output_queues = list()
        self.__input_queues = list()
        self.__set_queues()

    def __set_queues(self):

        # Get the registered queues, 1 input and 1 output for each defined module
        for module_index, module_name in enumerate(MODULES_NAMES):
            self.__output_queues.append(getattr(self.__manager, self.__get_core_to_controller_str[module_index])())
            self.__input_queues.append(getattr(self.__manager, self.__get_controller_to_core_str[module_index])())

    def __set_controller_params(self):

        # Create names to get the input and the output queues, 1 of both for each defined module
        for module_name in MODULES_NAMES:
            self.__get_core_to_controller_str.append('get_core_to_' + module_name + '_q')
            self.__get_controller_to_core_str.append('get_' + module_name + '_to_core_q')

    def __make_server_manager(self):
        """ Create a manager for the core, listening on the given port.
            Return a manager object with get_<queue name>_q methods.
        """

        # This is based on the examples in the official docs of multiprocessing.
        # get_<queue name>_q return synchronized proxies for the actual Queue objects.
        # class JobQueueManager(multiprocessing.managers.SyncManager): # moved outside Class for Windows compatibility
        #     pass

        # Register 1 input and 1 output queues for each defined module
        # - input queue registration name: get_core_to_<module_name>_q
        # - output queue registration name: get_<module_name>_to_core_q
        oqs = list()  # list of output queues
        iqs = list()  # list of input queues
        for module_index, module_name in enumerate(MODULES_NAMES):
            # increase number of queues for each defined module

            # oqs.append(queue.Queue())
            # iqs.append(queue.Queue())
            # JobQueueManager.register(self.__get_core_to_controller_str[module_index],
            #                          callable=lambda bound_mi=module_index: oqs[bound_mi])
            # JobQueueManager.register(self.__get_controller_to_core_str[module_index],
            #                          callable=lambda bound_mi=module_index: iqs[bound_mi])

            # Windows compliant
            oqs.append(Queue())
            iqs.append(Queue())
            JobQueueManager.register(self.__get_core_to_controller_str[module_index],
                                     callable=partial(get_q, oqs[module_index]))
            JobQueueManager.register(self.__get_controller_to_core_str[module_index],
                                     callable=partial(get_q, iqs[module_index]))

        manager = JobQueueManager(address=('127.0.0.1', SERVER_PORTNUM), authkey=SERVER_AUTHKEY)
        manager.start()
        logger.info('ServerQueueManager started at port %s' % SERVER_PORTNUM)

        return manager

    def shutdownserverqueues(self):

        self.__manager.shutdown()

    def sendMsg(self, queue_number, msg, WAIT_ACK=True):

        # the core the output queue is "core to controller", and the input queue is "controller to core"
        return sendMsg(self.__output_queues[queue_number], self.__input_queues[queue_number], msg, WAIT_ACK)

    def recvMsg(self, queue_number, SEND_ACK=True):

        # the core output queue is "core to controller", and the input queue is "controller to core"
        return recvMsg(self.__output_queues[queue_number], self.__input_queues[queue_number], SEND_ACK)

    def queues_number(self):

        return range(len(self.__input_queues))


class ControllerQueues:

    def __init__(self, module_name):

        self.__module_name = module_name

        self.__get_core_to_controller_q = None
        self.__get_controller_to_core_q = None
        self.__manager = None
        self.__shared_core_to_controller_q = None
        self.__shared_controller_to_core_q = None

        self.__set_controller_params()
        self.__connect()

    def __set_controller_params(self):

        self.__get_core_to_controller_q = 'get_core_to_' + self.__module_name + '_q'
        self.__get_controller_to_core_q = 'get_' + self.__module_name + '_to_core_q'

    def __make_client_manager(self):
        """ Create a manager for a controller. This manager connects to a server on the
            given address and exposes the get_<queue name>_q methods for
            accessing the shared queues from the server.
            Return a manager object.
        """

        class ServerQueueManager(multiprocessing.managers.SyncManager):
            pass

        ServerQueueManager.register(self.__get_core_to_controller_q)
        ServerQueueManager.register(self.__get_controller_to_core_q)

        manager = ServerQueueManager(address=(SERVER_IP_ADDRESS, SERVER_PORTNUM), authkey=SERVER_AUTHKEY)

        # connection_counter = 0

        while True:
            try:
                # check if IP address is reachable, unless the connect call stucks in block-loop
                # with socket.socket(getattr(socket, 'AF_INET')) as s:
                s = socket.socket(getattr(socket, 'AF_INET'))
                # s.settimeout(SYNCMANAGER_CONNECTION_TIMEOUT)
                s.connect((SERVER_IP_ADDRESS, SERVER_PORTNUM))

                manager.connect()
                break
            except ConnectionRefusedError as err:
                logger.error(err)
                logger.info("Waiting for connection...")
                # if connection_counter == MAX_CONNECTION_ATTEMPTS:
                #     raise ConnectionError("Max connection attempts reached.")
                # connection_counter += 1
                time.sleep(SYNCMANAGER_CONNECTION_TIMEOUT)

            except socket.timeout as err:
                logger.error(err)
                raise ConnectionError("IP not available in the network.")

            except Exception as err:
                logger.error(err)
                raise Exception("Unknown error.")

        logger.info("ClientQueueManager connected to %s:%s" % (SERVER_IP_ADDRESS, SERVER_PORTNUM))
        return manager

    def __connect(self):
        self.__manager = self.__make_client_manager()
        self.__shared_core_to_controller_q = getattr(self.__manager, self.__get_core_to_controller_q)()
        self.__shared_controller_to_core_q = getattr(self.__manager, self.__get_controller_to_core_q)()

    def reconnect(self):
        logger.info("Requested reconnection.")
        self.__connect()

    def sendMsg(self, msg, WAIT_ACK=True):

        # the controller output queue is "controller to core", and the input queue is "core to controller"
        return sendMsg(self.__shared_controller_to_core_q, self.__shared_core_to_controller_q, msg, WAIT_ACK)

    def recvMsg(self, SEND_ACK=True):

        # the controller output queue is "controller to core", and the input queue is "core to controller"
        return recvMsg(self.__shared_controller_to_core_q, self.__shared_core_to_controller_q, SEND_ACK)
