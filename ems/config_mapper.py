import sys
import re
import time
import logging
logger = logging.getLogger(__name__)


class BaseObject:

    def __init__(self, specs_keys, data_keys, config_data):

        self.__param_keys = ['id', 'tag', 'name', 'description', 'connectedTo']
        self.__specs_keys = specs_keys
        self.__data_keys = data_keys

        # params
        self.__params = dict()

        # specs
        self.__specs = dict()

        # data
        self.__data = dict()

        # set input data
        self.set(config_data)

    def update(self, input_dict):

        for k, v in input_dict.items():

            if k in self.__param_keys:
                if v != self.__params.get(k):  # or self.__params.get(k) is None
                    self.__params.update({k: v})
                    logger.info('Update in object ' + str(self.__params.get('id')) + ' for param "' + str(k) + '": ' + str(v))

            elif k in self.__specs_keys:
                if v != self.__specs.get(k):
                    self.__specs.update({k: v})
                    logger.info('Update in object ' + str(self.__params.get('id')) + ' for spec "' + str(k) + '": ' + str(v))

            elif k in ['specs']:
                self.update(v)

            elif k == 'data':
                continue  # keys for sub-dictionaries

            else:
                logger.warning('Unrecognized key: ' + k)

    def set(self, input_dict):

        for k, v in input_dict.items():

            if k in self.__param_keys:
                # setattr(self, k, v)
                self.__params.update({k: v})
            elif k in self.__specs_keys:
                self.__specs.update({k: v})
            elif k in self.__data_keys:
                self.__data.update({k: v})
            elif k in ['specs', 'data']:
                # continue  # keys for sub-dictionaries
                self.set(v)
            else:
                logger.warning('Unrecognized key: ' + k)

    def get(self, key):

        # avoid to loop over string chars
        if type(key) == str:
            if key in self.__param_keys:
                return self.__params.get(key)
            elif key in self.__specs_keys:
                return self.__specs.get(key)
            elif key in self.__data_keys:
                return self.__data.get(key)
            else:
                logger.warning('Unrecognized key: ' + key)

        else:

            output_dict = dict()

            for k in key:
                if k in self.__param_keys:
                    output_dict.update({k: self.__params.get(k)})
                elif k in self.__specs_keys:
                    output_dict.update({k: self.__specs.get(k)})
                elif k in self.__data_keys:
                    output_dict.update({k: self.__data.get(k)})
                else:
                    logger.warning('Unrecognized key: ' + k)

            return output_dict

    def get_id(self):

        return self.get('id')

    def get_param(self):

        return self.get(self.__param_keys)

    def get_specs(self):

        return self.get(self.__specs_keys)

    def get_data(self):

        return self.get(self.__data_keys)

    def to_dict(self, keys):

        output_dict = {}

        for key in keys if isinstance(keys, list) else [keys]:

            if key == 'params':
                output_dict.update(self.get_param())

            elif key == 'specs':
                output_dict.update({'specs': self.get_specs()})

            elif key == 'data':
                output_dict.update({'data': self.get_data()})

            elif key == 'params+specs':
                output_dict.update(self.get_param())
                output_dict.update({'specs': self.get_specs()})

            elif key == 'params+data':
                output_dict.update(self.get_param())
                output_dict.update({'data': self.get_data()})

            elif key == 'params+specs+data' or key is None:
                output_dict.update(self.get_param())
                output_dict.update({'specs': self.get_specs()})
                output_dict.update({'data': self.get_data()})

            elif key in self.__param_keys:
                output_dict.update(self.get(key))

            elif key in self.__specs_keys:

                if output_dict.get('specs'):
                    output_dict['specs'].update({key: self.get(key)})
                else:
                    output_dict.update({'specs': {key: self.get(key)}})

            elif key in self.__data_keys:

                if output_dict.get('data'):
                    output_dict['data'].update({key: self.get(key)})
                else:
                    output_dict.update({'data': {key: self.get(key)}})

            else:
                logger.warning('Unrecognized key.')

        return output_dict


class Device(BaseObject):
    pass


class Load(Device):

    def __init__(self, configs_data):
        if configs_data['id'].split('-')[0] == 'ep_load':
            super().__init__(['idf', 'thermal_zones', 'max', 'setpoints'], ['loads'], configs_data)
        else:
            super().__init__(['max'], ['loads'], configs_data)


class Production(Device):

    def __init__(self, configs_data):
        super().__init__(['max', 'size'], ['production', 'use'], configs_data)


class Storage(Device):

    def __init__(self, configs_data):
        super().__init__(['size', 'charge_eff', 'discharge_eff'],
                         ['init_charge', 'levels', 'charge_activities', 'discharge_activities'],
                         configs_data)


class Pipe(Device):

    def __init__(self, configs_data):
        super().__init__(['max'], [], configs_data)


class Seller(Device):

    def __init__(self, configs_data):
        super().__init__(['max', 'selling_prices'], ['sales'], configs_data)


class Purchaser(Device):

    def __init__(self, configs_data):
        super().__init__(['max', 'buying_prices'], ['consumption'], configs_data)


class Generator(Device):

    def __init__(self, configs_data):
        super().__init__(['max', 'eff'], [], configs_data)


class Trigenerator(Device):

    def __init__(self, configs_data):
        super().__init__(['max', 'ele', 'heat', 'chill'], [], configs_data)


class Task(BaseObject):

    def __init__(self, configs_data):
        super().__init__(['energy', 'forced_slots', 'allowed_slots', 'sduration', 'mdelay', 'duration', 'mduration',
                          'sdelay'],
                         ['start_time', 'loads'],
                         configs_data)


class Settings:

    __general_settings_keys = ['resolution', 'horizon', 'db', 'location', 'position']
    __module_settings_keys = ['prediction', 'thermal', 'interface', 'scheduler']

    def __init__(self):

        # general params
        self.general = dict()
        # module params
        self.module = dict()

        self.__detected_change = False

    def __set_detected_change(self):

        # change value only the first time
        if not self.__detected_change:
            self.detected_change = True

    def __len__(self):

        return len(self.general) + len(self.module)

    def initialize(self, configs):

        for k in self.__general_settings_keys:
            self.general.update({k: configs['general'][k]})

        for k in self.__module_settings_keys:
            self.module.update({k: configs['module'][k]})

    def update(self, configs):

        for k in self.__general_settings_keys:
            if configs['general'][k] != self.general.get(k):
                self.general.update({k: configs['general'][k]})
                logger.info('Updated general parameters ' + k + ' to ' + str(self.general.get(k)))
                self.__set_detected_change()

        for k in self.__module_settings_keys:
            if configs['module'][k] != self.module.get(k):
                self.module.update({k: configs['module'][k]})
                logger.info('Updated parameters of module ' + k + ' to ' + str(self.module.get(k)))
                self.__set_detected_change()

        return self.__detected_change

    def to_dict(self):

        return {'general': self.general, 'module': self.module}


class Grid:

    __commonData_keys = ['weatherData']

    def __init__(self):

        # Devices
        self.loads = list()  # list of Load()
        self.prods = list()  # list of Production()
        self.storages = list()  # list of Storage()
        self.purchasers = list()  # list of Purchaser()
        self.sellers = list()  # List of Seller()
        self.pipes = list()  # List of Pipe()
        self.gens = list()  # List of Generator()
        self.trigens = list()  # List of Trigenerators()

        # Tasks
        self.tasks = []  # list of Task()

        # common data
        self.commonData = dict()

        self.__detected_change = False

    def __len__(self):

        return len(self.loads) + len(self.prods) + len(self.storages) + \
               len(self.purchasers) + len(self.sellers) + len(self.pipes) + len(self.tasks) + len(self.gens) + \
               len(self.trigens) + len(self.commonData)

    def initialize(self, configs):

        self.update(configs)
        self.commonData.update(configs['commonData'])

    def update(self, configs):

        # Devices
        self.__update_objects('loads', configs.get('devices'))
        self.__update_objects('prods', configs.get('devices'))
        self.__update_objects('storages', configs.get('devices'))
        self.__update_objects('purchasers', configs.get('devices'))
        self.__update_objects('sellers', configs.get('devices'))
        self.__update_objects('pipes', configs.get('devices'))
        self.__update_objects('gens', configs.get('devices'))
        self.__update_objects('trigens', configs.get('devices'))

        # Task
        self.__update_objects('tasks', configs.get('tasks'))

        return self.__detected_change

    def __set_detected_change(self):

        # change value only the first time
        if not self.__detected_change:
            self.detected_change = True

    def __add_object(self, objects_type, object_configs):

        class_str = ''
        if objects_type == 'loads':
            class_str = 'Load'
        elif objects_type == 'storages':
            class_str = 'Storage'
        elif objects_type == 'prods':
            class_str = 'Production'
        elif objects_type == 'purchasers':
            class_str = 'Purchaser'
        elif objects_type == 'sellers':
            class_str = 'Seller'
        elif objects_type == 'pipes':
            class_str = 'Pipe'
        elif objects_type == 'gens':
            class_str = 'Generator'
        elif objects_type == 'trigens':
            class_str = 'Trigenerator'
        elif objects_type == 'tasks':
            class_str = 'Task'
        else:
            logger.warning('Unrecognized object type: ' + objects_type)

        object_class = getattr(sys.modules[__name__], class_str)
        objects_list = getattr(self, objects_type)
        objects_list.append(object_class(object_configs))

        logger.info('Added new ' + objects_type + ' object: ' + object_configs.get('id'))

    def __update_objects(self, objects_type, objects_configs):

        ids_in_config = []

        if objects_type == 'tasks':
            objects_list = objects_configs or []
        else:
            # remove final 's'
            objects_list = [objects for objects in objects_configs if objects.get('tag') == objects_type[:-1]]

        for object_configs in objects_list:
            # check unique ID
            if object_configs.get('id') in ids_in_config:
                    logger.warning('Object ID already used. ' + objects_type + ' object with ID: ' + object_configs.get(
                        'id') + ' not added!')
                    continue

            ids_in_config.append(object_configs.get('id'))
            # check if new object
            _object = self.__get_object_from_id(getattr(self, objects_type), object_configs.get('id'))
            if not _object:
                # new object detected
                self.__add_object(objects_type, object_configs)
                self.__set_detected_change()

            else:
                # object already present
                # check changes in data
                _object.update(object_configs)
                self.__set_detected_change()

        # remove objects if needed
        loop = True
        while loop:

            loop = False
            for object_configs in getattr(self, objects_type):
                # unique id
                if objects_type == 'tasks':
                    self.__task_started(object_configs.get_id())
                if object_configs.get_id() not in ids_in_config:
                    logger.info('Removed object type ' + objects_type + ' with ID: ' + object_configs.get_id())
                    objects_dict = getattr(self, objects_type)
                    objects_dict.remove(object_configs)
                    loop = True
                    # If task executed can be remove without cause a "detected_change = True"
                    # If (objects_type == 'tasks' and self.__task_started(object_configs.get_id())) == True
                    # task already started, can be removed without request a new schedule
                    if not (objects_type == 'tasks' and self.__task_started(object_configs.get_id())):
                        self.__set_detected_change()
                    break

    def __task_started(self, task_id):

        # check in DB
        # temporary, check "start_time" in current "data"

        start_time = self.get_task(task_id).get_data()['start_time']
        # if not strart_time or time.strptime(strart_time, '%d-%m-%Y %H:%M:%S') < time.localtime():
        if not start_time or (time.localtime(start_time) < time.localtime()):
            # task not yet scheduled (start_time=None) or started
            return False

        else:
            # start_time >= localtime -> task started
            return True

    def __get_object_from_id(self, objects_list, objects_id):

        for _object in objects_list:
            if _object.get_id() == objects_id:
                return _object

    def get_load(self, load_id):
        return self.__get_object_from_id(self.loads, load_id)

    def get_production(self, production_id):
        return self.__get_object_from_id(self.prods, production_id)

    def get_storage(self, storage_id):
        return self.__get_object_from_id(self.storages, storage_id)

    def get_purchaser(self, purchaser_id):
        return self.__get_object_from_id(self.purchasers, purchaser_id)

    def get_seller(self, seller_id):
        return self.__get_object_from_id(self.sellers, seller_id)

    def get_pipe(self, pipe_id):
        return self.__get_object_from_id(self.pipes, pipe_id)

    def get_generator(self, generator_id):
        return self.__get_object_from_id(self.gens, generator_id)

    def get_trigenerator(self, trigenerator_id):
        return self.__get_object_from_id(self.trigens, trigenerator_id)

    def get_task(self, task_id):
        return self.__get_object_from_id(self.tasks, task_id)

    def __get_objects_from_regex(self, objects_list, objects_id_regex):

        output_list = list()

        p = re.compile(objects_id_regex)

        for _object in objects_list:
            if p.match(_object.get_id()):
                output_list.append(_object)

        return output_list

    def __objects_to_dict(self, objects_list, keys):

        output_list_of_dicts = []

        for _object in objects_list:
            output_list_of_dicts.append((_object.to_dict(keys)))

        return output_list_of_dicts

    def get_loads(self, load_id_regex='', keys=None):
        # returns list of matched object, otherwise
        # if keys is given return list of dict
        output = self.__get_objects_from_regex(self.loads, load_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_productions(self, production_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.prods, production_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_storages(self, storage_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.storages, storage_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_purchasers(self, purchaser_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.purchasers, purchaser_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_sellers(self, seller_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.sellers, seller_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_pipes(self, pipe_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.pipes, pipe_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_generators(self, generator_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.gens, generator_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_trigenerators(self, trigenerator_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.trigens, trigenerator_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def get_tasks(self, task_id_regex='', keys=None):
        output = self.__get_objects_from_regex(self.tasks, task_id_regex)
        return output if not keys else self.__objects_to_dict(output, keys)

    def loads_to_dict(self, keys=None):
        return self.__objects_to_dict(self.loads, keys)

    def productions_to_dict(self, keys=None):
        return self.__objects_to_dict(self.prods, keys)

    def storages_to_dict(self, keys=None):
        return self.__objects_to_dict(self.storages, keys)

    def purchasers_to_dict(self, keys=None):
        return self.__objects_to_dict(self.purchasers, keys)

    def sellers_to_dict(self, keys=None):
        return self.__objects_to_dict(self.sellers, keys)

    def pipes_to_dict(self, keys=None):
        return self.__objects_to_dict(self.pipes, keys)

    def generators_to_dict(self, keys=None):
        return self.__objects_to_dict(self.gens, keys)

    def trigenerators_to_dict(self, keys=None):
        return self.__objects_to_dict(self.trigens, keys)

    def tasks_to_dict(self, keys=None):
        return self.__objects_to_dict(self.tasks, keys)

    def devices_to_dict(self):

        return self.loads_to_dict() + self.productions_to_dict()+ self.storages_to_dict() + self.purchasers_to_dict() +\
               self.sellers_to_dict() + self.pipes_to_dict() + self.generators_to_dict() + self.trigenerators_to_dict()

    def __set_object_data(self, objects_list, object_id, object_data):

        _object = self.__get_object_from_id(objects_list, object_id)

        if _object:
            _object.set(object_data)
            logger.info('Set "Data" for object ' + str(object_id))

    def __set_objects_data(self, objects_list, objects_data):

        # if len(objects_list) != len(objects_data):
        #     raise Exception('Mismatch between stored objects list and current one -> ' + str(objects_list))

        for object_input_data in objects_data:
            if object_input_data.get('data'):
                self.__set_object_data(objects_list, object_input_data['id'], object_input_data['data'])
            else:
                logger.info('No "Data" to set for object ' + str(object_input_data['id']))

    def set_load_data(self, load_id, input_data):
        self.__set_object_data(self.loads, load_id, input_data)

    def set_production_data(self, production_id, input_data):
        self.__set_object_data(self.prods, production_id, input_data)

    def set_storage_data(self, storage_id, input_data):
        self.__set_object_data(self.storages, storage_id, input_data)

    def set_purchaser_data(self, purchaser_id, input_data):
        self.__set_object_data(self.purchasers, purchaser_id, input_data)

    def set_seller_data(self, seller_id, input_data):
        self.__set_object_data(self.sellers, seller_id, input_data)

    def set_pipe_data(self, pipe_id, input_data):
        self.__set_object_data(self.pipes, pipe_id, input_data)

    def set_generator_data(self, generator_id, input_data):
        self.__set_object_data(self.gens, generator_id, input_data)

    def set_trigenerator_data(self, trigenerator_id, input_data):
        self.__set_object_data(self.trigens, trigenerator_id, input_data)

    def set_task_data(self, task_id, input_data):
        self.__set_object_data(self.tasks, task_id, input_data)

    def set_loads_data(self, input_data):
        self.__set_objects_data(self.loads, input_data)

    def set_productions_data(self, input_data):
        self.__set_objects_data(self.prods, input_data)

    def set_storages_data(self, input_data):
        self.__set_objects_data(self.storages, input_data)

    def set_purchasers_data(self, input_data):
        self.__set_objects_data(self.purchasers, input_data)

    def set_sellers_data(self, input_data):
        self.__set_objects_data(self.sellers, input_data)

    def set_pipes_data(self, input_data):
        self.__set_objects_data(self.pipes, input_data)

    def set_generators_data(self, input_data):
        self.__set_objects_data(self.gens, input_data)

    def set_trigenerators_data(self, input_data):
        self.__set_objects_data(self.trigens, input_data)

    def set_tasks_data(self, input_data):
        self.__set_objects_data(self.tasks, input_data)

    def set_devices_data(self, devices_list):

        for device in devices_list:
            dev_tag = device.get('tag')
            dev_id = device.get('id')
            dev_data = device.get('data')

            if dev_tag == 'load':
                self.set_load_data(dev_id, dev_data)
            elif dev_tag == 'prod':
                self.set_production_data(dev_id, dev_data)
            elif dev_tag == 'storages':
                self.set_storage_data(dev_id, dev_data)
            elif dev_tag == 'purchaser':
                self.set_purchaser_data(dev_id, dev_data)
            elif dev_tag == 'seller':
                self.set_seller_data(dev_id, dev_data)
            elif dev_tag == 'pipe':
                self.set_pipe_data(dev_id, dev_data)
            elif dev_tag == 'gen':
                self.set_generator_data(dev_id, dev_data)
            elif dev_tag == 'trigen':
                self.set_trigenerator_data(dev_id, dev_data)

    def remove_task(self, task_id):

        task_obj = self.get_task(task_id)

        for task in self.tasks:
            self.tasks.remove(task_obj)
