# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from defines import *
from ems_db.schedule.routines import add_or_update_schedule, get_last_schedule_data
from ems_db.sensor.routines import get_record
import logging
logger = logging.getLogger(__name__)

############################################################################
# EMS Status
############################################################################
# scheduling_state meaning:
# 0 -> nothing
# 1 -> prediction request
# 2 -> prediction wait
# 3 -> thermal-modelling request
# 4 -> thermal-modelling wait
# 5 -> scheduling request
# 6 -> scheduling wait
# 7 -> completed
############################################################################

EMS_WAIT = 0
EMS_PREDICTION_REQUEST = 1
EMS_PREDICTION_WAIT = 2
EMS_THERMAL_REQUEST = 3
EMS_THERMAL_WAIT = 4
EMS_SCHEDULING_REQUEST = 5
EMS_SCHEDULING_WAIT = 6
EMS_COMPLETE = 7

_stateToName = {
    EMS_WAIT: 'EMS_WAIT',
    EMS_PREDICTION_REQUEST: 'EMS_PREDICTION_REQUEST',
    EMS_PREDICTION_WAIT: 'EMS_PREDICTION_WAIT',
    EMS_THERMAL_REQUEST: 'EMS_THERMAL_REQUEST',
    EMS_THERMAL_WAIT: 'EMS_THERMAL_WAIT',
    EMS_SCHEDULING_REQUEST: 'EMS_SCHEDULING_REQUEST',
    EMS_SCHEDULING_WAIT: 'EMS_SCHEDULING_WAIT',
    EMS_COMPLETE: 'EMS_COMPLETE'
}

_nameToState = {
    'EMS_WAIT': EMS_WAIT,
    'EMS_PREDICTION_REQUEST': EMS_PREDICTION_REQUEST,
    'EMS_PREDICTION_WAIT': EMS_PREDICTION_WAIT,
    'EMS_THERMAL_REQUEST': EMS_THERMAL_REQUEST,
    'EMS_THERMAL_WAIT': EMS_THERMAL_WAIT,
    'EMS_SCHEDULING_REQUEST': EMS_SCHEDULING_REQUEST,
    'EMS_SCHEDULING_WAIT': EMS_SCHEDULING_WAIT,
    'EMS_COMPLETE': EMS_COMPLETE
}


def getStateName(state):
    """
    Return the textual representation of state 'state'.

    If the state, as string or number, is one of the predefined states (EMS_*),
    then you get the corresponding number or string, respectively.

    Otherwise, the string "State %s" % state is returned.
    """
    return _stateToName.get(state, _nameToState.get(state, ("State %s" % state)))


class EMSStateClass:
    def __init__(self, init_state='EMS_WAIT'):

        self.__state = None  # stored as number
        self.__event = threading.Event()
        self.set(init_state)

    def __repr__(self):
        return 'EMS State: ' + getStateName(self.__state)

    def __strToNum(self, state):
        if isinstance(state, str):
            state = getStateName(state)

        return state

    def update(self):

        self.__event.set()

        if self.__state == getStateName('EMS_COMPLETE'):
            self.__state = 0
            self.__event.clear()
        else:
            self.__state += 1

    def reset(self):
        self.__state = 0
        self.__event.clear()

    def set(self, state):
        self.__state = self.__strToNum(state)

        if self.__state == getStateName('EMS_WAIT'):
            self.__event.clear()
        else:
            self.__event.set()

    def isEqual(self, state):

        return self.__state == self.__strToNum(state)

    def get(self):

        return self.__state

    def getName(self):
        return getStateName(self.__state)

    def get_event(self):

        return self.__event


class EMSData:

    def __init__(self):

        self.timestamp = None
        self.settings = Settings()
        self.grid = Grid()

        self.__empty_status = False  # avoid to spam log info about missing config file in loop

        self.__initialize()

    def __len__(self):

        return len(self.settings) + len(self.grid)

    def __repr__(self):

        output_str = '#' * 50 + '\n'
        # output_str += '{:<15}'.format('Keys') + 'Values\n'
        # output_str += '-' * 50 + '\n'

        def print_the_structure(structure_name, structures):

            _output_str = '-' * 50 + '\n'
            _output_str += structure_name + ':\n'
            _output_str += '-' * 50 + '\n'

            if isinstance(structures, dict):
                for k, v in structures.items():
                    _output_str += '{:<15}'.format(k + ':') + str(v) + '\n'

            elif isinstance(structures, list):

                for counter, structure in enumerate(structures):
                    for k, v in structure.items():
                        _output_str += '{:<15}'.format(k + ':') + str(v) + '\n'

                    if counter < (len(structures)-1):
                        _output_str += '-' * 50 + '\n'
            else:
                logger.warning('Unsupported structure type.')

            return _output_str

        output_str += print_the_structure('Settings', self.settings.to_dict())
        output_str += print_the_structure('Loads', self.grid.loads_to_dict())
        output_str += print_the_structure('Productions', self.grid.productions_to_dict())
        output_str += print_the_structure('Storages', self.grid.storages_to_dict())
        output_str += print_the_structure('Purchasers', self.grid.purchasers_to_dict())
        output_str += print_the_structure('Sellers', self.grid.sellers_to_dict())
        output_str += print_the_structure('Pipes', self.grid.pipes_to_dict())
        output_str += print_the_structure('Generators', self.grid.generators_to_dict())
        output_str += print_the_structure('Trigenerators', self.grid.trigenerators_to_dict())
        output_str += print_the_structure('commonData', self.grid.commonData)
        output_str += print_the_structure('Tasks', self.grid.tasks_to_dict())

        output_str += '#' * 50 + '\n'

        return output_str

    def __initialize(self):

        config_json = self.__parse_cfg_file()

        if not config_json:
            logger.error('Error in config file!')
            self.__empty_status = True
            return
            # raise Exception('Error in config file!')

        self.settings.initialize(config_json['settings'])
        self.grid.initialize(config_json)

        if self.__empty_status:
            self.__empty_status = False

    def __parse_cfg_file(self):

        try:
            with open(EMS_CFG_FILE, 'r') as cfg_file:
                return json.load(cfg_file)

        except FileNotFoundError:
            if not self.__empty_status:
                logger.info('Missing config file!')
            return None

    def isEmpty(self):

        return True if len(self) == 0 else False

    def update(self):
        # update settings
        logger.debug('Performing config update.')
        config_json = self.__parse_cfg_file()

        if not config_json:
            if not self.__empty_status:
                if not self.isEmpty():
                    logger.warning('Keeping previous configuration.')
                else:
                    logger.warning('No previous configuration available.')
                self.__empty_status = True
            return

        if self.settings.update(config_json['settings']) or self.grid.update(config_json):
            # Detected changes in structure
            self.timestamp = None  # Set to None to repeat scheduler

        if self.__empty_status:
            self.__empty_status = False

    def get_data_for_interface(self):
        # Data for the interface base configuration
        # - all general settings, and db
        # - module settings = 'interface'

        output_data = dict()

        output_data.update({'timestamp': self.timestamp})
        output_data.update(self.settings.general)
        output_data.update(self.settings.module.get('interface'))

        return output_data

    def get_data_for_prediction(self):  # TODO: add pvs reference
        # Data for prediction module includes:
        # - all general settings
        # - module settings = 'prediction'
        # - "params" and "specs" for loads and pvs

        output_data = dict()

        output_data.update({'timestamp': self.timestamp})
        output_data.update(self.settings.general)
        output_data.update(self.settings.module.get('prediction'))

        # Load list for load prediction module, avoid to send ep_load
        output_data.update({'load_prediction': self.grid.get_loads('load', 'params+specs')})

        # actually only PV prediction is supported, other loads ignored in the module
        # output_data.update({'devices': output_data.get('devices') + self.grid.get_productions('pv', 'params+specs')})
        output_data.update({'pv_prediction': self.grid.get_productions('pv', 'params+specs')})

        return output_data

    def set_data_from_prediction(self, input_data):
        # The prediction module returns:
        # - weatherData -> commonData
        # - a list of dictionaries {'id':, 'data': {'load':}} (for each load) with key 'loads'
        # - a list of dictionaries {'id':, 'data': {'productions':}} (for each pv) with key 'PV-#'

        self.grid.commonData.update({'weatherData': input_data.get('commonData').get('weatherData')})

        # _devices_data = input_data.get('devices')
        # self.grid.set_loads_data(_devices_data)
        # self.grid.set_productions_data(_devices_data)
        self.grid.set_loads_data(input_data.get('load_prediction'))
        self.grid.set_productions_data(input_data.get('pv_prediction'))

    def get_data_for_thermal(self):
        # Data for thermal module includes:
        # - all general settings
        # - 'weatherData' -> in 'commonData'
        # - module settings = 'thermal'
        # - 'id', 'location', 'position', 'setpoints', 'idf' for thermals

        output_data = dict()

        output_data.update({'timestamp': self.timestamp})
        output_data.update(self.settings.general)
        output_data.update({'commonData': self.grid.commonData})
        output_data.update(self.settings.module.get('thermal'))

        # ep_load computed in the thermal module
        output_data.update({'thermal': self.grid.get_loads('ep_load', ['params+specs'])})

        return output_data

    def set_data_from_thermal(self, input_data):
        # The thermal module returns:
        # - a list of dictionaries {'id':, 'data': {'load':}} (for each thermal) with key 'thermals'

        self.grid.set_loads_data(input_data.get('thermal'))

    def get_data_for_scheduler(self):
        # Data for scheduler module includes:
        # - all general settings
        # - 'id', specs and data from all the structures

        output_data = dict()

        output_data.update({'timestamp': self.timestamp})
        output_data.update(self.settings.general)
        output_data.update(self.settings.module['scheduler'])

        # Send all devices
        output_data.update({'devices': self.grid.devices_to_dict()})

        # Send all task
        output_data.update({'tasks': self.grid.tasks_to_dict()})

        return output_data

    def set_data_from_scheduler(self, input_data):

        # The scheduler returns:
        # - a list of dictionaries {'id':, 'data': {'level':}} (for each storage) with key 'storages'
        # - a list of dictionaries {'id':, 'data': {'consumption':}} (for each el_purchaser) with key 'purchasers'
        # - a list of dictionaries {'id':, 'data': {'sales':}} (for each seller) with key 'sellers'

        self.grid.set_devices_data(input_data.get('devices'))
        self.grid.set_tasks_data(input_data.get('tasks'))

    def compute_estimated_indices(self):
        # compute indices from ems data (estimated data): predictions, thermals and scheduler outputs
        estimated_indices = dict()

        # ratio between solar use and its production ###################################################################
        pv_prod = 0
        pv_use = 0
        for pv_data in self.grid.get_productions('pv'):
            pv_prod += sum(pv_data.get('production') or [0])
            pv_use += sum(pv_data.get('use') or [0])
        estimated_solar_use_ratio = round(pv_use / pv_prod if pv_prod else 1.0, FLOAT_PRECISION)
        estimated_indices.update({'estimated solar use ratio': estimated_solar_use_ratio})
        ################################################################################################################

        # overall renewable production #################################################################################
        # current scenario only PV
        estimated_overall_renewable_production = 0
        for pv_data in self.grid.get_productions('pv'):
            estimated_overall_renewable_production += sum(pv_data.get('production') or [0])
        estimated_indices.update({'estimated overall renewable production': round(estimated_overall_renewable_production, FLOAT_PRECISION)})
        ################################################################################################################

        # overall consumption ##########################################################################################
        # sum of all loads and tasks
        estimated_overall_consumption = 0
        for load in self.grid.get_loads():
            estimated_overall_consumption += sum(load.get_data().get('loads') or [0])
        for task in self.grid.get_tasks():
            estimated_overall_consumption += sum(task.get_data().get('loads') or [0])
        estimated_indices.update({'estimated overall consumption': round(estimated_overall_consumption, FLOAT_PRECISION)})
        ################################################################################################################

        # renewable quota ##############################################################################################
        # percentage of renewable energy in overall consumption, currently only PV
        estimated_overall_solar_use = 0
        for pv_data in self.grid.get_productions('pv'):
            estimated_overall_solar_use += sum(pv_data.get('use') or [0])
        estimated_renewable_quota = 100 * (estimated_overall_solar_use / estimated_overall_consumption if estimated_overall_consumption else 0.0)
        estimated_indices.update({'estimated renewable quota': round(estimated_renewable_quota, FLOAT_PRECISION)})
        ################################################################################################################

        # sold energy ##################################################################################################
        # energy sold to grid
        estimated_sold_energy = 0
        for seller in self.grid.get_sellers():
            estimated_sold_energy += sum(seller.get_data().get('sales') or [0])
        estimated_indices.update({'estimated sold energy': round(estimated_sold_energy, FLOAT_PRECISION)})
        ################################################################################################################

        # purchased energy #############################################################################################
        # energy purchase from grid
        estimated_purchased_energy = 0
        for purchaser in self.grid.get_purchasers():
            estimated_purchased_energy += sum(purchaser.get_data().get('consumption') or [0])
        estimated_indices.update({'estimated purchased energy': round(estimated_purchased_energy, FLOAT_PRECISION)})
        ################################################################################################################

        # storage loss #################################################################################################
        # eenrgy lost duet to storage charge and discharge activities
        estimated_storage_loss = 0
        for storage in self.grid.get_storages():
            pass
        estimated_indices.update({'estimated storage loss': round(estimated_storage_loss, FLOAT_PRECISION)})
        ################################################################################################################

        self.grid.commonData.update({'indices': estimated_indices})

    def compute_real_indices(self):

        # compute indices from stored sensor data
        real_indices = dict()
        horizon = self.settings.general.get('horizon') or 360
        resolution = self.settings.general.get('resolution') or 15
        start_time = datetime.datetime.utcfromtimestamp(self.timestamp)
        end_time = start_time + datetime.timedelta(minutes=horizon-resolution)

        tasks_consumption = list()
        delta = datetime.timedelta(minutes=15)

        start_date = start_time
        end_date = end_time

        while start_date < end_date:
            tasks_consumption.append([start_date, 0.0])
            start_date += delta

        tasks_consumption = np.asarray(tasks_consumption)

        # ratio between solar use and its production ###################################################################
        # simulation scenario: solar production = solar1 and solar2,
        # solar use = overall consumption - solar production, per ogni istante e scartadno i risultati negativi
        real_solar_use_ratio = list()
        pv_prod = 0
        pv_use = 0
        real_purchased_energy = 0
        for sensor_name in PV_SENSORS:
            data = np.asarray(get_record(sensor_name, start_time, end_time))
            pv_prod += data[:, 1]

        # compute overall house consumption with scheduled tasks
        # scheduled tasks consumption, stored in a shared array to consider also the actual profile that overlap with other windows
        for task in self.grid.tasks:
            ref_profile = task.get('actual_profile')
            ref_date = datetime.datetime.utcfromtimestamp(task.get('start_time'))
            ref_data = list()
            for value in ref_profile:
                tasks_consumption[tasks_consumption[:, 0] == ref_date, 1] += value
                ref_date += datetime.timedelta(minutes=15)

        overall_consumption = np.asarray(get_record('overall', start_time, end_time))
        overall_consumption[:, 1] = overall_consumption[:, 1] + \
                              tasks_consumption[
                                  np.where(np.logical_and(tasks_consumption[:, 0] >= overall_consumption[0, 0],
                                                          tasks_consumption[:, 0] <= overall_consumption[-1, 0])), 1]

        # overall_consumption: include also thermal load
        for load in self.grid.get_loads('ep_load'):
            overall_consumption[:, 1] += load.get_data().get('loads')

        for value_consumption, value_production in zip(overall_consumption[:, 1], pv_prod):
            diff = value_consumption - value_production
            if diff >= 0:  # consumption greater than production
                pv_use += value_production
                real_purchased_energy += diff
            else:  # consumption lower than production
                pv_use += value_consumption
                # real_purchased_energy += 0

        pv_prod = pv_prod.sum()
        real_solar_use_ratio = round(pv_use / pv_prod if pv_prod else 1.0, FLOAT_PRECISION)
        real_indices.update({'real solar use ratio': real_solar_use_ratio})
        ################################################################################################################

        # overall renewable production #################################################################################
        # current scenario only PV
        real_indices.update(
            {'real overall renewable production': round(pv_prod, FLOAT_PRECISION)})
        ################################################################################################################

        # overall consumption ##########################################################################################
        # sum of all loads and tasks
        real_overall_consumption = overall_consumption[:, 1].sum()
        real_indices.update({'real overall consumption': round(real_overall_consumption, FLOAT_PRECISION)})
        ################################################################################################################

        # renewable quota ##############################################################################################
        # percentage of renewable energy in overall consumption, currently only PV
        real_renewable_quota = 100 * (pv_use / real_overall_consumption if real_overall_consumption else 0.0)
        real_indices.update({'real renewable quota': round(real_renewable_quota, FLOAT_PRECISION)})
        ################################################################################################################

        # sold energy ##################################################################################################
        # not allowed in simulation
        # energy sold to grid
        real_sold_energy = 0
        real_indices.update({'real sold energy': round(real_sold_energy, FLOAT_PRECISION)})
        ################################################################################################################

        # purchased energy #############################################################################################
        # in simulation equal conditionally (overall_consumption - pv prod)
        real_indices.update({'real purchased energy': round(real_purchased_energy, FLOAT_PRECISION)})
        ################################################################################################################

        # storage loss #################################################################################################
        # energy lost duet to storage charge and discharge activities
        real_storage_loss = 0
        for storage in self.grid.get_storages():
            pass
        real_indices.update({'real storage loss': round(real_storage_loss, FLOAT_PRECISION)})
        ################################################################################################################

        # ems error ##################################################################################################
        # indices = self.grid.commonData.get('indices')
        # prefix_indices = ['estimated ', 'real ']
        # postfix_indices = ['solar use ratio', 'overall renewable production', 'overall consumption', 'renewable quota',
        #                    'sold energy',
        #                    'purchased energy', 'storage loss']
        #
        # ems_error = 0
        # for key in postfix_indices:
        #     ems_error += (indices.get(prefix_indices[0] + key) - indices.get(prefix_indices[1] + key))
        ###############################################################################################################

        self.grid.commonData['indices'].update(real_indices)

    def reschedule_preparation(self):

        actual_time = datetime.datetime.now()
        schedule_time = datetime.datetime.utcfromtimestamp(self.timestamp)

        horizon = self.settings.general.get('horizon') or 360
        resolution = self.settings.general.get('resolution') or 15
        n_slot = horizon // resolution

        slots_times = list()
        delta = datetime.timedelta(minutes=resolution)

        start_date = schedule_time
        end_date = start_date + datetime.timedelta(minutes=horizon - resolution)

        while start_date < end_date:
            slots_times.append(start_date)
            start_date += delta

        for task in self.grid.tasks:

            if task.get('start_time') < actual_time:
                # task already in execution
                start_slot = 0
                allowed_slots = list()
                forced_slots = list()
                while slots_times[start_slot] < task.get('start_time'):
                    start_slot += 1
                    allowed_slots.append(0)
                    forced_slots.append(0)

                allowed_slots += [1] * task.get('duration')
                forced_slots += [1] * task.get('duration')

                allowed_slots += [0] * (n_slot - len(allowed_slots))
                forced_slots += [0] * (n_slot - len(forced_slots))

                task.set({'allowed_slots': allowed_slots})
                task.set({'forced_slots': forced_slots})

            else:
                # task not executed jet, block all slots before actual time
                start_slot = 0
                allowed_slots = list()
                forced_slots = list()
                while slots_times[start_slot] < actual_time:
                    start_slot += 1
                    allowed_slots.append(0)
                    forced_slots.append(0)

                allowed_slots += [0] * (n_slot - len(allowed_slots))
                forced_slots += [0] * (n_slot - len(forced_slots))

                task.set({'allowed_slots': allowed_slots})
                task.set({'forced_slots': forced_slots})

    def to_dict(self):

        return {'timestamp': self.timestamp,
                'settings': self.settings.to_dict(),
                'devices':
                    self.grid.loads_to_dict() + self.grid.productions_to_dict()+ self.grid.storages_to_dict() +
                    self.grid.purchasers_to_dict() + self.grid.sellers_to_dict() + self.grid.pipes_to_dict() +
                    self.grid.generators_to_dict() + self.grid.trigenerators_to_dict(),
                'tasks': self.grid.tasks_to_dict()
                }

    def __to_json(self):
        """
        create a json-object, first member is the pair 'timestamp': timestamp_value
        remaining data are stored in the member 'data' following the structure of the configuration.json and adding
        the computed data:

        {
          'timestamp': 123456789,
          'data':
              {
                  'settings': {},
                  'buildings': [{}, ...],
                  'buildings': [{}, ...],
                  'storages': [{}, ...],
              }
        }
        """

        return json.dumps(self.to_dict())

    def __json_to_emsdata(self, json_string):

        tmp_dict = json.loads(json_string)

        self.timestamp = tmp_dict['timestamp']

        self.grid.set_devices_data(tmp_dict['data']['devices'])
        self.grid.set_tasks_data(tmp_dict['data']['tasks'])

    def __db_dict_to_emsdata(self, input_dict):

        self.timestamp = input_dict['issued_time'].timestamp()  # type datetime.datetime

        self.grid.set_devices_data(input_dict['rvalue']['devices'])
        self.grid.set_tasks_data(input_dict['rvalue']['tasks'])

    def __store_to_file(self):

        with open(EMS_STORE_DATA_FILE, 'a') as f:
            f.write(self.__to_json())
            f.write('\n')

    def __load_from_file(self):
        """
            return the True if load, False if file doesn't exist or empty
            Load only the data, not the configuration
        """

        last_line = None

        # if os.path.isfile(EMS_STORE_DATA_FILE):
        with open(EMS_STORE_DATA_FILE, 'r') as f:
            # lines = f.readlines() easy but memory consuming for large file
            # last = lines[-1]

            for last_line in (line for line in f if line.rstrip('\n')):
                pass

            if last_line:
                # load the data in the class
                self.__json_to_emsdata(last_line.rstrip('\n'))

    def __store_to_db(self):

        add_or_update_schedule(self.timestamp, self.to_dict())

    def __load_from_db(self):

        self.__db_dict_to_emsdata(get_last_schedule_data())

    def store(self):

        # self.__store_to_file()
        self.__store_to_db()

    def load(self):

        try:
            # self.__load_from_file()
            self.__load_from_db()
            logger.info('Loaded EMS data with schedule time: ' + time.strftime('%d-%m-%Y %H:%M:%S',
                                                                               time.localtime(self.timestamp)))

        except Exception as err:
            logger.error(err)
            logger.warning('Stored data not loaded.')
            return

if __name__ == '__main__':
    """ Testing your code! """

    setup_logging('defines')

    ems_data = EMSData()

    # ems_data.grid.get_loads_with_id_matching('load')
    ems_data.update()

    ems_data.load()
    print(ems_data)
