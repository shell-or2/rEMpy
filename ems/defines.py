# This file is part of rEMpy.
#
# rEMpy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# rEMpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rEMpy.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import logging.handlers
import sys
import datetime
import time
import threading
import shutil
from config_mapper import Grid, Settings
logger = logging.getLogger(__name__)

############################################################################
# Enable optional modes

# history mode, default is False (NOT YET AVAILABLE)
EMS_HISTORY_MODE = False

# test mode, do not use ServerQueue in order to run one module alone
EMS_TEST_MODE = False

############################################################################
# SHARED QUEUES SERVER

SERVER_PORTNUM = 5000
SERVER_AUTHKEY = b'asdqwe'
SERVER_IP_ADDRESS = '127.0.0.1'
# If an address of ‘0.0.0.0’ is used, the address will not be a connectable end point
# on Windows. If you require a connectable end-point, you should use ‘127.0.0.1’.
# Note: the IP is not required for the server manager

############################################################################
# Modules and input/output queues order: 0) interface, 1) prediction, 2) thermal
MODULES_NAMES = ['interface', 'prediction', 'thermal', 'diagnostic', 'overload_manager']
INTERFACE_QUEUE = MODULES_NAMES.index('interface')
PREDICTION_QUEUE = MODULES_NAMES.index('prediction')
THERMAL_QUEUE = MODULES_NAMES.index('thermal')
DIAGNOSTIC_QUEUE = MODULES_NAMES.index('diagnostic')
OVERLOAD_MANAGER_QUEUE = MODULES_NAMES.index('overload_manager')

# Message sets and IDs
MSD_ID_LENGTH = 3

MSG_ID_CONFIG = '000'
MSG_ID_REQUEST_CONFIG = '001'
MSG_ID_ACK = '111'
MSG_KILL_ALL = '999'

MSG_ID_DATA_TO_INTERFACE = '101'
MSG_ID_DATA_FROM_INTERFACE = '102'
MSG_ID_UPDATE_CONFIG_FILE = '103'
MSG_ID_INTERFACE_REQUEST_CONFIG = '199'

MSG_ID_PREDICTION_REQUEST = '200'
MSG_ID_PREDICTION_RESPONSE = '201'
MSG_ID_PREDICTION_CANCEL = '299'

MSG_ID_THERMAL_REQUEST = '300'
MSG_ID_THERMAL_RESPONSE = '301'
MSG_ID_THERMAL_CANCEL = '399'

MSG_ID_DATA_TO_DIAGNOSTIC = '401'
MSG_ID_DATA_FROM_DIAGNOSTIC = '402'

MSG_ID_DATA_TO_OVERLOAD_MANAGER = '501'
MSG_ID_DATA_FROM_OVERLOAD_MANAGER = '502'

# MSG_ID_TIMEOUT = '001'

# ERROR codes
# 3 digits: XYZ
# X denotes the alert level: 0-WARNING, 1-ERROR, 2-CRITICAL
# YZ denote the alert code

OVERLOAD_ALERT_MIN = '000'
OVERLOAD_ALERT_MED = '101'
OVERLOAD_ALERT_MAX = '102'

DIAGNOSTIC_ALERT_SENSOR_WARNING = '010'
DIAGNOSTIC_ALERT_SENSOR_STUCK = '111'
DIAGNOSTIC_ALERT_SENSOR_FAILURE = '112'
DIAGNOSTIC_ALERT_LOST_COMMUNICATION_SHORT = '113'
DIAGNOSTIC_ALERT_LOST_COMMUNICATION_LONG = '214'
DIAGNOSTIC_ALERT_ACTUATOR_FAULT = '115'
DIAGNOSTIC_ALERT_MISSING_DATA = '016'

_alertsDescription = {
    DIAGNOSTIC_ALERT_SENSOR_WARNING: 'The measured value of the sensor exceeds the operating threshold.',
    DIAGNOSTIC_ALERT_SENSOR_STUCK: 'The measured value of the sensor has not changed.',
    DIAGNOSTIC_ALERT_SENSOR_FAILURE: 'The measured value of the sensor has not changed, it holds to full scale.',
    DIAGNOSTIC_ALERT_LOST_COMMUNICATION_SHORT: 'The measured value of the sensor is not received.',
    DIAGNOSTIC_ALERT_LOST_COMMUNICATION_LONG: 'The measured value of the sensor is not received.',
    DIAGNOSTIC_ALERT_ACTUATOR_FAULT: 'The appliance does not start.',
    DIAGNOSTIC_ALERT_MISSING_DATA: 'No data in the database.'
}

############################################################################
# SyncManager timing
SYNCMANAGER_CONNECTION_TIMEOUT = 30  # in seconds
SYNCMANAGER_MAX_CONNECTION_ATTEMPT = 10

############################################################################
# Timing
MSG_CHECK_TIME = 1  # in seconds
CONNECTION_TIMEOUT = 15  # in seconds
MAX_CONNECTION_ATTEMPTS = 10

# PREDICTION_TIMEOUT = 300  # in seconds
# THERMAL_TIMEOUT = 300  # in seconds

SOFTKILL_TIMEOUT = 30  # (in seconds) maximum wait-time

DIAGNOSTIC_LOOP_TIME = 300
OVERLOAD_MANAGER_LOOP_TIME = 60

############################################################################
# Default settings
DEFAULT_HORIZON = 360  # in minurtes
DEFAULT_RESOLUTION = 15  # in minutes

############################################################################
# Data formats

# float precision
FLOAT_FORMAT = "{:.2f}"
FLOAT_PRECISION = 3


############################################################################
# SYSTEM INFO & PATHS

EMS_DIR = os.path.dirname(os.path.abspath(__file__))

EMS_DATASETS = os.path.join(os.sep.join(EMS_DIR.split(os.sep)[:-1]), 'datasets')
EMS_CFG_FILE = os.path.join(EMS_DIR, 'config', 'ems.json')

EMS_DB_SCHEDULES = os.path.join(EMS_DIR, 'ems_db', 'schedule', 'schedules.db')
EMS_DB_SENSORS = os.path.join(EMS_DIR, 'ems_db', 'sensor', 'sensors.db')  # 'ems_data.db')

EMS_STORE_DATA = os.path.join(EMS_DIR, 'ems_stored_data')
EMS_STORE_DATA_FILENAME = 'stored_data.mjson'
EMS_STORE_DATA_FILE = os.path.join(EMS_STORE_DATA, EMS_STORE_DATA_FILENAME)

LOG_DIR = os.path.join(EMS_DIR, 'logs')
EP_DIR = os.path.join(EMS_DIR, 'thermal', 'ep_data')
EP_OUTPUT_DIR = os.path.join(EP_DIR, 'ep_output')

# ems_stored_data dir control
# if not os.path.exists(EMS_STORE_DATA):
#     os.makedirs(EMS_STORE_DATA)

# log dir control
if not os.path.exists(LOG_DIR):
    os.makedirs(LOG_DIR)

# Energyplus path sets
if not os.path.exists(EP_DIR):
    os.makedirs(EP_DIR)
    os.makedirs(EP_OUTPUT_DIR)

EP_EXE = 'energyplus'
EPW_FILENAME = 'ancona.epw'
IDF_FILENAME = 'shell_fancoil.idf' # 'shell_ideal.idf' 'shell_radpav.idf' 'shell_fancoil.idf'

EP_EPW_FILE = os.path.join(EP_DIR, EPW_FILENAME)
EP_IDD_FILE = os.path.join(EP_DIR, 'Energy+.idd')
EP_IDF_FILE = os.path.join(EP_DIR, IDF_FILENAME)
EP_IDF_FILE_TMP = os.path.join(EP_DIR, 'tmp_' + IDF_FILENAME)

EP_ESO_FILE = os.path.join(EP_OUTPUT_DIR, 'eplusout.eso')
EP_CALL = EP_EXE + ' -d ' + EP_OUTPUT_DIR + ' -i ' + EP_IDD_FILE + ' -w ' + EP_EPW_FILE + ' ' + EP_IDF_FILE_TMP

# TODO: idf file 1) hardcoded, 2) manually pre-saved in dir


# Sensors, temporary, to move in config file

PV_SENSORS = ['solar1', 'solar2']  # PV sensors


############################################################################
# LOGGER
def setup_logging(filename):
    """ 
        Create logger streams and set their logging level
    """
        # '{:<30}'.format(filename.upper())
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s ' + '{:<17}'.format(filename.upper()) + ':%(name)50s %(levelname)8s %(message)s',
                        stream=sys.stdout)

    logfile = logging.handlers.RotatingFileHandler(os.path.join(LOG_DIR, filename+'.log'), maxBytes=1024 * 1024, backupCount=5)
    formatter = logging.Formatter('%(asctime)s %(name)-30s %(levelname)-8s %(message)s')
    logfile.setFormatter(formatter)
    logfile.setLevel(logging.DEBUG)
    logging.getLogger('').addHandler(logfile)
