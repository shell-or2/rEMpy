# rEMpy - *residential Energy Manager in python* 

This is the rEMpy official repository. [![license](https://www.gnu.org/graphics/gplv3-88x31.png)](https://gitlab.com/shell-or2/rEMpy-private/blob/master/LICENSE)

------------------

## Getting started

* Set the *ems* folder as *Sources Root*.

* Run *run_ems.py* (in the *ems* folder) to launch the Energy Manager (all modules).

------------------

## Requirements

Before the excetution some additional softwares and packages are required.

For major information consult the dedicated [wiki section](https://gitlab.com/shell-or2/rEMPy-private/wikis/required_softwares_and_packages).


## Citation

Please cite this paper when referring to __rEMpy__:

M. Fagiani, *et al.*, *"rEMpy: a comprehensive software framework for residential energy management"*, Energy and Buildings, Volume 171, 2018, Pages 131-143, ISSN 0378-7788, [DOI](https://doi.org/10.1016/j.enbuild.2018.04.023).

M. Fagiani, *et al.*, *"A New Open-Source Energy Management Framework: Functional Description and Preliminary Results"*, 2017 IEEE Congress on Evolutionary Computation (CEC), San Sebastian, 2017, pp. 1207-1214, [DOI](http://dx.doi.org/10.1109/CEC.2017.7969443).